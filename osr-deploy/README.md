# osr-deploy

OSR Deployment system - see osr-adapter-xxx for the desired target 

## Build Artefacts

- [x] Public/Private mask (@osr-sync)
- [x] BOM (@osr-sync)
- [x] CAD file fuckery
  - [ ] Assembly/Part indexes (@osr-cad)
  - [-] Web Preview/Explorer (@osr-web-cad)
  - [ ] Sync targets (@osr-sync)
  - [ ] Directory indexes (local & web)
- [x] Laser file fuckery
- [x] Sheetmetal file fuckery
- [ ] CAM file fuckery
- [ ] Media conv
- [ ] Expressions
- [ ] PP-Next

## Targets

- [x] Bazar
- [x] Site
- [ ] Library
- [ ] PP Next
- [ ] PDF
  - [c] index
- [ ] Local HTML
- [x] GitHub
- [x] SEO/Preview
- [ ] Misc platforms (Mailchimp)
- [ ] API (@osr-cad outputs)
- [ ] Report (@osr-validate|@osr-cad outputs)
- [ ] Social
  - [ ]  Reddit
  - [ ]  yn
  - [ ]  instagram
  - [ ]  facebook
- [ ]  wikis/libs
  - [ ] wikiefactory
  - [ ] OSE (outlet)


## Common 

- [ ] Configs
- [ ] Contextmenu (@osr-tools)


## OSR - v2.2

- [ ] Electrics
- [ ] Manufacturing meta

# OSR policy, guidelines conventions for product documentation

Each product must include everything for reviews, peer contributions and future iterations.
Conventions, specifications and validations depend on different deployment configuration, eg: internal, development, review, production and well different target platforms (vendor, customer, OpenSource).

The following conventions and guidelines refer always to the top most deployment configuraton : *'internal'*. The very configuration uses the more strict policies and validation rules.



### 1. Folders & Files

This layout is the bare mandatory minimum for products and components.

| Name                              | Mandatory |   Validation|
|---------------------              |-----------|-------------|
| [cad](./cad)                      |   [x]     |    [x]      |
| [laser](./laser)                  |   [ ]     |    [x]      |
| [resources](./resources)          |   [x]     |    [x]      |
| [drawings](./drawings)            |   [x]     |    [x]      |
| [tests](./tests)                  |   [x]     |    [x]      |
| [media](./media)                  |   [x]     |    [ ]      |
| [renderings](./renderings)        |   [ ]     |    [ ]      |
| [.osr-sync.json](/.osr-sync.json) |   [ ]     |    [x]      |
| [.osr-validate.json](/.osr-validate.json)|[ ] |    [x]
| [config.json](/.osr-sync.json)    |   [x]     |    [x]      |
| [config.yaml](/config.yaml)    |   [x]     |    [x]      |

1.1 CAD

All CAD files have to be placed with the ```./cad/``` folder. Parts and subassemblies which are specific to a certain configuration only have to be stored in seperate folder, eg: ```./cad/[ConfigurationName]```. Temporary versions have to be stored in ```./cad_my_proposal```. Such very versions are stripped off during deployment.

**Filenames**

- file names for parts should have be all in small capitals
- no special characters
- white space are allowed

1.1 Laser

**Filenames**

The final file names being send to laser services are encoded as follows:

```PRODUCT-CODE_PART-ID_PARTSHORTNAME__xQTY_THICKNESS_MATERIAL.dxf```

In Example: ```EMP_002_BaseFrameSidepanel_x2_5.0mm_ferro.DXF```

- filenames should be short as possible
- no whitespaces allowed
- the use of capital letters must be consistent
- no special characters except for local and internal deployments
- variable names must be in capital letters
- the Part-ID as well the part name must match the CAD part name

**Attention** :

- OSR platform fills in the product code (```EMP_```) automatically as well localizes material names, using the local or parent configuration file ([product/config.json](./config.json)).
- Variable placeholders can be used to fill in variables resolved in local or parent configuration. For instance ```EMP_002_BaseframeSidepanel_x2_5.0mm_${PANEL_MATERIAL}.DXF```.


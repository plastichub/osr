# Policies for CAD

### General

As soon it's clear you're working on a product, we recommend to reduce any - often significant - overhead regarding 'I fix it later', especially in ongoing production environments:

1. Sketch and figure out early potentional sub-assemblies. Simple drawings on paper helps to determine components to be replaced or being configurable. The global assembly should contain sub-assemblies and a few mates for positioning. Keep in mind that once the CAD is done, the documentation requires renderings, assembly and production drawings. Make it easy for your team mate!

2. When importing stock parts from 3D Model providers, don't polute the root folders. Place everything in sub folders, eg: ```/cad/components/switch```. Also, rename the parts in an imported assembly right away, according to the specifications and guidelines.

3. Assgign visual and physical properties early. It's important to keep track of weights and other properties important to sustainability.

4. Research well your parts! It's important to know whether the part is only accessible in your region or not. Keep notes about alternative sources. Also place notes why you choose the part or vendor!

### Part naming

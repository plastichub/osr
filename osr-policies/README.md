# OSR-Policies

### Commons

- [ ] Integrity 
- [ ] Transparency
- [ ] Organisation
- [x] Content
- [x] Communication
- [ ] Failures/Feedback/Critics
- [ ] Financials

### HR
- [ ] Contributors
- [ ] Temporary volunteers
- [ ] Students
- [ ] Partnership / Collaboration / Affiliation / Acquisition
- [ ] Onoarding/interviews

### Online platform

- [ ] Community

### Lab/Production

- [ ] Workspace
- [ ] Production
- [ ] Development / Prototypes

### International

## Research

- [ ] Mozilla foundation, lot´s of the common problems have been reported and there are good discussion about Org vs. Corp
- [ ] Cooperative


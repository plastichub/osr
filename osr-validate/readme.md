# OSR Validate

This is a *'command line interface'* (CLI) to verify OSR but also foreign deployments or contributions.

## Validations

### General

- [ ] Folder and file sizes
- [ ] Folder and file names
- [ ] Supported formats and file extensions
- [ ] Folder and file structure (according to input specification/template)
- [ ] Shell Extension (OS context menu)
- [ ] Sustainability report (material overhead, manufacturing, consumables, chemicals, energy consumption, life expectancy, etc...) 
- [ ] Project / product - config parser & link checker
- [ ] Quality of data (rating per type: CAD)
 - [ ] Interop 
- [ ] Completeness 
- [ ] Platform deployments (config/template hash ?)

### Reporting

- [ ] JSON report (for OSR-API consumer)
- [ ] HTML report

### CAD

- [ ] Check missing drawings and web outputs (osr-sync & osr-convert)
- [ ] SW/STEP model parser (check SW API)

### Tools

- [ ] File watcher with notifications

### Firmware

- [ ] Verify meta files and version
- [ ] CI travis reports

<hr>

**Notes**

- each project or product may override the input specification (staging)
- suggestions as auto-renaming files may require user inquiry via CLI or GUI
- dry-mode

## Research

- [ ] Property trees (see [Boost implementation](https://cppsecrets.com/users/1226118105107114971101165151575164103109971051084699111109/C00-boostpropertytreeptree.php)) and property tree queries.
- [ ] BOM structures and fields (see [gh:docs-oshwapp (alpha)](https://github.com/mafrego/docs-oshwapp))
- [ ] Graph query languages ([site:neo4j](https://neo4j.com/)) and [site:RedisGraph](https://redislabs.com/modules/redis-graph/)


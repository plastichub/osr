# Coolant, Mist, Lubrication

- [site:anvilfire.com - all about drilling and tapping](https://www.anvilfire.com/21st-century-blacksmithing/basic-tools/drill-press/coolant/)

# Sensing

## Scales

- [site: DRO scale readout - Arduino](https://www.electro-tech-online.com/threads/arduino-quadrature-decoder-digital-readout.156159/)

## IO Boards

### Vendors

- [site: Phoenix](https://www.phoenixcontact.com/online/portal/us?1dmy&urile=wcm%3apath%3a/usen/web/main/products/list_pages/Components_for_PLC_and_IO_systems_P-21-13/39dd45a0-c14e-4520-be27-456d76f1f77a)

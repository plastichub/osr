#ifndef VERSION_H 

    #define VERSION_H 

    #define VERSION "1.0.0|PlasticHub|OSR|Cassandra|5a3e1a46de96044b09cece8c17b2adf6b0e3041f"

    #define SUPPORT "support@plastic-hub.com | www.plastic-hub.com"
    #define BUILD "Cassandra-RevA"
    #define FW_SRC "https://gitlab.com/plastichub/osr/pp-next/tree/master/machines/sheetpress/60cm-cassandra/firmware/firmware"
    #define CID "-1"

#endif
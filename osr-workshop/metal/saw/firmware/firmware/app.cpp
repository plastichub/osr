#include <Vector.h>
#include <Streaming.h>
#include <Arduino.h>

#include "app.h"
#include "features.h"
#include <MemoryFree.h>

static Addon *addonsArray[10];

#ifdef HAS_DIP
int dipSwitchPins[] = {48, 49};
#endif

short App::ok()
{
    if (now - dt > 100)
    {
        dt = now;
    }
    return E_OK;
}

App::App() : Addon("APP", APP, 1 << STATE),
#ifdef HAS_DIRECTION_SWITCH
             dirSwitch(new DirectionSwitch(DIR_SWITCH_UP_PIN, DIR_SWITCH_DOWN_PIN)),
#endif

#ifdef HAS_POWER
#if defined(POWER_0) && defined(POWER_1)
             powerSwitch(new Power(POWER_0, POWER_1)),
#else
             powerSwitch(new Power(POWER_0, POWER_0)),
#endif
#endif
             debugTS(0),
             servo(new ExternalServo(PRESS_SERVO_STEP_PIN, PRESS_SERVO_DIR_PIN)),
             actuator(new LinearActuator(
                 PRESS_LIMIT_UP_PIN, 
                 PRESS_LIMIT_DOWN_PIN, 
                 PRESS_SERVO_DIR_PIN, 
                 PRESS_SERVO_STEP_PIN, 
                 PRESS_SERVO_ALARM_PIN, 
                 PRESS_SERVO_POWER_PIN, 
                 this, 
                 (AddonFnPtr)&App::onActuatorChange, 
                 PRESS_MAX_MOVE_TIME)),
             statusLightUp(STATUS_UP_PIN),
             statusLightDown(STATUS_DOWN_PIN),
             statusLightHeat(STATUS_HEAT_PIN)
{
}
#ifdef HAS_STATES
String App::state()
{
    const int capacity = JSON_OBJECT_SIZE(6);
    StaticJsonDocument<capacity> doc;
    doc["0"] = id;
    doc["1"] = _state;
    return doc.as<String>();
}
#endif

short App::getAppState(short val)
{
    return _state;
}
void (*resetFunction)(void) = 0; // Self reset (to be used with watchdog)

short App::setAppState(short newState)
{
}

void printMem()
{
    Serial.print("mem: ");
    Serial.print(freeMemory());
    Serial.println('--');
}
short App::setup()
{
    Serial.begin(DEBUG_BAUD_RATE);

    Serial.println("Booting Firmware ...................... ");

    addons.setStorage(addonsArray);
    setup_addons();

#ifdef MEARSURE_PERFORMANCE
    printPerfTS = 0;
    addonLoopTime = 0;
    bridgeLoopTime = 0;
#endif
    loopTS = 0;
    _state = 0;
    bootTime = millis();

    /*
    timer.every(5000, [](App *app) -> void {
        printMem();
    },
                this);
                */
}

void App::loop_service()
{
#ifdef HAS_POWER
    powerSwitch->on(POWER_PRIMARY);
#endif
    // _loop_manual();
}
void App::debug_mode_loop()
{
    uchar s = addons.size();
    for (uchar i = 0; i < s; i++)
    {
        Addon *addon = addons[i];
        if (addon->hasFlag(LOOP))
        {
            addon->loop();
        }
    }
}

short App::loop()
{
    // debug();
    now = millis();
    if (millis() - loopTS > LOOP_DELAY)
    {
        loopTS = now;
        loop_addons();
        loopPress();
    }

    /*
    loop_addons();
    // timer.tick();
    now = millis();
    loopPress();*/
}

# osr-workshop
All for the OpenSource workshop, templates, books, references, machine conversions, etc...


## Metal working

### Milling

- [ ] [semi-automatic controller aka 'sac'](./metal/milling/sac) providing basic geometries, patterns, etc.. 

### Turning

- [ ] semi-automatic controller providing basic geometries, patterns, etc.. 

### Drilling & Tapping

- [ ] semi-automatic controller to repeat trained interactions

# Q3 Todos

### Band saw

- [ ] semi-automatic conversion

### Welding

- [ ] 5 axis robot arm (on a gantry)

### Sheet metal

- [ ] CNC aid conversion







/////////////////////////////////////////////////////
//
//  Application types
//
export enum OutputTarget {
    STDOUT = 'console',
    FILE = 'file'
}

export enum OutputFormat {
    text = 'text',
    json = 'json'
}

export interface PATH_INFO {
    DIR?: string;
    NAME?: string;
    FILE_NAME?: string;
    FILE_EXT?: string;
    PATH?: string;
    IS_FILE?: boolean;
    IS_FOLDER?: boolean;
    IS_EXPRESSION?: boolean;
    IS_GLOB?: boolean;
    path: string;
    GLOB: string;
    GLOB_EXTENSIONS: string[];
    FILES: string[];
}

export interface VARIABLES extends PATH_INFO {

};

export interface Options {
    src: string;
    srcInfo: PATH_INFO,
    dstInfo: PATH_INFO,
    dst?: string;
    format?: string[];
    glob?: string;
    debug: boolean;
    verbose: boolean;
    skip: boolean;
    dry: boolean;
    report?: string;
    variables: any;
    sw?: string;
    script?:string;
}


export interface SolidworkOptions extends Options {
    view?: string;
}

export type OutputResult = boolean;

export interface ReportEntry {
    name: string;
}


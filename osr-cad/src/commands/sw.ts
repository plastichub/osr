import * as CLI from 'yargs';
import * as debug from '../log';
import { info } from '..';
import * as path from 'path';
import { sync as write } from "@plastichub/fs/write";
import { csvToMarkdown } from '../lib/';
import { SolidworkOptions } from '../types';
import { defaultOptions, sanitize } from '../argv';

const fg = require('fast-glob');

const csv = require('csv-stringify/lib/sync');

import { convert } from '../cad/index';

const OSR_REGEX = /^[0-9].+$/;
const isOSR = (filename) => filename.match(OSR_REGEX) != null;

// SolidWorks base converter, using powershell & interop interface (pkgdir/sw)

let options = (yargs: CLI.Argv) => defaultOptions(yargs);

// node ./build/main.js sw --src=../plastichub/products/elena/cad --glob="*.SLDASM" --format="step" --dst="../test"

export const register = (cli: CLI.Argv) => {

    return cli.command('sw', '', options, async (argv: CLI.Arguments) => {
        if (argv.help) { return; }

        const options = sanitize(argv) as SolidworkOptions;

        options.verbose && debug.inspect("options " + argv.dst, options);

        return convert(options);
    });
};

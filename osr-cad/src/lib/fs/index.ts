const fg = require('fast-glob');

import * as path from 'path';
import * as fs from 'fs';
import * as bluebird from 'bluebird';
import { Converter } from 'showdown';
import { sync as read } from '@plastichub/fs/read';
import { sync as exists } from '@plastichub/fs/exists';
import { html_beautify } from 'js-beautify';

export { sync as read } from '@plastichub/fs/read';
export { sync as exists } from '@plastichub/fs/exists';
export { sync as dir } from '@plastichub/fs/dir';
export { sync as write } from '@plastichub/fs/write';

import { sync as write } from '@plastichub/fs/write';

import { Helper } from '../process/index';
import { firstOf, lastOf } from '../common/array';
import { img } from '../content/html';

const IMAGES_GLOB = '*.+(JPG|jpg|png|PNG|gif)';

import * as isGlob from 'is-glob';
import { PATH_INFO } from 'types';
const globBase = require('glob-base');
const parseGlob = require('parse-glob');
export const files = (dir, glob) => fg.sync(glob, { dot: true, cwd: dir, absolute: true }) as [];
export const images = (source) => files(source, IMAGES_GLOB) as any[];
export const head_image = (_images) => firstOf(_images);
export const tail_image = (_images) => lastOf(_images);
const GLOB_GROUP_PATTERN = /[!*+?@]\(.*\)/;

const getExtensions = (glob: string) => {
    const match = glob.match(GLOB_GROUP_PATTERN);
    if (match) {
        return glob.substring(match.index + 2, glob.lastIndexOf(')')).split('|');
    } else {
        return [parseGlob(glob).path.ext];
    }
}
export async function resize_images(files) {
    return bluebird.mapSeries(files, (file: string) => {
        const inParts = path.parse(file);
        const promise = Helper.run(inParts.dir, 'convert',
            [
                `"${inParts.base}"`,
                '-quality 70',
                '-resize 1980',
                '-sharpen 0x1.0',
                `"${inParts.name}${inParts.ext}"`
            ]);
        return promise;
    });
}

export const md2html = (content) => {
    let converter = new Converter({ tables: true });
    converter.setOption('literalMidWordUnderscores', 'true');
    return converter.makeHtml(content);
}


export const toHTML = (path, markdown) => {
    const content = read(path, 'string') as string;
    if (!markdown) {
        let converter = new Converter({ tables: true });
        converter.setOption('literalMidWordUnderscores', 'true');
        return converter.makeHtml(content);
    } else {
        return content;
    }
}

const jekyllNop = "---\n#jekyll\n---\n";
const frontMatter = /^---[.\r\n]*---/;

export const thumbs = (source: string, meta: boolean = true, sep: string = "<hr/>") => {
    let pictures = images(source);
    let content = "";
    pictures.forEach((f, i) => {
        if (meta) {
            let picMD = path.resolve(path.join(path.parse(f).dir, path.sep, path.parse(f).name + '.md'));
            if (exists(picMD)) {
                const picMDContent = read(picMD, "string") as string;
                if (picMDContent.length > 3 && picMDContent !== jekyllNop) {
                    content += picMDContent.substr(picMDContent.lastIndexOf('---') + 3, picMDContent.length)
                    content += "\n";
                } else {
                    write(picMD, jekyllNop);
                }
            } else {
                write(picMD, jekyllNop);
            }
        }

        content += img(`./${path.parse(f).base}`, `${i + 1}. `, path.parse(f).base);
        content += "\n";
        content += sep;
    });
    return html_beautify(content);
}

export const isFile = (src: string) => {
    let srcIsFile = false;
    try {
        srcIsFile = fs.lstatSync(src).isFile();
    } catch (e) {

    }
    return srcIsFile;
}
export const isFolder = (src: string) => {
    let srcIsFolder = false;
    try {
        srcIsFolder = fs.lstatSync(src).isDirectory();
    } catch (e) {

    }
    return srcIsFolder;
}

export const pathInfo = (src: string): PATH_INFO => {

    const srcParts = path.parse(src);

    let variables: PATH_INFO = {
        PATH: src
    } as PATH_INFO;
    variables.DIR = srcParts.dir;
    variables.NAME = srcParts.name;
    variables.FILE_NAME = srcParts.base;
    variables.FILE_EXT = srcParts.ext.replace('.', '');
    variables.PATH = src;
    variables.IS_FILE = isFile(src);
    variables.IS_FOLDER = isFolder(src);
    variables.IS_EXPRESSION = src.match(/\$\{([^\s\:\}]+)(?:\:([^\s\:\}]+))?\}/g) != null;
    
    if (!variables.IS_FOLDER && !variables.IS_FILE) {
        variables.IS_GLOB = isGlob(srcParts.base);
    }

    if (variables.IS_GLOB) {
        //important: use the forward slash since path.resolve will return backslashes on Windows
        const glob_base = globBase(src);
        variables.DIR = path.resolve(glob_base.base);
        variables.FILE_NAME = glob_base.glob;
        variables.GLOB = glob_base.glob;
        variables.GLOB_EXTENSIONS = getExtensions(glob_base.glob);
        variables.FILES = fg.sync(glob_base.glob, { dot: true, cwd: variables.DIR, absolute: true });
        variables.FILES = variables.FILES.filter((f)=> {
            return !path.parse(f).name.startsWith('~$');
        });
    }
    return variables;
}
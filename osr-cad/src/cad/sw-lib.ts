import * as debug from '../log';
import { info } from '..';
import * as path from 'path';
import * as bluebird from 'bluebird';
import { Promise as BPromise } from 'bluebird';
import { sync as exists } from "@plastichub/fs/exists";
import { sync as write } from "@plastichub/fs/write";
import { substitute } from '../lib/';
import { Options, SolidworkOptions } from '../types';
import { Helper } from '../lib/process/index';

import { reportCSV, reportMarkdown } from '../report';

const clone = (obj) => {
    if (null == obj || "object" != typeof obj) return obj;
    var copy = obj.constructor();
    for (var attr in obj) {
        if (obj.hasOwnProperty(attr)) copy[attr] = obj[attr];
    }
    return copy;
}

export async function convertFiles(file, targets: string[], view, onNode: (data: any) => void = () => { }, options: Options) {
    // debug.inspect(`convert ${file} to `, targets);
    if (options.dry) {
        return bluebird.resolve();
    }
    return BPromise.resolve(targets).map((target) => {
        if (options.skip && exists(target)) {
            onNode({
                src: file,
                target
            });
            return bluebird.resolve();
        }

        const promise = Helper.run(path.resolve(options.sw || __dirname + '../../../sw'), options.script, [
            `"${file}"`,
            `"${target}"`,
            `"*${view}"`,
        ]);

        promise.then((d) => {
            onNode({
                ...d,
                src: file,
                target
            });
        });
        return promise;

    }, { concurrency: 1 });

};

export const report = (data, dst: string) => {

    let report: any = null;
    if (dst.endsWith('.md')) {
        report = reportMarkdown(data);
    }

    if (dst.endsWith('.csv')) {
        report = reportCSV(data);
    }

    if (report) {
        info(`Write report to ${dst}`);
        write(dst, report);
    }
    return report;
}

export const targets = (f: string, options: Options) => {

    const srcParts = path.parse(f);
    const variables = clone(options.variables);
    const targets = [];
    if (options.dstInfo.IS_GLOB) {
        options.dstInfo.GLOB_EXTENSIONS.forEach((e) => {
            variables.SRC_NAME = srcParts.name;
            variables.SRC_DIR = srcParts.dir;
            let targetPath = substitute(options.variables.DST_PATH, variables);
            targetPath = path.resolve(targetPath.replace(options.variables.DST_FILE_EXT, '') + e);
            targets.push(targetPath);
        })
    }
    return targets;
}

export async function convert(options: SolidworkOptions) {

    let reports = [];

    const onNode = (data) => { reports.push(data) };

    await BPromise.resolve(options.srcInfo.FILES).map((f) => {
        const outputs = targets(f, options);
        options.verbose && debug.inspect(`Convert ${f} to `, outputs);
        return convertFiles(f, outputs, options.view, onNode, options);
    }, { concurrency: 1 });

    if (options.report) {
        const reportOutFile: string = substitute(options.report, {
            dst: options.srcInfo.DIR
        });
        options.verbose && debug.info(`Write report to ${reportOutFile}`);
        report(reports, reportOutFile);
    }
}

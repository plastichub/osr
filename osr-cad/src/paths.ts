import * as path from 'path';
import { URL } from 'url';
export const STATS_SUFFIX = '_stats.json';
export const TRACE_SUFFIX = '_trace.json';
export const HOWTO_SUFFIX = '_howto.json';
export const HTML_SUFFIX = '_dump.html';
// utils to create output file name for url, format : hostname_time.json
const _url_short = (url: string) =>
    new URL(url).hostname;

const _date_suffix = () =>
    new Date().toLocaleTimeString().replace(/:/g, '_');

const _default_filename = (url: string) =>
    `${_url_short(url)}`;

export const default_path = (cwd: string, url: string) =>
    `${path.join(cwd, _default_filename(url))}${STATS_SUFFIX}`;

export const default_path_crawler = (cwd: string, url: string) =>
    `${path.join(cwd, _default_filename(url))}${HTML_SUFFIX}`;

export const default_path_howto = (cwd: string, url: string) =>
    `${path.join(cwd, _default_filename(url))}${HOWTO_SUFFIX}`;

export const default_trace_path = (cwd: string, url: string) =>
    `${path.join(cwd, _default_filename(url))}${TRACE_SUFFIX}`;
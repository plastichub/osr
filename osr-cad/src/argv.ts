import * as CLI from 'yargs';
import {
    error,
    Options, SolidworkOptions
} from './';

import * as path from 'path';
import { isArray } from './lib/common/primitives';
import { substitute, pathInfo } from './lib';
import { sync as read } from "@plastichub/fs/read";

// default options for all commands
export const defaultOptions = (yargs: CLI.Argv) => {
    return yargs.option('src', {
        default: './',
        describe: 'The source directory or source file. Glob patters are supported!',
        demandOption: true
    }).option('format', {
        describe: 'The target format. Multiple formats are allowed as well, use --format=pdf --format=jpg'
    }).option('dst', {
        describe: 'Destination folder or file'
    }).option('view', {
        default: 'Isometric',
        describe: 'Sets the target view'
    }).option('Report', {
        describe: 'Optional conversion report. Can be JSON, HTML, CSV or Markdown'
    }).option('debug', {
        default: 'false',
        describe: 'Enable internal debug message'
    }).option('skip', {
        default: 'false',
        describe: 'Skip existing files'
    }).option('dry', {
        default: 'false',
        describe: 'Run without conversion but create reports'
    }).option('verbose', {
        default: 'true',
        describe: 'Show internal messages'
    }).option('sw', {
        describe: 'Set explicit the path to the Solidworks binaries & scripts.\
        "It assumes SolidWorks.Interop.sldworks.dll and export.cmd at this location!'
    }).option('script', {
        describe: 'Set explicit the path to the Solidworks script'
    })
};

// Sanitizes faulty user argv options for all commands.
export const sanitize = (argv: CLI.Arguments): Options => {

    const src = path.resolve('' + argv.src);

    const config = argv.config ? read(path.resolve('' + argv.config), 'json') : {};

    const extraVariables = {};
    for (const key in config) {
        if (Object.prototype.hasOwnProperty.call(config, key)) {
            const element = config[key];
            if (typeof element === 'string') {
                extraVariables[key] = element;
            }
        }
    }

    const args: Options = {
        src: src,
        dst: '' + argv.dst as string,
        report: argv.report ? path.resolve(argv.report as string) : null,
        debug: argv.debug === 'true',
        verbose: argv.verbose === 'true',
        dry: argv.dry === 'true',
        skip: argv.skip === 'true',
        glob: argv.glob as string,
        sw: argv.sw ? path.resolve(argv.sw as string) : __dirname + '../../sw',
        script: argv.script || 'export.cmd',
        variables: {
            ...extraVariables
        }
    } as Options;

    if (!args.src) {
        error('Invalid source, abort');
        return process.exit();
    }

    if (argv.format) {
        if (typeof argv.format === 'string') {
            args.format = [argv.format];
        } else if (argv.source && isArray(argv.format)) {
            args.format = argv.format;
        }
    }

    args.srcInfo = pathInfo(argv.src as string);

    for (const key in args.srcInfo) {
        if (Object.prototype.hasOwnProperty.call(args.srcInfo, key)) {
            args.variables['SRC_' + key] = args.srcInfo[key];
        }
    }

    if (argv.dst) {
        args.dst = path.resolve(substitute(args.dst, args.variables));
        args.dstInfo = pathInfo(args.dst as string);
        args.dstInfo.PATH = argv.dst as string;

        for (const key in args.dstInfo) {
            if (Object.prototype.hasOwnProperty.call(args.dstInfo, key)) {
                args.variables['DST_' + key] = args.dstInfo[key];
            }
        }
    }

    (args as SolidworkOptions).view = argv.view as string || "Isometric";

    // check for single file direct conversion
    if (!args.dstInfo.IS_GLOB && !args.format && args.dstInfo.IS_GLOB) {
        // args.format = [dstParts.ext.replace('*', '')]
    }
    return args;
};


import * as path from 'path';

import { csvToMarkdown } from '../lib/';
const csv = require('csv-stringify/lib/sync');

export const reportMarkdown = (data) => {

    const image = (path) => `![](./${(encodeURI(path))})`;
    const file = (path) => `[${path}](./${(encodeURI(path))})`;

    const set = data.map((d) => [
        `${image(path.parse(d.target).name + path.parse(d.target).ext)}`,
        `${file(path.parse(d.src).name + path.parse(d.src).ext)}`,
    ]
    );

    const csvString = csv(set, {
        header: true,
        delimiter: ',',
        columns: { 'a': 'Thumbnail', 'b': 'File' }
    });

    return csvToMarkdown(csvString);
}
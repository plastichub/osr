#!/usr/bin/env node
import { defaults } from './_cli'; defaults();

import * as cli from 'yargs'; 
import { register as registerSW } from './commands/sw'; registerSW(cli);
import { register as registerSVG2JPG } from './commands/svg2jpg'; registerSVG2JPG(cli);
import { register as registerWatch } from './commands/watch'; registerWatch(cli);
import { register as registerSanitize } from './commands/common/sanitize-filename'; registerSanitize(cli);
import { register as registerPDF2JPG } from './commands/pdf2jpg'; registerPDF2JPG(cli);
const argv = cli.argv;

if (argv.h || argv.help) {
    cli.showHelp();
    process.exit();
} else if (argv.v || argv.version) {
    // tslint:disable-next-line:no-var-requires
    process.exit();
}

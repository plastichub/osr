### General usage

```sh
osr-cad sw --src=(FOLDER||FILE)/GLOB --dst=EXPRESSION||FILE||FOLDER/GLOB
```

### Parameters


**src** : The source directory or file. This can be a glob pattern.

**dst** : The source directory or file. This can be a glob pattern with expressions.

### Variables


**SRC_DIR** : The directory of the current file being converted

**SRC_NAME** : The file name of the current file being converted

**SRC_FILE_EXT** : The file extension of the current file being converted


## Basics

### Convert all assembly files to PDF files in the current directory

```sh
osr-cad sw --src='../plastichub/products/elena/cad/*.SLDASM' --dst='${SRC_NAME}.pdf'
```
### Convert all assembly files to PDF files in the source directory

```sh
osr-cad sw --src='../plastichub/products/elena/cad/*.SLDASM' --dst='${SRC_DIR}/${SRC_NAME}.pdf'
```

### Convert all assembly files to PDF files in the source directory, recursively

**Note** : Recursion can be added by using `**/`.

```sh
osr-cad sw --src='../plastichub/products/elena/cad/**/*.SLDASM' --dst='${SRC_DIR}/${SRC_NAME}.pdf'
```

### Convert all assembly and part files to PDF files in the source directory, recursively

**Note** : Recursion can be added by using `**/`.

**Note** : To select or use multiple file extensions, write ```*.+(SLDASM|SLDPRT)``` instead of ```*.SLDASM``` 

```sh
osr-cad sw --src='../plastichub/products/elena/cad/**/*.+(SLDASM|SLDPRT)' --dst='${SRC_DIR}/${SRC_NAME}.pdf'
```

### Convert all assembly and part files to PDF and JPG files in the source directory, recursively

**Note** : Recursion can be added by using `**/`.

**Note** : To select or use multiple file extensions, write ```*.+(SLDASM|SLDPRT)``` instead of ```*.SLDASM``` 

```sh
osr-cad sw --src='../plastichub/products/elena/cad/**/*.+(SLDASM|SLDPRT)' --dst='${SRC_DIR}/${SRC_NAME}.+(pdf|jpg)'
```

### Convert all assembly files to STEP and PDF files in the source directory

node ..\osr-convert-cad\build\main.js sw --src='./products/asterix-pp/cad/*.+(SLDASM)' --dst='${SRC_DIR}/${SRC_NAME}.+(step|pdf)'

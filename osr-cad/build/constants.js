"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GIT_REPO = exports.GIT_CHANGELOG_MESSAGE_PREFIX = void 0;
exports.GIT_CHANGELOG_MESSAGE_PREFIX = 'ChangeLog:';
exports.GIT_REPO = 'https://gitlab.com/plastichub/osr/plastic-hub';
//# sourceMappingURL=constants.js.map
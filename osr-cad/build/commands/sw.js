"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.register = void 0;
const debug = require("../log");
const argv_1 = require("../argv");
const fg = require('fast-glob');
const csv = require('csv-stringify/lib/sync');
const index_1 = require("../cad/index");
const OSR_REGEX = /^[0-9].+$/;
const isOSR = (filename) => filename.match(OSR_REGEX) != null;
// SolidWorks base converter, using powershell & interop interface (pkgdir/sw)
let options = (yargs) => argv_1.defaultOptions(yargs);
// node ./build/main.js sw --src=../plastichub/products/elena/cad --glob="*.SLDASM" --format="step" --dst="../test"
const register = (cli) => {
    return cli.command('sw', '', options, (argv) => __awaiter(void 0, void 0, void 0, function* () {
        if (argv.help) {
            return;
        }
        const options = argv_1.sanitize(argv);
        options.verbose && debug.inspect("options " + argv.dst, options);
        return index_1.convert(options);
    }));
};
exports.register = register;
//# sourceMappingURL=sw.js.map
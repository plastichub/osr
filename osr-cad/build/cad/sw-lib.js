"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.convert = exports.targets = exports.report = exports.convertFiles = void 0;
const debug = require("../log");
const __1 = require("..");
const path = require("path");
const bluebird = require("bluebird");
const bluebird_1 = require("bluebird");
const exists_1 = require("@plastichub/fs/exists");
const write_1 = require("@plastichub/fs/write");
const lib_1 = require("../lib/");
const index_1 = require("../lib/process/index");
const report_1 = require("../report");
const clone = (obj) => {
    if (null == obj || "object" != typeof obj)
        return obj;
    var copy = obj.constructor();
    for (var attr in obj) {
        if (obj.hasOwnProperty(attr))
            copy[attr] = obj[attr];
    }
    return copy;
};
function convertFiles(file, targets, view, onNode = () => { }, options) {
    return __awaiter(this, void 0, void 0, function* () {
        // debug.inspect(`convert ${file} to `, targets);
        if (options.dry) {
            return bluebird.resolve();
        }
        return bluebird_1.Promise.resolve(targets).map((target) => {
            if (options.skip && exists_1.sync(target)) {
                onNode({
                    src: file,
                    target
                });
                return bluebird.resolve();
            }
            const promise = index_1.Helper.run(path.resolve(options.sw || __dirname + '../../../sw'), options.script, [
                `"${file}"`,
                `"${target}"`,
                `"*${view}"`,
            ]);
            promise.then((d) => {
                onNode(Object.assign(Object.assign({}, d), { src: file, target }));
            });
            return promise;
        }, { concurrency: 1 });
    });
}
exports.convertFiles = convertFiles;
;
const report = (data, dst) => {
    let report = null;
    if (dst.endsWith('.md')) {
        report = report_1.reportMarkdown(data);
    }
    if (dst.endsWith('.csv')) {
        report = report_1.reportCSV(data);
    }
    if (report) {
        __1.info(`Write report to ${dst}`);
        write_1.sync(dst, report);
    }
    return report;
};
exports.report = report;
const targets = (f, options) => {
    const srcParts = path.parse(f);
    const variables = clone(options.variables);
    const targets = [];
    if (options.dstInfo.IS_GLOB) {
        options.dstInfo.GLOB_EXTENSIONS.forEach((e) => {
            variables.SRC_NAME = srcParts.name;
            variables.SRC_DIR = srcParts.dir;
            let targetPath = lib_1.substitute(options.variables.DST_PATH, variables);
            targetPath = path.resolve(targetPath.replace(options.variables.DST_FILE_EXT, '') + e);
            targets.push(targetPath);
        });
    }
    return targets;
};
exports.targets = targets;
function convert(options) {
    return __awaiter(this, void 0, void 0, function* () {
        let reports = [];
        const onNode = (data) => { reports.push(data); };
        yield bluebird_1.Promise.resolve(options.srcInfo.FILES).map((f) => {
            const outputs = exports.targets(f, options);
            options.verbose && debug.inspect(`Convert ${f} to `, outputs);
            return convertFiles(f, outputs, options.view, onNode, options);
        }, { concurrency: 1 });
        if (options.report) {
            const reportOutFile = lib_1.substitute(options.report, {
                dst: options.srcInfo.DIR
            });
            options.verbose && debug.info(`Write report to ${reportOutFile}`);
            exports.report(reports, reportOutFile);
        }
    });
}
exports.convert = convert;
//# sourceMappingURL=sw-lib.js.map
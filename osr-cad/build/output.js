"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.file = exports.stdout = void 0;
const write_1 = require("@plastichub/fs/write");
exports.stdout = (result) => console.log(result);
exports.file = (result, path) => write_1.sync(path, result);
/*
export const render = (result: any, options: Options): OutputResult => {
    const report = format(result, options);
    switch (options.target) {
        case OutputTarget.STDOUT: {
            stdout(report);
            return true;
        }
        case OutputTarget.FILE: {
            file(report, options.path);
            return true;
        }
        default: {
            //private, should never happen since options had to be sanitized
            error('output::render Invalid value in options.target');
            return false;
        }
    }
}
*/ 
//# sourceMappingURL=output.js.map
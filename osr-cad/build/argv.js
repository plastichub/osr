"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.sanitize = exports.defaultOptions = void 0;
const _1 = require("./");
const path = require("path");
const primitives_1 = require("./lib/common/primitives");
const lib_1 = require("./lib");
const read_1 = require("@plastichub/fs/read");
// default options for all commands
const defaultOptions = (yargs) => {
    return yargs.option('src', {
        default: './',
        describe: 'The source directory or source file. Glob patters are supported!',
        demandOption: true
    }).option('format', {
        describe: 'The target format. Multiple formats are allowed as well, use --format=pdf --format=jpg'
    }).option('dst', {
        describe: 'Destination folder or file'
    }).option('view', {
        default: 'Isometric',
        describe: 'Sets the target view'
    }).option('Report', {
        describe: 'Optional conversion report. Can be JSON, HTML, CSV or Markdown'
    }).option('debug', {
        default: 'false',
        describe: 'Enable internal debug message'
    }).option('skip', {
        default: 'false',
        describe: 'Skip existing files'
    }).option('dry', {
        default: 'false',
        describe: 'Run without conversion but create reports'
    }).option('verbose', {
        default: 'true',
        describe: 'Show internal messages'
    }).option('sw', {
        describe: 'Set explicit the path to the Solidworks binaries & scripts.\
        "It assumes SolidWorks.Interop.sldworks.dll and export.cmd at this location!'
    }).option('script', {
        describe: 'Set explicit the path to the Solidworks script'
    });
};
exports.defaultOptions = defaultOptions;
// Sanitizes faulty user argv options for all commands.
const sanitize = (argv) => {
    const src = path.resolve('' + argv.src);
    const config = argv.config ? read_1.sync(path.resolve('' + argv.config), 'json') : {};
    const extraVariables = {};
    for (const key in config) {
        if (Object.prototype.hasOwnProperty.call(config, key)) {
            const element = config[key];
            if (typeof element === 'string') {
                extraVariables[key] = element;
            }
        }
    }
    const args = {
        src: src,
        dst: '' + argv.dst,
        report: argv.report ? path.resolve(argv.report) : null,
        debug: argv.debug === 'true',
        verbose: argv.verbose === 'true',
        dry: argv.dry === 'true',
        skip: argv.skip === 'true',
        glob: argv.glob,
        sw: argv.sw ? path.resolve(argv.sw) : __dirname + '../../sw',
        script: argv.script || 'export.cmd',
        variables: Object.assign({}, extraVariables)
    };
    if (!args.src) {
        _1.error('Invalid source, abort');
        return process.exit();
    }
    if (argv.format) {
        if (typeof argv.format === 'string') {
            args.format = [argv.format];
        }
        else if (argv.source && primitives_1.isArray(argv.format)) {
            args.format = argv.format;
        }
    }
    args.srcInfo = lib_1.pathInfo(argv.src);
    for (const key in args.srcInfo) {
        if (Object.prototype.hasOwnProperty.call(args.srcInfo, key)) {
            args.variables['SRC_' + key] = args.srcInfo[key];
        }
    }
    if (argv.dst) {
        args.dst = path.resolve(lib_1.substitute(args.dst, args.variables));
        args.dstInfo = lib_1.pathInfo(args.dst);
        args.dstInfo.PATH = argv.dst;
        for (const key in args.dstInfo) {
            if (Object.prototype.hasOwnProperty.call(args.dstInfo, key)) {
                args.variables['DST_' + key] = args.dstInfo[key];
            }
        }
    }
    args.view = argv.view || "Isometric";
    // check for single file direct conversion
    if (!args.dstInfo.IS_GLOB && !args.format && args.dstInfo.IS_GLOB) {
        // args.format = [dstParts.ext.replace('*', '')]
    }
    return args;
};
exports.sanitize = sanitize;
//# sourceMappingURL=argv.js.map
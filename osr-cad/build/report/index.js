"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.reportMarkdown = exports.reportCSV = void 0;
var csv_1 = require("./csv");
Object.defineProperty(exports, "reportCSV", { enumerable: true, get: function () { return csv_1.reportCSV; } });
var markdown_1 = require("./markdown");
Object.defineProperty(exports, "reportMarkdown", { enumerable: true, get: function () { return markdown_1.reportMarkdown; } });
//# sourceMappingURL=index.js.map
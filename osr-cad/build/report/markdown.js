"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.reportMarkdown = void 0;
const path = require("path");
const lib_1 = require("../lib/");
const csv = require('csv-stringify/lib/sync');
const reportMarkdown = (data) => {
    const image = (path) => `![](./${(encodeURI(path))})`;
    const file = (path) => `[${path}](./${(encodeURI(path))})`;
    const set = data.map((d) => [
        `${image(path.parse(d.target).name + path.parse(d.target).ext)}`,
        `${file(path.parse(d.src).name + path.parse(d.src).ext)}`,
    ]);
    const csvString = csv(set, {
        header: true,
        delimiter: ',',
        columns: { 'a': 'Thumbnail', 'b': 'File' }
    });
    return lib_1.csvToMarkdown(csvString);
};
exports.reportMarkdown = reportMarkdown;
//# sourceMappingURL=markdown.js.map
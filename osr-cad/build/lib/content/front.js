"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.projects_header = exports.product_header = exports.machine_header = exports.drawing_image = exports.gallery_image = exports.howto_header = void 0;
const strings_1 = require("../common/strings");
const howto_header = (title, category, image, description = "", tagline = "", config = "") => {
    return `---
image: ${image}
category: "${category}"
title: "${title}"
tagline: ${tagline || '""'}
description: ${description || `"Precious Plastic - Howto : ${category} :: ${title} "`}
${config}
---\n`;
};
exports.howto_header = howto_header;
const gallery_image = (path, title = "", alt = "") => {
    return `
 - url: "${path}"
   image_path: "${path}"
   alt: "${alt}"
   title: "${title}"`;
};
exports.gallery_image = gallery_image;
const drawing_image = (path, pdf, title = "", alt = "") => {
    return `
 - url: "${pdf}"
   image_path: "${path}"
   alt: "${alt}"
   title: "${title}"`;
};
exports.drawing_image = drawing_image;
const machine_header = (title, category, image, slug, rel, description = "", tagline = "", config = "") => {
    return `---
image: ${image}
category: "${category}"
title: "${title}"
product_rel: "/${rel}"
tagline: ${tagline || '""'}
description: ${description || `"Precious Plastic - Machine : ${strings_1.capitalize(category)} :: ${title}"`}
${config}
---\n`;
};
exports.machine_header = machine_header;
const product_header = (title, category, image, slug, rel, description = "", tagline = "", config = "") => {
    return `---
image: ${image}
category: "${category}"
title: "${title}"
product_rel: "/${rel}"
tagline: ${tagline || '""'}
description: ${description || `"Precious Plastic - Machine : ${title} by PlasticHub"`}
${config}
---\n`;
};
exports.product_header = product_header;
const projects_header = (title, category, image, slug, rel, description = "", tagline = "", config = "") => {
    return `---
image: ${image}
category: "${category}"
title: "${title}"
product_rel: "/${rel}"
tagline: ${tagline || '""'}
description: ${description || `"Precious Plastic - ${strings_1.capitalize(category)} :: ${title}"`}
${config}
sidebar: 
   nav: "projects"
---\n`;
};
exports.projects_header = projects_header;
//# sourceMappingURL=front.js.map
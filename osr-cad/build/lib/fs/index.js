"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.pathInfo = exports.isFolder = exports.isFile = exports.thumbs = exports.toHTML = exports.md2html = exports.resize_images = exports.tail_image = exports.head_image = exports.images = exports.files = exports.write = exports.dir = exports.exists = exports.read = void 0;
const fg = require('fast-glob');
const path = require("path");
const fs = require("fs");
const bluebird = require("bluebird");
const showdown_1 = require("showdown");
const read_1 = require("@plastichub/fs/read");
const exists_1 = require("@plastichub/fs/exists");
const js_beautify_1 = require("js-beautify");
var read_2 = require("@plastichub/fs/read");
Object.defineProperty(exports, "read", { enumerable: true, get: function () { return read_2.sync; } });
var exists_2 = require("@plastichub/fs/exists");
Object.defineProperty(exports, "exists", { enumerable: true, get: function () { return exists_2.sync; } });
var dir_1 = require("@plastichub/fs/dir");
Object.defineProperty(exports, "dir", { enumerable: true, get: function () { return dir_1.sync; } });
var write_1 = require("@plastichub/fs/write");
Object.defineProperty(exports, "write", { enumerable: true, get: function () { return write_1.sync; } });
const write_2 = require("@plastichub/fs/write");
const index_1 = require("../process/index");
const array_1 = require("../common/array");
const html_1 = require("../content/html");
const IMAGES_GLOB = '*.+(JPG|jpg|png|PNG|gif)';
const isGlob = require("is-glob");
const globBase = require('glob-base');
const parseGlob = require('parse-glob');
const files = (dir, glob) => fg.sync(glob, { dot: true, cwd: dir, absolute: true });
exports.files = files;
const images = (source) => exports.files(source, IMAGES_GLOB);
exports.images = images;
const head_image = (_images) => array_1.firstOf(_images);
exports.head_image = head_image;
const tail_image = (_images) => array_1.lastOf(_images);
exports.tail_image = tail_image;
const GLOB_GROUP_PATTERN = /[!*+?@]\(.*\)/;
const getExtensions = (glob) => {
    const match = glob.match(GLOB_GROUP_PATTERN);
    if (match) {
        return glob.substring(match.index + 2, glob.lastIndexOf(')')).split('|');
    }
    else {
        return [parseGlob(glob).path.ext];
    }
};
function resize_images(files) {
    return __awaiter(this, void 0, void 0, function* () {
        return bluebird.mapSeries(files, (file) => {
            const inParts = path.parse(file);
            const promise = index_1.Helper.run(inParts.dir, 'convert', [
                `"${inParts.base}"`,
                '-quality 70',
                '-resize 1980',
                '-sharpen 0x1.0',
                `"${inParts.name}${inParts.ext}"`
            ]);
            return promise;
        });
    });
}
exports.resize_images = resize_images;
const md2html = (content) => {
    let converter = new showdown_1.Converter({ tables: true });
    converter.setOption('literalMidWordUnderscores', 'true');
    return converter.makeHtml(content);
};
exports.md2html = md2html;
const toHTML = (path, markdown) => {
    const content = read_1.sync(path, 'string');
    if (!markdown) {
        let converter = new showdown_1.Converter({ tables: true });
        converter.setOption('literalMidWordUnderscores', 'true');
        return converter.makeHtml(content);
    }
    else {
        return content;
    }
};
exports.toHTML = toHTML;
const jekyllNop = "---\n#jekyll\n---\n";
const frontMatter = /^---[.\r\n]*---/;
const thumbs = (source, meta = true, sep = "<hr/>") => {
    let pictures = exports.images(source);
    let content = "";
    pictures.forEach((f, i) => {
        if (meta) {
            let picMD = path.resolve(path.join(path.parse(f).dir, path.sep, path.parse(f).name + '.md'));
            if (exists_1.sync(picMD)) {
                const picMDContent = read_1.sync(picMD, "string");
                if (picMDContent.length > 3 && picMDContent !== jekyllNop) {
                    content += picMDContent.substr(picMDContent.lastIndexOf('---') + 3, picMDContent.length);
                    content += "\n";
                }
                else {
                    write_2.sync(picMD, jekyllNop);
                }
            }
            else {
                write_2.sync(picMD, jekyllNop);
            }
        }
        content += html_1.img(`./${path.parse(f).base}`, `${i + 1}. `, path.parse(f).base);
        content += "\n";
        content += sep;
    });
    return js_beautify_1.html_beautify(content);
};
exports.thumbs = thumbs;
const isFile = (src) => {
    let srcIsFile = false;
    try {
        srcIsFile = fs.lstatSync(src).isFile();
    }
    catch (e) {
    }
    return srcIsFile;
};
exports.isFile = isFile;
const isFolder = (src) => {
    let srcIsFolder = false;
    try {
        srcIsFolder = fs.lstatSync(src).isDirectory();
    }
    catch (e) {
    }
    return srcIsFolder;
};
exports.isFolder = isFolder;
const pathInfo = (src) => {
    const srcParts = path.parse(src);
    let variables = {
        PATH: src
    };
    variables.DIR = srcParts.dir;
    variables.NAME = srcParts.name;
    variables.FILE_NAME = srcParts.base;
    variables.FILE_EXT = srcParts.ext.replace('.', '');
    variables.PATH = src;
    variables.IS_FILE = exports.isFile(src);
    variables.IS_FOLDER = exports.isFolder(src);
    variables.IS_EXPRESSION = src.match(/\$\{([^\s\:\}]+)(?:\:([^\s\:\}]+))?\}/g) != null;
    if (!variables.IS_FOLDER && !variables.IS_FILE) {
        variables.IS_GLOB = isGlob(srcParts.base);
    }
    if (variables.IS_GLOB) {
        //important: use the forward slash since path.resolve will return backslashes on Windows
        const glob_base = globBase(src);
        variables.DIR = path.resolve(glob_base.base);
        variables.FILE_NAME = glob_base.glob;
        variables.GLOB = glob_base.glob;
        variables.GLOB_EXTENSIONS = getExtensions(glob_base.glob);
        variables.FILES = fg.sync(glob_base.glob, { dot: true, cwd: variables.DIR, absolute: true });
        variables.FILES = variables.FILES.filter((f) => {
            return !path.parse(f).name.startsWith('~$');
        });
    }
    return variables;
};
exports.pathInfo = pathInfo;
//# sourceMappingURL=index.js.map
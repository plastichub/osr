#!/usr/bin/env node
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const _cli_1 = require("./_cli");
_cli_1.defaults();
const cli = require("yargs");
const sw_1 = require("./commands/sw");
sw_1.register(cli);
const svg2jpg_1 = require("./commands/svg2jpg");
svg2jpg_1.register(cli);
const watch_1 = require("./commands/watch");
watch_1.register(cli);
const sanitize_filename_1 = require("./commands/common/sanitize-filename");
sanitize_filename_1.register(cli);
const pdf2jpg_1 = require("./commands/pdf2jpg");
pdf2jpg_1.register(cli);
const argv = cli.argv;
if (argv.h || argv.help) {
    cli.showHelp();
    process.exit();
}
else if (argv.v || argv.version) {
    // tslint:disable-next-line:no-var-requires
    process.exit();
}
//# sourceMappingURL=main.js.map
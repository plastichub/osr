# OSR CAD Tools

This is a CLI(CommandLineInterface) toolset to convert 3D files, using Solidworks and other software.

## Requirements

1. [Node-JS](https://nodejs.org/en/download/)
2. Install [Git (and the tools!)](https://git-scm.com/downloads) (Make sure you enable Linux tools on Windows console)
3. Solidworks 2020. In case you are using another version, please find on your disc 'SolidWorks.Interop.sldworks.dll' and replace the one in [./sw](https://gitlab.com/plastichub/osr/osr-convert-cad/tree/main/sw)


## Optional requirements (for any image/raster conversion related commands)

- [GhostScript](https://www.ghostscript.com/download/gsdnld.html) (needed for converting PDF to JPG)
- [Imagick](https://imagemagick.org/script/download.php) (needed for resizing images)

## Installation

```sh

git clone https://gitlab.com/plastichub/osr/osr-convert-cad.git
cd osr-convert-cad
npm i

# or 

npm i @plastichub/osr-cad -g

```

## Usage

Open a terminal and run this:

```sh
osr-cad --help (RTFM)
```

See more in [./examples.md](./examples.md)

### References

- [site:Solidworks API basics - examples](https://www.codestack.net/labs/solidworks/)
- [site:Rhino-API](https://developer.rhino3d.com/api/RhinoCommon/html/R_Project_RhinoCommon.htm)
- [site:Solidworks Reverse Decoder](http://heybryan.org/solidworks_file_format.html)

## Todos

- [x] Select default views via CLI Argument
- [ ] Arg: Skip suppressed | hidden (difficult since it's out of part file scope, check explorer api ) | dry mode
- [ ] Arg: Overwrite files
- [ ] Arg: skip non OSR parts
- [ ] Arg: displaymode : wireframe, shaded, ... (see [SW Docs](http://help.solidworks.com/2017/english/api/sldworksapi/solidworks.interop.sldworks~solidworks.interop.sldworks.iview~setdisplaymode3.html))
- [x] report
- [ ] export as lib
- [ ] Multi view (trainings data for @plastichub/part-detector)
- [ ] Speed: use sampe instance for multiple exports
- [ ] Context Menu Shell Extension (@osr-tools)
- [ ] Local/Global config
- [ ] Report templates
  - [ ] xls
  - [ ] md
  - [ ] txt
- [ ] Plugin interface for custom format (chained)

### Commands

- [x] Solidworks
 - [ ] Set system wide options for JPG output
 - [ ] Set system wide options for PDF output
- [ ] Directory index
-  [ ] arg: local HTML path/dir offset
-  [ ] arg: generate UNC paths
-  [ ] format: PDF
-  [ ] arg: sw drawing/BOMs to CSV/xls
- [ ] Part/Sub-Assembly web(&local) compilation/index
- [ ] Web directory ([xeokit](https://gitlab.com/plastichub/osr/xeokit-sdk))

### Lib

- [ ] SW: 4view single image

### Issues

- [ ] Incorrect JPG output with sw2020

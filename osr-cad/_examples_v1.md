This examples assume Linux shell support on your Windows setup which comes with the [GIT package](https://git-scm.com/downloads). If not, please use Windows compatible paths instead! 

For instance : ```--src=../plastichub/products/elena/cad``` (With Linux shell support) should be written as 
```--src="..\\plastichub\\products\\elena\\cad"```

*Note*: you need to write the quotes only if using whitespaces in folder or filenames.

### Convert all assembly files to step files

```sh
osr-cad sw --src=../plastichub/products/elena/cad --glob="*.SLDASM" --format="step" --dst="../test"
```

### Convert all assembly files to jpg files
```sh
osr-cad sw --src=../plastichub/products/elena/cad --glob="*.SLDASM" --format="jpg" --dst="../test"
```

### Convert all parts to jpg files
```sh
osr-cad sw --src=../plastichub/products/elena/cad --glob="*.SLDPRT" --format="jpg" --dst="../test"
```

### Convert all parts to jpg files - using Dimetric view
```sh
osr-cad sw --src=../plastichub/products/obelix/cad --glob="*.SLDPRT" --format="jpg" --dst="../test" --view="Dimetric"
```

### Convert all parts to jpg files - using Front view
```sh
osr-cad sw --src=../plastichub/products/obelix/cad --glob="*.SLDPRT" --format="jpg" --dst="../test" --view="Front"
```

### Convert all parts and assemblies to jpg files - using Isometric view

```sh
osr-cad sw --src=../plastichub/products/obelix/cad --glob="*.SLDPRT|*.SLDASM" --format="jpg" --dst="../test" --view="Isometric"
```

### Generate a CSV report
```sh
osr-cad sw --src=../plastichub/products/obelix/cad_minimal --glob="*.SLDPRT|*.SLDASM" --format="jpg" --dst="../plastichub/products/obelix/cad_minimal/parts" --view="Isometric" --report='${dst}/sw_export.csv'
```

### Generate a Markdown report (is implicit)
```sh
osr-cad sw --src=../plastichub/products/obelix/cad_minimal --glob="*.SLDPRT|*.SLDASM" --format="jpg" --dst="../plastichub/products/obelix/cad_minimal/parts" --view="Isometric" --report='${dst}/report.md'
```

### Convert all drawings to PDF, recursively
```sh
osr-cad sw --src=../plastichub/products/obelix/cad_new --glob="**/*.SLDDRW" --format="pdf" --dst="../plastichub/products/obelix/drawings" --view="Isometric" --report='${dst}/report.md'
```

### Convert all drawings to PDF and JPG, recursively
```sh
osr-cad sw --src=../plastichub/products/obelix/cad_new --glob="**/*.SLDDRW" --format="pdf" --format="jpg" --dst="../plastichub/products/obelix/drawings" --view="Isometric" --report='${dst}/report.md'
```
### Implementation Details

An OSR specification is currently designed as human readable configuration file (YAML), to be used in various scenarios

- scafffolding / boilerplating entities
- validation
- representation (print, web, ...)


![](./assets/osr-specs-overview.jpg)

To reduce complexity for the reader, the configuration file is accompanied with a meta file which adds description but also rules to assist higher level tools to interpret the information. In example, we'd like to ensure that a certain field is being populated:

***Product_Specification.yaml***

```yaml
- product_parts_drawing: "path_to_drawing_file"
```

In the meta database, we set some hints about this field:

***Product_Specification_Meta.yaml***

```yaml
- product_parts_drawing:
  - optional: false # the validation process will trigger an error if not set
  - auto: true      # set this to true to auto-fill this field with a default
  - type: 'file'    # set the type to 'file', to assist validation and boilerplating tools
  - default: '${default_place_drawing_holder}'  # the default for this value whereby 'default_place_drawing_holder' is being resolved in the variable system
```

Whereby ```type``` file is also being specified in a global and/or context database:

***Types.yaml***
```yaml
- types:
    - type: 'file'                      # type identifier label
        - base: 'string'                # base class
        - resolve: '@plastichub/fs'    # resolve this type with this implementation
        - id: 'some uuid here'          # to enable renaming of the type
```

### Conclusions/Requirements

- must have interop capabilties with other formats as JSON
- must provide defaults
- simple expressions, ie: ```= other_value * 5 ```. For now, we will use a custom bison based language, eg : filtrex which enables compatibilty with Google/LibreOffice sheets
- higher level tools needs to be able to trace details back (meta store)
- Queries and accessors, eg: XPath like expressions as [JSON path](https://goessner.net/articles/JsonPath/)
- inheritance of specifications, similar to the OOP construct we might allow derivation from existing specifications but limit to 2 levels to prevent cluttering aka 'fat base class problem'
- recommendations, eg: to promote best practice
- compliance, each specification should unveil met standards. for now it's enough to populate the description of each field with additional infos about ISOs & DINs
- enable specs per context. eg: emerge spec files from parent files when fields are missing

### References

- [Expert Systems](https://en.wikipedia.org/wiki/Expert_system#Disadvantages)

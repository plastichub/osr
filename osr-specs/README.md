# OSR-specs

OSR specs for verticals

## Todos - sprint27

### Directory

- [ ] Contact
- [ ] Service
- [ ] Location
- [ ] Region

### Knowledgebase

- [ ] Process
  - [ ] Step
- [ ] Research
  - [ ] Trial/Logs

### Product

- [ ] Public Rep.
- [ ] Private Rep.
- [ ] Target Transform

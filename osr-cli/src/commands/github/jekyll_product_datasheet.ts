import * as CLI from 'yargs';
import * as debug from '../..';
import * as path from 'path';
const YAML = require('json-to-pretty-yaml');
import { git_log, changelog, dir, read, write, exists, machine_header, product_header, images, gallery_image, parse_config, drawing_image, read_fragments, substitute, resolveConfig } from '../../lib/';

const defaultOptions = (yargs: CLI.Argv) => {
    return yargs.option('product', {
        default: 'elena',
        describe: 'name of the product which matches the folder name inside the products folder'
    }).option('products', {
        default: './',
        describe: 'location of the products folder which contains the \'bazar\' and \'products\' folders'
    }).option('format', {
        default: 'html',
        describe: 'selects the output format, can be \'html\' or \'md\''
    }).option('debug', {
        default: 'true',
        describe: 'Enable internal debug message'
    })
};

let options = (yargs: CLI.Argv) => defaultOptions(yargs);

// npm run build ; node ./build/main.js machine-jekyll --debug=true --products=../../products --product=elena
export const register = (cli: CLI.Argv) => {
    return cli.command('product-jekyll-datasheet', 'Creates Jekyll markdown datasheet page', options, async (argv: CLI.Arguments) => {
        if (argv.help) { return; }

        const markdown = true;

        const isDebug = argv.debug === 'true';

        const product_rel_path = argv.product;
        const product_rel_path_name = `${path.parse(product_rel_path as string).dir}/${path.parse(product_rel_path as string).name}/`;

        debug.info('rel', path.parse(product_rel_path as string));

        const root = path.resolve(`${argv.products}`);

        // global config
        const cPath = argv.products ? path.resolve(`${argv.products}/templates/site/config.json`) : path.resolve('./config.json');
        isDebug && debug.info(`read config at ${cPath}`);
        const config = read(cPath, 'json') as any;

        // jekyll machine pages root
        const machines_directory = path.resolve(`${argv.products}/_pages/`);

        const templates_path = path.resolve(`${argv.products}/templates/site`);
        if (!exists(templates_path)) {
            debug.error(`\t Cant find templates at ${templates_path}, path doesn't exists`);
            return;
        }

        const shared_templates_path = path.resolve(`${argv.product}/templates/shared`);

        const template_path = path.resolve(`${templates_path}/machine.md`);
        // machine directory
        const machine_path = path.resolve(`${argv.products || config.products_path}/${argv.product}`);

        const fragments_path = path.resolve(`${templates_path}`);

        debug.info(fragments_path);

        isDebug && debug.info(`\n Generate machine description for ${argv.product}, reading from ${machine_path},
            using fragments at ${fragments_path}`);

        if (!exists(machine_path)) {
            debug.error(`\t Cant find machine at ${machine_path}, path doesn't exists`);
            return;
        }

        let fragments: any = { ...config };

        // read all global fragments
        isDebug && debug.info(`Read global fragments at ${fragments_path}`);


        read_fragments(shared_templates_path, fragments, '/templates/shared/', "machine-shared");
        read_fragments(fragments_path, fragments, '/templates/site/', "machine global");

        // read all product specific fragments
        const product_fragments_path = path.resolve(`${machine_path}/templates/site`);
        if (!exists(product_fragments_path)) {
            debug.error(`Machine has no template files! Creating folder structure ..`);
            dir(product_fragments_path);
        } else {
            isDebug && debug.info(`read machine fragments at ${product_fragments_path}`);
        }

        const _git_log = await git_log(root, product_rel_path) as any;

        const change_log_html = changelog(_git_log.files);

        const change_log_path = path.resolve(`${machine_path}/templates/site/changelog.html`);
        write(change_log_path, change_log_html);

        read_fragments(product_fragments_path, fragments, product_rel_path_name, "machine");

        let machine_config;

        // read product variables
        if (!exists(path.resolve(`${machine_path}/config.json`))) {
            isDebug && debug.warn(`product has no config.json, please ensure there is a config.json in ${machine_path}`);
            write(path.resolve(`${machine_path}/config.json`), '{}');
        } else {
            machine_config = read(path.resolve(`${machine_path}/config.json`), 'json');
            fragments = { ...fragments, ...machine_config as any };
            isDebug && debug.info(`Loaded machine variables`);
        }

        fragments['product_rel'] = product_rel_path_name;
        parse_config(fragments, machine_path);
        resolveConfig(fragments);

        let config_yaml = read(path.resolve(`${machine_path}/config.yaml`), 'string') as any || "";
        let _yaml_extra = {};
        for (const k in machine_config) {
            _yaml_extra[k] = fragments[k];
        }

        _yaml_extra['git_last'] = _git_log.last.date;

        _yaml_extra['layout'] = 'print';
        _yaml_extra['permalink'] = `${product_rel_path}/datasheet`;
        _yaml_extra['showResources'] = false;

        _yaml_extra = YAML.stringify(_yaml_extra);
        config_yaml = _yaml_extra + config_yaml;
        config_yaml = substitute(config_yaml, fragments);

        if (!fragments.product) {
            debug.error(`Have no product template! : ${machine_path} - ${templates_path}`);
            return;
        }

        const products_description = substitute(fragments.product_datasheet, fragments);

        let content = product_header(fragments['product_name'],
            fragments['category'],
            fragments['product_perspective'] ? fragments['product_perspective'] : `/products/${fragments['slug']}/renderings/perspective.JPG`,
            fragments['slug'],
            fragments['product_rel'],
            config.description || "",
            config.tagline || "",
            config_yaml);

        content += products_description;

        let out_path = path.resolve(`${machines_directory}/${fragments['slug']}-datasheet.md`);
        isDebug && debug.info(`Write jekyll product page ${out_path}`);
        write(out_path, content);
    });
};

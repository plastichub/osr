import * as CLI from 'yargs';
import * as debug from '../..';
import * as path from 'path';
import { git_log, changelog, lastOf, dir, read, write, exists, machine_header, images, gallery_image, parse_config, drawing_image, read_fragments, substitute, projects_header } from '../../lib/';

const defaultOptions = (yargs: CLI.Argv) => {
    return yargs.option('project', {
        default: 'elena',
        describe: 'name of the product which matches the folder name inside the projects folder'
    }).option('products', {
        default: './',
        describe: 'location of the products folder which contains the \'bazar\' and \'products\' folders'
    }).option('format', {
        default: 'html',
        describe: 'selects the output format, can be \'html\' or \'md\''
    }).option('debug', {
        default: 'true',
        describe: 'Enable internal debug message'
    })
};

let options = (yargs: CLI.Argv) => defaultOptions(yargs);

// npm run build ; node ./build/main.js project-jekyll --debug=true --products=../../products --project=elena
export const register = (cli: CLI.Argv) => {
    return cli.command('project-jekyll', 'Creates Jekyll Project Markdown page', options, async (argv: CLI.Arguments) => {
        if (argv.help) { return; }

        const markdown = true;

        const isDebug = argv.debug === 'true';

        const project_rel_path : string = argv.project as string;
        const project_rel_path_name = `${path.parse(project_rel_path as string).dir}/${path.parse(project_rel_path as string).name}/`;

        debug.info('rel', path.parse(project_rel_path as string));

        const root = path.resolve(`${argv.products}`);

        // global config
        const cPath = argv.products ? path.resolve(`${argv.products}/templates/jekyll/config.json`) : path.resolve('./config.json');
        isDebug && debug.info(`read config at ${cPath}`);
        const config = read(cPath, 'json') as any;

        // jekyll projects pages root
        const projects_directory = path.resolve(`${argv.products}/_projects/`);

        const templates_path = path.resolve(`${argv.products}/templates/jekyll`);
        if (!exists(templates_path)) {
            debug.error(`\t Cant find templates at ${templates_path}, path doesn't exists`);
            return;
        }

        const template_path = path.resolve(`${templates_path}/machine.md`);

        // machine directory
        const project_path = path.resolve(`${argv.products}/${argv.project}`);

        const fragments_path = path.resolve(`${templates_path}`);

        debug.info(fragments_path);

        isDebug && debug.info(`\n Generate project description for ${argv.project}, reading from ${project_path},
            using fragments at ${fragments_path}`);

        if (!exists(project_path)) {
            debug.error(`\t Cant find machine at ${project_path}, path doesn't exists`);
            return;
        }

        let fragments: any = { ...config };

        // read all global fragments
        isDebug && debug.info(`Read global fragments at ${fragments_path}`);


        read_fragments(fragments_path, fragments, '/templates/jekyll/', "project global");


        // read all product specific fragments
        const product_fragments_path = path.resolve(`${project_path}/templates/jekyll`);
        if (!exists(product_fragments_path)) {
            debug.error(`Machine has no template files! Creating folder structure ..`);
            dir(product_fragments_path);
        } else {
            isDebug && debug.info(`read machine fragments at ${product_fragments_path}`);
        }
        
        // const _git_log = await git_log(root, product_rel_path);
        // debug.info('log', _git_log );
        // const change_log_html = changelog(_git_log);

        //const change_log_path = path.resolve(`${machine_path}/templates/jekyll/changelog.html`);
        //write(change_log_path,change_log_html);

        read_fragments(product_fragments_path, fragments, project_rel_path_name, "machine");


        // read product variables
        if (!exists(path.resolve(`${project_path}/config.json`))) {
            isDebug && debug.warn(`product has no config.json, please ensure there is a config.json in ${project_path}`);
            write(path.resolve(`${project_path}/config.json`), '{}');
        } else {
            fragments = { ...fragments, ...read(path.resolve(`${project_path}/config.json`), 'json') as any };
            isDebug && debug.info(`Loaded machine variables`);
        }

        fragments['product_rel'] = project_rel_path_name;

        parse_config(fragments, project_path);
        
        // compile and write out

        for (const key in fragments) {
            const resolved = substitute(fragments[key], fragments);
            fragments[key] = resolved;
        }

        for (const key in fragments) {
            const resolved = substitute(fragments[key], fragments);
            fragments[key] = resolved;
        }

        let config_yaml = read(path.resolve(`${project_path}/config.yaml`), 'string') as any || "";


        fragments['slug'] = lastOf(project_rel_path.split('/'));
        fragments['product-id'] = fragments['slug'];
        
        config_yaml = substitute(config_yaml, fragments);


        if (!fragments.project) {
            debug.error(`Have no projects template! : ${project_path} - ${templates_path}`);
            return;
        }

        const project_description = substitute(fragments.project, fragments);

        let gallery = "";
        if (fragments['gallery'] !== false && exists(path.resolve(`${project_path}/media`))) {
            gallery = "gallery:"
            let _images = images(path.resolve(`${project_path}/media`));
            _images = _images.map((f) => {
                let _path = `/${project_rel_path}/media/${path.parse(f).base}`;
                return `${gallery_image(_path)}`;
            }).join("") as any;
            gallery += _images;
        }

        let gallery_social = "";
        if (fragments['gallery_social'] !== false && exists(path.resolve(`${project_path}/media/social`))) {
            gallery_social = "\ngallery_social:"
            let _images = images(path.resolve(`${project_path}/media/social`));
            _images = _images.map((f) => {
                let _path = `/${project_rel_path}/media/social/${path.parse(f).base}`;
                return `${gallery_image(_path)}`;
            }).join("") as any;
            gallery_social += _images;
        }

        let gallery_drawings = "";
        if (fragments['gallery_drawings'] !== false && exists(path.resolve(`${project_path}/drawings`))) {
            gallery_drawings = "\ngallery_drawings:"
            let _images = images(path.resolve(`${project_path}/drawings`));
            debug.info(`Read drawings at ${path.resolve(`${project_path}/drawings`)} ${_images.length}`);
            _images = _images.map((f) => {
                let _path = `/${project_rel_path}/drawings/${path.parse(f).base}`;
                let _pdf = `/${project_rel_path}/drawings/${path.parse(f).name}.PDF`;
                return `${drawing_image(_path, _pdf)}`;
            }).join("") as any;
            gallery_drawings += _images;
        }

        let content = projects_header(fragments['product_name'],
            fragments['category'],
            fragments['product_perspective'] ? fragments['product_perspective'] : `/projects/${fragments['slug']}/renderings/perspective.JPG`,
            fragments['slug'],
            fragments['product_rel'],
            config.description || "",
            config.tagline || "",
            config_yaml +
            gallery +
            gallery_social +
            gallery_drawings);

        content += project_description;

        let out_path = path.resolve(`${projects_directory}/${fragments['slug']}.md`);
        isDebug && debug.info(`Write jekyll projects page ${out_path}`);
        write(out_path, content);
    });
};

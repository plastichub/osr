import * as CLI from 'yargs';
import { defaultOptions } from '../argv';

import { getScope, Scope } from '../renderer/puppeteer/Scope';

import { info, default_path_howto } from '../';
// no extra options, using defaults
const options = (yargs: CLI.Argv) => defaultOptions(yargs);
import { Response } from 'puppeteer'

import * as path from 'path';

'node ./build/main.js onearmy --output=../../onearmy-raw'

let scope: Scope;

export function exportToJson(idbDatabase) {
    // var t = window.indexedDB.open('OneArmyCache');
    // exportToJson(t.result).then((e)=>{console.log});

    /*
    var r = indexedDB.open('OneArmyCache');
    r.onsuccess = function(event) {
    var db = event.target.result;
    exportToJson(db).then((e)=>{window.ht = e});
    };
    */
    return new Promise((resolve, reject) => {
        const exportObject = {}
        debugger;
        if (idbDatabase.objectStoreNames.length === 0) {
            resolve(JSON.stringify(exportObject))
        } else {
            const transaction = idbDatabase.transaction(
                idbDatabase.objectStoreNames,
                'readonly'
            )

            transaction.addEventListener('error', reject)

            for (const storeName of idbDatabase.objectStoreNames) {
                const allObjects = []
                transaction
                    .objectStore(storeName)
                    .openCursor()
                    .addEventListener('success', event => {
                        const cursor = event.target.result
                        if (cursor) {
                            // Cursor holds value, put it into store data
                            allObjects.push(cursor.value)
                            cursor.continue()
                        } else {
                            // No more values, store is done
                            exportObject[storeName] = allObjects

                            // Last store was handled
                            if (
                                idbDatabase.objectStoreNames.length ===
                                Object.keys(exportObject).length
                            ) {
                                resolve(JSON.stringify(exportObject))
                            }
                        }
                    })
            }
        }
    })
}

async function onResponse(response: Response, scope: Scope) {
    const url = response.url();
    if (url.includes('firestore')) {
        const data = await response.text();
        if (data.includes('document')) {
            info('got firestore response : ' + url);
            // var t = window.indexedDB.open('OneArmyCache');
            // exportToJson(t.result).then((e)=>{console.log});
            // write(scope.args.dst, data);
            await scope.page.evaluate(() => {
                console.log('something');
                const request = window.indexedDB.open('OneArmyCache', 1);
                request.onerror = e => {
                    console.error('Failure ' + e, e);
                };
                request.onsuccess = () => {
                    var db = request.result;
                    console.log('Success', db);
                };

                console.log();
            });
        }
    }
}

export const register = (cli: CLI.Argv) => {

    return cli.command('HowtoBackup', '', options, async (argv: CLI.Arguments) => {
        //@TODO: this guard might not be necessary
        if (argv.help) { return; }

        const url = "https://community.preciousplastic.com/how-to/hands-free-door-opener";

        const options = {
            headless: false,
            url: url,
            dst: path.resolve(default_path_howto(path.resolve(argv.out as string), url))
        };

        scope = getScope(options);
        await scope.init();
        scope.onResponse = onResponse;
        await scope.page.goto(options.url, {
            timeout: 600000,
            waitUntil: 'networkidle0'
        }).then(() => {
            console.log('ready');
        });



        /*
        const url = "https://community.preciousplastic.com/how-to/hands-free-door-opener";

        const browser = await launch({
            headless: options.headless,
            devtools: true
        });
        const page = await browser.newPage();
        
        
        
        page.on('console', msg => {
            const text = msg.text();
            // inspect('Console Message:', msg.text());
            if (text.includes('[v3_events] docs retrieved')) {

                page.content().then((html) => {
                    console.log('ready!');
                    debug(`Write HTML of ${url} to ${dst}`);
                    write(dst, html);
                    Crawler.end(page);
                })
            }
        });

        page.goto(url, {
            timeout: 600000,
            waitUntil: 'networkidle0'
        }).then((v) => {
            console.log('ready');
        });
        */


        /*
        const args = sanitize(argv) as Options;
        const result = await Puppeteer.crawler(args.url, args);
        output(result, args);*/
    });
};

import { Browser, launch, Page, Response } from 'puppeteer';
import { inspect, error, debug } from '../../log';
import { capture_requests, capture_responses } from './network';
let instance: Scope;
import * as path from 'path';
import { URL } from 'url';
export const STATS_SUFFIX = '_stats.json';
export const SESSION_EVENTS_SUFFIX = '_session.json';
export const TRACE_SUFFIX = '_trace.json';
const included_categories = ['devtools.timeline'];
const _url_short = (url: string) =>
    new URL(url).hostname;

const _date_suffix = () =>
    new Date().toLocaleTimeString().replace(/:/g, '_');

const _random_suffix = () =>
    Math.random() * 100;

const _default_filename = (url: string) =>
    `${_url_short(url)}_${_random_suffix()}`;

export const default_path = (cwd: string, url: string) =>
    `${path.join(cwd, _default_filename(url))}${STATS_SUFFIX}`;

export const default_session_events_path = (cwd: string, url: string) =>
    `${path.join(cwd || process.cwd(), 'sessions', _default_filename(url))}${SESSION_EVENTS_SUFFIX}`;

export const default_trace_path = (cwd: string, url: string) =>
    `${path.join(cwd, _default_filename(url))}${TRACE_SUFFIX}`;

export class Scope {
    browser!: Browser;
    context!: any;
    page!: Page;
    args!: any;
    requests: any[] = [];
    responses: Response[] = [];
    eventBeacons: any[] = [];
    mutationBeacons: any[] = [];
    sessionSuffix: string = '';
    onResponse;
    onRequest;
    async init() {
        this.sessionSuffix = ' - ' + new Date().getTime();
        const args = [
            '--no-sandbox',
            '--disable-setuid-sandbox',
            '--disable-infobars',
            '--window-position=0,0',
            '--ignore-certifcate-errors',
            '--ignore-certifcate-errors-spki-list',
            '--user-agent="Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3312.0 Safari/537.36"'
        ];
        this.browser = await launch({ headless: this.args.headless === 'true', devtools: true,args:args});
        // const context = await this.browser.createIncognitoBrowserContext();
        this.page = await this.browser.newPage();
        // this.page = await context.newPage();
        
        this.page.on('console', msg => {
            // error('Browser error:', msg);
        });
        this.page.on('error', msg => error('Browser Error:', msg));
        // this.page.on('pageerror', msg => error('Browser Page Error:', msg));
        // this.page.on('requestfailed', msg => error('Browser Page Request Error:', msg));

        //capture_requests(this, this.page);
        //capture_responses(this, this.page);

        // this.args.disableRequests !== 'true' && capture_requests(this, this.page);
        // this.args.disableResponses !== 'true' && capture_requests(this, this.page);
        // capture_responses(this, this.page);
        const page2 = this.page as any;
        //page2.setCacheEnabled(false);
        /*
        await page2._client.send('Security.setOverrideCertificateErrors', { 
            override: true 
        });
        */
        await page2._client.on('Security.certificateError', (event: any) => {
            page2._client.send('Security.handleCertificateError', {
                eventId: event.eventId,
                action: 'continue' // ignore error and continue request
            })
        })
    }
}

export const getScope = (cliArgs?: any) => {
    if (!instance) {
        instance = new Scope();
        instance.args = cliArgs;
    }
    return instance;
}

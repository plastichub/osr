"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.resolveConfig = exports.read_fragments = exports.md_edit_wrap = exports.parse_config = void 0;
const debug = require("../..");
const path = require("path");
const util_1 = require("util");
const lib_1 = require("../../lib/");
const js_beautify_1 = require("js-beautify");
const dir_1 = require("@plastichub/fs/dir");
const strings_1 = require("../common/strings");
const md_tables = require('markdown-table');
const parse_config = (config, root) => {
    if (Object.keys(config)) {
        for (const key in config) {
            let val = config[key];
            if (util_1.isArray(val)) {
                if (key !== 'authors') {
                    config[key] = lib_1.md2html(md_tables(val));
                }
            }
            else if (util_1.isString(val)) {
                if (val.endsWith('.csv')) {
                    const parsed = path.parse(root);
                    let csv = path.resolve(`${parsed.dir}/${parsed.base}/${val}`);
                    debug.info("Parsing CSV " + csv);
                    if (lib_1.exists(csv)) {
                        csv = lib_1.read(csv) || "";
                        try {
                            csv = lib_1.md2html(lib_1.csvToMarkdown(csv));
                            config[key] = csv;
                        }
                        catch (e) {
                            debug.error(`Error converting csv to md ${val}`);
                        }
                    }
                    else {
                        debug.error(`Can't find CSV file at ${csv}`, parsed);
                    }
                }
            }
        }
    }
};
exports.parse_config = parse_config;
const md_edit_wrap = (content, f, prefix = '', context = '') => {
    return js_beautify_1.html_beautify(`<div prefix="${prefix}" file="${path.parse(f).base}" context="${context}" class="fragment">${content}</div>`);
};
exports.md_edit_wrap = md_edit_wrap;
const read_fragments = (src, config, prefix = '', context = '') => {
    if (!lib_1.exists(src)) {
        debug.warn(`Create template folder ${src}`);
        dir_1.sync(src);
    }
    let fragments = lib_1.files(src, '*.html');
    fragments.map((f) => {
        config[path.parse(f).name] = exports.md_edit_wrap(lib_1.toHTML(f, true), f, prefix, context);
    });
    fragments = lib_1.files(src, '*.md');
    fragments.map((f) => {
        config[path.parse(f).name] = exports.md_edit_wrap(lib_1.toHTML(f, false), f, prefix, context);
    });
    return config;
};
exports.read_fragments = read_fragments;
const _resolve = (config) => {
    for (const key in config) {
        if (config[key] && typeof config[key] == 'string') {
            const resolved = strings_1.substitute(config[key], config);
            config[key] = resolved;
        }
    }
    return config;
};
const resolveConfig = (config) => {
    config = _resolve(config);
    config = _resolve(config);
    return config;
};
exports.resolveConfig = resolveConfig;
//# sourceMappingURL=md.js.map
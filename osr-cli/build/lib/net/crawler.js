"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.crawler = void 0;
const debug = require("../../log");
const path = require("path");
const write_1 = require("@plastichub/fs/write");
const dir_1 = require("@plastichub/fs/dir");
const exists_1 = require("@plastichub/fs/exists");
const slugify_1 = require("slugify");
const download = require("download");
var sanitize = require("sanitize-filename");
const normalizeUrl = require('normalize-url');
const topicFolder = (title, cli, prefix) => {
    const dst = path.resolve('' + cli.output || './' + '/topics/');
    if (!exists_1.sync(dst)) {
        dir_1.sync(dst);
    }
    const folder = path.resolve(dst + '/' + prefix + '/' + sanitize(slugify_1.default(title)));
    if (!exists_1.sync(folder)) {
        dir_1.sync(folder);
    }
    return folder;
};
function downloadFiles(dst, urls) {
    return __awaiter(this, void 0, void 0, function* () {
        yield Promise.all(urls.map((i) => {
            if (!i || !i.substring) {
                return false;
            }
            const f = i.substring(i.lastIndexOf('/') + 1);
            const image = path.resolve(`${dst}/${f}`);
            if (!exists_1.sync(image)) {
                try {
                    console.log(`download file ` + normalizeUrl(i));
                    return download(normalizeUrl(i), dst);
                }
                catch (e) {
                    debug.error('error download step image', e);
                }
            }
        }));
    });
}
const Apify = require('apify');
function crawler(url = 'https://davehakkens.nl/community/forums/forum/precious-plastic/general/', cli, prefix) {
    return __awaiter(this, void 0, void 0, function* () {
        // Apify.openRequestQueue() is a factory to get a preconfigured RequestQueue instance.
        // We add our first request to it - the initial page the crawler will visit.
        const requestQueue = yield Apify.openRequestQueue();
        yield requestQueue.addRequest({ url: url });
        // Create an instance of the PuppeteerCrawler class - a crawler
        // that automatically loads the URLs in headless Chrome / Puppeteer.
        const crawler = new Apify.PuppeteerCrawler({
            requestQueue,
            // Here you can set options that are passed to the Apify.launchPuppeteer() function.
            launchPuppeteerOptions: {
                // For example, by adding "slowMo" you'll slow down Puppeteer operations to simplify debugging
                //slowMo: 500,
                headless: true,
                devtools: false
            },
            // Stop crawling after several pages
            maxRequestsPerCrawl: 50000,
            maxConcurrency: 5,
            // This function will be called for each URL to crawl.
            // Here you can write the Puppeteer scripts you are familiar with,
            // with the exception that browsers and pages are automatically managed by the Apify SDK.
            // The function accepts a single parameter, which is an object with the following fields:
            // - request: an instance of the Request class with information such as URL and HTTP method
            // - page: Puppeteer's Page object (see https://pptr.dev/#show=api-class-page)
            handlePageTimeoutSecs: 300,
            gotoTimeoutSecs: 300,
            handlePageFunction: ({ request, page }) => __awaiter(this, void 0, void 0, function* () {
                console.log(`Processing ${request.url}...`);
                let isIndex = request.url.indexOf('forums/forum') !== -1;
                // A function to be evaluated by Puppeteer within the browser context.
                const pageFunctionTopic = () => {
                    let nextPages = [];
                    let data = {};
                    const $ = window['jQuery'];
                    const jQuery = $;
                    let title;
                    let authorLink;
                    let postDate;
                    let postBody;
                    let authorName;
                    try {
                        title = $('#bbpress-forums > div.topic-lead > div.author > h1')[0].innerText;
                    }
                    catch (e) {
                        debugger;
                    }
                    try {
                        authorLink = $('#bbpress-forums > div.topic-lead > div.author > a:nth-child(5)').attr('href');
                        authorName = $('#bbpress-forums > div.topic-lead > div.author > a:nth-child(5)')[0].innerText;
                    }
                    catch (e) {
                        authorName = "anon";
                        authorLink = "ban G";
                    }
                    postDate = $('#bbpress-forums > div.topic-lead > div.author > div.date')[0].innerText.split(' at')[0];
                    postBody = $('#bbpress-forums > div.topic-lead > div.content').html();
                    const likes = parseInt(jQuery('#bbpress-forums > div.topic-lead > div.actions > div > div.dav_topic_like')[0].innerText.split(' ')[0]);
                    const saved = parseInt(jQuery('#bbpress-forums > div.topic-lead > div.actions > div > div.dav_topic_favorit > span')[0].innerText.split(' ')[0]);
                    const nbReplies = parseInt(jQuery('#bbpress-forums > div.topic-lead > div.actions > div > div.dav_reply_topic > span')[0].innerText.split(' ')[0]);
                    const pics = [];
                    jQuery('.d4p-bbp-attachment > a').each((i, a) => {
                        pics.push(jQuery(a).attr('href').replace('?ssl=1', ''));
                    });
                    const replies = [];
                    jQuery('#bbpress-forums > div.list-replies-container > div.list-replies > div.topic-reply').each((i, e) => {
                        try {
                            let replyDate;
                            let nbLikes;
                            let avatar = jQuery('.author > a > img', e).attr('src').replace(' 2x', '');
                            let user = jQuery('.content .replyheader .smallusername', e)[0].innerText || 'anonymous';
                            replyDate = jQuery('.content .replyheader .reply-date', e)[0].innerText;
                            nbLikes = parseInt(jQuery('.content > div.wpulike.wpulike-heart > div > span', e)[0].innerText) || 0;
                            let authorUrl = jQuery('div.content > div.replyheader > div.smallusername > a', e).attr('href');
                            jQuery('.content > div.wpulike.wpulike-heart', e).remove();
                            jQuery('.content .replyheader', e).remove();
                            let replyBody = jQuery('.content', e).html();
                            const replyPics = [];
                            jQuery('.d4p-bbp-attachment > a', e).each((i, a) => {
                                replyPics.push(jQuery(a).attr('href').replace('?ssl=1', ''));
                            });
                            jQuery('.content .bbp-attachments', e).remove();
                            replyBody = jQuery('.content', e).html();
                            replies.push({
                                authorUrl,
                                avatar,
                                user,
                                replyDate,
                                nbLikes,
                                replyBody,
                                replyPics
                            });
                        }
                        catch (e) {
                            console.error('meah', e);
                            debugger;
                        }
                    });
                    let pagination = jQuery('#bbpress-forums > div.list-replies-container > div.bbp-pagination > div.bbp-pagination-links');
                    if (pagination && pagination.children()) {
                        let c = pagination.children();
                        for (let i = 0; i < c.length; i++) {
                            let b = $(c[i]);
                            if (b.attr("href")) {
                                nextPages.push(encodeURI(b.attr("href")));
                            }
                        }
                    }
                    const authorImage = jQuery("#bbpress-forums > div.topic-lead > div.author > a.bbp-author-avatar > img").attr("src");
                    data = {
                        url: window.location.href,
                        authorImage: authorImage,
                        // next pages to crawl, ('Set' removes duplicates)
                        nextPages: [...new Set(nextPages)],
                        body: postBody,
                        postDate,
                        likes,
                        saved,
                        nbReplies,
                        pics,
                        replies,
                        authorLink,
                        authorName,
                        title,
                        content: jQuery('body').html()
                    };
                    return data;
                };
                const pageFunctionIndex = () => {
                    let data = {};
                    const $ = window['jQuery'];
                    const jQuery = $;
                    let nextPages = [];
                    jQuery('.bbp-topic-permalink').each((i, e) => {
                        nextPages.push(jQuery(e).attr("href"));
                    });
                    let pagination = jQuery('.bbp-pagination-links');
                    if (pagination && pagination.children()) {
                        let c = pagination.children();
                        for (let i = 0; i < c.length; i++) {
                            let b = $(c[i]);
                            if (b.attr("href") && b.attr("href").length) {
                                nextPages.push(b.attr("href"));
                            }
                        }
                    }
                    data = {
                        nextPages: [...new Set(nextPages)]
                    };
                    return data;
                };
                if (!isIndex) {
                    const data = yield page.$$eval('return window', pageFunctionTopic);
                    console.log('Process Topic : ', data.title);
                    if (data.nextPages && data.nextPages.length) {
                        let q = [];
                        data.nextPages.forEach(element => {
                            if (element !== url) {
                                q.push(requestQueue.addRequest({ url: element }));
                            }
                        });
                        yield Promise.all(q);
                    }
                    const dst = path.resolve('' + cli.output || './' + '/topics/');
                    if (!exists_1.sync(dst)) {
                        dir_1.sync(dst);
                    }
                    const folder = topicFolder(data.title, cli, prefix);
                    let fileName = 'data';
                    if (request.url.split('page').length == 2) {
                        fileName = fileName + "-" + parseInt(request.url.split('page')[1].replace('/', ''));
                    }
                    fileName = fileName + ".json";
                    let dataPath = path.resolve(`${folder}/${fileName}`);
                    debug.info("write to ", dataPath);
                    const content = data.content;
                    data.content = null;
                    write_1.sync(dataPath, data, { jsonIndent: 1 });
                    write_1.sync(dataPath.replace('.json', '.html'), content);
                    yield downloadFiles(topicFolder(data.title, cli, prefix), data.pics);
                    let pics = [];
                    data.replies.forEach(r => {
                        if (r.replyPics.length) {
                            pics = pics.concat(r.replyPics);
                        }
                    });
                    yield downloadFiles(topicFolder(data.title, cli, prefix), pics);
                    if (data.authorImage) {
                        yield downloadFiles(path.resolve(cli.output + '/authors'), [data.authorImage]);
                        if (data.replies) {
                            yield Promise.all(data.replies.map((i) => downloadFiles(path.resolve(cli.output + '/authors'), [i.authorLogo])));
                        }
                    }
                }
                else {
                    const data = yield page.$$eval('return window', pageFunctionIndex);
                    if (data.nextPages && data.nextPages.length) {
                        let q = [];
                        data.nextPages.forEach(element => {
                            if (element !== url) {
                                q.push(requestQueue.addRequest({ url: element }));
                            }
                        });
                        yield Promise.all(q);
                    }
                    console.log('got forum index : ', data);
                }
            }),
            // This function is called if the page processing failed more than maxRequestRetries+1 times.
            handleFailedRequestFunction: ({ request }) => __awaiter(this, void 0, void 0, function* () {
                console.log(`Request ${request.url} failed too many times`);
                yield Apify.pushData({
                    '#debug': Apify.utils.createRequestDebugInfo(request),
                });
            }),
            gotoFunction: ({ page, request }) => __awaiter(this, void 0, void 0, function* () {
                return page.goto(request.url, {
                    waitUntil: 'domcontentloaded'
                });
            })
        });
        // Run the crawler and wait for it to finish.
        yield crawler.run();
    });
}
exports.crawler = crawler;
;
//# sourceMappingURL=crawler.js.map
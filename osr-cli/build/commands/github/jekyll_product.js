"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.register = void 0;
const debug = require("../..");
const path = require("path");
const YAML = require('json-to-pretty-yaml');
const lib_1 = require("../../lib/");
const defaultOptions = (yargs) => {
    return yargs.option('product', {
        default: 'elena',
        describe: 'name of the product which matches the folder name inside the products folder'
    }).option('products', {
        default: './',
        describe: 'location of the products folder which contains the \'bazar\' and \'products\' folders'
    }).option('format', {
        default: 'html',
        describe: 'selects the output format, can be \'html\' or \'md\''
    }).option('debug', {
        default: 'true',
        describe: 'Enable internal debug message'
    });
};
let options = (yargs) => defaultOptions(yargs);
// npm run build ; node ./build/main.js machine-jekyll --debug=true --products=../../products --product=elena
const register = (cli) => {
    return cli.command('product-jekyll', 'Creates Jekyll markdown page', options, (argv) => __awaiter(void 0, void 0, void 0, function* () {
        if (argv.help) {
            return;
        }
        const markdown = true;
        const isDebug = argv.debug === 'true';
        const product_rel_path = argv.product;
        const product_rel_path_name = `${path.parse(product_rel_path).dir}/${path.parse(product_rel_path).name}/`;
        debug.info('rel', path.parse(product_rel_path));
        const root = path.resolve(`${argv.products}`);
        // global config
        const cPath = argv.products ? path.resolve(`${argv.products}/templates/site/config.json`) : path.resolve('./config.json');
        isDebug && debug.info(`read config at ${cPath}`);
        const config = lib_1.read(cPath, 'json');
        // jekyll machine pages root
        const machines_directory = path.resolve(`${argv.products}/_products/`);
        const templates_path = path.resolve(`${argv.products}/templates/site`);
        if (!lib_1.exists(templates_path)) {
            debug.error(`\t Cant find templates at ${templates_path}, path doesn't exists`);
            return;
        }
        const shared_templates_path = path.resolve(`${argv.product}/templates/shared`);
        const template_path = path.resolve(`${templates_path}/machine.md`);
        // machine directory
        const machine_path = path.resolve(`${argv.products || config.products_path}/${argv.product}`);
        const fragments_path = path.resolve(`${templates_path}`);
        debug.info(fragments_path);
        isDebug && debug.info(`\n Generate machine description for ${argv.product}, reading from ${machine_path},
            using fragments at ${fragments_path}`);
        if (!lib_1.exists(machine_path)) {
            debug.error(`\t Cant find machine at ${machine_path}, path doesn't exists`);
            return;
        }
        let fragments = Object.assign({}, config);
        // read all global fragments
        isDebug && debug.info(`Read global fragments at ${fragments_path}`);
        lib_1.read_fragments(shared_templates_path, fragments, '/templates/shared/', "machine-shared");
        lib_1.read_fragments(fragments_path, fragments, '/templates/site/', "machine global");
        // read all product specific fragments
        const product_fragments_path = path.resolve(`${machine_path}/templates/site`);
        if (!lib_1.exists(product_fragments_path)) {
            debug.error(`Machine has no template files! Creating folder structure ..`);
            lib_1.dir(product_fragments_path);
        }
        else {
            isDebug && debug.info(`read machine fragments at ${product_fragments_path}`);
        }
        const _git_log = yield lib_1.git_log(root, product_rel_path);
        const change_log_html = lib_1.changelog(_git_log.files);
        const change_log_path = path.resolve(`${machine_path}/templates/site/changelog.html`);
        lib_1.write(change_log_path, change_log_html);
        lib_1.read_fragments(product_fragments_path, fragments, product_rel_path_name, "machine");
        let machine_config;
        // read product variables
        if (!lib_1.exists(path.resolve(`${machine_path}/config.json`))) {
            isDebug && debug.warn(`product has no config.json, please ensure there is a config.json in ${machine_path}`);
            lib_1.write(path.resolve(`${machine_path}/config.json`), '{}');
        }
        else {
            machine_config = lib_1.read(path.resolve(`${machine_path}/config.json`), 'json');
            fragments = Object.assign(Object.assign({}, fragments), machine_config);
            isDebug && debug.info(`Loaded machine variables`);
        }
        fragments['product_rel'] = product_rel_path_name;
        lib_1.parse_config(fragments, machine_path);
        lib_1.resolveConfig(fragments);
        let config_yaml = lib_1.read(path.resolve(`${machine_path}/config.yaml`), 'string') || "";
        let _yaml_extra = {};
        for (const k in machine_config) {
            _yaml_extra[k] = fragments[k];
        }
        _yaml_extra['git_last'] = _git_log.last.date;
        _yaml_extra = YAML.stringify(_yaml_extra);
        config_yaml = _yaml_extra + config_yaml;
        config_yaml = lib_1.substitute(config_yaml, fragments);
        if (!fragments.product) {
            debug.error(`Have no product template! : ${machine_path} - ${templates_path}`);
            return;
        }
        const products_description = lib_1.substitute(fragments.product, fragments);
        let gallery = "";
        if (fragments['gallery'] !== false && lib_1.exists(path.resolve(`${machine_path}/media/gallery`))) {
            gallery = "gallery:";
            let _images = lib_1.images(path.resolve(`${machine_path}/media/gallery`));
            _images = _images.map((f) => {
                let _path = `/${product_rel_path}/media/gallery/${path.parse(f).base}`;
                return `${lib_1.gallery_image(_path)}`;
            }).join("");
            debug.info("Added gallery images:  " + _images.length);
            gallery += _images;
        }
        let gallery_social = "";
        if (fragments['gallery_social'] !== false && lib_1.exists(path.resolve(`${machine_path}/media/social`))) {
            gallery_social = "\ngallery_social:";
            let _images = lib_1.images(path.resolve(`${machine_path}/media/social`));
            _images = _images.map((f) => {
                let _path = `/${product_rel_path}/media/social/${path.parse(f).base}`;
                return `${lib_1.gallery_image(_path)}`;
            }).join("");
            gallery_social += _images;
        }
        let gallery_drawings = "";
        if (fragments['gallery_drawings'] !== false && lib_1.exists(path.resolve(`${machine_path}/drawings`))) {
            gallery_drawings = "\ngallery_drawings:";
            let _images = lib_1.images(path.resolve(`${machine_path}/drawings`));
            debug.info(`Read drawings at ${path.resolve(`${machine_path}/drawings`)} ${_images.length}`);
            _images = _images.map((f) => {
                let _path = `/${product_rel_path}/drawings/${path.parse(f).base}`;
                let _pdf = `/${product_rel_path}/drawings/${path.parse(f).name}.PDF`;
                return `${lib_1.drawing_image(_path, _pdf)}`;
            }).join("");
            gallery_drawings += _images;
        }
        let content = lib_1.product_header(fragments['product_name'], fragments['category'], fragments['product_perspective'] ? fragments['product_perspective'] : `/products/${fragments['slug']}/renderings/perspective.JPG`, fragments['slug'], fragments['product_rel'], config.description || "", config.tagline || "", config_yaml +
            gallery +
            gallery_social +
            gallery_drawings);
        content += products_description;
        let out_path = path.resolve(`${machines_directory}/${fragments['slug']}.md`);
        isDebug && debug.info(`Write jekyll product page ${out_path}`);
        lib_1.write(out_path, content);
    }));
};
exports.register = register;
//# sourceMappingURL=jekyll_product.js.map
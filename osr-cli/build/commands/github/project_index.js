"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.register = void 0;
const debug = require("../..");
const path = require("path");
const lib_1 = require("../../lib/");
const defaultOptions = (yargs) => {
    return yargs.option('project', {
        default: 'elena',
        describe: 'name of the product which matches the folder name inside the projects folder'
    }).option('products', {
        default: './',
        describe: 'location of the products folder which contains the \'bazar\' and \'products\' folders'
    }).option('format', {
        default: 'html',
        describe: 'selects the output format, can be \'html\' or \'md\''
    }).option('debug', {
        default: 'true',
        describe: 'Enable internal debug message'
    });
};
let options = (yargs) => defaultOptions(yargs);
// npm run build ; node ./build/main.js project-jekyll --debug=true --products=../../products --project=elena
const register = (cli) => {
    return cli.command('project-jekyll', 'Creates Jekyll Project Markdown page', options, (argv) => __awaiter(void 0, void 0, void 0, function* () {
        if (argv.help) {
            return;
        }
        const markdown = true;
        const isDebug = argv.debug === 'true';
        const project_rel_path = argv.project;
        const project_rel_path_name = `${path.parse(project_rel_path).dir}/${path.parse(project_rel_path).name}/`;
        debug.info('rel', path.parse(project_rel_path));
        const root = path.resolve(`${argv.products}`);
        // global config
        const cPath = argv.products ? path.resolve(`${argv.products}/templates/jekyll/config.json`) : path.resolve('./config.json');
        isDebug && debug.info(`read config at ${cPath}`);
        const config = lib_1.read(cPath, 'json');
        // jekyll projects pages root
        const projects_directory = path.resolve(`${argv.products}/_projects/`);
        const templates_path = path.resolve(`${argv.products}/templates/jekyll`);
        if (!lib_1.exists(templates_path)) {
            debug.error(`\t Cant find templates at ${templates_path}, path doesn't exists`);
            return;
        }
        const template_path = path.resolve(`${templates_path}/machine.md`);
        // machine directory
        const project_path = path.resolve(`${argv.products}/${argv.project}`);
        const fragments_path = path.resolve(`${templates_path}`);
        debug.info(fragments_path);
        isDebug && debug.info(`\n Generate project description for ${argv.project}, reading from ${project_path},
            using fragments at ${fragments_path}`);
        if (!lib_1.exists(project_path)) {
            debug.error(`\t Cant find machine at ${project_path}, path doesn't exists`);
            return;
        }
        let fragments = Object.assign({}, config);
        // read all global fragments
        isDebug && debug.info(`Read global fragments at ${fragments_path}`);
        lib_1.read_fragments(fragments_path, fragments, '/templates/jekyll/', "project global");
        // read all product specific fragments
        const product_fragments_path = path.resolve(`${project_path}/templates/jekyll`);
        if (!lib_1.exists(product_fragments_path)) {
            debug.error(`Machine has no template files! Creating folder structure ..`);
            lib_1.dir(product_fragments_path);
        }
        else {
            isDebug && debug.info(`read machine fragments at ${product_fragments_path}`);
        }
        // const _git_log = await git_log(root, product_rel_path);
        // debug.info('log', _git_log );
        // const change_log_html = changelog(_git_log);
        //const change_log_path = path.resolve(`${machine_path}/templates/jekyll/changelog.html`);
        //write(change_log_path,change_log_html);
        lib_1.read_fragments(product_fragments_path, fragments, project_rel_path_name, "machine");
        // read product variables
        if (!lib_1.exists(path.resolve(`${project_path}/config.json`))) {
            isDebug && debug.warn(`product has no config.json, please ensure there is a config.json in ${project_path}`);
            lib_1.write(path.resolve(`${project_path}/config.json`), '{}');
        }
        else {
            fragments = Object.assign(Object.assign({}, fragments), lib_1.read(path.resolve(`${project_path}/config.json`), 'json'));
            isDebug && debug.info(`Loaded machine variables`);
        }
        fragments['product_rel'] = project_rel_path_name;
        lib_1.parse_config(fragments, project_path);
        // compile and write out
        for (const key in fragments) {
            const resolved = lib_1.substitute(fragments[key], fragments);
            fragments[key] = resolved;
        }
        for (const key in fragments) {
            const resolved = lib_1.substitute(fragments[key], fragments);
            fragments[key] = resolved;
        }
        let config_yaml = lib_1.read(path.resolve(`${project_path}/config.yaml`), 'string') || "";
        fragments['slug'] = lib_1.lastOf(project_rel_path.split('/'));
        fragments['product-id'] = fragments['slug'];
        config_yaml = lib_1.substitute(config_yaml, fragments);
        if (!fragments.project) {
            debug.error(`Have no projects template! : ${project_path} - ${templates_path}`);
            return;
        }
        const project_description = lib_1.substitute(fragments.project, fragments);
        let gallery = "";
        if (fragments['gallery'] !== false && lib_1.exists(path.resolve(`${project_path}/media`))) {
            gallery = "gallery:";
            let _images = lib_1.images(path.resolve(`${project_path}/media`));
            _images = _images.map((f) => {
                let _path = `/${project_rel_path}/media/${path.parse(f).base}`;
                return `${lib_1.gallery_image(_path)}`;
            }).join("");
            gallery += _images;
        }
        let gallery_social = "";
        if (fragments['gallery_social'] !== false && lib_1.exists(path.resolve(`${project_path}/media/social`))) {
            gallery_social = "\ngallery_social:";
            let _images = lib_1.images(path.resolve(`${project_path}/media/social`));
            _images = _images.map((f) => {
                let _path = `/${project_rel_path}/media/social/${path.parse(f).base}`;
                return `${lib_1.gallery_image(_path)}`;
            }).join("");
            gallery_social += _images;
        }
        let gallery_drawings = "";
        if (fragments['gallery_drawings'] !== false && lib_1.exists(path.resolve(`${project_path}/drawings`))) {
            gallery_drawings = "\ngallery_drawings:";
            let _images = lib_1.images(path.resolve(`${project_path}/drawings`));
            debug.info(`Read drawings at ${path.resolve(`${project_path}/drawings`)} ${_images.length}`);
            _images = _images.map((f) => {
                let _path = `/${project_rel_path}/drawings/${path.parse(f).base}`;
                let _pdf = `/${project_rel_path}/drawings/${path.parse(f).name}.PDF`;
                return `${lib_1.drawing_image(_path, _pdf)}`;
            }).join("");
            gallery_drawings += _images;
        }
        let content = lib_1.projects_header(fragments['product_name'], fragments['category'], fragments['product_perspective'] ? fragments['product_perspective'] : `/projects/${fragments['slug']}/renderings/perspective.JPG`, fragments['slug'], fragments['product_rel'], config.description || "", config.tagline || "", config_yaml +
            gallery +
            gallery_social +
            gallery_drawings);
        content += project_description;
        let out_path = path.resolve(`${projects_directory}/${fragments['slug']}.md`);
        isDebug && debug.info(`Write jekyll projects page ${out_path}`);
        lib_1.write(out_path, content);
    }));
};
exports.register = register;
//# sourceMappingURL=project_index.js.map
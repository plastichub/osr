"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.register = exports.exportToJson = void 0;
const argv_1 = require("../argv");
const Scope_1 = require("../renderer/puppeteer/Scope");
const __1 = require("../");
// no extra options, using defaults
const options = (yargs) => argv_1.defaultOptions(yargs);
const path = require("path");
'node ./build/main.js onearmy --output=../../onearmy-raw';
let scope;
function exportToJson(idbDatabase) {
    // var t = window.indexedDB.open('OneArmyCache');
    // exportToJson(t.result).then((e)=>{console.log});
    /*
    var r = indexedDB.open('OneArmyCache');
    r.onsuccess = function(event) {
    var db = event.target.result;
    exportToJson(db).then((e)=>{window.ht = e});
    };
    */
    return new Promise((resolve, reject) => {
        const exportObject = {};
        debugger;
        if (idbDatabase.objectStoreNames.length === 0) {
            resolve(JSON.stringify(exportObject));
        }
        else {
            const transaction = idbDatabase.transaction(idbDatabase.objectStoreNames, 'readonly');
            transaction.addEventListener('error', reject);
            for (const storeName of idbDatabase.objectStoreNames) {
                const allObjects = [];
                transaction
                    .objectStore(storeName)
                    .openCursor()
                    .addEventListener('success', event => {
                    const cursor = event.target.result;
                    if (cursor) {
                        // Cursor holds value, put it into store data
                        allObjects.push(cursor.value);
                        cursor.continue();
                    }
                    else {
                        // No more values, store is done
                        exportObject[storeName] = allObjects;
                        // Last store was handled
                        if (idbDatabase.objectStoreNames.length ===
                            Object.keys(exportObject).length) {
                            resolve(JSON.stringify(exportObject));
                        }
                    }
                });
            }
        }
    });
}
exports.exportToJson = exportToJson;
function onResponse(response, scope) {
    return __awaiter(this, void 0, void 0, function* () {
        const url = response.url();
        if (url.includes('firestore')) {
            const data = yield response.text();
            if (data.includes('document')) {
                __1.info('got firestore response : ' + url);
                // var t = window.indexedDB.open('OneArmyCache');
                // exportToJson(t.result).then((e)=>{console.log});
                // write(scope.args.dst, data);
                yield scope.page.evaluate(() => {
                    console.log('something');
                    const request = window.indexedDB.open('OneArmyCache', 1);
                    request.onerror = e => {
                        console.error('Failure ' + e, e);
                    };
                    request.onsuccess = () => {
                        var db = request.result;
                        console.log('Success', db);
                    };
                    console.log();
                });
            }
        }
    });
}
const register = (cli) => {
    return cli.command('HowtoBackup', '', options, (argv) => __awaiter(void 0, void 0, void 0, function* () {
        //@TODO: this guard might not be necessary
        if (argv.help) {
            return;
        }
        const url = "https://community.preciousplastic.com/how-to/hands-free-door-opener";
        const options = {
            headless: false,
            url: url,
            dst: path.resolve(__1.default_path_howto(path.resolve(argv.out), url))
        };
        scope = Scope_1.getScope(options);
        yield scope.init();
        scope.onResponse = onResponse;
        yield scope.page.goto(options.url, {
            timeout: 600000,
            waitUntil: 'networkidle0'
        }).then(() => {
            console.log('ready');
        });
        /*
        const url = "https://community.preciousplastic.com/how-to/hands-free-door-opener";

        const browser = await launch({
            headless: options.headless,
            devtools: true
        });
        const page = await browser.newPage();
        
        
        
        page.on('console', msg => {
            const text = msg.text();
            // inspect('Console Message:', msg.text());
            if (text.includes('[v3_events] docs retrieved')) {

                page.content().then((html) => {
                    console.log('ready!');
                    debug(`Write HTML of ${url} to ${dst}`);
                    write(dst, html);
                    Crawler.end(page);
                })
            }
        });

        page.goto(url, {
            timeout: 600000,
            waitUntil: 'networkidle0'
        }).then((v) => {
            console.log('ready');
        });
        */
        /*
        const args = sanitize(argv) as Options;
        const result = await Puppeteer.crawler(args.url, args);
        output(result, args);*/
    }));
};
exports.register = register;
//# sourceMappingURL=howto.js.map
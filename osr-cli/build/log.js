"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.spinner = exports.inspect = exports.stack = exports.debug = exports.warn = exports.error = exports.info = exports.log = void 0;
const chalk_1 = require("chalk");
const _ora = require("ora");
// tslint:disable-next-line:no-var-requires
const jsome = require('jsome');
jsome.level.show = true;
const glog = console.log;
const log = (msg, ...rest) => glog(chalk_1.default.magenta(msg), ...rest);
exports.log = log;
const info = (msg, ...rest) => glog(chalk_1.default.green(msg), ...rest);
exports.info = info;
const error = (msg, ...rest) => glog(chalk_1.default.red(msg), ...rest);
exports.error = error;
const warn = (msg, ...rest) => glog(chalk_1.default.yellow(msg), ...rest);
exports.warn = warn;
const debug = (msg, ...rest) => glog(chalk_1.default.blue(msg), ...rest);
exports.debug = debug;
const stack = (msg, ...rest) => glog(chalk_1.default.red(msg), new Error().stack);
exports.stack = stack;
const inspect = (msg, d = null) => {
    glog(chalk_1.default.blue(msg));
    d && jsome(d);
};
exports.inspect = inspect;
const spinner = (msg) => _ora(msg);
exports.spinner = spinner;
//# sourceMappingURL=log.js.map
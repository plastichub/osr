"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.defaultMediumMutations = exports.defaultHeavyMutations = exports.defaultMutationTag = exports.defaultMutationRoot = exports.responseRetryTime = exports.maxSessionWaitingTime = exports.sessionName = exports.replay_api_overview = exports.defaultPageOptions = exports.loginUrlDemoDev = exports.loginUrl = exports.userSessionsTab = exports.defaultTenant = void 0;
exports.defaultTenant = 1;
const userSessionsTab = (base) => `${base}/e/${exports.defaultTenant}/#usersearchoverview;gtf=l_2_HOURS`;
exports.userSessionsTab = userSessionsTab;
const loginUrl = (base) => `${base}/login`;
exports.loginUrl = loginUrl;
const loginUrlDemoDev = () => `https://proxy-dev.dynatracelabs.com/sso/ProxyLocator.jsp`;
exports.loginUrlDemoDev = loginUrlDemoDev;
const defaultPageOptions = () => {
    return {
        timeout: 5000,
        waitUntil: 'networkidle2'
    };
};
exports.defaultPageOptions = defaultPageOptions;
exports.replay_api_overview = 'uemshareddetails/rumoverviewdata/usersearchoverview';
const ts = () => {
    const d = new Date();
    return d.getHours() + '_' + d.getMinutes() + '_' + d.getSeconds();
};
const sessionName = (url) => `Pupeteer :${ts()}`;
exports.sessionName = sessionName;
exports.maxSessionWaitingTime = 1000 * 60 * 3;
exports.responseRetryTime = 1000 * 8;
exports.defaultMutationRoot = '#mutationsRoot';
exports.defaultMutationTag = 'div';
exports.defaultHeavyMutations = 2000;
exports.defaultMediumMutations = 500;
//# sourceMappingURL=constants.js.map
"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getScope = exports.Scope = exports.default_trace_path = exports.default_session_events_path = exports.default_path = exports.TRACE_SUFFIX = exports.SESSION_EVENTS_SUFFIX = exports.STATS_SUFFIX = void 0;
const puppeteer_1 = require("puppeteer");
const log_1 = require("../../log");
let instance;
const path = require("path");
const url_1 = require("url");
exports.STATS_SUFFIX = '_stats.json';
exports.SESSION_EVENTS_SUFFIX = '_session.json';
exports.TRACE_SUFFIX = '_trace.json';
const included_categories = ['devtools.timeline'];
const _url_short = (url) => new url_1.URL(url).hostname;
const _date_suffix = () => new Date().toLocaleTimeString().replace(/:/g, '_');
const _random_suffix = () => Math.random() * 100;
const _default_filename = (url) => `${_url_short(url)}_${_random_suffix()}`;
const default_path = (cwd, url) => `${path.join(cwd, _default_filename(url))}${exports.STATS_SUFFIX}`;
exports.default_path = default_path;
const default_session_events_path = (cwd, url) => `${path.join(cwd || process.cwd(), 'sessions', _default_filename(url))}${exports.SESSION_EVENTS_SUFFIX}`;
exports.default_session_events_path = default_session_events_path;
const default_trace_path = (cwd, url) => `${path.join(cwd, _default_filename(url))}${exports.TRACE_SUFFIX}`;
exports.default_trace_path = default_trace_path;
class Scope {
    constructor() {
        this.requests = [];
        this.responses = [];
        this.eventBeacons = [];
        this.mutationBeacons = [];
        this.sessionSuffix = '';
    }
    init() {
        return __awaiter(this, void 0, void 0, function* () {
            this.sessionSuffix = ' - ' + new Date().getTime();
            const args = [
                '--no-sandbox',
                '--disable-setuid-sandbox',
                '--disable-infobars',
                '--window-position=0,0',
                '--ignore-certifcate-errors',
                '--ignore-certifcate-errors-spki-list',
                '--user-agent="Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3312.0 Safari/537.36"'
            ];
            this.browser = yield puppeteer_1.launch({ headless: this.args.headless === 'true', devtools: true, args: args });
            // const context = await this.browser.createIncognitoBrowserContext();
            this.page = yield this.browser.newPage();
            // this.page = await context.newPage();
            this.page.on('console', msg => {
                // error('Browser error:', msg);
            });
            this.page.on('error', msg => log_1.error('Browser Error:', msg));
            // this.page.on('pageerror', msg => error('Browser Page Error:', msg));
            // this.page.on('requestfailed', msg => error('Browser Page Request Error:', msg));
            //capture_requests(this, this.page);
            //capture_responses(this, this.page);
            // this.args.disableRequests !== 'true' && capture_requests(this, this.page);
            // this.args.disableResponses !== 'true' && capture_requests(this, this.page);
            // capture_responses(this, this.page);
            const page2 = this.page;
            //page2.setCacheEnabled(false);
            /*
            await page2._client.send('Security.setOverrideCertificateErrors', {
                override: true
            });
            */
            yield page2._client.on('Security.certificateError', (event) => {
                page2._client.send('Security.handleCertificateError', {
                    eventId: event.eventId,
                    action: 'continue' // ignore error and continue request
                });
            });
        });
    }
}
exports.Scope = Scope;
const getScope = (cliArgs) => {
    if (!instance) {
        instance = new Scope();
        instance.args = cliArgs;
    }
    return instance;
};
exports.getScope = getScope;
//# sourceMappingURL=Scope.js.map
# Precious Plastic Command Line Tools

## Requirments

- Please install [NodeJS](https://nodejs.org/en/download/)
- Please install [GIT](https://git-scm.com/downloads)

## Installation

1. git clone "https://gitlab.com/plastichub/osr/plastichub-forum.git"

2. Inside 'plastichub-forum'  folder, run

```
cd tools

npm install

npm build

```

## Usage

run inside plastichub-forum/tools

1. Download forum

```
node ./build/main.js forum --type="download" --output="../forum"
```

Notice:

- you have to do that for each forum section. This can be changed in lang\cli\src\commands\forumDownloader.ts :: Line 272, set the index from 0 to 4

- If you want to change the script which is being excecuted in the browser to collect the data or debug, change launchPuppeteerOptions in lang\cli\src\lib\net\crawler.ts Line 62, headless:false ! Also you want to limit the number of parallel tasks, set to 1, maxConcurrency:1 !

This will open the Chromium instance instead of running in background (headless) and now you can place a debugger statement within the pagefunction for the crawler (lang\cli\src\lib\net\crawler.ts::handlePageFunction). From there you can play around. Best is using jQuery for collect data.

1. Index forum

```
node ./build/main.js forum --type="index" --output="../forum"
```

This will process the downloaded forum and creates some extra files.

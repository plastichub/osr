# OSR Firmware

General purpose & Arduino based firmware for various machines

- [ ] move commons and staging/version toolchain to here

### Addons

- [x] External servo driver (Pulser)

### Components

- [ ] Analog load cell
- [ ] VOC reader

### VFD - Extension

- [ ] Hold (injection)

### Trips

- [ ] VOC
- [ ] Flood
- [x] Servo fatal
- [x] PID fatal

### HMI

- [ ] Modbus Generics
- [ ] PID essentials

### References

- [ABB Robot Studio Station Files](https://gitlab.com/plastichub/osr/abb-robotstudio/)
- [C++/C Defer Implementation (Long/ShortJmp)](https://gustedt.gitlabpages.inria.fr/defer/)|[GitLab - BSD](https://gitlab.inria.fr/gustedt/defer)

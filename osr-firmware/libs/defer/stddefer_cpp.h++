#include <exception>

namespace std {

class defer_exception : std::exception {
  static constexpr int buflen = 128;
  char buf[buflen];
public:
  int const code;
  defer_exception(int err = 0);
  virtual const char* what() const noexcept {
    return buf;
  }
};

class defer_boundary {
  int const prev = _Defer_language(_Defer_language_Cpp);
public:
  void panic();
};

}

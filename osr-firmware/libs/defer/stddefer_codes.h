/* -*- C -*-  */

/*
 * The error codes for stddefer.h
 */

/*
 * BEWARE: the real source file is the file ending in .hs, it is
 * processed by shnell to produce the .h file. So don't modify the .h
 * file.
 */

#ifndef _STDDEFER_CODES_H
#define _STDDEFER_CODES_H 1

#ifdef __cplusplus
#include <cerrno>
#include <climits>
#include <csignal>
#include <atomic>
using std::atomic_int;
using std::atomic_short;
using std::sig_atomic_t;
#else
#include <errno.h>
#include <limits.h>
#include <signal.h>
#include <stdatomic.h>
// We still need this for static_assert
#include <assert.h>
#endif

// The unwind state shares a number range with signal numbers such
// that we can have them in the same member. This requires some
// acrobacy such that we do not have conflicts for the values.
#if ATOMIC_INT_LOCK_FREE > 1

typedef atomic_int _Defer_int;
enum { _DEFER_MAX = INT_MAX, };

#elif ATOMIC_SHORT_LOCK_FREE > 1

typedef atomic_short _Defer_int;
enum { _DEFER_MAX = SHRT_MAX, };

#else

typedef sig_atomic_t _Defer_int;
enum { _DEFER_MAX = SIG_ATOMIC_MAX, };

#endif


// Then we will also return `errno` numbers and signal numbers in the
// same integer, so here also we have to be sure not to map different
// events to the same number.
#define DEFER_SIGNO(X) (_DEFER_MAX-((int)+(X)))


/* Lists of errno and signal numbers. All of these are checked with
   `#ifdef` later on, so it should be ok to add a few more here that
   might appear on some platforms. */


enum _Defer_codes {
    _Defer_Lev = _DEFER_MAX/2 - 1,
    /** This is supposed to be larger than any signal number. **/
    /** An inner guarded block **/
    _DEFER_NONE = _Defer_Lev - 0,
    _DEFER_EXIT = _Defer_Lev - 1,
    _DEFER_FUNC = _Defer_Lev - 2,
    _DEFER_BLCK = _Defer_Lev - 3,

    /** Signal numbers **/
#ifdef SIGABRT
    DEFER_ABRT = DEFER_SIGNO(SIGABRT),
#endif
#ifdef SIGALRM
    DEFER_ALRM = DEFER_SIGNO(SIGALRM),
#endif
#ifdef SIGBUS
    DEFER_BUS = DEFER_SIGNO(SIGBUS),
#endif
#ifdef SIGCHLD
    DEFER_CHLD = DEFER_SIGNO(SIGCHLD),
#endif
#ifdef SIGCLD
    DEFER_CLD = DEFER_SIGNO(SIGCLD),
#endif
#ifdef SIGCONT
    DEFER_CONT = DEFER_SIGNO(SIGCONT),
#endif
#ifdef SIGEMT
    DEFER_EMT = DEFER_SIGNO(SIGEMT),
#endif
#ifdef SIGFPE
    DEFER_FPE = DEFER_SIGNO(SIGFPE),
#endif
#ifdef SIGHUP
    DEFER_HUP = DEFER_SIGNO(SIGHUP),
#endif
#ifdef SIGILL
    DEFER_ILL = DEFER_SIGNO(SIGILL),
#endif
#ifdef SIGINFO
    DEFER_INFO = DEFER_SIGNO(SIGINFO),
#endif
#ifdef SIGINT
    DEFER_INT = DEFER_SIGNO(SIGINT),
#endif
#ifdef SIGIO
    DEFER_IO = DEFER_SIGNO(SIGIO),
#endif
#ifdef SIGIOT
    DEFER_IOT = DEFER_SIGNO(SIGIOT),
#endif
#ifdef SIGKILL
    DEFER_KILL = DEFER_SIGNO(SIGKILL),
#endif
#ifdef SIGLOST
    DEFER_LOST = DEFER_SIGNO(SIGLOST),
#endif
#ifdef SIGPIPE
    DEFER_PIPE = DEFER_SIGNO(SIGPIPE),
#endif
#ifdef SIGPOLL
    DEFER_POLL = DEFER_SIGNO(SIGPOLL),
#endif
#ifdef SIGPROF
    DEFER_PROF = DEFER_SIGNO(SIGPROF),
#endif
#ifdef SIGPWR
    DEFER_PWR = DEFER_SIGNO(SIGPWR),
#endif
#ifdef SIGQUIT
    DEFER_QUIT = DEFER_SIGNO(SIGQUIT),
#endif
#ifdef SIGSEGV
    DEFER_SEGV = DEFER_SIGNO(SIGSEGV),
#endif
#ifdef SIGSTKFLT
    DEFER_STKFLT = DEFER_SIGNO(SIGSTKFLT),
#endif
#ifdef SIGSTOP
    DEFER_STOP = DEFER_SIGNO(SIGSTOP),
#endif
#ifdef SIGSYS
    DEFER_SYS = DEFER_SIGNO(SIGSYS),
#endif
#ifdef SIGTERM
    DEFER_TERM = DEFER_SIGNO(SIGTERM),
#endif
#ifdef SIGTRAP
    DEFER_TRAP = DEFER_SIGNO(SIGTRAP),
#endif
#ifdef SIGTSTP
    DEFER_TSTP = DEFER_SIGNO(SIGTSTP),
#endif
#ifdef SIGTTIN
    DEFER_TTIN = DEFER_SIGNO(SIGTTIN),
#endif
#ifdef SIGTTOU
    DEFER_TTOU = DEFER_SIGNO(SIGTTOU),
#endif
#ifdef SIGUNUSED
    DEFER_UNUSED = DEFER_SIGNO(SIGUNUSED),
#endif
#ifdef SIGURG
    DEFER_URG = DEFER_SIGNO(SIGURG),
#endif
#ifdef SIGUSR1
    DEFER_USR1 = DEFER_SIGNO(SIGUSR1),
#endif
#ifdef SIGUSR2
    DEFER_USR2 = DEFER_SIGNO(SIGUSR2),
#endif
#ifdef SIGVTALRM
    DEFER_VTALRM = DEFER_SIGNO(SIGVTALRM),
#endif
#ifdef SIGWINCH
    DEFER_WINCH = DEFER_SIGNO(SIGWINCH),
#endif
#ifdef SIGXCPU
    DEFER_XCPU = DEFER_SIGNO(SIGXCPU),
#endif
#ifdef SIGXFSZ
    DEFER_XFSZ = DEFER_SIGNO(SIGXFSZ),
#endif

    _DEFER_EMAX = _Defer_Lev/2,

    /** system error numbers **/

#ifdef E2BIG
    DEFER_E2BIG = E2BIG,
#else
    DEFER_E2BIG = _DEFER_EMAX - 0,
#endif


#ifdef EACCES
    DEFER_EACCES = EACCES,
#else
    DEFER_EACCES = _DEFER_EMAX - 1,
#endif


#ifdef EADDRINUSE
    DEFER_EADDRINUSE = EADDRINUSE,
#else
    DEFER_EADDRINUSE = _DEFER_EMAX - 2,
#endif


#ifdef EADDRNOTAVAIL
    DEFER_EADDRNOTAVAIL = EADDRNOTAVAIL,
#else
    DEFER_EADDRNOTAVAIL = _DEFER_EMAX - 3,
#endif


#ifdef EAFNOSUPPORT
    DEFER_EAFNOSUPPORT = EAFNOSUPPORT,
#else
    DEFER_EAFNOSUPPORT = _DEFER_EMAX - 4,
#endif


#ifdef EAGAIN
    DEFER_EAGAIN = EAGAIN,
#else
    DEFER_EAGAIN = _DEFER_EMAX - 5,
#endif


#ifdef EALREADY
    DEFER_EALREADY = EALREADY,
#else
    DEFER_EALREADY = _DEFER_EMAX - 6,
#endif


#ifdef EBADE
    DEFER_EBADE = EBADE,
#else
    DEFER_EBADE = _DEFER_EMAX - 7,
#endif


#ifdef EBADF
    DEFER_EBADF = EBADF,
#else
    DEFER_EBADF = _DEFER_EMAX - 8,
#endif


#ifdef EBADFD
    DEFER_EBADFD = EBADFD,
#else
    DEFER_EBADFD = _DEFER_EMAX - 9,
#endif


#ifdef EBADMSG
    DEFER_EBADMSG = EBADMSG,
#else
    DEFER_EBADMSG = _DEFER_EMAX - 10,
#endif


#ifdef EBADR
    DEFER_EBADR = EBADR,
#else
    DEFER_EBADR = _DEFER_EMAX - 11,
#endif


#ifdef EBADRQC
    DEFER_EBADRQC = EBADRQC,
#else
    DEFER_EBADRQC = _DEFER_EMAX - 12,
#endif


#ifdef EBADSLT
    DEFER_EBADSLT = EBADSLT,
#else
    DEFER_EBADSLT = _DEFER_EMAX - 13,
#endif


#ifdef EBUSY
    DEFER_EBUSY = EBUSY,
#else
    DEFER_EBUSY = _DEFER_EMAX - 14,
#endif


#ifdef ECANCELED
    DEFER_ECANCELED = ECANCELED,
#else
    DEFER_ECANCELED = _DEFER_EMAX - 15,
#endif


#ifdef ECHILD
    DEFER_ECHILD = ECHILD,
#else
    DEFER_ECHILD = _DEFER_EMAX - 16,
#endif


#ifdef ECHRNG
    DEFER_ECHRNG = ECHRNG,
#else
    DEFER_ECHRNG = _DEFER_EMAX - 17,
#endif


#ifdef ECOMM
    DEFER_ECOMM = ECOMM,
#else
    DEFER_ECOMM = _DEFER_EMAX - 18,
#endif


#ifdef ECONNABORTED
    DEFER_ECONNABORTED = ECONNABORTED,
#else
    DEFER_ECONNABORTED = _DEFER_EMAX - 19,
#endif


#ifdef ECONNREFUSED
    DEFER_ECONNREFUSED = ECONNREFUSED,
#else
    DEFER_ECONNREFUSED = _DEFER_EMAX - 20,
#endif


#ifdef ECONNRESET
    DEFER_ECONNRESET = ECONNRESET,
#else
    DEFER_ECONNRESET = _DEFER_EMAX - 21,
#endif


#ifdef EDEADLK
    DEFER_EDEADLK = EDEADLK,
#else
    DEFER_EDEADLK = _DEFER_EMAX - 22,
#endif


#ifdef EDEADLOCK
    DEFER_EDEADLOCK = EDEADLOCK,
#else
    DEFER_EDEADLOCK = _DEFER_EMAX - 23,
#endif


#ifdef EDESTADDRREQ
    DEFER_EDESTADDRREQ = EDESTADDRREQ,
#else
    DEFER_EDESTADDRREQ = _DEFER_EMAX - 24,
#endif


#ifdef EDOM
    DEFER_EDOM = EDOM,
#else
    DEFER_EDOM = _DEFER_EMAX - 25,
#endif


#ifdef EDQUOT
    DEFER_EDQUOT = EDQUOT,
#else
    DEFER_EDQUOT = _DEFER_EMAX - 26,
#endif


#ifdef EEXIST
    DEFER_EEXIST = EEXIST,
#else
    DEFER_EEXIST = _DEFER_EMAX - 27,
#endif


#ifdef EFAULT
    DEFER_EFAULT = EFAULT,
#else
    DEFER_EFAULT = _DEFER_EMAX - 28,
#endif


#ifdef EFBIG
    DEFER_EFBIG = EFBIG,
#else
    DEFER_EFBIG = _DEFER_EMAX - 29,
#endif


#ifdef EHOSTDOWN
    DEFER_EHOSTDOWN = EHOSTDOWN,
#else
    DEFER_EHOSTDOWN = _DEFER_EMAX - 30,
#endif


#ifdef EHOSTUNREACH
    DEFER_EHOSTUNREACH = EHOSTUNREACH,
#else
    DEFER_EHOSTUNREACH = _DEFER_EMAX - 31,
#endif


#ifdef EHWPOISON
    DEFER_EHWPOISON = EHWPOISON,
#else
    DEFER_EHWPOISON = _DEFER_EMAX - 32,
#endif


#ifdef EIDRM
    DEFER_EIDRM = EIDRM,
#else
    DEFER_EIDRM = _DEFER_EMAX - 33,
#endif


#ifdef EILSEQ
    DEFER_EILSEQ = EILSEQ,
#else
    DEFER_EILSEQ = _DEFER_EMAX - 34,
#endif


#ifdef EINPROGRESS
    DEFER_EINPROGRESS = EINPROGRESS,
#else
    DEFER_EINPROGRESS = _DEFER_EMAX - 35,
#endif


#ifdef EINTR
    DEFER_EINTR = EINTR,
#else
    DEFER_EINTR = _DEFER_EMAX - 36,
#endif


#ifdef EINVAL
    DEFER_EINVAL = EINVAL,
#else
    DEFER_EINVAL = _DEFER_EMAX - 37,
#endif


#ifdef EIO
    DEFER_EIO = EIO,
#else
    DEFER_EIO = _DEFER_EMAX - 38,
#endif


#ifdef EISCONN
    DEFER_EISCONN = EISCONN,
#else
    DEFER_EISCONN = _DEFER_EMAX - 39,
#endif


#ifdef EISDIR
    DEFER_EISDIR = EISDIR,
#else
    DEFER_EISDIR = _DEFER_EMAX - 40,
#endif


#ifdef EISNAM
    DEFER_EISNAM = EISNAM,
#else
    DEFER_EISNAM = _DEFER_EMAX - 41,
#endif


#ifdef EKEYEXPIRED
    DEFER_EKEYEXPIRED = EKEYEXPIRED,
#else
    DEFER_EKEYEXPIRED = _DEFER_EMAX - 42,
#endif


#ifdef EKEYREJECTED
    DEFER_EKEYREJECTED = EKEYREJECTED,
#else
    DEFER_EKEYREJECTED = _DEFER_EMAX - 43,
#endif


#ifdef EKEYREVOKED
    DEFER_EKEYREVOKED = EKEYREVOKED,
#else
    DEFER_EKEYREVOKED = _DEFER_EMAX - 44,
#endif


#ifdef EL2HLT
    DEFER_EL2HLT = EL2HLT,
#else
    DEFER_EL2HLT = _DEFER_EMAX - 45,
#endif


#ifdef EL2NSYNC
    DEFER_EL2NSYNC = EL2NSYNC,
#else
    DEFER_EL2NSYNC = _DEFER_EMAX - 46,
#endif


#ifdef EL3HLT
    DEFER_EL3HLT = EL3HLT,
#else
    DEFER_EL3HLT = _DEFER_EMAX - 47,
#endif


#ifdef EL3RST
    DEFER_EL3RST = EL3RST,
#else
    DEFER_EL3RST = _DEFER_EMAX - 48,
#endif


#ifdef ELIBACC
    DEFER_ELIBACC = ELIBACC,
#else
    DEFER_ELIBACC = _DEFER_EMAX - 49,
#endif


#ifdef ELIBBAD
    DEFER_ELIBBAD = ELIBBAD,
#else
    DEFER_ELIBBAD = _DEFER_EMAX - 50,
#endif


#ifdef ELIBEXEC
    DEFER_ELIBEXEC = ELIBEXEC,
#else
    DEFER_ELIBEXEC = _DEFER_EMAX - 51,
#endif


#ifdef ELIBMAX
    DEFER_ELIBMAX = ELIBMAX,
#else
    DEFER_ELIBMAX = _DEFER_EMAX - 52,
#endif


#ifdef ELIBSCN
    DEFER_ELIBSCN = ELIBSCN,
#else
    DEFER_ELIBSCN = _DEFER_EMAX - 53,
#endif


#ifdef ELNRANGE
    DEFER_ELNRANGE = ELNRANGE,
#else
    DEFER_ELNRANGE = _DEFER_EMAX - 54,
#endif


#ifdef ELOOP
    DEFER_ELOOP = ELOOP,
#else
    DEFER_ELOOP = _DEFER_EMAX - 55,
#endif


#ifdef EMEDIUMTYPE
    DEFER_EMEDIUMTYPE = EMEDIUMTYPE,
#else
    DEFER_EMEDIUMTYPE = _DEFER_EMAX - 56,
#endif


#ifdef EMFILE
    DEFER_EMFILE = EMFILE,
#else
    DEFER_EMFILE = _DEFER_EMAX - 57,
#endif


#ifdef EMLINK
    DEFER_EMLINK = EMLINK,
#else
    DEFER_EMLINK = _DEFER_EMAX - 58,
#endif


#ifdef EMSGSIZE
    DEFER_EMSGSIZE = EMSGSIZE,
#else
    DEFER_EMSGSIZE = _DEFER_EMAX - 59,
#endif


#ifdef EMULTIHOP
    DEFER_EMULTIHOP = EMULTIHOP,
#else
    DEFER_EMULTIHOP = _DEFER_EMAX - 60,
#endif


#ifdef ENAMETOOLONG
    DEFER_ENAMETOOLONG = ENAMETOOLONG,
#else
    DEFER_ENAMETOOLONG = _DEFER_EMAX - 61,
#endif


#ifdef ENETDOWN
    DEFER_ENETDOWN = ENETDOWN,
#else
    DEFER_ENETDOWN = _DEFER_EMAX - 62,
#endif


#ifdef ENETRESET
    DEFER_ENETRESET = ENETRESET,
#else
    DEFER_ENETRESET = _DEFER_EMAX - 63,
#endif


#ifdef ENETUNREACH
    DEFER_ENETUNREACH = ENETUNREACH,
#else
    DEFER_ENETUNREACH = _DEFER_EMAX - 64,
#endif


#ifdef ENFILE
    DEFER_ENFILE = ENFILE,
#else
    DEFER_ENFILE = _DEFER_EMAX - 65,
#endif


#ifdef ENOANO
    DEFER_ENOANO = ENOANO,
#else
    DEFER_ENOANO = _DEFER_EMAX - 66,
#endif


#ifdef ENOBUFS
    DEFER_ENOBUFS = ENOBUFS,
#else
    DEFER_ENOBUFS = _DEFER_EMAX - 67,
#endif


#ifdef ENODATA
    DEFER_ENODATA = ENODATA,
#else
    DEFER_ENODATA = _DEFER_EMAX - 68,
#endif


#ifdef ENODEV
    DEFER_ENODEV = ENODEV,
#else
    DEFER_ENODEV = _DEFER_EMAX - 69,
#endif


#ifdef ENOENT
    DEFER_ENOENT = ENOENT,
#else
    DEFER_ENOENT = _DEFER_EMAX - 70,
#endif


#ifdef ENOEXEC
    DEFER_ENOEXEC = ENOEXEC,
#else
    DEFER_ENOEXEC = _DEFER_EMAX - 71,
#endif


#ifdef ENOKEY
    DEFER_ENOKEY = ENOKEY,
#else
    DEFER_ENOKEY = _DEFER_EMAX - 72,
#endif


#ifdef ENOLCK
    DEFER_ENOLCK = ENOLCK,
#else
    DEFER_ENOLCK = _DEFER_EMAX - 73,
#endif


#ifdef ENOLINK
    DEFER_ENOLINK = ENOLINK,
#else
    DEFER_ENOLINK = _DEFER_EMAX - 74,
#endif


#ifdef ENOMEDIUM
    DEFER_ENOMEDIUM = ENOMEDIUM,
#else
    DEFER_ENOMEDIUM = _DEFER_EMAX - 75,
#endif


#ifdef ENOMEM
    DEFER_ENOMEM = ENOMEM,
#else
    DEFER_ENOMEM = _DEFER_EMAX - 76,
#endif


#ifdef ENOMSG
    DEFER_ENOMSG = ENOMSG,
#else
    DEFER_ENOMSG = _DEFER_EMAX - 77,
#endif


#ifdef ENONET
    DEFER_ENONET = ENONET,
#else
    DEFER_ENONET = _DEFER_EMAX - 78,
#endif


#ifdef ENOPKG
    DEFER_ENOPKG = ENOPKG,
#else
    DEFER_ENOPKG = _DEFER_EMAX - 79,
#endif


#ifdef ENOPROTOOPT
    DEFER_ENOPROTOOPT = ENOPROTOOPT,
#else
    DEFER_ENOPROTOOPT = _DEFER_EMAX - 80,
#endif


#ifdef ENOSPC
    DEFER_ENOSPC = ENOSPC,
#else
    DEFER_ENOSPC = _DEFER_EMAX - 81,
#endif


#ifdef ENOSR
    DEFER_ENOSR = ENOSR,
#else
    DEFER_ENOSR = _DEFER_EMAX - 82,
#endif


#ifdef ENOSTR
    DEFER_ENOSTR = ENOSTR,
#else
    DEFER_ENOSTR = _DEFER_EMAX - 83,
#endif


#ifdef ENOSYS
    DEFER_ENOSYS = ENOSYS,
#else
    DEFER_ENOSYS = _DEFER_EMAX - 84,
#endif


#ifdef ENOTBLK
    DEFER_ENOTBLK = ENOTBLK,
#else
    DEFER_ENOTBLK = _DEFER_EMAX - 85,
#endif


#ifdef ENOTCONN
    DEFER_ENOTCONN = ENOTCONN,
#else
    DEFER_ENOTCONN = _DEFER_EMAX - 86,
#endif


#ifdef ENOTDIR
    DEFER_ENOTDIR = ENOTDIR,
#else
    DEFER_ENOTDIR = _DEFER_EMAX - 87,
#endif


#ifdef ENOTEMPTY
    DEFER_ENOTEMPTY = ENOTEMPTY,
#else
    DEFER_ENOTEMPTY = _DEFER_EMAX - 88,
#endif


#ifdef ENOTRECOVERABLE
    DEFER_ENOTRECOVERABLE = ENOTRECOVERABLE,
#else
    DEFER_ENOTRECOVERABLE = _DEFER_EMAX - 89,
#endif


#ifdef ENOTSOCK
    DEFER_ENOTSOCK = ENOTSOCK,
#else
    DEFER_ENOTSOCK = _DEFER_EMAX - 90,
#endif


#ifdef ENOTSUP
    DEFER_ENOTSUP = ENOTSUP,
#else
    DEFER_ENOTSUP = _DEFER_EMAX - 91,
#endif


#ifdef ENOTTY
    DEFER_ENOTTY = ENOTTY,
#else
    DEFER_ENOTTY = _DEFER_EMAX - 92,
#endif


#ifdef ENOTUNIQ
    DEFER_ENOTUNIQ = ENOTUNIQ,
#else
    DEFER_ENOTUNIQ = _DEFER_EMAX - 93,
#endif


#ifdef ENXIO
    DEFER_ENXIO = ENXIO,
#else
    DEFER_ENXIO = _DEFER_EMAX - 94,
#endif


#ifdef EOPNOTSUPP
    DEFER_EOPNOTSUPP = EOPNOTSUPP,
#else
    DEFER_EOPNOTSUPP = _DEFER_EMAX - 95,
#endif


#ifdef EOVERFLOW
    DEFER_EOVERFLOW = EOVERFLOW,
#else
    DEFER_EOVERFLOW = _DEFER_EMAX - 96,
#endif


#ifdef EOWNERDEAD
    DEFER_EOWNERDEAD = EOWNERDEAD,
#else
    DEFER_EOWNERDEAD = _DEFER_EMAX - 97,
#endif


#ifdef EPERM
    DEFER_EPERM = EPERM,
#else
    DEFER_EPERM = _DEFER_EMAX - 98,
#endif


#ifdef EPFNOSUPPORT
    DEFER_EPFNOSUPPORT = EPFNOSUPPORT,
#else
    DEFER_EPFNOSUPPORT = _DEFER_EMAX - 99,
#endif


#ifdef EPIPE
    DEFER_EPIPE = EPIPE,
#else
    DEFER_EPIPE = _DEFER_EMAX - 100,
#endif


#ifdef EPROTO
    DEFER_EPROTO = EPROTO,
#else
    DEFER_EPROTO = _DEFER_EMAX - 101,
#endif


#ifdef EPROTONOSUPPORT
    DEFER_EPROTONOSUPPORT = EPROTONOSUPPORT,
#else
    DEFER_EPROTONOSUPPORT = _DEFER_EMAX - 102,
#endif


#ifdef EPROTOTYPE
    DEFER_EPROTOTYPE = EPROTOTYPE,
#else
    DEFER_EPROTOTYPE = _DEFER_EMAX - 103,
#endif


#ifdef ERANGE
    DEFER_ERANGE = ERANGE,
#else
    DEFER_ERANGE = _DEFER_EMAX - 104,
#endif


#ifdef EREMCHG
    DEFER_EREMCHG = EREMCHG,
#else
    DEFER_EREMCHG = _DEFER_EMAX - 105,
#endif


#ifdef EREMOTE
    DEFER_EREMOTE = EREMOTE,
#else
    DEFER_EREMOTE = _DEFER_EMAX - 106,
#endif


#ifdef EREMOTEIO
    DEFER_EREMOTEIO = EREMOTEIO,
#else
    DEFER_EREMOTEIO = _DEFER_EMAX - 107,
#endif


#ifdef ERESTART
    DEFER_ERESTART = ERESTART,
#else
    DEFER_ERESTART = _DEFER_EMAX - 108,
#endif


#ifdef ERFKILL
    DEFER_ERFKILL = ERFKILL,
#else
    DEFER_ERFKILL = _DEFER_EMAX - 109,
#endif


#ifdef EROFS
    DEFER_EROFS = EROFS,
#else
    DEFER_EROFS = _DEFER_EMAX - 110,
#endif


#ifdef ESHUTDOWN
    DEFER_ESHUTDOWN = ESHUTDOWN,
#else
    DEFER_ESHUTDOWN = _DEFER_EMAX - 111,
#endif


#ifdef ESOCKTNOSUPPORT
    DEFER_ESOCKTNOSUPPORT = ESOCKTNOSUPPORT,
#else
    DEFER_ESOCKTNOSUPPORT = _DEFER_EMAX - 112,
#endif


#ifdef ESPIPE
    DEFER_ESPIPE = ESPIPE,
#else
    DEFER_ESPIPE = _DEFER_EMAX - 113,
#endif


#ifdef ESRCH
    DEFER_ESRCH = ESRCH,
#else
    DEFER_ESRCH = _DEFER_EMAX - 114,
#endif


#ifdef ESTALE
    DEFER_ESTALE = ESTALE,
#else
    DEFER_ESTALE = _DEFER_EMAX - 115,
#endif


#ifdef ESTRPIPE
    DEFER_ESTRPIPE = ESTRPIPE,
#else
    DEFER_ESTRPIPE = _DEFER_EMAX - 116,
#endif


#ifdef ETIME
    DEFER_ETIME = ETIME,
#else
    DEFER_ETIME = _DEFER_EMAX - 117,
#endif


#ifdef ETIMEDOUT
    DEFER_ETIMEDOUT = ETIMEDOUT,
#else
    DEFER_ETIMEDOUT = _DEFER_EMAX - 118,
#endif


#ifdef ETOOMANYREFS
    DEFER_ETOOMANYREFS = ETOOMANYREFS,
#else
    DEFER_ETOOMANYREFS = _DEFER_EMAX - 119,
#endif


#ifdef ETXTBSY
    DEFER_ETXTBSY = ETXTBSY,
#else
    DEFER_ETXTBSY = _DEFER_EMAX - 120,
#endif


#ifdef EUCLEAN
    DEFER_EUCLEAN = EUCLEAN,
#else
    DEFER_EUCLEAN = _DEFER_EMAX - 121,
#endif


#ifdef EUNATCH
    DEFER_EUNATCH = EUNATCH,
#else
    DEFER_EUNATCH = _DEFER_EMAX - 122,
#endif


#ifdef EUSERS
    DEFER_EUSERS = EUSERS,
#else
    DEFER_EUSERS = _DEFER_EMAX - 123,
#endif


#ifdef EWOULDBLOCK
    DEFER_EWOULDBLOCK = EWOULDBLOCK,
#else
    DEFER_EWOULDBLOCK = _DEFER_EMAX - 124,
#endif


#ifdef EXDEV
    DEFER_EXDEV = EXDEV,
#else
    DEFER_EXDEV = _DEFER_EMAX - 125,
#endif


#ifdef EXFULL
    DEFER_EXFULL = EXFULL,
#else
    DEFER_EXFULL = _DEFER_EMAX - 126,
#endif


#ifdef EUNDERFLOW
    DEFER_EUNDERFLOW = EUNDERFLOW,
#else
    DEFER_EUNDERFLOW = _DEFER_EMAX - 127,
#endif


};

typedef enum _Defer_codes _Defer_codes;


// errno numbers should be small
static_assert(DEFER_E2BIG < _DEFER_EMAX, "ranges for defer state values may overlap for " "E2BIG");
static_assert(DEFER_EACCES < _DEFER_EMAX, "ranges for defer state values may overlap for " "EACCES");
static_assert(DEFER_EADDRINUSE < _DEFER_EMAX, "ranges for defer state values may overlap for " "EADDRINUSE");
static_assert(DEFER_EADDRNOTAVAIL < _DEFER_EMAX, "ranges for defer state values may overlap for " "EADDRNOTAVAIL");
static_assert(DEFER_EAFNOSUPPORT < _DEFER_EMAX, "ranges for defer state values may overlap for " "EAFNOSUPPORT");
static_assert(DEFER_EAGAIN < _DEFER_EMAX, "ranges for defer state values may overlap for " "EAGAIN");
static_assert(DEFER_EALREADY < _DEFER_EMAX, "ranges for defer state values may overlap for " "EALREADY");
static_assert(DEFER_EBADE < _DEFER_EMAX, "ranges for defer state values may overlap for " "EBADE");
static_assert(DEFER_EBADF < _DEFER_EMAX, "ranges for defer state values may overlap for " "EBADF");
static_assert(DEFER_EBADFD < _DEFER_EMAX, "ranges for defer state values may overlap for " "EBADFD");
static_assert(DEFER_EBADMSG < _DEFER_EMAX, "ranges for defer state values may overlap for " "EBADMSG");
static_assert(DEFER_EBADR < _DEFER_EMAX, "ranges for defer state values may overlap for " "EBADR");
static_assert(DEFER_EBADRQC < _DEFER_EMAX, "ranges for defer state values may overlap for " "EBADRQC");
static_assert(DEFER_EBADSLT < _DEFER_EMAX, "ranges for defer state values may overlap for " "EBADSLT");
static_assert(DEFER_EBUSY < _DEFER_EMAX, "ranges for defer state values may overlap for " "EBUSY");
static_assert(DEFER_ECANCELED < _DEFER_EMAX, "ranges for defer state values may overlap for " "ECANCELED");
static_assert(DEFER_ECHILD < _DEFER_EMAX, "ranges for defer state values may overlap for " "ECHILD");
static_assert(DEFER_ECHRNG < _DEFER_EMAX, "ranges for defer state values may overlap for " "ECHRNG");
static_assert(DEFER_ECOMM < _DEFER_EMAX, "ranges for defer state values may overlap for " "ECOMM");
static_assert(DEFER_ECONNABORTED < _DEFER_EMAX, "ranges for defer state values may overlap for " "ECONNABORTED");
static_assert(DEFER_ECONNREFUSED < _DEFER_EMAX, "ranges for defer state values may overlap for " "ECONNREFUSED");
static_assert(DEFER_ECONNRESET < _DEFER_EMAX, "ranges for defer state values may overlap for " "ECONNRESET");
static_assert(DEFER_EDEADLK < _DEFER_EMAX, "ranges for defer state values may overlap for " "EDEADLK");
static_assert(DEFER_EDEADLOCK < _DEFER_EMAX, "ranges for defer state values may overlap for " "EDEADLOCK");
static_assert(DEFER_EDESTADDRREQ < _DEFER_EMAX, "ranges for defer state values may overlap for " "EDESTADDRREQ");
static_assert(DEFER_EDOM < _DEFER_EMAX, "ranges for defer state values may overlap for " "EDOM");
static_assert(DEFER_EDQUOT < _DEFER_EMAX, "ranges for defer state values may overlap for " "EDQUOT");
static_assert(DEFER_EEXIST < _DEFER_EMAX, "ranges for defer state values may overlap for " "EEXIST");
static_assert(DEFER_EFAULT < _DEFER_EMAX, "ranges for defer state values may overlap for " "EFAULT");
static_assert(DEFER_EFBIG < _DEFER_EMAX, "ranges for defer state values may overlap for " "EFBIG");
static_assert(DEFER_EHOSTDOWN < _DEFER_EMAX, "ranges for defer state values may overlap for " "EHOSTDOWN");
static_assert(DEFER_EHOSTUNREACH < _DEFER_EMAX, "ranges for defer state values may overlap for " "EHOSTUNREACH");
static_assert(DEFER_EHWPOISON < _DEFER_EMAX, "ranges for defer state values may overlap for " "EHWPOISON");
static_assert(DEFER_EIDRM < _DEFER_EMAX, "ranges for defer state values may overlap for " "EIDRM");
static_assert(DEFER_EILSEQ < _DEFER_EMAX, "ranges for defer state values may overlap for " "EILSEQ");
static_assert(DEFER_EINPROGRESS < _DEFER_EMAX, "ranges for defer state values may overlap for " "EINPROGRESS");
static_assert(DEFER_EINTR < _DEFER_EMAX, "ranges for defer state values may overlap for " "EINTR");
static_assert(DEFER_EINVAL < _DEFER_EMAX, "ranges for defer state values may overlap for " "EINVAL");
static_assert(DEFER_EIO < _DEFER_EMAX, "ranges for defer state values may overlap for " "EIO");
static_assert(DEFER_EISCONN < _DEFER_EMAX, "ranges for defer state values may overlap for " "EISCONN");
static_assert(DEFER_EISDIR < _DEFER_EMAX, "ranges for defer state values may overlap for " "EISDIR");
static_assert(DEFER_EISNAM < _DEFER_EMAX, "ranges for defer state values may overlap for " "EISNAM");
static_assert(DEFER_EKEYEXPIRED < _DEFER_EMAX, "ranges for defer state values may overlap for " "EKEYEXPIRED");
static_assert(DEFER_EKEYREJECTED < _DEFER_EMAX, "ranges for defer state values may overlap for " "EKEYREJECTED");
static_assert(DEFER_EKEYREVOKED < _DEFER_EMAX, "ranges for defer state values may overlap for " "EKEYREVOKED");
static_assert(DEFER_EL2HLT < _DEFER_EMAX, "ranges for defer state values may overlap for " "EL2HLT");
static_assert(DEFER_EL2NSYNC < _DEFER_EMAX, "ranges for defer state values may overlap for " "EL2NSYNC");
static_assert(DEFER_EL3HLT < _DEFER_EMAX, "ranges for defer state values may overlap for " "EL3HLT");
static_assert(DEFER_EL3RST < _DEFER_EMAX, "ranges for defer state values may overlap for " "EL3RST");
static_assert(DEFER_ELIBACC < _DEFER_EMAX, "ranges for defer state values may overlap for " "ELIBACC");
static_assert(DEFER_ELIBBAD < _DEFER_EMAX, "ranges for defer state values may overlap for " "ELIBBAD");
static_assert(DEFER_ELIBEXEC < _DEFER_EMAX, "ranges for defer state values may overlap for " "ELIBEXEC");
static_assert(DEFER_ELIBMAX < _DEFER_EMAX, "ranges for defer state values may overlap for " "ELIBMAX");
static_assert(DEFER_ELIBSCN < _DEFER_EMAX, "ranges for defer state values may overlap for " "ELIBSCN");
static_assert(DEFER_ELNRANGE < _DEFER_EMAX, "ranges for defer state values may overlap for " "ELNRANGE");
static_assert(DEFER_ELOOP < _DEFER_EMAX, "ranges for defer state values may overlap for " "ELOOP");
static_assert(DEFER_EMEDIUMTYPE < _DEFER_EMAX, "ranges for defer state values may overlap for " "EMEDIUMTYPE");
static_assert(DEFER_EMFILE < _DEFER_EMAX, "ranges for defer state values may overlap for " "EMFILE");
static_assert(DEFER_EMLINK < _DEFER_EMAX, "ranges for defer state values may overlap for " "EMLINK");
static_assert(DEFER_EMSGSIZE < _DEFER_EMAX, "ranges for defer state values may overlap for " "EMSGSIZE");
static_assert(DEFER_EMULTIHOP < _DEFER_EMAX, "ranges for defer state values may overlap for " "EMULTIHOP");
static_assert(DEFER_ENAMETOOLONG < _DEFER_EMAX, "ranges for defer state values may overlap for " "ENAMETOOLONG");
static_assert(DEFER_ENETDOWN < _DEFER_EMAX, "ranges for defer state values may overlap for " "ENETDOWN");
static_assert(DEFER_ENETRESET < _DEFER_EMAX, "ranges for defer state values may overlap for " "ENETRESET");
static_assert(DEFER_ENETUNREACH < _DEFER_EMAX, "ranges for defer state values may overlap for " "ENETUNREACH");
static_assert(DEFER_ENFILE < _DEFER_EMAX, "ranges for defer state values may overlap for " "ENFILE");
static_assert(DEFER_ENOANO < _DEFER_EMAX, "ranges for defer state values may overlap for " "ENOANO");
static_assert(DEFER_ENOBUFS < _DEFER_EMAX, "ranges for defer state values may overlap for " "ENOBUFS");
static_assert(DEFER_ENODATA < _DEFER_EMAX, "ranges for defer state values may overlap for " "ENODATA");
static_assert(DEFER_ENODEV < _DEFER_EMAX, "ranges for defer state values may overlap for " "ENODEV");
static_assert(DEFER_ENOENT < _DEFER_EMAX, "ranges for defer state values may overlap for " "ENOENT");
static_assert(DEFER_ENOEXEC < _DEFER_EMAX, "ranges for defer state values may overlap for " "ENOEXEC");
static_assert(DEFER_ENOKEY < _DEFER_EMAX, "ranges for defer state values may overlap for " "ENOKEY");
static_assert(DEFER_ENOLCK < _DEFER_EMAX, "ranges for defer state values may overlap for " "ENOLCK");
static_assert(DEFER_ENOLINK < _DEFER_EMAX, "ranges for defer state values may overlap for " "ENOLINK");
static_assert(DEFER_ENOMEDIUM < _DEFER_EMAX, "ranges for defer state values may overlap for " "ENOMEDIUM");
static_assert(DEFER_ENOMEM < _DEFER_EMAX, "ranges for defer state values may overlap for " "ENOMEM");
static_assert(DEFER_ENOMSG < _DEFER_EMAX, "ranges for defer state values may overlap for " "ENOMSG");
static_assert(DEFER_ENONET < _DEFER_EMAX, "ranges for defer state values may overlap for " "ENONET");
static_assert(DEFER_ENOPKG < _DEFER_EMAX, "ranges for defer state values may overlap for " "ENOPKG");
static_assert(DEFER_ENOPROTOOPT < _DEFER_EMAX, "ranges for defer state values may overlap for " "ENOPROTOOPT");
static_assert(DEFER_ENOSPC < _DEFER_EMAX, "ranges for defer state values may overlap for " "ENOSPC");
static_assert(DEFER_ENOSR < _DEFER_EMAX, "ranges for defer state values may overlap for " "ENOSR");
static_assert(DEFER_ENOSTR < _DEFER_EMAX, "ranges for defer state values may overlap for " "ENOSTR");
static_assert(DEFER_ENOSYS < _DEFER_EMAX, "ranges for defer state values may overlap for " "ENOSYS");
static_assert(DEFER_ENOTBLK < _DEFER_EMAX, "ranges for defer state values may overlap for " "ENOTBLK");
static_assert(DEFER_ENOTCONN < _DEFER_EMAX, "ranges for defer state values may overlap for " "ENOTCONN");
static_assert(DEFER_ENOTDIR < _DEFER_EMAX, "ranges for defer state values may overlap for " "ENOTDIR");
static_assert(DEFER_ENOTEMPTY < _DEFER_EMAX, "ranges for defer state values may overlap for " "ENOTEMPTY");
static_assert(DEFER_ENOTRECOVERABLE < _DEFER_EMAX, "ranges for defer state values may overlap for " "ENOTRECOVERABLE");
static_assert(DEFER_ENOTSOCK < _DEFER_EMAX, "ranges for defer state values may overlap for " "ENOTSOCK");
static_assert(DEFER_ENOTSUP < _DEFER_EMAX, "ranges for defer state values may overlap for " "ENOTSUP");
static_assert(DEFER_ENOTTY < _DEFER_EMAX, "ranges for defer state values may overlap for " "ENOTTY");
static_assert(DEFER_ENOTUNIQ < _DEFER_EMAX, "ranges for defer state values may overlap for " "ENOTUNIQ");
static_assert(DEFER_ENXIO < _DEFER_EMAX, "ranges for defer state values may overlap for " "ENXIO");
static_assert(DEFER_EOPNOTSUPP < _DEFER_EMAX, "ranges for defer state values may overlap for " "EOPNOTSUPP");
static_assert(DEFER_EOVERFLOW < _DEFER_EMAX, "ranges for defer state values may overlap for " "EOVERFLOW");
static_assert(DEFER_EOWNERDEAD < _DEFER_EMAX, "ranges for defer state values may overlap for " "EOWNERDEAD");
static_assert(DEFER_EPERM < _DEFER_EMAX, "ranges for defer state values may overlap for " "EPERM");
static_assert(DEFER_EPFNOSUPPORT < _DEFER_EMAX, "ranges for defer state values may overlap for " "EPFNOSUPPORT");
static_assert(DEFER_EPIPE < _DEFER_EMAX, "ranges for defer state values may overlap for " "EPIPE");
static_assert(DEFER_EPROTO < _DEFER_EMAX, "ranges for defer state values may overlap for " "EPROTO");
static_assert(DEFER_EPROTONOSUPPORT < _DEFER_EMAX, "ranges for defer state values may overlap for " "EPROTONOSUPPORT");
static_assert(DEFER_EPROTOTYPE < _DEFER_EMAX, "ranges for defer state values may overlap for " "EPROTOTYPE");
static_assert(DEFER_ERANGE < _DEFER_EMAX, "ranges for defer state values may overlap for " "ERANGE");
static_assert(DEFER_EREMCHG < _DEFER_EMAX, "ranges for defer state values may overlap for " "EREMCHG");
static_assert(DEFER_EREMOTE < _DEFER_EMAX, "ranges for defer state values may overlap for " "EREMOTE");
static_assert(DEFER_EREMOTEIO < _DEFER_EMAX, "ranges for defer state values may overlap for " "EREMOTEIO");
static_assert(DEFER_ERESTART < _DEFER_EMAX, "ranges for defer state values may overlap for " "ERESTART");
static_assert(DEFER_ERFKILL < _DEFER_EMAX, "ranges for defer state values may overlap for " "ERFKILL");
static_assert(DEFER_EROFS < _DEFER_EMAX, "ranges for defer state values may overlap for " "EROFS");
static_assert(DEFER_ESHUTDOWN < _DEFER_EMAX, "ranges for defer state values may overlap for " "ESHUTDOWN");
static_assert(DEFER_ESOCKTNOSUPPORT < _DEFER_EMAX, "ranges for defer state values may overlap for " "ESOCKTNOSUPPORT");
static_assert(DEFER_ESPIPE < _DEFER_EMAX, "ranges for defer state values may overlap for " "ESPIPE");
static_assert(DEFER_ESRCH < _DEFER_EMAX, "ranges for defer state values may overlap for " "ESRCH");
static_assert(DEFER_ESTALE < _DEFER_EMAX, "ranges for defer state values may overlap for " "ESTALE");
static_assert(DEFER_ESTRPIPE < _DEFER_EMAX, "ranges for defer state values may overlap for " "ESTRPIPE");
static_assert(DEFER_ETIME < _DEFER_EMAX, "ranges for defer state values may overlap for " "ETIME");
static_assert(DEFER_ETIMEDOUT < _DEFER_EMAX, "ranges for defer state values may overlap for " "ETIMEDOUT");
static_assert(DEFER_ETOOMANYREFS < _DEFER_EMAX, "ranges for defer state values may overlap for " "ETOOMANYREFS");
static_assert(DEFER_ETXTBSY < _DEFER_EMAX, "ranges for defer state values may overlap for " "ETXTBSY");
static_assert(DEFER_EUCLEAN < _DEFER_EMAX, "ranges for defer state values may overlap for " "EUCLEAN");
static_assert(DEFER_EUNATCH < _DEFER_EMAX, "ranges for defer state values may overlap for " "EUNATCH");
static_assert(DEFER_EUSERS < _DEFER_EMAX, "ranges for defer state values may overlap for " "EUSERS");
static_assert(DEFER_EWOULDBLOCK < _DEFER_EMAX, "ranges for defer state values may overlap for " "EWOULDBLOCK");
static_assert(DEFER_EXDEV < _DEFER_EMAX, "ranges for defer state values may overlap for " "EXDEV");
static_assert(DEFER_EXFULL < _DEFER_EMAX, "ranges for defer state values may overlap for " "EXFULL");
static_assert(DEFER_EUNDERFLOW < _DEFER_EMAX, "ranges for defer state values may overlap for " "EUNDERFLOW");

// signal numbers should be small
#ifdef SIGABRT
static_assert(SIGABRT < _DEFER_EMAX, "ranges for defer state values may overlap for SIG" "ABRT");
static_assert(_DEFER_NONE < DEFER_ABRT, "ranges for defer state values may overlap for SIG" "ABRT");
#endif
#ifdef SIGALRM
static_assert(SIGALRM < _DEFER_EMAX, "ranges for defer state values may overlap for SIG" "ALRM");
static_assert(_DEFER_NONE < DEFER_ALRM, "ranges for defer state values may overlap for SIG" "ALRM");
#endif
#ifdef SIGBUS
static_assert(SIGBUS < _DEFER_EMAX, "ranges for defer state values may overlap for SIG" "BUS");
static_assert(_DEFER_NONE < DEFER_BUS, "ranges for defer state values may overlap for SIG" "BUS");
#endif
#ifdef SIGCHLD
static_assert(SIGCHLD < _DEFER_EMAX, "ranges for defer state values may overlap for SIG" "CHLD");
static_assert(_DEFER_NONE < DEFER_CHLD, "ranges for defer state values may overlap for SIG" "CHLD");
#endif
#ifdef SIGCLD
static_assert(SIGCLD < _DEFER_EMAX, "ranges for defer state values may overlap for SIG" "CLD");
static_assert(_DEFER_NONE < DEFER_CLD, "ranges for defer state values may overlap for SIG" "CLD");
#endif
#ifdef SIGCONT
static_assert(SIGCONT < _DEFER_EMAX, "ranges for defer state values may overlap for SIG" "CONT");
static_assert(_DEFER_NONE < DEFER_CONT, "ranges for defer state values may overlap for SIG" "CONT");
#endif
#ifdef SIGEMT
static_assert(SIGEMT < _DEFER_EMAX, "ranges for defer state values may overlap for SIG" "EMT");
static_assert(_DEFER_NONE < DEFER_EMT, "ranges for defer state values may overlap for SIG" "EMT");
#endif
#ifdef SIGFPE
static_assert(SIGFPE < _DEFER_EMAX, "ranges for defer state values may overlap for SIG" "FPE");
static_assert(_DEFER_NONE < DEFER_FPE, "ranges for defer state values may overlap for SIG" "FPE");
#endif
#ifdef SIGHUP
static_assert(SIGHUP < _DEFER_EMAX, "ranges for defer state values may overlap for SIG" "HUP");
static_assert(_DEFER_NONE < DEFER_HUP, "ranges for defer state values may overlap for SIG" "HUP");
#endif
#ifdef SIGILL
static_assert(SIGILL < _DEFER_EMAX, "ranges for defer state values may overlap for SIG" "ILL");
static_assert(_DEFER_NONE < DEFER_ILL, "ranges for defer state values may overlap for SIG" "ILL");
#endif
#ifdef SIGINFO
static_assert(SIGINFO < _DEFER_EMAX, "ranges for defer state values may overlap for SIG" "INFO");
static_assert(_DEFER_NONE < DEFER_INFO, "ranges for defer state values may overlap for SIG" "INFO");
#endif
#ifdef SIGINT
static_assert(SIGINT < _DEFER_EMAX, "ranges for defer state values may overlap for SIG" "INT");
static_assert(_DEFER_NONE < DEFER_INT, "ranges for defer state values may overlap for SIG" "INT");
#endif
#ifdef SIGIO
static_assert(SIGIO < _DEFER_EMAX, "ranges for defer state values may overlap for SIG" "IO");
static_assert(_DEFER_NONE < DEFER_IO, "ranges for defer state values may overlap for SIG" "IO");
#endif
#ifdef SIGIOT
static_assert(SIGIOT < _DEFER_EMAX, "ranges for defer state values may overlap for SIG" "IOT");
static_assert(_DEFER_NONE < DEFER_IOT, "ranges for defer state values may overlap for SIG" "IOT");
#endif
#ifdef SIGKILL
static_assert(SIGKILL < _DEFER_EMAX, "ranges for defer state values may overlap for SIG" "KILL");
static_assert(_DEFER_NONE < DEFER_KILL, "ranges for defer state values may overlap for SIG" "KILL");
#endif
#ifdef SIGLOST
static_assert(SIGLOST < _DEFER_EMAX, "ranges for defer state values may overlap for SIG" "LOST");
static_assert(_DEFER_NONE < DEFER_LOST, "ranges for defer state values may overlap for SIG" "LOST");
#endif
#ifdef SIGPIPE
static_assert(SIGPIPE < _DEFER_EMAX, "ranges for defer state values may overlap for SIG" "PIPE");
static_assert(_DEFER_NONE < DEFER_PIPE, "ranges for defer state values may overlap for SIG" "PIPE");
#endif
#ifdef SIGPOLL
static_assert(SIGPOLL < _DEFER_EMAX, "ranges for defer state values may overlap for SIG" "POLL");
static_assert(_DEFER_NONE < DEFER_POLL, "ranges for defer state values may overlap for SIG" "POLL");
#endif
#ifdef SIGPROF
static_assert(SIGPROF < _DEFER_EMAX, "ranges for defer state values may overlap for SIG" "PROF");
static_assert(_DEFER_NONE < DEFER_PROF, "ranges for defer state values may overlap for SIG" "PROF");
#endif
#ifdef SIGPWR
static_assert(SIGPWR < _DEFER_EMAX, "ranges for defer state values may overlap for SIG" "PWR");
static_assert(_DEFER_NONE < DEFER_PWR, "ranges for defer state values may overlap for SIG" "PWR");
#endif
#ifdef SIGQUIT
static_assert(SIGQUIT < _DEFER_EMAX, "ranges for defer state values may overlap for SIG" "QUIT");
static_assert(_DEFER_NONE < DEFER_QUIT, "ranges for defer state values may overlap for SIG" "QUIT");
#endif
#ifdef SIGSEGV
static_assert(SIGSEGV < _DEFER_EMAX, "ranges for defer state values may overlap for SIG" "SEGV");
static_assert(_DEFER_NONE < DEFER_SEGV, "ranges for defer state values may overlap for SIG" "SEGV");
#endif
#ifdef SIGSTKFLT
static_assert(SIGSTKFLT < _DEFER_EMAX, "ranges for defer state values may overlap for SIG" "STKFLT");
static_assert(_DEFER_NONE < DEFER_STKFLT, "ranges for defer state values may overlap for SIG" "STKFLT");
#endif
#ifdef SIGSTOP
static_assert(SIGSTOP < _DEFER_EMAX, "ranges for defer state values may overlap for SIG" "STOP");
static_assert(_DEFER_NONE < DEFER_STOP, "ranges for defer state values may overlap for SIG" "STOP");
#endif
#ifdef SIGSYS
static_assert(SIGSYS < _DEFER_EMAX, "ranges for defer state values may overlap for SIG" "SYS");
static_assert(_DEFER_NONE < DEFER_SYS, "ranges for defer state values may overlap for SIG" "SYS");
#endif
#ifdef SIGTERM
static_assert(SIGTERM < _DEFER_EMAX, "ranges for defer state values may overlap for SIG" "TERM");
static_assert(_DEFER_NONE < DEFER_TERM, "ranges for defer state values may overlap for SIG" "TERM");
#endif
#ifdef SIGTRAP
static_assert(SIGTRAP < _DEFER_EMAX, "ranges for defer state values may overlap for SIG" "TRAP");
static_assert(_DEFER_NONE < DEFER_TRAP, "ranges for defer state values may overlap for SIG" "TRAP");
#endif
#ifdef SIGTSTP
static_assert(SIGTSTP < _DEFER_EMAX, "ranges for defer state values may overlap for SIG" "TSTP");
static_assert(_DEFER_NONE < DEFER_TSTP, "ranges for defer state values may overlap for SIG" "TSTP");
#endif
#ifdef SIGTTIN
static_assert(SIGTTIN < _DEFER_EMAX, "ranges for defer state values may overlap for SIG" "TTIN");
static_assert(_DEFER_NONE < DEFER_TTIN, "ranges for defer state values may overlap for SIG" "TTIN");
#endif
#ifdef SIGTTOU
static_assert(SIGTTOU < _DEFER_EMAX, "ranges for defer state values may overlap for SIG" "TTOU");
static_assert(_DEFER_NONE < DEFER_TTOU, "ranges for defer state values may overlap for SIG" "TTOU");
#endif
#ifdef SIGUNUSED
static_assert(SIGUNUSED < _DEFER_EMAX, "ranges for defer state values may overlap for SIG" "UNUSED");
static_assert(_DEFER_NONE < DEFER_UNUSED, "ranges for defer state values may overlap for SIG" "UNUSED");
#endif
#ifdef SIGURG
static_assert(SIGURG < _DEFER_EMAX, "ranges for defer state values may overlap for SIG" "URG");
static_assert(_DEFER_NONE < DEFER_URG, "ranges for defer state values may overlap for SIG" "URG");
#endif
#ifdef SIGUSR1
static_assert(SIGUSR1 < _DEFER_EMAX, "ranges for defer state values may overlap for SIG" "USR1");
static_assert(_DEFER_NONE < DEFER_USR1, "ranges for defer state values may overlap for SIG" "USR1");
#endif
#ifdef SIGUSR2
static_assert(SIGUSR2 < _DEFER_EMAX, "ranges for defer state values may overlap for SIG" "USR2");
static_assert(_DEFER_NONE < DEFER_USR2, "ranges for defer state values may overlap for SIG" "USR2");
#endif
#ifdef SIGVTALRM
static_assert(SIGVTALRM < _DEFER_EMAX, "ranges for defer state values may overlap for SIG" "VTALRM");
static_assert(_DEFER_NONE < DEFER_VTALRM, "ranges for defer state values may overlap for SIG" "VTALRM");
#endif
#ifdef SIGWINCH
static_assert(SIGWINCH < _DEFER_EMAX, "ranges for defer state values may overlap for SIG" "WINCH");
static_assert(_DEFER_NONE < DEFER_WINCH, "ranges for defer state values may overlap for SIG" "WINCH");
#endif
#ifdef SIGXCPU
static_assert(SIGXCPU < _DEFER_EMAX, "ranges for defer state values may overlap for SIG" "XCPU");
static_assert(_DEFER_NONE < DEFER_XCPU, "ranges for defer state values may overlap for SIG" "XCPU");
#endif
#ifdef SIGXFSZ
static_assert(SIGXFSZ < _DEFER_EMAX, "ranges for defer state values may overlap for SIG" "XFSZ");
static_assert(_DEFER_NONE < DEFER_XFSZ, "ranges for defer state values may overlap for SIG" "XFSZ");
#endif

static_assert(_DEFER_EMAX < _DEFER_NONE, "ranges for defer state values may overlap for DEFER_" "NONE");
static_assert(_DEFER_EMAX < _DEFER_EXIT, "ranges for defer state values may overlap for DEFER_" "EXIT");
static_assert(_DEFER_EMAX < _DEFER_FUNC, "ranges for defer state values may overlap for DEFER_" "FUNC");
static_assert(_DEFER_EMAX < _DEFER_BLCK, "ranges for defer state values may overlap for DEFER_" "BLCK");


enum { _Defer_language_C, _Defer_language_Cpp, };

#ifdef __cplusplus
namespace std {
extern "C" int _Defer_language(int);
extern "C" int _Defer_cpp_get();
extern "C" void _Defer_panic_cpp(int, void (*)(int) = nullptr);
extern "C" void defer_exit(int);
extern "C" void defer_thrd_exit(int);
}
#endif

#ifdef __cplusplus
struct _Defer_language_Cpp_trigger {
    _Defer_language_Cpp_trigger() {
        std::_Defer_language(_Defer_language_Cpp);
    }
};
// Overloading main is a hack, but should work for C++ since it can't
// be called recursively.
#define main(...) /* eat the leading int */ _Defer_dummy_for_main; /* Define the object */ static _Defer_language_Cpp_trigger _Defer_language_Cpp_trigger_object; /* Define the real main */ int main(__VA_ARGS__)
#endif


#endif

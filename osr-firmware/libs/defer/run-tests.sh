#!/bin/sh

# This is a simple consistency test for the defer feature.  The idea
# is to run ./defer with different numbers of arguments such that we
# cover all (or most of) the cases. If all is well this should output
# nothing and return 0, success. If something is weird, it should list
# a diff of the difference of the output to a reference output and
# return 1, that is failure.

make -k clean
touch --reference=./stddefer.hs ./stddefer.h
touch --reference=./stddefer_codes.hs ./stddefer_codes.h
make libdefer.so libdefer.a defer defer2 defer3 test_lang test_lang++
mkdir defer-tests

COM="./defer"

i=0

while [ "${i}" -lt 30 ] ; do
    ${COM} 2> defer-tests/test-"${i}.2" 1> defer-tests/test-"${i}.1" || true
    COM="${COM} ${i}"
    i=$((${i} + 1))
done

COM="./defer2"

i=0

while [ "${i}" -lt 4 ] ; do
    ${COM} 2> defer-tests/test2-"${i}.2" | sort > defer-tests/test2-"${i}.1" || true
    COM="${COM} ${i}"
    i=$((${i} + 1))
done

COM="./defer3"

i=0

while [ "${i}" -lt 8 ] ; do
    ${COM} "${i}" 2> defer-tests/test3-"${i}.2" 1> defer-tests/test3-"${i}.1" || true
    i=$((${i} + 1))
done

COM="./test_lang"

i=0

while [ "${i}" -lt 30 ] ; do
    ${COM} "${i}" 2> defer-tests/test_lang-"${i}.2" 1> defer-tests/test_lang-"${i}.1" || true
    COM="${COM} ${i}"
    i=$((${i} + 1))
done

COM="./test_lang++"

i=0

while [ "${i}" -lt 30 ] ; do
    ${COM} "${i}" 2> defer-tests/test_lang++-"${i}.2" 1> defer-tests/test_lang++-"${i}.1" || true
    COM="${COM} ${i}"
    i=$((${i} + 1))
done

# now diff the result with the reference and watch false positives
# that only change the path name with a leading ./

exec git diff --minimal --no-index -G'^[^.]+$' defer-reference defer-tests

#ifdef __cplusplus
extern "C" {
#endif

  int test_lang_C_0(int rec);
  int test_lang_C_1(int rec);
  int test_lang_C_2(int rec);
  int test_lang_C_3(int rec);
  int test_lang_C_4(int rec);

  int test_lang_CPP_0(int rec);
  int test_lang_CPP_1(int rec);
  int test_lang_CPP_2(int rec);
  int test_lang_CPP_3(int rec);

#ifdef __cplusplus
}
#endif

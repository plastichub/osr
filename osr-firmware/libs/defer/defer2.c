#include <stdio.h>
#ifndef __STDC_NO_THREADS__
#include <threads.h>
#endif

#include <stddef.h>
#include <threads.h>

// because stddefer rewrites `return` this comes better after system
// includes
#include "stddefer.h"

/**
 ** @file
 **
 ** This is a test program for the defer mechanism that v
 ** shows some compliant solutions using defer
 ** for https://wiki.sei.cmu.edu/confluence/x/LdYxBQ
 **/


mtx_t lock;

unsigned int completed = 0;
enum { max_threads = 5 };

int do_work(void *namep) {
  guard {
  unsigned int const* name = namep;
    printf(".thread%u:%d: starting do_work guarded block\n", *name, __LINE__);
    if (thrd_success != mtx_lock(&lock)) {
      thrd_exit(thrd_error);
    }
    defer {
      // remark: the dot at the beginning inhibits that an error is
      // detected if this program is used for CI and by chance the
      // order in which the threads access the mutex is changed.
      printf(".thread%u:%d: deferred mtx_unlock = %.03u\n", *name, __LINE__, completed);
      if (thrd_success != mtx_unlock(&lock)) {
        panic(thrd_error);
      }
    }
    /* Access data protected by the lock */
    completed += 1;
  }

  // these last two lines should do the same thing
  // return thrd_success;
  thrd_exit(EXIT_SUCCESS);
}

// Whenever there is the possibility that the "main" thread quits
// before another thread, objects that are accessed by both should
// either have static or allocated storage duration.
static unsigned int names[max_threads];

// Use old-fashioned resource management for `threads`. If this is
// allocated, this resource should only be released after all threads
// have terminated. So this is a case where the `defer` mechanism is
// not suitable.
static thrd_t* threads;
static void cleanup_threads(void) {
  printf(".cleanup: %p\n", (void*)threads);
  free(threads);
}

int main(int argc, char* argv[static argc+1]) {
  // install the overall cleanup handlers
  atexit(cleanup_threads);
  at_quick_exit(cleanup_threads);
  // optimal compliant solution
  guard {
    thrd_t thread;
    int result;
    puts("main0: starting main guarded block");
    if (thrd_success != mtx_init(&lock, mtx_plain)) {
       exit(EXIT_FAILURE);
    }
    defer mtx_destroy(&lock);
    for (unsigned int i = 0; i < max_threads; i++) {
      names[i] = i;
      if (thrd_success != thrd_create(&thread, do_work, &names[i])) {
        exit(EXIT_FAILURE);
      }
      defer_capture(thread) {
        if (thrd_success != thrd_join(thread, &result)) {
          printf("main0: thrd_join failure result = %d.\n", result);
          thrd_exit(EXIT_FAILURE);
        }
        printf("main0: thread joined result = %d.\n", result);
      }
    } // end for
  } // end guard

 // compliant solution using threads array
  guard {
    puts("main1: starting main guarded block");
    if (thrd_success != mtx_init(&lock, mtx_plain)) {
       exit(EXIT_FAILURE);
    }
    defer mtx_destroy(&lock);
    // This allocation will usually fail, if the program is called
    // with an argument. In that case, a panic will be triggered, but
    // the above `mtx_destroy` should still be called, followed by the
    // atexit cleanup.
    threads = defer_malloc(max_threads * (size_t)(2-argc) * sizeof *threads);
    for (unsigned int i = 0; i < max_threads; i++) {
      names[i] = i;
      if (thrd_success != thrd_create(&threads[i], do_work, &names[i])) {
        exit(EXIT_FAILURE);
      }
      defer_capture(i) {
        if (thrd_success != thrd_join(threads[i], NULL)) {
          puts("main1: thrd_join failure");
          thrd_exit(EXIT_FAILURE);
        }
        printf("main1: thread %u joined.\n", i);
      }
    } // end for
  } // end guard

  printf("main: completed = %u.\n", completed);
  return EXIT_SUCCESS;
}

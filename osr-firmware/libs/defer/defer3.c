#include <stdio.h>
#ifndef __STDC_NO_THREADS__
#include <threads.h>
#endif

#include <stddef.h>
#include <threads.h>
#include <stdbool.h>


/**
 ** @file
 **
 ** This is a sample program for the defer mechanism that
 ** shows some compliant solutions illustrates a potential
 ** problem between with panic in code that has been partially modernized to
 ** use the defer mechanism.
 **/

// Implement a simple spinlock based on standard C features, header part
#include "stdatomic.h"

typedef atomic_flag afsl;

inline
void afsl_lock(afsl* fl) {
  puts(__func__);
  while (atomic_flag_test_and_set_explicit(fl, memory_order_acq_rel)) {
    // empty
  }
}

inline
void afsl_unlock(afsl* fl) {
  puts(__func__);
  atomic_flag_clear_explicit(fl, memory_order_release);
}

#define AFSL_INIT ATOMIC_FLAG_INIT

// Implement a simple spinlock based on standard C features, library part

void afsl_lock(afsl* fl);
void afsl_unlock(afsl* fl);

// Implement a simple spinlock based on standard C features, end

// This should now always come after all system includes
#include "stddefer.h"

afsl lock1 = AFSL_INIT;
afsl lock2 = AFSL_INIT;

unsigned int completed = 0;

bool badness[3] = { false, false, false, };

// Bottom layer generates a call to panic.
int unforced(int a) {
  puts(__func__);
  panic(-1);
}

int function_goto(void) {
  puts(__func__);
  if (badness[0]) { return 1; }
  afsl_lock(&lock1);
  if (badness[1]) { goto unlock1; }
  afsl_lock(&lock2);
  if (badness[2]) { goto unlock2; }

  /* Access data protected by the spinlock then force a panic */
  completed += 1;
  // This printf should never get called.
  printf("unforced error: %d.\n", unforced(completed));

 unlock2:
  afsl_unlock(&lock2);
 unlock1:
  afsl_unlock(&lock1);
  return 0;
}

int function_defer(void) {
  // this macro is only needed if not compiled with gcc & Co.
  defer_function_fallback {
    puts(__func__);
    if (badness[0]) { return 1; }
    afsl_lock(&lock1);
    defer afsl_unlock(&lock1);
    if (badness[1]) { return 1; }
    afsl_lock(&lock2);
    defer afsl_unlock(&lock2);
    if (badness[2]) { return 1; }

    /* Access data protected by the spinlock then force a panic */
    completed += 1;
    // This printf should never get called.
    printf("unforced error: %d.\n", unforced(completed));

    return 0;
  }
}

int main(int argc, char* argv[static argc+1]) {
  // Distinguish test cases according to the first command line argument
  unsigned testcase = 0;
  if (argc > 1) {
    testcase = strtoul(argv[1], 0, 0);
  }
  for (unsigned i = 0; i < 3; ++i) {
    if (testcase & (1u << i)) badness[i] = true;
    puts(badness[i] ? "true" : "false");
  }

  guard {
    defer {
      int err = recover();
      switch (err) {
      case 0:
        puts("testing function_defer: no error encountered, continuing as planned");
        break;
      case -1:
        puts("testing function_defer: recover and continue...");
        break;
      default:
        printf("testing function_defer: unknown error %d, cross fingers and continue\n", err);
        break;
      }
      puts("end of deferred statement, continuing");
    }
    function_defer();
  }

  puts("successfully tested function_defer, continuing");

  guard {
    defer {
      int err = recover();
      switch (err) {
      case 0:
        puts("testing function_goto: no error encountered, continuing as planned");
        break;
      case -1:
        puts("testing function_goto: recover and continue...");
        break;
      default:
        printf("testing function_goto: unknown error %d, cross fingers and continue\n", err);
        break;
      }
    }
    puts("about to call function_goto()");
    function_goto();
  }
  printf("main: completed = %u.\n", completed);
  return EXIT_SUCCESS;
}

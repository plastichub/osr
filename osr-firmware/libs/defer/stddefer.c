#include <stdio.h>
#include "stddefer.h"
#include "stddefer_cpp.h"

// The compilation in this TU itself to realize the defer mechanism
// must not use the defer return definition but the original one.!
#undef return

/**
 ** @file
 **
 ** This TU implements the remaining functions of the defer mechanism,
 ** instantiates the inline functions and defines the thread-local
 ** state variable that is needed to establish cross function defer
 ** clause unwinding.
 **/

_Thread_local _Defer_state _Defer_head = { ._Top = { ._Lev = _DEFER_FUNC, }, };

void*const _Defer_headp;

void _Defer_abort(int);
void _Defer_panic_default(int);
void (_Defer_report)(char const* _Defer_str);
int _Defer_recover_signal(_Defer_state volatile*const);
int _Defer_code(_Defer_state volatile*const);
int _Defer_recover(_Defer_state volatile*const);
_Defer_buf* _Defer_find(_Defer_state volatile _P[static 1], void*);
_Defer_buf* _Defer_toplevel(_Defer_state volatile _P[static 1], void const*, void const*);
_Defer_buf* _Defer_pop(_Defer_state volatile _P[static 1], int);
_Defer_buf* _Defer_push(_Defer_state volatile _P[static 1], _Defer_buf*);
_Defer_buf* _Defer_new(_Defer_state volatile _P[static 1], void const*, void const*, void const*);
void _Defer_delete_latest(_Defer_state volatile _P[static 1]);
void _Defer_panic3(_Defer_state volatile*, int const, defer_handler*const);
int _Defer_exchange(_Defer_int volatile*, int);
void _Defer_store(_Defer_int volatile*, int);
int _Defer_load(_Defer_int volatile*);
void _Defer_fence(_Defer_int volatile*);

void _Defer_exit_wrapper(int, defer_handler*);

int _Defer_language2(int, _Defer_state volatile _P[static 1]);
int _Defer_language(int);

#define _Defer_show_std(F) do {                                         \
    if (_Hdl == (F)) fputs("panic with library function " #F "\n", stderr); \
} while(0)

#define _Defer_show_wrap(F) do {                                         \
    if (_Hdl == (__real_ ## F)) fputs("panic with library function " #F "\n", stderr); \
} while(0)

#define _Defer_show_signo_case(X)                                       \
  case DEFER_ ## X: fputs("panic by signal SIG" #X "\n", stderr); break

inline
void (defer_show)(int a){
  char const* _Str = _Defer_head._Str;
  if (!_Str) {
    if (_Defer_head._Act == _DEFER_EXIT) {
      return;
    }
    _Str = "panic from unknown location";
  }
  fputs(_Str, stderr);
  fputc('\n', stderr);
  if (a < 0) {
    if (-a < _DEFER_EMAX) {
      int err = errno;
      errno = -a;
      perror("panic error condition");
      if (errno != -a) {
        perror("perror has produced error");
      }
      errno = err;
      if (err && err != -a) {
        perror("panic previous errno");
      }
    } else {
      switch (-a) {
        _Defer_show_signo_case(ABRT);
        _Defer_show_signo_case(FPE);
        _Defer_show_signo_case(ILL);
        _Defer_show_signo_case(INT);
        _Defer_show_signo_case(SEGV);
        _Defer_show_signo_case(TERM);
        _Defer_show_signo_case(KILL);
      default: fputs("panic by unknown signal\n", stderr); break;
      }
    }
  } else if (a == 0) {
    fputs("exit or similar unrecoverable termination of thread or whole execution\n", stderr);
  } else {
    static char const format[] = "panic with numerical condition: %d\n";
    enum { bufsize = 30 + sizeof format, };
    char buffer[bufsize];
    snprintf(buffer, bufsize, format, a);
    fputs(buffer, stderr);
  }
  defer_handler* _Hdl = _Defer_head._Hdl;
  _Defer_show_std(exit);
  _Defer_show_std(quick_exit);
#ifndef __STDC_NO_THREADS__
  _Defer_show_std(thrd_exit);
#endif
  _Defer_show_wrap(exit);
  _Defer_show_wrap(quick_exit);
#ifndef __STDC_NO_THREADS__
  _Defer_show_wrap(thrd_exit);
#endif
  _Defer_show_std(_Exit);
  _Defer_show_std(_Defer_abort);
}

_Noreturn
inline
void _Defer_panic3(_Defer_state volatile* _Defer_headp, int const _Code, defer_handler*const _Hdl) {
  UNUSED int const _Defer_lang = _Defer_headp->_Language;
  if (_Defer_headp->_Head) {
    if (_Hdl) _Defer_headp->_Hdl = _Hdl;
    _Defer_headp->_Code = _Code;
    _Defer_store(&_Defer_headp->_Act, _DEFER_FUNC);
    _Defer_longjmp();
    fputs("panic reached unreachable code after unwind\n", stderr);
  } else {
    _Hdl(_Code);
    fputs("panic reached unreachable code after application provided handler\n", stderr);
  }
  abort();
}

void _Defer_trace(char const* ID, _Defer_state volatile*const _Defer_headp) {
  defer_handler*const _Hdl = _Defer_headp->_Hdl;
  int _Code = _Defer_code(_Defer_headp);
  if (_Code && (!_Hdl || _Hdl == _Defer_abort || _Hdl == _Defer_panic_default)) {
    fputs("unwinding ", stderr);
    fputs(ID, stderr);
    fputc('\n', stderr);
  }
}

void defer_sig_flag(int sig) {
  _Defer_store(&_Defer_head._Act, sig);
}

void defer_sig_jump(int sig) {
  _Defer_state volatile*const _Defer_headp = &_Defer_head;
  _Defer_store(&_Defer_headp->_Act, sig);
  _Defer_headp->_Str = _Defer_ID " in signal handler";
  if (_Defer_headp->_Head) {
    int const _Defer_lang = _Defer_headp->_Language;
    _Defer_headp->_Hdl = defer_get_handler();
    _Defer_longjmp();
    fputs(_Defer_ID " defer_sig_jump reached unreachable code after unwind\n", stderr);
  } else {
    (defer_get_handler())(-DEFER_SIGNO(sig));
    fputs(_Defer_ID " defer_sig_jump reached unreachable code after panic\n", stderr);
  }
  abort();
}

_Noreturn
void _Defer_panic_default(int a){
  defer_show(a);
  _Defer_system_exit(a);
}

_Noreturn void defer_exit(int);
_Noreturn void defer_quick_exit(int);
#ifndef __STDC_NO_THREADS__
_Noreturn void defer_thrd_exit(int);
#endif

#ifndef DEFER_NO_WRAP
#pragma weak __wrap_exit = defer_exit
#pragma weak __wrap_quick_exit = defer_quick_exit
#ifndef __STDC_NO_THREADS__
#pragma weak __wrap_thrd_exit = defer_thrd_exit
#endif
#endif

void* defer_malloc(size_t);
void* defer_calloc(size_t, size_t);
void* defer_realloc(void *, size_t);
void* defer_aligned_alloc(size_t, size_t);

#ifdef DEFER_MALLOC_WRAP
#pragma weak __wrap_malloc = defer_malloc
#pragma weak __wrap_calloc = defer_calloc
#pragma weak __wrap_realloc = defer_realloc
#pragma weak __wrap_aligned_alloc = defer_aligned_alloc
#endif

defer_handler* defer_set_handler(defer_handler* _Hdl) {
  static _Atomic(defer_handler*) _Defer_handler_default = _Defer_panic_default;
  if (_Hdl)
    return atomic_exchange_explicit(&_Defer_handler_default, _Hdl, memory_order_acq_rel);
  else
    return atomic_load_explicit(&_Defer_handler_default, memory_order_acquire);
}

void _Defer_panic_cpp(int err, defer_handler* hdl) {
  _Defer_state volatile*const _Defer_headp = &_Defer_head;
  int const _Defer_lang = _Defer_language_C;
  if (!_Defer_headp->_Str)
    _Defer_headp->_Str =
      err
      ? "panic issued from C++"
      : "exit or similar issued from C++";
  if (!_Defer_load(&_Defer_headp->_Act)) {
    _Defer_store(&_Defer_headp->_Act, err ? _DEFER_FUNC : _DEFER_EXIT);
  }
  if (hdl) _Defer_headp->_Hdl = hdl;
  defer_show(err);
  if (err) {
    _Defer_language2(_Defer_lang, _Defer_headp);
    _Defer_panic3(_Defer_headp, err, defer_get_handler());
  }
  _Defer_longjmp();
}

UNUSED static _Noreturn void dummy(int err, int act) {
  abort();
}

#pragma weak _Defer_throw = dummy

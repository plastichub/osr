/* -*- C -*-  */

/*
 * A first (reference, ... whatever) implementation of a defer feature.
 *
 * This started with an implementation by Freek Wiedijk and was
 * completed by Jens Gustedt.
 */

/*
 * BEWARE: the real source file is the file ending in .hs, it is
 * processed by shnell to produce the .h file. So don't modify the .h
 * file.
 */


/**
 ** @file
 **
 ** @brief an implementation of a guard/defer/panic/recover feature set
 **
 ** @code
 **
 **  guard {
 **    void*const p = malloc(25);
 **    defer {
 **      free(p);
 **    }
 **
 **    void*const q = malloc(25);
 **    defer {
 **      free(q);
 **    }
 **
 **    if (something_bad) {
 **      break;
 **    }
 **
 **    ...
 **  }
 **
 ** @endcode
 **
 ** Is equivalent to something like
 **
 ** @code
 **
 ** {
 **    void*const p = malloc(25);
 **    if (false) {
 **      DEFER0_START:;
 **      free(p);
 **      goto GUARDED_END;
 **    }
 **
 **    void*const q = malloc(25);
 **    if (false) {
 **      DEFER1_START:;
 **      free(q);
 **      goto DEFER0_START;
 **    }
 **
 **    if (something_bad) {
 **      goto DEFER1_START;
 **    }
 **    ...
 **    goto DEFER1_START;
 **    GUARDED_END:;
 ** }
 **
 ** @endcode
 **
 ** That is, defer clauses inside a guarded block are not executed
 ** directly, but only later when the execution of the whole block
 ** ends, either "naturally" or either because its execution is
 ** interrupted by a `break` or `defer_break`.
 **
 ** This principle of deferred execution extends to nested
 ** guarded blocks in a natural way, even if they are stacked in
 ** different function calls.
 **
 ** There are three "severity" levels for termination of a
 ** guarded block, `break` just terminates the guarded block,
 ** `return` terminates all guarded blocks of the same function
 ** and `panic` of the same thread across all function calls.
 **
 ** A deferred statement can be triggered in two ways.
 **
 ** - explicitly: by leaving a guarded block when coming to the end of
 **   it or by using `break`, `defer_break`, `exit`
 **   ... from within.
 **
 ** - implicitly: from a signal handler. Two signal handlers are
 **   provided `defer_sig_flag` and `defer_sig_jump`. Their actions
 **   are as their names indicate. The first just sets a flag and the
 **   user code then typically investigates that flag and acts
 **   accordingly. This would typically be used for a signal like
 **   `SIGINT` where the user interrupts the program execution. The
 **   second handler jumps to the first defer clause on the defer
 **   stack and triggers a `panic`. This one is more appropriate
 **   for signals that are related to faulty operations that by
 **   themselves are not recoverable.
 **
 **   This implementation does not install any of these handlers by default.
 **
 ** Once execution starts inside a deferred statement, the condition that
 ** lead there can be investigated by means of `recover`. This returns
 ** an integer error code that indicates what happened. If it is `0`,
 ** this execution of the defer clause is just "normal" retreat as of
 ** `break`, `return` or `exit`. If it is any other value
 ** something "remarkable" might have happened, generally a
 ** `panic` is on its way down in the call stack.
 **
 ** Once an error condition has been recovered, the responsibility to
 ** handle the error is passed back to the user. If they don't want
 ** that they may re-issue the panic by calling `panic(code,
 ** 0)`, where the `0` indicates that the same final action (e.g
 ** `exit`) as before the recovery is used.
 **
 ** For the error values there is a convention that user error codes
 ** (e.g send with `exit`) are always positive, and that
 ** "system" error codes are generally negated `errno` numbers. (For
 ** signal numbers there is a sophisticated encoding in even smaller
 ** negative values.)
 **
 ** Two convenience macros ease the use of this feature, `defer_if` as
 ** a pseudo selection statement, and `recover_signal` to
 ** recover just signals and do nothing if there was none.
 **
 ** As examples how this could be used by the C library, there are
 ** also augmented versions of storage allocation function, such as
 ** `defer_malloc`. The idea that these will panic if they encounter
 ** an out-of-memory condition. That has several effects
 **
 ** - Explicit handling of such error conditions has not to be
 **   repeated at every call site.
 **
 ** - Cleanup actions that the user has installed by means of defer clause
 **   will be called, e.g to close files or to free large allocations.
 **
 ** - User code may establish a recovery mechanism through
 **   `recover` or alike. This will probably be rarely used by
 **   "normal" applications, but security critical applications would
 **   get a handle to avoid catastrophes, here.
 **
 ** Inspection of the assembler that is created for these functions
 ** shows that all of this generates very little overhead for the fast
 ** execution path. In general, this technique might provide a
 ** sensible way to provide the same error detection facilities as
 ** Annex K, but in a way that
 **
 **  - preserves the prototypes of the functions
 **
 **  - is thread safe.
 **
 ** The important interfaces of this tool are:
 **
 **  - `guard` prefixes a guarded block
 **  - `defer` prefixes a defer clause
 **  - `break` ends a guarded block and executes all its defer clauses
 **  - `return` unwinds all guarded blocks of the current function and returns to the caller
 **  - `exit` unwinds all defer clauses of all active function calls of the thread and exits normally
 **  - `panic` starts global unwinding of all guarded blocks
 **  - `recover` inside a defer clause stops a panic and provides an error code
 **
 ** The features `exit`, `quick_exit` and `thrd_exit` are overloaded
 ** to do the unwinding as soon as this header is included. To be
 ** consistent when such code is linked to legacy object files that
 ** have been compiled differently, you should wrap the system
 ** calls. This can usually be done by providing `-wrap=exit` etc
 ** arguments to the linker. (You'd have to check your local manual
 ** for that.) This feature may (but shouldn't) be switched off by
 ** providing the environment variable `DEFER_NO_WRAP` to the build.
 **
 ** In a similar way, this implementation also offers to wrap the
 ** allocation functions of the C library, but the default here is the
 ** other way around: per default `malloc` and Co are not wrapped. You
 ** may enable this by setting `DEFER_MALLOC_WRAP`. Otherwise, you may
 ** use functions `defer_malloc` and similar to catch allocation
 ** errors.
 **
 ** This implementation distinguishes "jumps" that are known to target
 ** the same function (`_Defer_shrtjmp`) and those that are known to
 ** jump to another function on the call stack (`_Defer_longjmp`). The
 ** unwind for a `return` will usually be all short jumps, whereas
 ** `exit` always initiates a long jump.
 **
 ** The implementation can distinguish cases where basically all jumps
 ** are finally implemented as being long, or platforms where some
 ** shortcut for a short jump can be taken. Currently this is only
 ** implemented for gcc and friends that implement so-called "computed
 ** `goto`", that is labels for which addresses can be taken, and
 ** where these addresses then can be used in an extended `goto`
 ** feature. Such a specialized implementation can gain a lot on the
 ** unwind side (these then are mostly simple jumps), but they still
 ** have to keep track of all the guarded blocks and defer clauses
 ** with `setjmp` because these could be jumped to from other
 ** functions or from signal handlers.
 **
 ** This implementation is special, because it is a "header only"
 ** implementation that overloads `return` (plus some others) with a
 ** macro. This is not ideal and can have some performance issues or
 ** even compilation failures with system-provided inline
 ** functions. As a rule of thumb, try to include this header as late
 ** as possible, such that there are as few interactions as possible.
 **/


#ifndef __STDDEFER_H__
#define __STDDEFER_H__ 1

#include <setjmp.h>
#include <stdlib.h>
#include <signal.h>
#include <stdint.h>

#ifndef __STDC_NO_ATOMICS__
#include <stdatomic.h>
#endif
#ifndef __STDC_NO_THREADS__
#include <threads.h>
#endif

#include "stddefer_codes.h"

#define _DEFER_NAME1(P, L) P ## L
#define _DEFER_NAME0(P, L) _DEFER_NAME1(P, L)

/** @brief internal macro to produce a unique name depending on @a X and the logical line number **/
#define _DEFER_NAME(X) _DEFER_NAME0(_DEFER_UNIQUE_ ## X ## _, __LINE__)

#ifndef DEFER_MALLOC_WRAP
#define __real_malloc(X) (malloc)(X)
#define __real_calloc(X, Y) (calloc)((X), (Y))
#define __real_realloc(X, Y) (realloc)((X), (Y))
#define __real_aligned_alloc(X, Y) (aligned_alloc)((X), (Y))
#define _Defer_system_alloc(X) (malloc)(X)
#define _Defer_system_calloc(X, Y) (calloc)((X), (Y))
#define _Defer_system_realloc(X, Y) (realloc)((X), (Y))
#define _Defer_system_aligned_alloc(X, Y) (aligned_alloc)((X), (Y))
#else
extern void* __real_malloc(size_t);
extern void* __real_calloc(size_t, size_t);
extern void* __real_realloc(void*, size_t);
extern void* __real_aligned_alloc(size_t, size_t);
#define _Defer_system_malloc(X) __real_malloc(X)
#define _Defer_system_calloc(X, Y) __real_calloc((X), (Y))
#define _Defer_system_realloc(X, Y) __real_realloc((X), (Y))
#define _Defer_system_aligned_alloc(X, Y) __real_aligned_alloc((X), (Y))
#endif

// If we have lock-free atomics use an exchange operation to recover
// signal values.
#if (ATOMIC_INT_LOCK_FREE > 1) || (ATOMIC_SHORT_LOCK_FREE > 1)

// In the case that we have lock-free atomics, we use acquire/release
// semantics. By that we do not necessarily force all of this into a
// global order between all threads, but only the users of the
// variable (the thread itself and/or signal handlers) will
// synchronize on the data.

inline
int _Defer_exchange(_Defer_int volatile* p, int val) {
    atomic_signal_fence(memory_order_release);
    int _Ret = atomic_exchange_explicit(p, val, memory_order_acq_rel);
    atomic_signal_fence(memory_order_acquire);
    return _Ret;
}

inline
void _Defer_store(_Defer_int volatile* p, int val) {
    atomic_signal_fence(memory_order_release);
    atomic_store_explicit(p, val, memory_order_release);
}

inline
int _Defer_load(_Defer_int volatile* p) {
    int _Ret = atomic_load_explicit(p, memory_order_acquire);
    atomic_signal_fence(memory_order_acquire);
    return _Ret;
}

inline
void _Defer_fence(_Defer_int volatile* p) {
    int expected = 0;
    atomic_compare_exchange_weak_explicit(p, &expected, expected, memory_order_acq_rel, memory_order_acquire);
    atomic_signal_fence(memory_order_acquire);
}

#else

inline
int _Defer_exchange(_Defer_int volatile* p, int val) {
    int _Ret = *p;
    *p = val;
    return _Ret;
}

inline
void _Defer_store(_Defer_int volatile* p, int val) {
    *p = val;
}

inline
int _Defer_load(_Defer_int volatile* p) {
    return *p;
}

inline
void _Defer_fence(_Defer_int volatile* p) {
    (void)*p;
}

#endif

struct _Defer_Forbidden_Nesting {
    int _A;
};
extern int _Defer_Nesting;

extern double _Defer_Inside;

typedef struct _Defer_buf _Defer_buf;

/**
 ** @brief Internal structure to chain defer registrations.
 **/
struct _Defer_buf {
    jmp_buf _Env;
    /* These never change once the are initialized, but we have to be
         able to initialize them by assignment. */
    _Defer_buf* _Next;
    _Defer_codes const _Lev;
    void const* _Frame;
    void const* _Label;
    void const* _Capture;
};

#if __STDC_VERSION__ > 202000L
# define UNUSED [[__maybe_unused__]]
#elif __GNUC__
# define UNUSED __attribute__((__unused__))
#endif

#if __STDC_VERSION__ > 202000L
# define MALLOC [[__noalias__]]
#elif __GNUC__
# define MALLOC __attribute__((__malloc__,__warn_unused_result__))
#endif

#if __GNUC__
# define LIKELY(X) __builtin_expect((X), 1)
# define UNLIKELY(X) __builtin_expect((X), 0)
# define ALWAYS_INLINE __attribute__((__always_inline__))
# define _DEFER_COMPUTED_GOTO
# define _Defer_frame_address() __builtin_frame_address(0)
# define _Defer_frame_top(P) (!(P) || !(P)->_Next || ((P)->_Frame && ((P)->_Frame != (P)->_Next->_Frame)))
# define defer_function_fallback
#endif

#ifndef UNUSED
# define UNUSED
#endif

#ifndef LIKELY
# define LIKELY(X) (X)
#endif

#ifndef UNLIKELY
# define UNLIKELY(X) (X)
#endif

#ifndef ALWAYS_INLINE
# define ALWAYS_INLINE
#endif

#ifndef _Defer_frame_address
# define _Defer_frame_address() ((void const*)0)
#endif

#ifndef _Defer_frame_top
# define _Defer_frame_top(P) (!P || ((P)->_Lev == _DEFER_FUNC))
#endif

#ifndef defer_function_fallback
# define defer_function_fallback guard
#endif

typedef void defer_handler(int);

/**
 ** @brief Set an execution wide default termination handler for `panic`
 **
 ** This is an overall default handler that determines the action that
 ** is taken when an application panics and no handler has been
 ** provided as a second argument to `panic`.
 **
 ** This is *not* suited as something that is to be meddled with by
 ** libraries, because different libraries may have different
 ** strategies and would thus step onto each other's feet. Libraries
 ** that need a particular termination strategy should use the second
 ** argument to `panic` to promote their handler during an occurring
 ** panic.
 **
 ** This *is* suited when your application needs better or less debug
 ** information and you know how to provide that information.
 **
 ** `_Hdl` should be a null pointer (in which case nothing is changed)
 ** or a pointer to a function that does not return and that
 ** terminates the whole execution. If a handler is used that does not
 ** have these properties and a panic is triggered that uses the
 ** default handler, the behavior of the execution is undefined.
 **
 ** This particular function keeps track of an internal state that
 ** holds the latest handler that was provided. The function returns
 ** that previous handler regardless if the global state has been
 ** successfully changed or not.
 **
 ** If `_Hdl` is a null pointer the global state is not changed and
 ** only the previous value is returned.
 **
 ** Although this function potentially changes global state, it is
 ** thread safe. If atomic pointer types are lock-free this function
 ** is also AS safe.
 **/
extern defer_handler* defer_set_handler(defer_handler* _Hdl);

/**
 ** @brief Return the current default termination handler.
 **
 ** This function is thread safe. If atomic pointer types are
 ** lock-free this function is also AS safe.
 **/
#define defer_get_handler() defer_set_handler(0)

typedef struct _Defer_state _Defer_state;

UNUSED static void const*const _DEFER_CAPT = 0;

/**
 ** @brief A thread specific top level defer registration list.
 **/
struct _Defer_state {
// @brief the last registered defer clause or guarded block
    _Defer_buf* _Head;
// This field is only used to free an element after its use.
    _Defer_buf* _Latest;
// the current programming language, used to mark transitions from
// one language to another
    int _Language;
// @brief The level of unwinding that has been requested
//
// If 0, the request was a `break` or similar and unwinding
// will only concern the current guarded block.
// If this is one of the special values `_DEFER_BLCK` or
// `_DEFER_FUNC` an unwind of all guards in the current function of
// of all functions has been requested. Otherwise it should be a
// signal number of a signal that has been caught.
    _Defer_int _Act;
// @brief The handler that will be called if a panic is not recovered.
    defer_handler* _Hdl;
// @brief The error code that will be called if a panic is not
// recovered, and that will be returned by `recover`.
    _Defer_codes _Code;
// @brief A string that identifies the origin of the last panic.
//
// Usually this should be pointing to a static string, such that the
// panic mechanism does not need to make an allocation.
    char const* _Str;
// If defer is used within a function without surrounding guard, we may need a buffer.
    _Defer_buf _Top;
};

/**
 ** @brief The thread local list of guarded blocks and defer clauses.
 **
 ** This thread local symbol is in generally only accessed by top
 ** level guarded blocks, and the address is then propagated to the
 ** other constructs. This is to avoid recovery of thread local
 ** information which might be a bit costly on some architectures, in
 ** particular in a shared library context.
 **
 ** Unfortunately, such a variable must have linkage, such that
 ** functions from different TU share the same variable. This is the
 ** main reason that there is even a proper TU (stddefer.c) associated
 ** to this header file. Otherwise we would probably be just fine by
 ** declaring all functions `static inline`. This restriction could
 ** potentially be removed on architectures that have weak symbols.
 **/
extern _Thread_local _Defer_state _Defer_head;

/** @brief a dummy variable that is only used for its type within the
 ** _DEFER_HEADP macro.
 **/
extern void*const _Defer_headp;

/**
 ** This is an internal macro that establishes a naming convention to
 ** access the thread local list.
 **/
#define _DEFER_HEADP _Generic(_Defer_headp, void*: &_Defer_head, default: _Defer_headp)

#define _Stringify(...) _Stringify0(__VA_ARGS__)
#define _Stringify0(...) #__VA_ARGS__

/**
 ** @brief Unwind the whole call stack and execute the defer clauses
 ** on the way down.
 **
 ** In the last stack frame, `F(C)` is executed, where @a F (the
 ** second argument) has the signature `defer_handler` and @a C (the
 ** first argument) is an `int` error code.
 **
 ** This unwind chain can be stopped with a `recover` call or a
 ** `defer_if` clause.
 **
 ** The error code should be negative if this is supposed to map to a
 ** system defined error condition as is for `errno`. Otherwise, all
 ** user supplied error numbers should be positive.
 **
 ** When unwinding an error condition triggered by the system, the
 ** error code will always be negative.
 **
 ** The second argument @a F is optional and defaults to
 ** `panic`, a function with the same name as this macro, see
 ** below.
 **/
#define panic(...) _Defer_panic4(__VA_ARGS__, defer_get_handler(), )

#define _Defer_ID "panic at " __FILE__ ":" _Stringify(__LINE__)

#define _Defer_panic4(C, F, ...) ( _DEFER_HEADP->_Str = _Defer_ID " with error: " #C, _Defer_panic3(_DEFER_HEADP, C, F) )

/**
 ** @brief Conditionally panic on a given condition @a C and error code @a E.
 **
 ** This is similar to a usual call to the `assert` macro in that it
 ** asserts that condition @a C the first argument holds. It cannot be
 ** switched off at compile time but the panic that is triggered can
 ** be recovered.
 **
 ** @code
 ** defer_assert(p, -DEFER_ENOMEM, "unexpected allocation failure");
 ** @endcode
 **/
#define defer_assert(...) _Defer_assert(__VA_ARGS__, "")

#define _Defer_assert(C, E, S, ...)                                                                   \
do {                                                                                                  \
    static char const _DEFER_NAME(ASSERT)[] = _Defer_ID " for condition '" #C "' error '" #E "': " S; \
    if (UNLIKELY(!(C))) {                                                                             \
        _DEFER_HEADP->_Str = _DEFER_NAME(ASSERT);                                                     \
        _Defer_panic3(_DEFER_HEADP, E, defer_get_handler());                                          \
    }                                                                                                 \
} while(0);

extern
void _Defer_trace(char const*, _Defer_state volatile*const);

/**
 ** @brief Equivalent to `abort()` but first execute all active
 ** defer clauses in all functions of this thread.
 **
 ** The unwinding of the call stack can be halted with `recover`
 ** or `defer_if`. The error code recovered is then `-DEFER_ABORT`.
 **/
#define defer_abort() panic(-DEFER_ABRT, _Defer_abort)

_Noreturn
void _Defer_panic3(_Defer_state volatile* _Defer_headp, int const _Code, defer_handler*const _Hdl);

_Noreturn
inline
void _Defer_abort(UNUSED int a) {
    abort();
}

/**
 ** @brief Show information about the latest `panic` on `stderr`.
 **
 ** This should usually be used as diagnosis after a `recover`
 ** to provide feedback to the user what went wrong with the
 ** execution.
 **/
void (defer_show)(int a);


/**
 ** @brief This is the default action that is undertaken when a panic
 ** is not recovered.
 **
 ** Usually this should print some information (by means of
 ** `defer_show`) and then exit the execution with an error code of
 ** `a`.
 **/
_Noreturn
void _Defer_panic_default(int a);


/**
 ** @brief Push the argument `_N` on the list of current defer clauses and
 ** guarded blocks.
 **
 ** This is a very small function of just some instructions, so it
 ** will generally be inlined. It is also declared `static` such that
 ** all code within the same TU sees the same compilation options.
 **/
inline
ALWAYS_INLINE
_Defer_buf* _Defer_push(_Defer_state volatile _P[static 1], _Defer_buf* _N) {
    _Defer_buf* _X = _P->_Head;
    _P->_Head = _N;
    return _X;
}

inline
ALWAYS_INLINE
_Defer_buf* _Defer_new(_Defer_state volatile _P[static 1], void const* _Label, void const* _Capture, void const* _Frame) {
    _Defer_buf*const _New = _Defer_system_calloc(1, sizeof *_New);
    if (_New) {
        _New->_Next = _Defer_push(_P, _New);
        _New->_Label = _Label;
        _New->_Capture = _Capture;
        _New->_Frame = _Frame;
    }
    return _New;
}

inline
ALWAYS_INLINE
void _Defer_delete_latest(_Defer_state volatile _P[static 1]) {
    free(_P->_Latest);
    _P->_Latest = 0;
}

inline
int _Defer_language2(int _Language, _Defer_state volatile _P[static 1]) {
    int ret = _P->_Language;
    _P->_Language = _Language;
    return ret;
}


inline
int _Defer_language(int _Language) {
    return _Defer_language2(_Language, _DEFER_HEADP);
}

extern _Noreturn void _Defer_throw(int err, int act);

/**
 ** @brief Pop the top level state from the list.
 **
 ** This is a very small function of just some instructions, so it
 ** should generally be inlined.
 **
 ** It has one error condition, that is when the list head is
 ** empty. This should never occur with guarded block or defer clause
 ** because there it is only called because we know that there is an
 ** element in the list. Such a condition would be a real bug that
 ** would have the whole defer mechanism compromised. So that would be
 ** nothing that user code should even try to recover from.
 **/
inline
ALWAYS_INLINE
_Defer_buf* _Defer_pop(_Defer_state volatile _P[static 1], int _Language) {
    _Defer_language2(_Language, _P);
    int _Code = _P->_Code;
    if (_Language == _Defer_language_Cpp) {
        defer_show(_Code);
        _Defer_throw(_Code, _Defer_load(&_P->_Act));
    }
    _Defer_buf* _X = _P->_Head;
    if (UNLIKELY(!_X)) {
        (defer_get_handler())(_Code);
    }
    _P->_Latest = _X;
    _P->_Head = _X->_Next;
    return _X;
}

/**
 ** @brief Mark a start of a guarded defer.
 **
 ** This can only be used at very restricted places, see clause
 ** 7.13.1.1 of the C standard for details.
 **
 ** In the following we only use it or its logical negation as the
 ** direct controlling expression in an `if` or `for` statement.
 **
 ** For both constructs, `guard` and `defer`, the idea is that
 ** the depending block statement is visited twice: once by returning
 ** immediately and once by executing the dependent statement.
 **
 ** `guard` does this execution of the dependent statement
 ** first, and whenever control returns to it by means of an
 ** unwind event it will just do nothing.
 **
 ** `defer` does nothing at the first encounter, and then, visited a
 ** second time because of a panic or the end of the guarded block, it will execute
 ** the dependent statement.
 **/
#define _Defer_mark() setjmp(_DEFER_HEADP->_Head->_Env)

inline
void (_Defer_report)(char const* _Defer_str) {
    extern int puts(char const*);
    puts(_Defer_str);
}

#ifndef _DEFER_DEBUG
# define _Defer_report(X) ((void)0)
#endif


/**
 ** @brief End a guarded block or defer clause and execute all
 ** previously encountered defer clauses up to the limit of the
 ** guarded block.
 **
 ** This effectively jumps to the first item in the defer list, which
 ** may be a guarded block or a defer clause. If the defer list is empty
 ** and a defer handler has been set, the handler is called.
 **
 ** The list is then worked down, because the guarded blocks or clauses
 ** to which the execution jumps executes this macro in turn.
 **
 ** This macro uses `_Defer_headp` (and not `_DEFER_HEADP`) such that
 ** it only may be called from the inside of a guarded block or a
 ** defer clause.
 **/
#define _Defer_longjmp() (_Defer_report("longjmp"), longjmp(_Defer_pop(_Defer_headp, _Defer_lang)->_Env, 1))



#ifdef _DEFER_COMPUTED_GOTO

/* This specialized implementation build on several features:
 *
 * - computed `goto` to addresses of labels, using a `&&` operator
 * - block expressions
 * - the fact that `__LINE__` expands to the same number for a multi-line macro
 */

#define _DEFER_ADDR(X) &&_DEFER_NAME(X)
#define _DEFER_LABEL(X) _DEFER_NAME(X):

#define _Defer_shrtjmp()                                                      \
({                                                                            \
_Defer_buf* _DEFER_NAME(P) = _Defer_pop(_Defer_headp, _Defer_language_C); \
goto *(_DEFER_NAME(P)->_Label);                                           \
})

#define defer_break ({                                              \
_Defer_buf* _DEFER_NAME(P) = _Defer_pop(_Defer_headp, _Defer_lang); \
if (_Generic(_Defer_Inside, int: 1, default: 0)) {                  \
goto *(_DEFER_NAME(P)->_Label);                             \
}                                                               \
/* This is needed to convince clang that computed goto */           \
/* is allowed in this function. */                                  \
_DEFER_LABEL(UNWIND)                                                \
(void)_Generic(_Defer_headp,                                        \
double*: _DEFER_ADDR(UNWIND),                        \
default: 0);                                         \
_Defer_report("longjmp from defer_break");                          \
longjmp(_DEFER_NAME(P)->_Env, 1);                                   \
})

#endif


#ifndef _DEFER_ADDR
# define _DEFER_ADDR(X) ((void*)0)
#endif

#ifndef _DEFER_LABEL
# define _DEFER_LABEL(X)
#endif


// Set the default values for platforms that have no special treatment

#ifndef defer_break
#define defer_break _Defer_longjmp()
#endif

#ifndef _Defer_shrtjmp
#define _Defer_shrtjmp() _Defer_longjmp()
#endif

/**
 ** @brief Ensure the execution of the depending statement, the "defer
 ** clause", at the end of the guarded block.
 **
 ** Defer clauses may not themselves contain guarded blocks or other
 ** defer clauses, and shall not call functions that may result in a
 ** `panic`.
 **
 ** Local variables that may change after this defer invocation and
 ** that are used in the defer clause must be declared `volatile` such
 ** that the latest value is taken into account. So as a rule of
 ** thumb, variables that you use inside a defer clause or block
 ** should be qualified: `const` qualified for those where the defer
 ** clause should use the original value, and `volatile` qualified for
 ** those that should be used with their latest changes.
 **
 ** In addition, to have a determinate value such automatic variables
 ** should be defined in a scope that is either surrounding the
 ** current guarded block or be that guarded block itself (so they are
 ** alive when jumping into the defer clause), or be initialized (by
 ** initialization or by assignment) within the defer clause (and so
 ** are revived from within).
 **
 ** The implementation uses allocation as of `calloc` and `free` to
 ** maintain the list of defer clauses. In case that `calloc` fails,
 ** the defer clause is executed and then the whole execution is
 ** unwound by means of `panic` and an error argument of
 ** `-DEFER_ENOMEM`.
 **/
#define defer                                                                                                                                                      \
/* Bail out when nested badly. */                                                                                                                                  \
if (0) {                                                                                                                                                           \
_Static_assert(_Generic(_Defer_Nesting, int: 1, default: 0), "nested defer clauses are forbidden");                                                                \
}                                                                                                                                                                  \
else                                                                                                                                                               \
    /* The first levels enable what has to be run in front */                                                                                                      \
    for (int _DEFONCE = 0, _DEFER_NAME(OK) = 0; !_DEFONCE ;)                                                                                                       \
        /* Will be used to store a pointer to the capture, if any. */                                                                                              \
        for (UNUSED void const* _DEFER_NAME(CAPT0) = 0; !_DEFONCE ;)                                                                                               \
            /* Now, inhibit nesting inside. */                                                                                                                     \
            for (UNUSED struct _Defer_Forbidden_Nesting const _Defer_Nesting = { 0 }; !_DEFONCE ;)                                                                 \
                /* Receive a pointer to the state, either from a surrounding */                                                                                    \
                /* or from the top level thread local variable. */                                                                                                 \
                /* Give it a uniform name such other defer constructs find it. */                                                                                  \
                for (_Defer_state volatile*const _DEFER_NAME(UP) = _DEFER_HEADP, *const _Defer_headp = _DEFER_NAME(UP); !_DEFONCE ;)                               \
                    /* A defer clause cannot be top most in a function */                                                                                          \
                    for (UNUSED const int _Defer_Lev = _DEFER_BLCK, _Defer_lang = _Defer_language2(_Defer_language_C, _Defer_headp); !_DEFONCE ;)                  \
                        /* The buffer needs to be on the heap, because we might jump sideways. */                                                                  \
                        for (_Defer_buf*const _DEFER_NAME(NEW) = _Defer_new(_Defer_headp, _DEFER_ADDR(DEFER), _DEFER_CAPT, _Defer_frame_address()); !_DEFONCE++ ;) \
                            /* Ensure that the defer clause is run, even under allocation failure. */                                                              \
                            /* We have to consider two allocations, the first is the new state itself, the second */                                               \
                            /* is the capture state, but only if that was required. */                                                                             \
                    switch ((int)!(_DEFER_NAME(NEW) && (_DEFER_CAPT || _Generic(_DEFER_CAPT, void const*: 1, default: 0)))) case 0:                                \
/* setjmp must be the controlling expression and cannot occur in */                                                                                                \
/* logical expressions other than logical not.                   */                                                                                                \
if (!_Defer_mark()) {                                                                                                                                              \
    /* At first a defer clause does nothing but to ensure synchronization. */                                                                                      \
    _Defer_fence(&_Defer_headp->_Act);                                                                                                                             \
    break;                                                                                                                                                         \
    /* Convince the compiler that the label is used. */                                                                                                            \
    goto _DEFER_NAME(DEFER);                                                                                                                                       \
    /* We come here because this element has popped and is jumped to. */                                                                                           \
} else _DEFER_NAME(DEFER): for (                                                                                                                                   \
            /* Ensure data consistency. */                                                                                                                         \
            _Defer_fence(&_Defer_headp->_Act),                                                                                                                     \
            /* Recover a capture, if any. */                                                                                                                       \
            _DEFER_NAME(CAPT0) = _Defer_headp->_Latest->_Capture,                                                                                                  \
            _Defer_delete_latest(_Defer_headp);                                                                                                                    \
            /* Set this only when we jump to the clause. */                                                                                                        \
            (_DEFER_NAME(OK) = 1) ;                                                                                                                                \
            /* After that we have to jump to the next element. */                                                                                                  \
            /* If _DEFER_NAME(OK) is still 0 at the end, we switched */                                                                                            \
            /* into this loop because we were out of memory. */                                                                                                    \
            (LIKELY(_DEFER_NAME(OK)) ? (_Defer_headp->_Head ? defer_break : (void)0): panic(-DEFER_ENOMEM, defer_get_handler())))                                  \
            /* Switch to here if allocation fails. */                                                                                                              \
        default:                                                                                                                                                   \
                /* The dummy switch ensures correct "break". */                                                                                                    \
            switch (0) case 0:


/**
 ** @brief Mark a whole block as "guarded" and as using the defer mechanism.
 **
 ** If such a block is terminated normally or with a `break` or
 ** `continue` statement, all defer clauses that are registered with
 ** `defer` macro invocations are executed in reverse order of their
 ** registration. There is also a macro `defer_break` that can be used in
 ** contexts where `break` or `continue` statements would refer to an
 ** inner loop or `switch` statement. Also, `return`, `exit`,
 ** `quick_exit` and `thrd_exit` all trigger the execution of defer
 ** clauses, up to their respective levels of nestedness of guarded
 ** blocks.
 **
 ** Other standard means of non-linear control flow out of the block
 ** (`goto`, `longjmp`, `_Exit`, `abort`), do not invoke that
 ** mechanism and may result in memory leaks or other damage when used
 ** within such a guarded block.
 **
 ** For some of these constructs, there are replacements `defer_goto`,
 ** `defer_abort` etc that can be used instead.
 **/
#define guard                                                                                                                        \
/* Bail out when nested badly. */                                                                                                    \
if (0) {                                                                                                                             \
_Static_assert(_Generic(_Defer_Nesting, int: 1, default: 0), "forbidden guard statement nested inside defer clause");                \
}                                                                                                                                    \
else                                                                                                                                 \
    /* The first levels just serve to declare the variables */                                                                       \
    for (UNUSED int _DEFONCE = 0, _Defer_Inside = 1; !_DEFONCE ;)                                                                    \
        /* A guarded block can be top most in a function or internal */                                                              \
    for (const int _Defer_Lev = _Generic(_Defer_headp, void*: _DEFER_FUNC, default: _DEFER_BLCK); !_DEFONCE ;)                       \
                /* Receive a pointer to the state, either from a surrounding */                                                      \
                /* or from the top level thread local variable. */                                                                   \
                /* Give it a uniform name such other defer constructs find it. */                                                    \
                for (_Defer_state volatile*const _DEFER_NAME(UP) = _DEFER_HEADP, *const _Defer_headp = _DEFER_NAME(UP); !_DEFONCE ;) \
                    /* Save the previous language and set the current language to C. */                                              \
                    for (int const _Defer_lang = _Defer_language2(_Defer_language_C, _Defer_headp); !_DEFONCE ;)                     \
                        /* Now starts the real thing that we want to do here. */                                                     \
                        /* Define a local jump buffer and make it the head of the list. */                                           \
                        for (_Defer_buf _DEFER_NAME(HERE)                                                                            \
                        = {                                                                                                          \
                        ._Next = _Defer_push(_Defer_headp, &_DEFER_NAME(HERE)),                                                      \
                            ._Lev = _Defer_Lev,                                                                                      \
                            ._Frame = _Defer_frame_address(),                                                                        \
                            ._Label = _DEFER_ADDR(GUARDED),                                                                          \
                            ._Capture = _DEFER_CAPT,                                                                                 \
                        };;)                                                                                                         \
/* Ensure that all these loops are only run once. */                                                                                 \
if (_DEFONCE++) {                                                                                                                    \
    _DEFER_LABEL(GUARDED)                                                                                                            \
    /* After the battle, ensure synchronization and unwinding if that is requested. */                                               \
    if (_Defer_load(&_Defer_headp->_Act)) {                                                                                          \
        if (_Defer_Lev == _DEFER_FUNC) {                                                                                             \
            static char const _DEFER_NAME(TRACE_FAR)[] = "far, " _Defer_ID;                                                          \
            _Defer_trace(_DEFER_NAME(TRACE_FAR), _Defer_headp);                                                                      \
            _Defer_longjmp();                                                                                                        \
        } else {                                                                                                                     \
            static char const _DEFER_NAME(TRACE_NEAR)[] = "near, " _Defer_ID;                                                        \
            _Defer_trace(_DEFER_NAME(TRACE_NEAR), _Defer_headp);                                                                     \
            _Defer_shrtjmp();                                                                                                        \
        }                                                                                                                            \
    }                                                                                                                                \
    break;                                                                                                                           \
} else                                                                                                                               \
    /* This implements the defer mechanism itself. */                                                                                \
    for (; !_Defer_mark(); _Defer_shrtjmp())                                                                                         \
        /* The dummy switch ensures correct "break" for the clause. */                                                               \
    switch(0) default:


inline
int _Defer_recover_signal(_Defer_state volatile*const headp) {
    int _Code = headp->_Code;
    if (!_Code) {
        int _Act  = _Defer_load(&headp->_Act);
        if (UNLIKELY(_Act)) {
            switch (_Act) {
            case           0:
                break;
            case _DEFER_BLCK:
                break;
            case _DEFER_FUNC:
                break;
            case _DEFER_EXIT:
                break;
            default:
                return _Defer_exchange(&headp->_Act, 0);
            }
        }
    }
    return 0;
}

/**
 ** @brief Stop an unwind originating from a signal and/or return the
 ** signal number.
 **
 ** This is less general than `recover` because it only reacts
 ** on signals that are caught by the provided signal handlers
 ** (`defer_sig_flag` and `defer_sig_jump`), not on other
 ** `break` or `panic` events.
 **
 ** Nevertheless, when a defer signal occurs within a guarded block or
 ** defer clause and *is* *not* caught, the execution is considered to
 ** be compromised, a panic results, and the defer clauses of the
 ** thread are unwound.
 **
 ** @return the number of the signal that has been caught.
 **
 ** This can be used in any context and does not need a surrounding
 ** guarded block or defer clause. Its cost is the lookup of a thread
 ** local variable and a memory synchronization with that variable. So
 ** it should probably not be placed inside performance critical
 ** loops.
 **/
#define recover_signal() _Defer_recover_signal(_DEFER_HEADP)

inline
int _Defer_code(_Defer_state volatile*const headp) {
    int _Sig = _Defer_recover_signal(headp);
    return _Sig ? -DEFER_SIGNO(_Sig) : headp->_Code;
}

inline
int _Defer_recover(_Defer_state volatile*const headp) {
    if (_Defer_load(&headp->_Act) == _DEFER_EXIT) return 0;
    int _Code = _Defer_code(headp);
    if (_Code) {
        headp->_Code = 0;
        _Defer_store(&headp->_Act, 0);
    }
    return _Code;
}

/**
 ** @brief Stop a panic with non-zero error code (or similar) and/or
 ** return such a error code.
 **
 ** A call to this must reside within a defer clause.
 **
 ** This will only stop unwinding of the panic if the error code had
 ** not been `0`. Normal `return`, `exit`, `thrd_exit` etc will not be
 ** be stopped and the function, program or thread will finish when
 ** all defer clauses have been executed.
 **
 ** If the error code is non-zero, execution of the defer clause
 ** then continues as if the nearest guarded block had been broken by
 ** `break`. That is, the execution of the defer clauses of
 ** that particular guarded block are continued and then execution
 ** resumes at the end of that guarded block.
 **
 ** In general, error codes provided by the system will be negative
 ** and user provided error codes shall be positive. So to see if a
 ** system error occurred you may e.g compare the negated value to
 ** `ERANGE`, or, if the error originated from a caught signal to
 ** `DEFER_SIGNO(SIGTERM)`.
 **/
#define recover() _Defer_recover(_Defer_headp)

/**
 ** @brief Stop an unwind originating from a `panic` call or
 ** from a signal with non-zero error code (or similar), return the
 ** error code in a local `int` variable named `ERR`, and execute the
 ** depending statement if the value is non-zero, or an alternative
 ** `else` clause, if any, if the value is zero.
 **
 ** The variable `ERR` can then be used inside the `if` or `else`
 ** clauses, but it is not mutable and its address cannot be
 ** taken. (And it is a bit useless within the `else` clause, because
 ** we know that it is zero, there.)
 **
 ** In fact
 ** @code
 ** defer_if(err) statement-1 else statement-2
 ** @endcode
 ** would be equivalent to a defer clause
 ** @code
 ** defer if(register int const err = recover(); err) statement-1 else statement-2
 ** @endcode
 ** if such declarations inside `if` conditionals were a thing for C, yet.
 **
 ** @see recover for the main logic behind this
 **/
#define defer_if(ERR)                                       \
defer                                                       \
for (int _DEFONCE = 0; !_DEFONCE ;)                         \
    for (register int const ERR = recover(); !_DEFONCE++ ;) \
        if (ERR)

inline
_Defer_buf* _Defer_find(_Defer_state volatile _P[static 1], void* _Label) {
    for (_Defer_buf* p = _P->_Head; p; p = p->_Next) {
        if (_Defer_frame_top(p)) {
            if (_Label) p->_Label = _Label;
            return p;
        }
    }
    return 0;
}

inline
_Defer_buf* _Defer_toplevel(_Defer_state volatile _P[static 1], void const* _Label, void const* _Frame) {
    _Defer_buf* _End = 0;
// See if there is anybody to jump to, and if so, if this is really
// within the same function.
    if (_P->_Head && (_P->_Head->_Frame == _Frame)) {
        _End = _Defer_find(_P, 0);
        if (_End) {
            /* The toplevel in the function is never a guard.  Use the
                     thread-local buffer and link it in at the end. */
            _Defer_buf* _Top = (_Defer_buf*)&_P->_Top;
            _Top->_Next = _End->_Next;
            _End->_Next = _Top;
            _Top->_Frame = _Frame;
            _Top->_Label = _Label;
            _End = _Top;
        }
    }
    return _End;
}

/**
 ** @brief Execute the defer lists of all nested guarded blocks within
 ** the same function and resume execution directly after this macro
 ** to execute a depending jump statement such as `return` or `goto`.
 **
 ** This will do something if it is found inside a guarded block or
 ** not.  In any case it only should serve as a prefix to a statement
 ** that finishes all nested guarded blocks and jumps to another
 ** context. For the most common use there are macros that combine
 ** this with such a statement, notably `return` and `defer_goto`.
 **/
#define defer_unwind_function                                                                                  \
/* The implementation decides at compile time if this is found */                                              \
/* inside a guard or not and distinguishes two cases. */                                                       \
if (_Generic(_Defer_headp, void*: 0, void const*: 0, default: 1)) {                                            \
        /* Be sure that no previous error code remains */                                                      \
        _DEFER_HEADP->_Code = 0;                                                                               \
        /* If we are inside a guarded block, there is always something to find. */                             \
        if (!setjmp(_Defer_find(_DEFER_HEADP, _DEFER_ADDR(RETURN))->_Env)) {                                   \
            _Defer_store(&_DEFER_HEADP->_Act, _DEFER_BLCK);                                                    \
            _Defer_shrtjmp();                                                                                  \
        }                                                                                                      \
        goto _DEFER_NAME(RETURN);                                                                              \
    } else if (1) {                                                                                            \
    /* jump here per compile time decision if not in a guarded block */                                        \
    _Defer_state* _Defer_headp = &_Defer_head;                                                                 \
    /* Save the previous language and set the current language to C. */                                        \
    UNUSED int const _Defer_lang = _Defer_language2(_Defer_language_C, _Defer_headp);                          \
    /* Be sure that no previous error code remains */                                                          \
    _Defer_headp->_Code = 0;                                                                                   \
    _Defer_buf* _DEFER_NAME(TOP) = _Defer_toplevel(_Defer_headp, _DEFER_ADDR(RETURN), _Defer_frame_address()); \
    if (_DEFER_NAME(TOP)) {                                                                                    \
        if (!setjmp(_DEFER_NAME(TOP)->_Env)) {                                                                 \
            _Defer_store(&_Defer_headp->_Act, _DEFER_BLCK);                                                    \
            _Defer_shrtjmp();                                                                                  \
        }                                                                                                      \
    }                                                                                                          \
    goto _DEFER_NAME(RETURN);                                                                                  \
} else if (1) {                                                                                                \
    /* Restore the state as if nothing happened to perform a regular return hereafter */                       \
    _DEFER_LABEL(RETURN)                                                                                       \
    _Defer_store(&_Defer_head._Act, 0);                                                                        \
    goto _DEFER_NAME(RETURN2);                                                                                 \
} else                                                                                                         \
    _DEFER_LABEL(RETURN2)

/**
 ** @brief Execute all active defer clauses in the current function
 ** and jump to a label outside any guarded block.
 **
 ** This can be used wherever a regular `goto` statement can, but the
 ** label to which this jumps must be outside of all guarded blocks.
 **
 ** Generally, this has probably limited use and should be avoided.
 **/
#define defer_goto defer_unwind_function goto

#ifdef DEFER_NO_WRAP
#define __real_exit exit
#define __real_quick_exit quick_exit
#define __real_thrd_exit thrd_exit
#define _Defer_system_exit(X) (exit)(X)
#else
extern _Noreturn void __real_exit(int);
extern _Noreturn void __real_quick_exit(int);
extern _Noreturn void __real_thrd_exit(int);
#define _Defer_system_exit(X) __real_exit(X)
#endif


// We provide three wrappers for C library functions. These are the
// three terminating functions that are known to do cleanup work.

inline
_Noreturn
void _Defer_exit_wrapper(int _C, defer_handler* _Hdl) {
    _Defer_state* _Defer_headp = &_Defer_head;
    /* Save the previous language and set the current language to C. */
    UNUSED int const _Defer_lang = _Defer_language2(_Defer_language_C, _Defer_headp);
    if (_Defer_headp->_Head) {
        _Defer_headp->_Hdl = _Hdl;
        _Defer_headp->_Code = _C;
        _Defer_store(&_Defer_headp->_Act, _DEFER_EXIT);
        _Defer_longjmp();
    } else {
        _Hdl(_C);
// to satisfy the _Noreturn, should never be reached
        abort();
    }
}

inline
_Noreturn
void defer_exit(int _C) {
    _Defer_exit_wrapper(_C, __real_exit);
}

inline
_Noreturn
void defer_quick_exit(int _C) {
    _Defer_exit_wrapper(_C, __real_quick_exit);
}

#ifndef __STDC_NO_THREADS__
inline
_Noreturn
void defer_thrd_exit(int _C) {
    _Defer_exit_wrapper(_C, __real_thrd_exit);
}
#endif



/**
 ** @brief This overloads `exit(X)` by first executing all active
 ** defer clauses in all functions of this thread.
 **/
#define exit(X) defer_exit(X)

/**
 ** @brief This overloads `quick_exit(X)` by first executing all active
 ** defer clauses in all functions of this thread.
 **/
#define quick_exit(X) defer_quick_exit(X)

/**
 ** @brief This overloads `thrd_exit(X)` by first executing all active
 ** defer clauses in all functions of this thread.
 **/
#define thrd_exit(X) defer_thrd_exit(X)

/**
 ** @brief Equivalent to `_Exit(X)` but first execute all active
 ** defer clauses in all functions of this thread.
 **
 ** The unwinding of the call stack can be halted with `recover`
 ** or `defer_if`, but only if `X` is non-zero.
 **/
#define defer__Exit(X) panic(X, _Exit)

/**
 ** @brief A signal handler that flags the execution of a thread as
 ** erroneous.
 **
 ** This is not too intrusive and hands control for the signal back to
 ** the user code. Thus handling is delayed until the application code
 ** reaches the end of a guarded block and/or actively issues a
 ** `recover_signal` operation to receive the code.
 **
 ** Generally this might be used to handle signals triggered by the
 ** user via the terminal.
 **
 ** This is not suited for signals that jump back to exactly the same
 ** code location where some repair work has to be performed to
 ** continue, such as arithmetic or segmentation faults.
 **/
void defer_sig_flag(int sig);


/**
 ** @brief A signal handler that flags the execution of a thread as
 ** erroneous and jumps to the first defer clause.
 **
 ** This is intrusive and jumps back into the next available defer
 ** clause. Therefore this has to assume that the error is caught by
 ** the same thread that is to perform that defer clause, and not all
 ** platforms may guarantee this.
 **
 ** This is suited for signals that otherwise would jump back to
 ** exactly the same code location where some repair work has to be
 ** performed to continue, such as arithmetic or segmentation faults.
 **
 ** Probably the C standard function `signal` itself is not
 ** appropriate for this kind of handler, because signals should be
 ** switched off during the handling of such interrupts. Your platform
 ** might propose a better tool than `signal` for that. Be careful.
 **/
void defer_sig_jump(int sig);

/** @brief Wrap the library function and use `panic` on error. **/
inline
void*MALLOC defer_malloc(size_t size) {
    defer_assert(size, -DEFER_EINVAL, "zero size allocation in malloc");
    void* _Ret = __real_malloc(size);
    defer_assert(_Ret, -DEFER_ENOMEM, "heap memory exhausted in malloc");
    return _Ret;
}

/** @brief Wrap the library function and use `panic` on error. **/
inline
void*MALLOC defer_calloc(size_t nmemb, size_t size) {
    defer_assert(size, -DEFER_EINVAL, "zero size allocation in calloc");
    defer_assert(nmemb, -DEFER_EINVAL, "zero element allocation in calloc");
    void* _Ret = __real_calloc(nmemb, size);
    defer_assert(_Ret, -DEFER_ENOMEM, "heap memory exhausted in calloc");
    return _Ret;
}

/** @brief Wrap the library function and use `panic` on error. **/
inline
void*MALLOC defer_realloc(void *ptr, size_t size) {
    defer_assert(size, -DEFER_EINVAL, "zero size allocation in realloc");
    void* _Ret = __real_realloc(ptr, size);
    defer_assert(_Ret, -DEFER_ENOMEM, "heap memory exhausted in realloc");
    return _Ret;
}

/** @brief Wrap the library function and use `panic` on error. **/
inline
void*MALLOC defer_aligned_alloc(size_t alignment, size_t size) {
    defer_assert(size, -DEFER_EINVAL, "zero size allocation in aligned_alloc");
    defer_assert(alignment, -DEFER_EINVAL, "zero alignment in aligned_alloc");
    void* _Ret = __real_aligned_alloc(alignment, size);
    defer_assert(_Ret, -DEFER_ENOMEM, "heap memory exhausted in aligned_alloc");
    return _Ret;
}

/** @brief An empty capture list is simply ignored. **/
#define DEFER_CAPTURE0(...) defer

/**
 ** The other case when there is really something to capture consist
 ** of two parts. The first is to do the capture itself, the second is
 ** to reconstruct the captured variables locally with the frozen
 ** values.
 **
 ** For the case of a single capture, this is still relatively simple,
 ** so this is handcoded.
 **/

#define DEFER_CAPT1(IDX)                                                                                                    \
for (int _DEFONCE = 0; !_DEFONCE ;)                                                                                         \
    for (register __typeof__(struct { __typeof__(IDX) IDX; })*const _DEFER_CAPT = malloc(sizeof *_DEFER_CAPT); !_DEFONCE ;) \
        for (_DEFER_CAPT->IDX = IDX; !_DEFONCE++ ;)

#define DEFER_CONSTR1(IDX)                                                                        \
for (int _DEFONCE = 0; !_DEFONCE ;)                                                               \
    for (register __typeof__(*_DEFER_CAPT)const*const _Capture = _DEFER_NAME(CAPT0); !_DEFONCE ;) \
        for (register UNUSED __typeof__(_Capture->IDX) const IDX = _Capture->IDX; !_DEFONCE ;)    \
            for (register UNUSED void const*const _DEFER_CAPT = 0; !_DEFONCE ;)                   \
                /* Free the storage we needed for the capture as soon as possible. */             \
                for (free((void*)_Capture); !_DEFONCE ;)                                          \
                    /* Make the capture pointer invisible to the code. */                         \
                    for (register UNUSED void const*const _DEFER_CAPT = 0; !_DEFONCE++ ;)

#define DEFER_CAPTURE1(IDX) DEFER_CAPT1(IDX) defer DEFER_CONSTR1(IDX)

/**
 ** For the case of multiple captures the code is generate with
 ** shnell. For the moment the list of captures is restricted to 8
 ** identifiers.
 **
 ** First an inner do loop generates 7 parametrized copies of the macro
 ** definition code, and then another evaluation expands them to their
 ** full beauty.
 **/


/**
 ** @brief The initial capture code, generated by shnell.
 **/
#define DEFER_CAPT2(                                                               \
/* generate the parameter list of the macro */                                     \
ID1,                                                                               \
IDX                                                                                \
)                                                                                  \
for (int _DEFONCE = 0; !_DEFONCE ;)                                                \
for (                                                                              \
    /* Define the structure that caches all the data, and allocate storage. */     \
    /* Encapsulating this inside an external __typeof__ is necessary for clang. */ \
__typeof__(struct {                                                                \
__typeof__(ID1) ID1;                                                               \
    __typeof__(IDX) IDX;                                                           \
})* _DEFER_CAPT = malloc(sizeof *_DEFER_CAPT); !_DEFONCE ;)                        \
for (                                                                              \
    /* Initializes all the members of that structure. */                           \
    _DEFER_CAPT->ID1 = ID1,                                                        \
                 _DEFER_CAPT->IDX = IDX; !_DEFONCE++ ;)

/**
 ** @brief The initial reconstruction code, generated by shnell.
 **/
#define DEFER_CONSTR2(                                                                                         \
/* generate the parameter list of the macro */                                                                 \
ID1,                                                                                                           \
IDX                                                                                                            \
)                                                                                                              \
for (int _DEFONCE = 0; !_DEFONCE ;)                                                                            \
/* Make sure that the capture pointer points to the state that we are processing. */                           \
for (register __typeof__(*_DEFER_CAPT)const*const _DEFER_NAME(CAPTURE) = _DEFER_NAME(CAPT0); !_DEFONCE ;)      \
/* Instaure local variables for all captures. */                                                               \
for (register UNUSED __typeof__(_DEFER_NAME(CAPTURE)->ID1) const ID1 = _DEFER_NAME(CAPTURE)->ID1; !_DEFONCE ;) \
for (register UNUSED __typeof__(_DEFER_NAME(CAPTURE)->IDX) const IDX = _DEFER_NAME(CAPTURE)->IDX; !_DEFONCE ;) \
/* Free the storage we needed for the capture as soon as possible. */                                          \
for (free((void*)_DEFER_NAME(CAPTURE)); !_DEFONCE ;)                                                           \
/* Make the capture pointer invisible to the code. */                                                          \
for (register UNUSED void const*const _DEFER_CAPT = 0; !_DEFONCE++ ;)

/**
 ** @brief The assembled code, generated by shnell.
 **/
#define DEFER_CAPTURE2( /* generate the parameter list of the macro */ ID1, IDX ) /* Execute the capture itself. */ DEFER_CAPT2( ID1, IDX ) defer /* Reconstruct the capture. */ DEFER_CONSTR2( ID1, IDX )


/**
 ** @brief The initial capture code, generated by shnell.
 **/
#define DEFER_CAPT3(                                                               \
/* generate the parameter list of the macro */                                     \
ID1,                                                                               \
ID2,                                                                               \
IDX                                                                                \
)                                                                                  \
for (int _DEFONCE = 0; !_DEFONCE ;)                                                \
for (                                                                              \
    /* Define the structure that caches all the data, and allocate storage. */     \
    /* Encapsulating this inside an external __typeof__ is necessary for clang. */ \
__typeof__(struct {                                                                \
__typeof__(ID1) ID1;                                                               \
    __typeof__(ID2) ID2;                                                           \
    __typeof__(IDX) IDX;                                                           \
})* _DEFER_CAPT = malloc(sizeof *_DEFER_CAPT); !_DEFONCE ;)                        \
for (                                                                              \
    /* Initializes all the members of that structure. */                           \
    _DEFER_CAPT->ID1 = ID1,                                                        \
                 _DEFER_CAPT->ID2 = ID2,                                           \
                              _DEFER_CAPT->IDX = IDX; !_DEFONCE++ ;)

/**
 ** @brief The initial reconstruction code, generated by shnell.
 **/
#define DEFER_CONSTR3(                                                                                         \
/* generate the parameter list of the macro */                                                                 \
ID1,                                                                                                           \
ID2,                                                                                                           \
IDX                                                                                                            \
)                                                                                                              \
for (int _DEFONCE = 0; !_DEFONCE ;)                                                                            \
/* Make sure that the capture pointer points to the state that we are processing. */                           \
for (register __typeof__(*_DEFER_CAPT)const*const _DEFER_NAME(CAPTURE) = _DEFER_NAME(CAPT0); !_DEFONCE ;)      \
/* Instaure local variables for all captures. */                                                               \
for (register UNUSED __typeof__(_DEFER_NAME(CAPTURE)->ID1) const ID1 = _DEFER_NAME(CAPTURE)->ID1; !_DEFONCE ;) \
for (register UNUSED __typeof__(_DEFER_NAME(CAPTURE)->ID2) const ID2 = _DEFER_NAME(CAPTURE)->ID2; !_DEFONCE ;) \
for (register UNUSED __typeof__(_DEFER_NAME(CAPTURE)->IDX) const IDX = _DEFER_NAME(CAPTURE)->IDX; !_DEFONCE ;) \
/* Free the storage we needed for the capture as soon as possible. */                                          \
for (free((void*)_DEFER_NAME(CAPTURE)); !_DEFONCE ;)                                                           \
/* Make the capture pointer invisible to the code. */                                                          \
for (register UNUSED void const*const _DEFER_CAPT = 0; !_DEFONCE++ ;)

/**
 ** @brief The assembled code, generated by shnell.
 **/
#define DEFER_CAPTURE3( /* generate the parameter list of the macro */ ID1, ID2, IDX ) /* Execute the capture itself. */ DEFER_CAPT3( ID1, ID2, IDX ) defer /* Reconstruct the capture. */ DEFER_CONSTR3( ID1, ID2, IDX )


/**
 ** @brief The initial capture code, generated by shnell.
 **/
#define DEFER_CAPT4(                                                               \
/* generate the parameter list of the macro */                                     \
ID1,                                                                               \
ID2,                                                                               \
ID3,                                                                               \
IDX                                                                                \
)                                                                                  \
for (int _DEFONCE = 0; !_DEFONCE ;)                                                \
for (                                                                              \
    /* Define the structure that caches all the data, and allocate storage. */     \
    /* Encapsulating this inside an external __typeof__ is necessary for clang. */ \
__typeof__(struct {                                                                \
__typeof__(ID1) ID1;                                                               \
    __typeof__(ID2) ID2;                                                           \
    __typeof__(ID3) ID3;                                                           \
    __typeof__(IDX) IDX;                                                           \
})* _DEFER_CAPT = malloc(sizeof *_DEFER_CAPT); !_DEFONCE ;)                        \
for (                                                                              \
    /* Initializes all the members of that structure. */                           \
    _DEFER_CAPT->ID1 = ID1,                                                        \
                 _DEFER_CAPT->ID2 = ID2,                                           \
                              _DEFER_CAPT->ID3 = ID3,                              \
                                           _DEFER_CAPT->IDX = IDX; !_DEFONCE++ ;)

/**
 ** @brief The initial reconstruction code, generated by shnell.
 **/
#define DEFER_CONSTR4(                                                                                         \
/* generate the parameter list of the macro */                                                                 \
ID1,                                                                                                           \
ID2,                                                                                                           \
ID3,                                                                                                           \
IDX                                                                                                            \
)                                                                                                              \
for (int _DEFONCE = 0; !_DEFONCE ;)                                                                            \
/* Make sure that the capture pointer points to the state that we are processing. */                           \
for (register __typeof__(*_DEFER_CAPT)const*const _DEFER_NAME(CAPTURE) = _DEFER_NAME(CAPT0); !_DEFONCE ;)      \
/* Instaure local variables for all captures. */                                                               \
for (register UNUSED __typeof__(_DEFER_NAME(CAPTURE)->ID1) const ID1 = _DEFER_NAME(CAPTURE)->ID1; !_DEFONCE ;) \
for (register UNUSED __typeof__(_DEFER_NAME(CAPTURE)->ID2) const ID2 = _DEFER_NAME(CAPTURE)->ID2; !_DEFONCE ;) \
for (register UNUSED __typeof__(_DEFER_NAME(CAPTURE)->ID3) const ID3 = _DEFER_NAME(CAPTURE)->ID3; !_DEFONCE ;) \
for (register UNUSED __typeof__(_DEFER_NAME(CAPTURE)->IDX) const IDX = _DEFER_NAME(CAPTURE)->IDX; !_DEFONCE ;) \
/* Free the storage we needed for the capture as soon as possible. */                                          \
for (free((void*)_DEFER_NAME(CAPTURE)); !_DEFONCE ;)                                                           \
/* Make the capture pointer invisible to the code. */                                                          \
for (register UNUSED void const*const _DEFER_CAPT = 0; !_DEFONCE++ ;)

/**
 ** @brief The assembled code, generated by shnell.
 **/
#define DEFER_CAPTURE4( /* generate the parameter list of the macro */ ID1, ID2, ID3, IDX ) /* Execute the capture itself. */ DEFER_CAPT4( ID1, ID2, ID3, IDX ) defer /* Reconstruct the capture. */ DEFER_CONSTR4( ID1, ID2, ID3, IDX )


/**
 ** @brief The initial capture code, generated by shnell.
 **/
#define DEFER_CAPT5(                                                                           \
/* generate the parameter list of the macro */                                                 \
ID1,                                                                                           \
ID2,                                                                                           \
ID3,                                                                                           \
ID4,                                                                                           \
IDX                                                                                            \
)                                                                                              \
for (int _DEFONCE = 0; !_DEFONCE ;)                                                            \
for (                                                                                          \
    /* Define the structure that caches all the data, and allocate storage. */                 \
    /* Encapsulating this inside an external __typeof__ is necessary for clang. */             \
__typeof__(struct {                                                                            \
__typeof__(ID1) ID1;                                                                           \
    __typeof__(ID2) ID2;                                                                       \
    __typeof__(ID3) ID3;                                                                       \
    __typeof__(ID4) ID4;                                                                       \
    __typeof__(IDX) IDX;                                                                       \
})* _DEFER_CAPT = malloc(sizeof *_DEFER_CAPT); !_DEFONCE ;)                                    \
for (                                                                                          \
    /* Initializes all the members of that structure. */                                       \
    _DEFER_CAPT->ID1 = ID1,                                                                    \
                 _DEFER_CAPT->ID2 = ID2,                                                       \
                              _DEFER_CAPT->ID3 = ID3,                                          \
                                           _DEFER_CAPT->ID4 = ID4,                             \
                                                        _DEFER_CAPT->IDX = IDX; !_DEFONCE++ ;)

/**
 ** @brief The initial reconstruction code, generated by shnell.
 **/
#define DEFER_CONSTR5(                                                                                         \
/* generate the parameter list of the macro */                                                                 \
ID1,                                                                                                           \
ID2,                                                                                                           \
ID3,                                                                                                           \
ID4,                                                                                                           \
IDX                                                                                                            \
)                                                                                                              \
for (int _DEFONCE = 0; !_DEFONCE ;)                                                                            \
/* Make sure that the capture pointer points to the state that we are processing. */                           \
for (register __typeof__(*_DEFER_CAPT)const*const _DEFER_NAME(CAPTURE) = _DEFER_NAME(CAPT0); !_DEFONCE ;)      \
/* Instaure local variables for all captures. */                                                               \
for (register UNUSED __typeof__(_DEFER_NAME(CAPTURE)->ID1) const ID1 = _DEFER_NAME(CAPTURE)->ID1; !_DEFONCE ;) \
for (register UNUSED __typeof__(_DEFER_NAME(CAPTURE)->ID2) const ID2 = _DEFER_NAME(CAPTURE)->ID2; !_DEFONCE ;) \
for (register UNUSED __typeof__(_DEFER_NAME(CAPTURE)->ID3) const ID3 = _DEFER_NAME(CAPTURE)->ID3; !_DEFONCE ;) \
for (register UNUSED __typeof__(_DEFER_NAME(CAPTURE)->ID4) const ID4 = _DEFER_NAME(CAPTURE)->ID4; !_DEFONCE ;) \
for (register UNUSED __typeof__(_DEFER_NAME(CAPTURE)->IDX) const IDX = _DEFER_NAME(CAPTURE)->IDX; !_DEFONCE ;) \
/* Free the storage we needed for the capture as soon as possible. */                                          \
for (free((void*)_DEFER_NAME(CAPTURE)); !_DEFONCE ;)                                                           \
/* Make the capture pointer invisible to the code. */                                                          \
for (register UNUSED void const*const _DEFER_CAPT = 0; !_DEFONCE++ ;)

/**
 ** @brief The assembled code, generated by shnell.
 **/
#define DEFER_CAPTURE5( /* generate the parameter list of the macro */ ID1, ID2, ID3, ID4, IDX ) /* Execute the capture itself. */ DEFER_CAPT5( ID1, ID2, ID3, ID4, IDX ) defer /* Reconstruct the capture. */ DEFER_CONSTR5( ID1, ID2, ID3, ID4, IDX )


/**
 ** @brief The initial capture code, generated by shnell.
 **/
#define DEFER_CAPT6(                                                                                        \
/* generate the parameter list of the macro */                                                              \
ID1,                                                                                                        \
ID2,                                                                                                        \
ID3,                                                                                                        \
ID4,                                                                                                        \
ID5,                                                                                                        \
IDX                                                                                                         \
)                                                                                                           \
for (int _DEFONCE = 0; !_DEFONCE ;)                                                                         \
for (                                                                                                       \
    /* Define the structure that caches all the data, and allocate storage. */                              \
    /* Encapsulating this inside an external __typeof__ is necessary for clang. */                          \
__typeof__(struct {                                                                                         \
__typeof__(ID1) ID1;                                                                                        \
    __typeof__(ID2) ID2;                                                                                    \
    __typeof__(ID3) ID3;                                                                                    \
    __typeof__(ID4) ID4;                                                                                    \
    __typeof__(ID5) ID5;                                                                                    \
    __typeof__(IDX) IDX;                                                                                    \
})* _DEFER_CAPT = malloc(sizeof *_DEFER_CAPT); !_DEFONCE ;)                                                 \
for (                                                                                                       \
    /* Initializes all the members of that structure. */                                                    \
    _DEFER_CAPT->ID1 = ID1,                                                                                 \
                 _DEFER_CAPT->ID2 = ID2,                                                                    \
                              _DEFER_CAPT->ID3 = ID3,                                                       \
                                           _DEFER_CAPT->ID4 = ID4,                                          \
                                                        _DEFER_CAPT->ID5 = ID5,                             \
                                                                     _DEFER_CAPT->IDX = IDX; !_DEFONCE++ ;)

/**
 ** @brief The initial reconstruction code, generated by shnell.
 **/
#define DEFER_CONSTR6(                                                                                         \
/* generate the parameter list of the macro */                                                                 \
ID1,                                                                                                           \
ID2,                                                                                                           \
ID3,                                                                                                           \
ID4,                                                                                                           \
ID5,                                                                                                           \
IDX                                                                                                            \
)                                                                                                              \
for (int _DEFONCE = 0; !_DEFONCE ;)                                                                            \
/* Make sure that the capture pointer points to the state that we are processing. */                           \
for (register __typeof__(*_DEFER_CAPT)const*const _DEFER_NAME(CAPTURE) = _DEFER_NAME(CAPT0); !_DEFONCE ;)      \
/* Instaure local variables for all captures. */                                                               \
for (register UNUSED __typeof__(_DEFER_NAME(CAPTURE)->ID1) const ID1 = _DEFER_NAME(CAPTURE)->ID1; !_DEFONCE ;) \
for (register UNUSED __typeof__(_DEFER_NAME(CAPTURE)->ID2) const ID2 = _DEFER_NAME(CAPTURE)->ID2; !_DEFONCE ;) \
for (register UNUSED __typeof__(_DEFER_NAME(CAPTURE)->ID3) const ID3 = _DEFER_NAME(CAPTURE)->ID3; !_DEFONCE ;) \
for (register UNUSED __typeof__(_DEFER_NAME(CAPTURE)->ID4) const ID4 = _DEFER_NAME(CAPTURE)->ID4; !_DEFONCE ;) \
for (register UNUSED __typeof__(_DEFER_NAME(CAPTURE)->ID5) const ID5 = _DEFER_NAME(CAPTURE)->ID5; !_DEFONCE ;) \
for (register UNUSED __typeof__(_DEFER_NAME(CAPTURE)->IDX) const IDX = _DEFER_NAME(CAPTURE)->IDX; !_DEFONCE ;) \
/* Free the storage we needed for the capture as soon as possible. */                                          \
for (free((void*)_DEFER_NAME(CAPTURE)); !_DEFONCE ;)                                                           \
/* Make the capture pointer invisible to the code. */                                                          \
for (register UNUSED void const*const _DEFER_CAPT = 0; !_DEFONCE++ ;)

/**
 ** @brief The assembled code, generated by shnell.
 **/
#define DEFER_CAPTURE6( /* generate the parameter list of the macro */ ID1, ID2, ID3, ID4, ID5, IDX ) /* Execute the capture itself. */ DEFER_CAPT6( ID1, ID2, ID3, ID4, ID5, IDX ) defer /* Reconstruct the capture. */ DEFER_CONSTR6( ID1, ID2, ID3, ID4, ID5, IDX )


/**
 ** @brief The initial capture code, generated by shnell.
 **/
#define DEFER_CAPT7(                                                                                                     \
/* generate the parameter list of the macro */                                                                           \
ID1,                                                                                                                     \
ID2,                                                                                                                     \
ID3,                                                                                                                     \
ID4,                                                                                                                     \
ID5,                                                                                                                     \
ID6,                                                                                                                     \
IDX                                                                                                                      \
)                                                                                                                        \
for (int _DEFONCE = 0; !_DEFONCE ;)                                                                                      \
for (                                                                                                                    \
    /* Define the structure that caches all the data, and allocate storage. */                                           \
    /* Encapsulating this inside an external __typeof__ is necessary for clang. */                                       \
__typeof__(struct {                                                                                                      \
__typeof__(ID1) ID1;                                                                                                     \
    __typeof__(ID2) ID2;                                                                                                 \
    __typeof__(ID3) ID3;                                                                                                 \
    __typeof__(ID4) ID4;                                                                                                 \
    __typeof__(ID5) ID5;                                                                                                 \
    __typeof__(ID6) ID6;                                                                                                 \
    __typeof__(IDX) IDX;                                                                                                 \
})* _DEFER_CAPT = malloc(sizeof *_DEFER_CAPT); !_DEFONCE ;)                                                              \
for (                                                                                                                    \
    /* Initializes all the members of that structure. */                                                                 \
    _DEFER_CAPT->ID1 = ID1,                                                                                              \
                 _DEFER_CAPT->ID2 = ID2,                                                                                 \
                              _DEFER_CAPT->ID3 = ID3,                                                                    \
                                           _DEFER_CAPT->ID4 = ID4,                                                       \
                                                        _DEFER_CAPT->ID5 = ID5,                                          \
                                                                     _DEFER_CAPT->ID6 = ID6,                             \
                                                                                  _DEFER_CAPT->IDX = IDX; !_DEFONCE++ ;)

/**
 ** @brief The initial reconstruction code, generated by shnell.
 **/
#define DEFER_CONSTR7(                                                                                         \
/* generate the parameter list of the macro */                                                                 \
ID1,                                                                                                           \
ID2,                                                                                                           \
ID3,                                                                                                           \
ID4,                                                                                                           \
ID5,                                                                                                           \
ID6,                                                                                                           \
IDX                                                                                                            \
)                                                                                                              \
for (int _DEFONCE = 0; !_DEFONCE ;)                                                                            \
/* Make sure that the capture pointer points to the state that we are processing. */                           \
for (register __typeof__(*_DEFER_CAPT)const*const _DEFER_NAME(CAPTURE) = _DEFER_NAME(CAPT0); !_DEFONCE ;)      \
/* Instaure local variables for all captures. */                                                               \
for (register UNUSED __typeof__(_DEFER_NAME(CAPTURE)->ID1) const ID1 = _DEFER_NAME(CAPTURE)->ID1; !_DEFONCE ;) \
for (register UNUSED __typeof__(_DEFER_NAME(CAPTURE)->ID2) const ID2 = _DEFER_NAME(CAPTURE)->ID2; !_DEFONCE ;) \
for (register UNUSED __typeof__(_DEFER_NAME(CAPTURE)->ID3) const ID3 = _DEFER_NAME(CAPTURE)->ID3; !_DEFONCE ;) \
for (register UNUSED __typeof__(_DEFER_NAME(CAPTURE)->ID4) const ID4 = _DEFER_NAME(CAPTURE)->ID4; !_DEFONCE ;) \
for (register UNUSED __typeof__(_DEFER_NAME(CAPTURE)->ID5) const ID5 = _DEFER_NAME(CAPTURE)->ID5; !_DEFONCE ;) \
for (register UNUSED __typeof__(_DEFER_NAME(CAPTURE)->ID6) const ID6 = _DEFER_NAME(CAPTURE)->ID6; !_DEFONCE ;) \
for (register UNUSED __typeof__(_DEFER_NAME(CAPTURE)->IDX) const IDX = _DEFER_NAME(CAPTURE)->IDX; !_DEFONCE ;) \
/* Free the storage we needed for the capture as soon as possible. */                                          \
for (free((void*)_DEFER_NAME(CAPTURE)); !_DEFONCE ;)                                                           \
/* Make the capture pointer invisible to the code. */                                                          \
for (register UNUSED void const*const _DEFER_CAPT = 0; !_DEFONCE++ ;)

/**
 ** @brief The assembled code, generated by shnell.
 **/
#define DEFER_CAPTURE7( /* generate the parameter list of the macro */ ID1, ID2, ID3, ID4, ID5, ID6, IDX ) /* Execute the capture itself. */ DEFER_CAPT7( ID1, ID2, ID3, ID4, ID5, ID6, IDX ) defer /* Reconstruct the capture. */ DEFER_CONSTR7( ID1, ID2, ID3, ID4, ID5, ID6, IDX )


/**
 ** @brief The initial capture code, generated by shnell.
 **/
#define DEFER_CAPT8(                                                                                                                  \
/* generate the parameter list of the macro */                                                                                        \
ID1,                                                                                                                                  \
ID2,                                                                                                                                  \
ID3,                                                                                                                                  \
ID4,                                                                                                                                  \
ID5,                                                                                                                                  \
ID6,                                                                                                                                  \
ID7,                                                                                                                                  \
IDX                                                                                                                                   \
)                                                                                                                                     \
for (int _DEFONCE = 0; !_DEFONCE ;)                                                                                                   \
for (                                                                                                                                 \
    /* Define the structure that caches all the data, and allocate storage. */                                                        \
    /* Encapsulating this inside an external __typeof__ is necessary for clang. */                                                    \
__typeof__(struct {                                                                                                                   \
__typeof__(ID1) ID1;                                                                                                                  \
    __typeof__(ID2) ID2;                                                                                                              \
    __typeof__(ID3) ID3;                                                                                                              \
    __typeof__(ID4) ID4;                                                                                                              \
    __typeof__(ID5) ID5;                                                                                                              \
    __typeof__(ID6) ID6;                                                                                                              \
    __typeof__(ID7) ID7;                                                                                                              \
    __typeof__(IDX) IDX;                                                                                                              \
})* _DEFER_CAPT = malloc(sizeof *_DEFER_CAPT); !_DEFONCE ;)                                                                           \
for (                                                                                                                                 \
    /* Initializes all the members of that structure. */                                                                              \
    _DEFER_CAPT->ID1 = ID1,                                                                                                           \
                 _DEFER_CAPT->ID2 = ID2,                                                                                              \
                              _DEFER_CAPT->ID3 = ID3,                                                                                 \
                                           _DEFER_CAPT->ID4 = ID4,                                                                    \
                                                        _DEFER_CAPT->ID5 = ID5,                                                       \
                                                                     _DEFER_CAPT->ID6 = ID6,                                          \
                                                                                  _DEFER_CAPT->ID7 = ID7,                             \
                                                                                               _DEFER_CAPT->IDX = IDX; !_DEFONCE++ ;)

/**
 ** @brief The initial reconstruction code, generated by shnell.
 **/
#define DEFER_CONSTR8(                                                                                         \
/* generate the parameter list of the macro */                                                                 \
ID1,                                                                                                           \
ID2,                                                                                                           \
ID3,                                                                                                           \
ID4,                                                                                                           \
ID5,                                                                                                           \
ID6,                                                                                                           \
ID7,                                                                                                           \
IDX                                                                                                            \
)                                                                                                              \
for (int _DEFONCE = 0; !_DEFONCE ;)                                                                            \
/* Make sure that the capture pointer points to the state that we are processing. */                           \
for (register __typeof__(*_DEFER_CAPT)const*const _DEFER_NAME(CAPTURE) = _DEFER_NAME(CAPT0); !_DEFONCE ;)      \
/* Instaure local variables for all captures. */                                                               \
for (register UNUSED __typeof__(_DEFER_NAME(CAPTURE)->ID1) const ID1 = _DEFER_NAME(CAPTURE)->ID1; !_DEFONCE ;) \
for (register UNUSED __typeof__(_DEFER_NAME(CAPTURE)->ID2) const ID2 = _DEFER_NAME(CAPTURE)->ID2; !_DEFONCE ;) \
for (register UNUSED __typeof__(_DEFER_NAME(CAPTURE)->ID3) const ID3 = _DEFER_NAME(CAPTURE)->ID3; !_DEFONCE ;) \
for (register UNUSED __typeof__(_DEFER_NAME(CAPTURE)->ID4) const ID4 = _DEFER_NAME(CAPTURE)->ID4; !_DEFONCE ;) \
for (register UNUSED __typeof__(_DEFER_NAME(CAPTURE)->ID5) const ID5 = _DEFER_NAME(CAPTURE)->ID5; !_DEFONCE ;) \
for (register UNUSED __typeof__(_DEFER_NAME(CAPTURE)->ID6) const ID6 = _DEFER_NAME(CAPTURE)->ID6; !_DEFONCE ;) \
for (register UNUSED __typeof__(_DEFER_NAME(CAPTURE)->ID7) const ID7 = _DEFER_NAME(CAPTURE)->ID7; !_DEFONCE ;) \
for (register UNUSED __typeof__(_DEFER_NAME(CAPTURE)->IDX) const IDX = _DEFER_NAME(CAPTURE)->IDX; !_DEFONCE ;) \
/* Free the storage we needed for the capture as soon as possible. */                                          \
for (free((void*)_DEFER_NAME(CAPTURE)); !_DEFONCE ;)                                                           \
/* Make the capture pointer invisible to the code. */                                                          \
for (register UNUSED void const*const _DEFER_CAPT = 0; !_DEFONCE++ ;)

/**
 ** @brief The assembled code, generated by shnell.
 **/
#define DEFER_CAPTURE8( /* generate the parameter list of the macro */ ID1, ID2, ID3, ID4, ID5, ID6, ID7, IDX ) /* Execute the capture itself. */ DEFER_CAPT8( ID1, ID2, ID3, ID4, ID5, ID6, ID7, IDX ) defer /* Reconstruct the capture. */ DEFER_CONSTR8( ID1, ID2, ID3, ID4, ID5, ID6, ID7, IDX )


/**
 ** Now we need a mechanism that chooses the correct macro according
 ** to the number of captures.
 **
 ** `_DEFER_CHOOSE` will always receive the parenthesized list of
 ** captures as first argument, and the macro that is to be invoked as
 ** the 10th.
 **/
#define _DEFER_CHOOSE(...) _DEFER_CHOOSE1(__VA_ARGS__)

#define _DEFER_CHOOSE1( _0, _1, _2, _3, _4, _5, _6, _7, _8, _9, ...) _9 _0

#define _DEFER_INTERNAL_COMMA ,

#define _DEFER_COMMA_IF_ID3(_0, _1, _2, ...) _2 ## _INTERNAL_COMMA
#define _DEFER_COMMA_IF_ID2(...) _DEFER_COMMA_IF_ID3(__VA_ARGS__)
#define _DEFER_COMMA_IF_ID1(_0, ...) _DEFER_COMMA_IF_ID2(_DEFER_INTERNAL_COMMA ## _0 , , _DEFER,)

/**
 ** @brief Produces a comma, if the argument list starts with an
 ** identifier.
 **/
#define _DEFER_COMMA_IF_ID(...) _DEFER_COMMA_IF_ID1(__VA_ARGS__,)

/**
 ** @brief Capture some variables and defer the execution of the
 ** depending statement.
 **
 ** The argument list must be empty, or contain a list of
 ** variables. The effect is that those variables are evaluated, and
 ** the values are stored in a secret place.
 **
 ** When the deferred statement is finally executed, address-less
 ** local variables with the same name and type (but const-qualified)
 ** are placed before the deferred statement, and are initialized with
 ** these frozen values.
 **/
#define defer_capture(...) _DEFER_CHOOSE((__VA_ARGS__), __VA_ARGS__ _DEFER_COMMA_IF_ID(__VA_ARGS__) DEFER_CAPTURE8, DEFER_CAPTURE7, DEFER_CAPTURE6, DEFER_CAPTURE5, DEFER_CAPTURE4, DEFER_CAPTURE3, DEFER_CAPTURE2, DEFER_CAPTURE1, DEFER_CAPTURE0, )


/**
 ** @brief Return from the current function after executing all active
 ** defer clauses.
 **
 ** This replaces regular `return` statement, and all forms (with and
 ** without return expression) are supported, depending on the return
 ** type of the function.
 **
 ** If used outside a guarded block, and if the platform supports
 ** this, this checks for possible deferred statements of the same
 ** function and launches them before doing the return.
 **
 ** If there is no guard and no defer statement inside this function,
 ** most mechanics for defer should be optimized out, but this can and
 ** will result in less optimal code than without the defer
 ** mechanism. In particular, regardless of some compiler flags, such
 ** a function will always have a frame pointer, and tail recursion
 ** might to be optimized. This is a restriction of this
 ** implementation here, not the general feature. But for doing a
 ** "library only" implementation we have to compromise.
 **
 ** If used outside a guarded block, and if the platform does not
 ** support this, the behavior is undefined. If you need portability
 ** *and* have to use this implementation use the
 ** `defer_function_fallback` macro to mark the entire block of the
 ** function.
 **
 ** Beware also, that the return expression is only evaluated *after*
 ** the deferred statements are executed.
 **
 ** As an implementation detail, note that this definition must be the
 ** very last in the header file such that this return here does not
 ** overwrite the return of the inline functions above.
 **/
#define return defer_unwind_function return

#endif

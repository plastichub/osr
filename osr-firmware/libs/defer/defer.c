#include <stdio.h>
#ifndef __STDC_NO_THREADS__
#include <threads.h>
#endif
// because stddefer rewrites `return` this comes better after system
// includes
#include "stddefer.h"

/**
 ** @file
 **
 ** This is a test program for the defer mechanism that should cover
 ** most of the defer clause triggers.
 **
 ** When this program is called with no argument, a "normal" execution
 ** is performed and all defer mechanisms are only triggered when a
 ** guarded block is left "naturally".
 **
 ** If run with arguments other mechanisms are triggered, depending on
 ** their number. Current these include `break`, `continue`, `defer_break`,
 ** `return`, `exit`, `defer_goto` as normal "user" level triggers in
 ** a variation of possibilities.
 **
 ** Others, such as allocation failures, arithmetic conditions,
 ** segmentation faults or signals are usually not controlled by the
 ** programmer.
 **
 ** Each such condition will print a short notice "testing ..." before
 ** the action is triggered. For the individual number of arguments t
 **/


// These two are just here to be able to generate illegal arithmetic
// as a possible test.  They use extern symbols and volatile so they
// cannot be optimized out.
int volatile number = 0;
int inverse(int a) {
  return a/number;
}

inline
void function_with_exit_recursive(unsigned level, int code) {
  defer_function_fallback {
    printf("immediately in %s, level %u, code %d\n", __func__, level, code);
    defer printf("deferred in %s, level %u, code %d\n", __func__, level, code);
    if (level) function_with_exit_recursive(level-1, code);
    else exit(code);
  }
}

inline
void function_with_exit(int code) {
  if (code > 0) function_with_exit_recursive(2, code);
  else function_with_exit_recursive(3, code);
}

inline
void function_with_indirect_exit(int code) {
  defer_function_fallback {
    printf("immediately in %s, %d\n", __func__, code);
    defer printf("deferred in %s, %d\n", __func__, code);
    function_with_exit(code);
  }
}

void function_with_exit_recursive(unsigned level, int code);
void function_with_exit(int code);
void function_with_indirect_exit(int code);

inline
void function_with_recursion(unsigned level) {
  defer_function_fallback {
    printf("immediately in %s, %d\n", __func__, level);
    defer printf("deferred in %s:%d, %d\n", __func__, __LINE__, level);
    if (level) function_with_recursion(level-1);
    printf("after recursion in %s, %d\n", __func__, level);
    defer printf("deferred in %s:%d, %d\n", __func__, __LINE__, level);
    return;
  }
}

inline
void function_with_recursion_and_exit(int code) {
  defer_function_fallback {
    printf("immediately in %s, %d\n", __func__, code);
    defer printf("deferred in %s, %d\n", __func__, code);
    function_with_recursion(8);
    exit(code);
  }
}

void function_with_recursion(unsigned);
void function_with_recursion_and_exit(int code);

inline
void function_with_panic(int code) {
  printf("immediately in %s, %d\n", __func__, code);
  panic(code);
}

inline
void function_with_indirect_panic(int code) {
  defer_function_fallback {
    printf("immediately in %s, %d\n", __func__, code);
    defer_capture(code) {
      printf("deferred in %s, %d\n", __func__, code);
      int err = recover();
      if (err) panic(err);
    }
    function_with_panic(code);
    return;
  }
}

void function_with_panic(int code);
void function_with_indirect_panic(int code);

#define SHOWME(X) printf(#X " has value %d\n", X)

static
void show_values(void) {
   SHOWME(DEFER_EDOM);
   SHOWME(DEFER_EILSEQ);
   SHOWME(DEFER_ERANGE);
   SHOWME(DEFER_ENOMEM);
   SHOWME(DEFER_EINVAL);
   SHOWME(DEFER_ENOTSUP);
   SHOWME(_DEFER_EMAX);
   SHOWME(_DEFER_BLCK);
   SHOWME(_DEFER_FUNC);
   SHOWME(_DEFER_EXIT);
   SHOWME(_DEFER_NONE);
   SHOWME(DEFER_ABRT);
   SHOWME(DEFER_FPE);
   SHOWME(DEFER_ILL);
   SHOWME(DEFER_INT);
   SHOWME(DEFER_SEGV);
   SHOWME(DEFER_TERM);
   SHOWME(DEFER_KILL);
}



int main(int argc, char* argv[argc+1]) {
  // Establish some signal handlers to show how this might work.
  // SIGINT and SIGTERM are C standard signals, we use them with the
  // two handlers that we provide to show the difference.
  signal(SIGINT, defer_sig_flag);
  signal(SIGTERM, defer_sig_jump);
  // The following three suppose that hardware signals for arithmetic
  // exceptions or invalid dereferencing stay inside the same thread.
  // This might or might not work on your system, be careful.
  signal(SIGFPE, defer_sig_jump);
  signal(SIGILL, defer_sig_jump);
  signal(SIGSEGV, defer_sig_jump);
  if (argc == 19) { puts("testing invalid arithmetic outside guarded block"); number = inverse(argc); }
  {
    // mess around with the stack pointer
    double volatile A[argc+1];
    A[argc] = argc;
    printf(".immediate print of VLA %g, %p\n", A[argc], (void*volatile)&A[argc]);
  }
  guard {
    puts("starting first guarded block");
    // A guarded block with no defer clause should not call longjmp, but
    // finish the loop normally.
    guard {
      puts("do something little in nested guarded block");
    }
    puts("after nested guarded block");
    guard {
      puts("before defer in second nested guarded block");
      defer puts("defer in second nested guarded block");
      puts("after defer in second nested guarded block");
    }
    for (volatile int i = 0; i < 5; ++i) {
      volatile int cl = 1;
      defer printf("deferred print of potentially dead variables with indeterminate value %d, %d\n", --i, cl);
#ifdef __GNUC__
      defer_capture()
        printf("deferred print of captured variables with no frozen values\n");
      defer_capture(i)
        printf("deferred print of captured variables with frozen values %d\n", i);
      defer_capture(i, cl)
        printf("deferred print of captured variables with frozen values %d, %d\n", i, cl);
      defer_capture(i, cl, argc)
        printf("deferred print of captured variables with frozen values %d, %d, %d\n", i, cl, argc);
#endif
      cl = 2;
    }
    puts("do something important");
    volatile int cr = 5;
    defer printf("deferred print of variable cr %d\n", cr);
    cr = 6;
    guard {
      puts("starting third nested guarded block");
      volatile int opy = 3;
      puts("looping for a possible interrupt");
      for (opy = 0; opy < (1l << 28); ++opy) {
        int rec = recover_signal();
        if (rec == SIGINT) {
          puts("caught SIGINT, stopping loop and continuing");
          break;
        }
      }
      printf("immediate print of variable opy %d\n", opy);
      defer {
        printf("deferred print of variable opy %d\n", opy);
      }
      if (argc == 10) { puts("testing panic"); panic(-DEFER_EILSEQ); }
      if (argc == 20) { puts("testing exit"); exit(3); }
      if (argc == 21) { puts("testing quick_exit"); quick_exit(3); }
      if (argc == 22) { puts("testing thrd_exit"); thrd_exit(3); }
      defer_if(err) {
        puts("inside defer_if, we encountered a special condition");
        defer_show(err);
        if (err <= 0 || err > 2) {
          puts("unrecoverable error, continue unwinding ...");
          panic(err, 0);
        }
        puts("error 1 or 2, cross fingers and continue");
      } else {
        puts("inside else of defer_if, all went well, it seems");
      }
      opy = 4;
      if (argc == 2) { puts("testing break"); break; }
      if (argc == 3) { puts("testing continue"); continue; }
      if (argc == 4) { puts("testing defer_break"); defer_break; }
      if (argc == 5) { puts("testing return"); return 1; }
      if (argc == 6) { puts("testing defer_goto"); defer_goto FINISH; }
      if (argc == 7) { puts("testing exit function pointer"); (exit)(2); }
      if (argc == 8) { puts("testing function call with exit"); function_with_exit(argc); }
      if (argc == 9) { puts("testing function call with indirect exit"); function_with_indirect_exit(argc); }
      if (argc == 25) { puts("testing function call with recursion and exit"); function_with_recursion_and_exit(argc); }
      // condition that argc == 10 is already handled above without the defer_if
      if (argc == 11) { puts("testing defer_abort"); defer_abort(); }
      if (argc == 12) { puts("testing defer_malloc(0)"); void*volatile p = defer_malloc(0); free(p); }
      if (argc == 13) { puts("testing defer_malloc(1000)"); void*volatile p = defer_malloc(1000); free(p); }
      // Malloc for SIZE_MAX should usually fail. We have to keep the
      // returned address in a volatile variable, because otherwise
      // the optimizer may just remove the malloc/free pair. Clang
      // does.
      if (argc == 14) { puts("testing defer_malloc(SIZE_MAX)"); void*volatile p = defer_malloc(SIZE_MAX); free(p); }
      if (argc == 15) { puts("testing raise(SIGTERM)"); raise(SIGTERM); }
      if (argc == 16) { puts("testing segmentation fault"); *(int volatile*)0; }
      if (argc == 17) { puts("testing invalid arithmetic"); number = inverse(cr); }
      if (argc == 18) { puts("showing values, all should be different"); show_values(); }
      puts("do something");
    }
    puts("after third nested guarded block");
    // A guarded block with no defer clause should not call longjmp, but
    // finish the loop normally.
    guard {
      puts("fourth nested guarded block");
    }
  }
  // testing panic in functions
  if (argc == 23) { puts("testing function call with panic"); function_with_panic(argc); }
  if (argc == 24) { puts("testing function call with indirect panic"); function_with_indirect_panic(argc); }
  FINISH:
  puts("after all guarded blocks, starting a defer section");
  defer puts("defer from within the defer section");
  puts("defer section about to finish");
  // This is only necessary because there is a "defer" that is not
  // within any guarded block
  return EXIT_SUCCESS;
}

/* -*- C -*- */

#include <stdio.h>
#include "test_lang.h"
#include "stddefer.h"


#define test_lang_C_5(...) test_lang_CPP_0(__VA_ARGS__)


int test_lang_C_0(int rec) {
    int ret = 0;
    guard {
        printf("%s: rec %d, entering, test '" "exit(EXIT_SUCCESS)" "'\n", __func__, rec);
        defer {
            printf("%s: rec %d, leaving, test '" "exit(EXIT_SUCCESS)" "'\n", __func__, rec);
        }
        if (rec) {
            ret = test_lang_C_1(rec-1);
        } else {
            printf("%s calling the test\n", __func__);
            exit(EXIT_SUCCESS);
        }
    }
    return ret;
}


int test_lang_C_1(int rec) {
    int ret = 0;
    guard {
        printf("%s: rec %d, entering, test '" "exit(EXIT_FAILURE)" "'\n", __func__, rec);
        defer {
            printf("%s: rec %d, leaving, test '" "exit(EXIT_FAILURE)" "'\n", __func__, rec);
        }
        if (rec) {
            ret = test_lang_C_2(rec-1);
        } else {
            printf("%s calling the test\n", __func__);
            exit(EXIT_FAILURE);
        }
    }
    return ret;
}


int test_lang_C_2(int rec) {
    int ret = 0;
    guard {
        printf("%s: rec %d, entering, test '" "panic(-DEFER_ENOMEM)" "'\n", __func__, rec);
        defer {
            printf("%s: rec %d, leaving, test '" "panic(-DEFER_ENOMEM)" "'\n", __func__, rec);
        }
        if (rec) {
            ret = test_lang_C_3(rec-1);
        } else {
            printf("%s calling the test\n", __func__);
            panic(-DEFER_ENOMEM);
        }
    }
    return ret;
}


int test_lang_C_3(int rec) {
    int ret = 0;
    guard {
        printf("%s: rec %d, entering, test '" "panic(-DEFER_ERANGE,thrd_exit)" "'\n", __func__, rec);
        defer {
            printf("%s: rec %d, leaving, test '" "panic(-DEFER_ERANGE,thrd_exit)" "'\n", __func__, rec);
        }
        if (rec) {
            ret = test_lang_C_4(rec-1);
        } else {
            printf("%s calling the test\n", __func__);
            panic(-DEFER_ERANGE,thrd_exit);
        }
    }
    return ret;
}


int test_lang_C_4(int rec) {
    int ret = 0;
    guard {
        printf("%s: rec %d, entering, test '" "return INT_MAX" "'\n", __func__, rec);
        defer {
            printf("%s: rec %d, leaving, test '" "return INT_MAX" "'\n", __func__, rec);
        }
        if (rec) {
            ret = test_lang_C_5(rec-1);
        } else {
            printf("%s calling the test\n", __func__);
            return INT_MAX;
        }
    }
    return ret;
}



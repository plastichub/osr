# [Modbus](./)

## [Modbus vulnerabilities](https://radiflow.com/blog/hack-the-modbus/)

Hack the Modbus

November 21, 2019

*By Liron Benbenishti, Cyber Researcher, CTO Team, Radiflow*

### Modbus – introduction

OT protocols are communication protocols designed for the exchange of control messages on industrial networks.

The MODBUS protocol, developed by Modicon for use with its programmable logic controllers (PLCs), provides client/server communication between devices connected on different types of buses or networks.

Modbus is used for transmitting information between electronic devices. The device requesting the information is called the Modbus Master and the devices supplying information are Modbus slaves. The protocol’s simplicity and efficiency caused it to become the most widely used network protocol in the industrial manufacturing environment.

### Modbus request-response scheme

A simple request-response scheme is used for all MBAP transactions. The client (also known as master) device initiates a request and the server (also known as slave) replies. For example, when a HMI workstation requires a value from a PLC it sends a request message to start the data transfer process.

The PLC then sends a response providing the requested information. In this situation, the device running the HMI is acting as the client/master and the PLC is acting as the server/slave. Each message contains a function code that is set by the client/master and indicates to the server/slave what kind of action to perform. Function codes are the number tells the slave which table to access and whether to read from or write to the table.

Following a request, there are 4 possible outcomes from the slave.

1. The request is successfully processed by the slave and a valid response is sent.
2. The request is not received by the slave therefore no response is sent.
3. The request is received by the slave with a parity, CRC or LRC error. The slave ignores the request and sends no response.
4. The request is received without an error, but cannot be processed by the slave for another reason. The slave replies with an exception response.

Example of an exception code:

02-Illegal Data Address-The data address received in the query is not an allowable address for the slave will generate exception 02.

### Modbus attacks

An Attacker can start his attack in reconnaissance phase by scanning the network trying to find Modbus devices with Modbus diagnostic commands: Clear Counters and Diagnostic Register: a request sent to PLC, with function code 8 (0x08) and subfunction code 10 (0x0A), will cause the target server to clear its counters and the diagnostic register. This function is typically implemented only in serial devices.

Another diagnostic command attacker can use is Read Device Identification as an attempt to gather information on Modbus device: A MODBUS request packed with function code 43 Read Device Identification will cause a MODBUS server to return the vendor name, product name, and version number. Additional information may also be provided in optional fields. An attacker sends the MODBUS request packet with function code 43 to all systems in the network and gathers intelligence that may be helpful in future attacks

### Modbus vulnerabilities

The MODBUS/TCP protocol implementation contains multiple vulnerabilities that could allow an attacker to perform reconnaissance activity or issue arbitrary commands.

1. Lack of Confidentiality: All MODBUS messages are transmitted in clear text across the transmission media.
2. Lack of Integrity: There are no integrity checks built into the MODBUS application protocol, and as a result it depends on lower layer protocols to preserve integrity
3. Lack of Authentication: There is no authentication at any level of the MODBUS protocol, with the possible exception of some undocumented programming commands.
4. Simplistic Framing: MODBUS/TCP frames are sent over established TCP connections. While such connections are usually reliable, they have a significant drawback for the
5. Lack of Session Structure: Like many request/response protocols (i.e. SNMP, HTTP, etc.) MODBUS/TCP consists of short-lived transactions where the master initiates a request to the slave that results in a single action. When combined with the lack of authentication and poor TCP initial sequence number (ISN) generation in many embedded devices, it becomes possible for attackers to inject commands with no knowledge of the existing session.

These vulnerabilities allow an attacker to perform reconnaissance activity on the targeted network. The first vulnerability exists because a MODBUS slave device may return  *Illegal Function Exception*  responses for queries that contain an unsupported function code. An unauthenticated, remote attacker could exploit this vulnerability by sending crafted function codes to carry out reconnaissance on the targeted network.

An additional reconnaissance vulnerability is due to multiple  *Illegal Address Exception*  responses generated for queries that contain an illegal slave address. An unauthenticated, remote attacker could exploit this vulnerability by sending queries that contain invalid addresses to the targeted network and gathering information about network hosts from returned messages.

Another vulnerability is due to lack of sufficient security checks in the MODBUS/TCP protocol implementation. The protocol specification does not include an authentication mechanism for validating communication between MODBUS master and slave devices. This flaw could allow an unauthenticated, remote attacker to issue arbitrary commands to any slave device via a MODBUS master.

The MODBUS/TCP protocol contains also vulnerability that could allow an attacker to cause a denial of service (DoS) condition on a targeted system. The vulnerability is due to an implementation error in the affected protocol when processing  *Read Discrete Inputs*  request and response messages.

An unauthenticated, remote attacker could exploit the vulnerability by sending request or response parameters that contain malicious values for the  *data field*  option to a system that contains a vulnerable MODBUS/TCP implementation. The processing of the messages could trigger a DoS condition on the vulnerable system.

Another attack on Modbus can be Modbus TCP packet that exceeds the maximum length.

Modbus TCP is a protocol commonly used in SCADA and DCS networks for process control. MODBUS limits the size of the PDU to 253 bytes to allow the packet to be sent on a serial line, RS-485 interface. Modbus TCP prepends a 7-byte MODBUS Application Protocol (MBAP) header to the PDU, and the MBAP_PDU is encapsulated in a TCP packet. This places an upper limit on legal packet size.

An attacker creates a specially crafted packet longer than 260 bytes and sends it to a MODBUS client and server. If the client or server were programmed incorrectly, this could lead to a successful buffer overflow or denial-of-service attack.

The easiest attack to use against Modbus is to simply sniff the traffic on a network, find the Modbus devices and then issue harmful commands to the Modbus devices.

Modbus/TCP has no security or encryption features so, it is easy to use Wireshark to gather information from packets of data that on your network to and from a Modbus port on a device and read the contents of those packets. Since Modbus is not encrypted or secured in any way, the packets of information travel in plain text. Wireshark allows you to easily see what is contained within these packets. In this case, we are looking at the IP address of the Modbus device. We can see the Function Code of the request and with all this data, is becomes easy to identify the Modbus device, find its Modbus Register Map to identify its control command options.

### Conclusion

SCADA/ ICS and similar MODBUS-based systems were always the target of many types of attacks. These systems become much more vulnerable. MODBUS communication protocol is a widespread communication standard in the critical infrastructures field.

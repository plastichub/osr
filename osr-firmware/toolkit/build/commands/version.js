"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.register = void 0;
const path = require("path");
const debug = require("../log");
const read_1 = require("@plastichub/fs/read");
const exists_1 = require("@plastichub/fs/exists");
const write_1 = require("@plastichub/fs/write");
const pretty = require('pretty');
const defaultOptions = (yargs) => {
    return yargs.option('target', {
        default: './Version.h',
        describe: 'The target file, '
    }).option('debug', {
        default: 'false',
        describe: 'Enable internal debug message'
    }).option('cwd', {
        describe: 'The current working directory which contains the .git folder (to retrieve the hash)'
    });
};
let options = (yargs) => defaultOptions(yargs);
// node ./build/main.js version --input=../pages
function status(workingDir) {
    return __awaiter(this, void 0, void 0, function* () {
        const git = require('simple-git/promise');
        let statusSummary = null;
        try {
            statusSummary = yield git(workingDir).log();
        }
        catch (e) {
            debug.error('error retrieving Git history');
        }
        return statusSummary;
    });
}
exports.register = (cli) => {
    return cli.command('version', '', options, (argv) => __awaiter(void 0, void 0, void 0, function* () {
        if (argv.help) {
            return;
        }
        const dst = path.resolve('' + argv.target);
        const cwd = path.resolve('' + argv.cwd);
        if (!exists_1.sync(dst)) {
            debug.error(`${dst} doesn't exists! Abort ...`);
        }
        if (!exists_1.sync(cwd)) {
            debug.error(`${dst} doesn't exists! Abort ...`);
        }
        const dstParts = path.parse(dst);
        debug.info(`Write Version at ${dst}, using ${cwd} as current directory`);
        const packageFile = path.resolve(`${dstParts.dir}/package.json`);
        if (!exists_1.sync(packageFile)) {
            debug.error(`${packageFile} doesn't exists! Abort ...`);
        }
        const pkgMeta = read_1.sync(packageFile, 'json');
        const pkgVersion = pkgMeta.version;
        const vendor = argv.vendor || pkgMeta.vendor || 'NoVendor';
        const family = argv.family || pkgMeta.family || 'NoFamily';
        const product = argv.product || pkgMeta.product || 'NoProduct';
        const cid = argv.cid || pkgMeta.cid || 'NoCID';
        const support = argv.support || pkgMeta.support || 'NoSupport';
        const buildConfiguration = argv.build || pkgMeta.build || 'NoBuild';
        const firmwareSource = argv.src || pkgMeta.src || 'NoSource';
        status(cwd).then((status) => {
            const version = `#ifndef VERSION_H \n
    #define VERSION_H \n
    #define VERSION "${pkgVersion}|${vendor}|${family}|${product}|${status.latest.hash}"\n
    #define SUPPORT "${support}"
    #define BUILD "${buildConfiguration}"
    #define FW_SRC "${firmwareSource}"
    #define CID "${cid}"\n
#endif`;
            write_1.sync('./Version.h', version);
            debug.info(version);
        });
    }));
};
//# sourceMappingURL=version.js.map
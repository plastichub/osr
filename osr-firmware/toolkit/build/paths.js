"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default_trace_path = exports.default_path_howto = exports.default_path_crawler = exports.default_path = exports.HTML_SUFFIX = exports.HOWTO_SUFFIX = exports.TRACE_SUFFIX = exports.STATS_SUFFIX = void 0;
const path = require("path");
const url_1 = require("url");
exports.STATS_SUFFIX = '_stats.json';
exports.TRACE_SUFFIX = '_trace.json';
exports.HOWTO_SUFFIX = '_howto.json';
exports.HTML_SUFFIX = '_dump.html';
// utils to create output file name for url, format : hostname_time.json
const _url_short = (url) => new url_1.URL(url).hostname;
const _date_suffix = () => new Date().toLocaleTimeString().replace(/:/g, '_');
const _default_filename = (url) => `${_url_short(url)}`;
exports.default_path = (cwd, url) => `${path.join(cwd, _default_filename(url))}${exports.STATS_SUFFIX}`;
exports.default_path_crawler = (cwd, url) => `${path.join(cwd, _default_filename(url))}${exports.HTML_SUFFIX}`;
exports.default_path_howto = (cwd, url) => `${path.join(cwd, _default_filename(url))}${exports.HOWTO_SUFFIX}`;
exports.default_trace_path = (cwd, url) => `${path.join(cwd, _default_filename(url))}${exports.TRACE_SUFFIX}`;
//# sourceMappingURL=paths.js.map
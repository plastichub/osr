#!/usr/bin/env node
import { defaults } from './_cli'; defaults();

import * as cli from 'yargs'; 
import { register as registerVersion } from './commands/version'; registerVersion(cli);

const argv = cli.argv;

if (argv.h || argv.help) {
    cli.showHelp();
    process.exit();
} else if (argv.v || argv.version) {
    // tslint:disable-next-line:no-var-requires
    const pkginfo = require('../package.json');
    process.exit();
}

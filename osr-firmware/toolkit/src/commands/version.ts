import * as CLI from 'yargs';
import { sanitize } from '../argv';
import { Options } from '..';
import { render as output } from '../output';
import * as chokidar from 'chokidar';
import * as path from 'path';
import * as  debug from '../log';
import * as utils from '../lib/common/strings';
import { sync as read } from '@plastichub/fs/read';
import { sync as exists } from '@plastichub/fs/exists';
import { sync as dir } from '@plastichub/fs/dir';
import { sync as write } from '@plastichub/fs/write';
import * as cheerio from 'cheerio';
const pretty = require('pretty');

import * as simpleGit from 'simple-git/promise';
import { SimpleGit } from 'simple-git';

const defaultOptions = (yargs: CLI.Argv) => {
    return yargs.option('target', {
        default: './Version.h',
        describe: 'The target file, '
    }).option('debug', {
        default: 'false',
        describe: 'Enable internal debug message'
    }).option('cwd', {
        describe: 'The current working directory which contains the .git folder (to retrieve the hash)'
    })
};

let options = (yargs: CLI.Argv) => defaultOptions(yargs);
// node ./build/main.js version --input=../pages

async function status(workingDir) {
    const git = require('simple-git/promise');

    let statusSummary = null;
    try {
        statusSummary = await git(workingDir).log();
    }
    catch (e) {
        debug.error('error retrieving Git history');
    }

    return statusSummary;
}

export const register = (cli: CLI.Argv) => {
    return cli.command('version', '', options, async (argv: CLI.Arguments) => {
        if (argv.help) { return; }
        const dst = path.resolve('' + argv.target);
        const cwd = path.resolve('' + argv.cwd);
        if (!exists(dst)) {
            debug.error(`${dst} doesn't exists! Abort ...`);
        }

        if (!exists(cwd)) {
            debug.error(`${dst} doesn't exists! Abort ...`);
        }

        const dstParts = path.parse(dst);

        debug.info(`Write Version at ${dst}, using ${cwd} as current directory`);

        const packageFile = path.resolve(`${dstParts.dir}/package.json`);
        if (!exists(packageFile)) {
            debug.error(`${packageFile} doesn't exists! Abort ...`);
        }

        const pkgMeta: any = read(packageFile, 'json');
        const pkgVersion = pkgMeta.version;
        const vendor = argv.vendor || pkgMeta.vendor ||  'NoVendor';
        const family = argv.family || pkgMeta.family || 'NoFamily';
        const product = argv.product || pkgMeta.product || 'NoProduct';
        const cid = argv.cid || pkgMeta.cid || 'NoCID';
        const support = argv.support || pkgMeta.support || 'NoSupport';
        const buildConfiguration = argv.build || pkgMeta.build || 'NoBuild';
        const firmwareSource = argv.src || pkgMeta.src || 'NoSource';

        status(cwd).then((status) => {
            const version =
                `#ifndef VERSION_H \n
    #define VERSION_H \n
    #define VERSION "${pkgVersion}|${vendor}|${family}|${product}|${status.latest.hash}"\n
    #define SUPPORT "${support}"
    #define BUILD "${buildConfiguration}"
    #define FW_SRC "${firmwareSource}"
    #define CID "${cid}"\n
#endif`;

            write('./Version.h', version);

            debug.info(version);
        });
    });
};

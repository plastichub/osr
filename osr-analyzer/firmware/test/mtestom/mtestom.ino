#include <Controllino.h> /* Usage of CONTROLLINO library allows you to use CONTROLLINO_xx aliases in your sketch. */

#define POWER_0 CONTROLLINO_R10 // Primary power circuit
#define POWER_1 CONTROLLINO_R11 // Secondary power circuit
#define PLUNGER_MOTOR_1_DIR_PIN CONTROLLINO_R15
#define PLUNGER_MOTOR_1_STEP_PIN CONTROLLINO_R14
#define PLUNGER_MOTOR_2_STEP_PIN CONTROLLINO_R9

void setup()
{
  Serial.begin(19200);
  digitalWrite(POWER_0, HIGH);
  digitalWrite(POWER_1, HIGH);
}

void loop()
{
  digitalWrite(PLUNGER_MOTOR_1_DIR_PIN, 1);
  digitalWrite(PLUNGER_MOTOR_2_STEP_PIN, LOW);
  digitalWrite(PLUNGER_MOTOR_1_STEP_PIN, HIGH);
  delay(1000);
  // dFC.initFC();
}
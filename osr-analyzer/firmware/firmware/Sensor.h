#ifndef SENSOR_H
#define SENSOR_H
#include "Addon.h"

#include "SensorBase.h"
#include "SensorSignal.h"
#include "./common/ppmath.h"

#define EMIT_IF(CMP, S)        \
    if (TEST(_mask, S) && CMP) \
    {                          \
        emit(S);               \
    }

template <class T>
class Sensor : public SensorSignal,
               public SensorBase<T>
{
public:
    Sensor(Addon *__owner,
           uint __mask,
           AddonFnPtr __change,
           T _min,
           T _max) : SensorSignal(__owner, __mask, __change),
                     SensorBase<T>(_min, _max)
    {
    }

    void updateValue(T value)
    {
        EMIT_IF(this->eq(this->sValue, value), SENSOR_CALLBACK_MASK::SENSOR_CHANGED);
        EMIT_IF(this->sValue < this->sMin, SENSOR_CALLBACK_MASK::SENSOR_MIN);
        EMIT_IF(this->sValue > this->sMax, SENSOR_CALLBACK_MASK::SENSOR_MAX);
        
        storeValue(value);
    }
};

#endif
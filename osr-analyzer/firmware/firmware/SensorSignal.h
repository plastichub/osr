#ifndef SENSOR_SIGNAL_H
#define SENSOR_SIGNAL_H

#include "./Addon.h"

class SensorSignal
{
public:
    SensorSignal(Addon *__owner,
                 uint __mask,
                 AddonFnPtr __change) : _owner(__owner),
                                        _mask(__mask),
                                        _onChange(__change)
    {
    }

    short emit(int signal)
    {
        if (_owner && _onChange)
        {
            short ret = (_owner->*_onChange)(signal);
            return ret;
        }
    }

private:
    uint _mask;
    Addon *_owner;
    AddonFnPtr _onChange;
};

#endif
#ifndef TEMPERATURE_SENSOR_H
#define TEMPERATURE_SENSOR_H

#include <max6675.h>
#include "../config.h"
#include "../common/macros.h"

class TemperatureSensor
{

public:
  TemperatureSensor(short sck,
                    short cs,
                    short so,
                    short _max,
                    short _interval) : ktc(MAX6675(sck, cs, so)),
                                       value(-1),
                                       temperature_TS(millis()),
                                       maxTemp(_max),
                                       interval(_interval) {}

  bool ok()
  {
    return value < maxTemp;
  }

  void loop()
  {

    if (millis() - temperature_TS > interval)
    {
      temperature_TS = millis();
      value = ktc.readCelsius();
    }
  }

  short value;

private:
  MAX6675 ktc;
  short maxTemp;
  short interval;
  millis_t temperature_TS;
};

#endif

#ifndef SDM230MB_H
#define SDM230MB_H

#include "../config.h"
#include "../common/macros.h"
#include "../Addon.h"
#include "../ModbusBridge.h"

class SDM230MB : public Addon
{

public:
  SDM230MB(short _slaveAddr,
           int _tcpOutValue,
           int _tcpOutDelta,
           ModbusBridge *_mb,
           int _interval) : slaveAddr(_slaveAddr),
                            tcpOutValue(_tcpOutValue),
                            tcpOutDelta(_tcpOutDelta),
                            interval(_interval),
                            mb(_mb),
                            Addon("SDM230Modbus", SDM230, ADDON_STATED)
  {
    _start = 0;
    _regW = 0;
  }

private:
  short interval;
  short slaveAddr;

  int _start;
  bool printMBErrors;

  int tcpOutDelta;
  int tcpOutValue;

  ModbusBridge *mb;

  int _regW;
  int value() { return _regW - _start; }
  short start() { _start = _regW; }

  short readMB(short slaveAddress, int addr, int prio = 0)
  {
    Query *same = mb->nextSame(QUEUED, slaveAddress, addr, ku8MBReadHoldingRegisters, 10);
    if (same != NULL && millis() - same->ts < 1000)
    {
      return;
    }

    if (mb->numSame(QUEUED, slaveAddress, addr, ku8MBReadHoldingRegisters, 1) > 1)
    {
      return;
    }

    Query *next = mb->nextQueryByState(DONE);
    if (next != NULL)
    {
      next->fn = ku8MBReadHoldingRegisters;
      next->slave = slaveAddress;
      next->value = 30;
      next->addr = addr;
      next->state = QUERY_STATE::QUEUED;
      next->ts = millis();
      next->prio = prio;
      next->owner = SDM230;
      return E_OK;
    }
    else
    {
      Serial.print("Buffer full");
    }
    return E_QUERY_BUFFER_END;
  }
  short poll()
  {
    mb->nextWaitingTime = MODBUS_READ_WAIT;
    readMB(slaveAddr, 0);
  }

  short loop()
  {
    if (millis() - now > interval)
    {
      interval = now;

      Query *nextCommand = mb->nextQueryByState(QUERY_STATE::QUEUED);
      if (nextCommand != NULL)
      {
        nextCommand->state = QUERY_STATE::PROCESSING;
        mb->nextWaitingTime = MODBUS_CMD_WAIT;
        mb->onMessage = (AddonRxFn)&SDM230MB::rawResponse;
        mb->onError = (AddonFnPtr)&SDM230MB::onError;
        /*
            if (debugUpdate)
            {
                Serial.print("query slave : ");
                Serial.print(nextCommand->slave);
                Serial.print(" qid: ");
                Serial.print(nextCommand->id);
                Serial.print(" ts: ");
                Serial.print(nextCommand->ts);
                Serial.print(" fn: ");
                Serial.print(nextCommand->fn);
                Serial.println("----");
            }
            */
        mb->query(nextCommand->slave, nextCommand->fn, nextCommand->addr, nextCommand->value, this, (AddonFnPtr)&SDM230MB::responseFn);
      }
    }
  }

  short rawResponse(short size, uint8_t rxBuffer[])
  {
    Query *current = mb->nextQueryByState(PROCESSING, SDM230);
  }

  short responseFn(short error)
  {

    Query *last = mb->nextQueryByState(QUERY_STATE::PROCESSING, SDM230);
    if (!last)
    {
      Serial.println("nothing to process !");
      return;
    }
  }

  short onError(short error)
  {
    if (printMBErrors)
    {
      Serial.print("SDM230 :: onError ");
      if (error == 255)
      {
        Serial.println("SDM230 :: Timeout");
      }
      else
      {
        Serial.println(error);
      }
    }
    Query *last = mb->nextQueryByState(QUERY_STATE::PROCESSING, SDM230);
    if (last)
    {
      last->reset();
    }
    else
    {
      Serial.println("SDM230 :: onError - can't find last query! ");
    }
  }

  short updateTCP()
  {
    if (tcpOutValue)
    {
      mb->mb->R[tcpOutValue] = _regW;
    }
    if (tcpOutDelta)
    {
      mb->mb->R[tcpOutDelta] = value();
    }
  }
};

#endif

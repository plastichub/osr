#ifndef TEMPERATURE_SENSORMB_H
#define TEMPERATURE_SENSORMB_H

#include "../config.h"
#include "../common/macros.h"
#include "../common/ppmath.h"

#include "./TemperatureSensor.h"

#include "../Addon.h"
#include "../Sensor.h"
#include "../ModbusBridge.h"

// Max6675 Modbus TCP wrapper
class TemperatureSensorMB : public Addon, public Sensor<uint>

{

public:
  TemperatureSensorMB(short sck,
                      short cs,
                      short so,
                      short _interval,
                      ModbusBridge *_mb,
                      // optional : read relay on signal
                      short _relayOnPin,
                      // optional : emit temperature sensor value on Modbus TCP
                      int _tcpOutValue,
                      // optional : emit relay active signal on Modbus TCP
                      int _tcpOutRelay,
                      // optional : sensor callback owner
                      Addon *owner,
                      // optional : sensor callback
                      AddonFnPtr sensorChange) : sensor(sck, cs, so, -1, _interval),
                                             interval(_interval),
                                             mb(_mb),
                                             tcpOutValue(_tcpOutValue),
                                             tcpOutRelay(_tcpOutRelay),
                                             relayOnPin(_relayOnPin),
                                             relayOn(false),
                                             Addon("TemperatureSensorMAX66775", MAX6675MB, ADDON_STATED),
                                             Sensor(owner, 0, sensorChange, TEMP_CLAMP_MIN, TEMP_CLAMP_MAX)
  {
  }

  short loop()
  {
    sensor.loop();
    if (relayOnPin)
    {
      relayOn = analogRead(relayOnPin) > ANALOG_LOGIC_ON_LEVEL;
    }
    updateValue(TEMP_SENSOR_SCALE * (TEMP_SENSOR_OFFSET + sensor.value));
    updateTCP();
  }

private:
  TemperatureSensor sensor;
  short interval;
  short relayOnPin;

  bool relayOn;

  int tcpOutValue;
  int tcpOutRelay;
  ModbusBridge *mb;

  short updateTCP()
  {
    if (tcpOutValue)
    {
      mb->mb->R[tcpOutValue] = sValue;
    }

    if (tcpOutRelay)
    {
      mb->mb->R[tcpOutRelay] = relayOn ? TEMP_RELAY_ON_LEVEL : TEMP_RELAY_OFF_LEVEL;
    }
  }
};

#endif

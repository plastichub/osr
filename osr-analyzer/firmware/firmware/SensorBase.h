#ifndef SENSOR_BASE_H
#define SENSOR_BASE_H

template <class T>
class SensorBase
{
public:
    SensorBase(T _min, T _max) : sMin(_min), sMax(_max)
    {
    }

    virtual void storeValue(T value)
    {
        sOld = sValue;
        sValue = value;
    }

    T sValue;
    T sMin;
    T sMax;
    T sOld;

    virtual bool is_eq(T value)
    {
        return sValue != value;
    }

    virtual bool is_eq(float value)
    {
        return (int)sValue != (int)value;
    }

private:
};

#endif
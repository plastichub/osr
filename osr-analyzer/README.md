# osr-analyzer
Various tools for HW analysis

- [ ] IR heat maps
- [ ] PID oscillation
- [ ] AC motor amperage logger
- [ ] OSI Melt flow rate

 
# scopes

- [ ] [gh:Rigol ds1054z](https://github.com/pklaus/ds1054z)

# matlab 

- [ ] [site:JS - REST](https://www.mathworks.com/help/mps/restfuljson/example-web-based-bond-pricing-tool-using-javascript.html)

import * as CLI from 'yargs';
import { crawler } from '../lib/net/crawler';

import * as path from 'path';
import { sync as exists } from '@plastichub/fs/exists';
import { sync as dir } from '@plastichub/fs/dir';

import { sync as read } from '@plastichub/fs/read';
import { sync as write } from '@plastichub/fs/write';

import { sync as rm } from '@plastichub/fs/remove';
import { sync as inspect } from "@plastichub/fs/inspect_tree";
import * as debug from '../log';
import { INode } from '@plastichub/fs/interfaces';
import { sync as treeWalker } from '@plastichub/fs/utils/tree_walker';
const fg = require('fast-glob');
import * as download from 'download';
import * as cheerio from 'cheerio';

const defaultOptions = (yargs: CLI.Argv) => {
    return yargs.option('output', {
        default: './',
        describe: 'The output directory'
    }).option('debug', {
        default: 'false',
        describe: 'Enable internal debug message'
    })
};

let options = (yargs: CLI.Argv) => defaultOptions(yargs);

// removes orhpaned or spammy topics
function clean(folder) {

    const data = {
        remove: []
    }
    const visitor = (path: string, inspectData: INode) => {
        if (inspectData.type === 'dir') {
            const files = fg.sync('*.json', { dot: true, cwd: path, absolute: true });
            if (files.length == 1) {                
                const topic = read(files[0], 'json') as any;
                if (topic.saved == 0 &&
                    topic.nbReplies == 0 &&
                    topic.replies.length == 0 &&
                    topic.pics.length == 0 &&
                    !topic.likes) {
                    console.log("delete " + path);
                    data.remove.push(path);
                }
            }
        }
    };
    treeWalker(folder, {
        inspectOptions: {
            mode: true,
            symlinks: true
        }
    }, visitor);

    return data;
}

function index(folder, dst) {

    const data = {
        topics: {},
        users: {},
        mostLiked: [],
        links: [],
        youtube: [],
        images: []
    }

    let image_index: string = '';

    const image = (url, src) => `<a href="${url}"><img src="${src}" style="max-width:300px;float:left;margin:8px"/><a><br/>\n\n`;

    const visitor = (path: string, inspectData: INode) => {
        if (inspectData.type === 'dir') {
            const files = fg.sync('*.json', { dot: true, cwd: path, absolute: true });
            if (files.length) {
                let image_topic_index = null;
                let hasImages = false;
                files.forEach(f => {
                    const topic = read(f, 'json') as any;
                    if (!image_topic_index) {
                        image_topic_index = `<div style="clear:both">\n\n## [${topic.title}](${topic.url})\n\n</div>\n`;
                    }
                    let name = inspectData.name;
                    if (topic.url.split('page').length == 2) {
                        name = name + "-" + parseInt(topic.url.split('page')[1].replace('/', ''));
                    }
                    data.topics[name] = topic;
                    if (!data.users[topic.authorName]) {
                        data.users[topic.authorName] = {
                            posts: 0
                        }
                    }
                    data.users[topic.authorName].posts++;
                    const hasLink = (url) => {
                        data.links.forEach((e) => {
                            if (e.url === url) {
                                return true;
                            }
                        });
                        return false;
                    }

                    const hasYoutube = (url) => {
                        data.youtube.forEach((e) => {
                            if (e.url === url) {
                                return true;
                            }
                        });
                        return false;
                    }

                    const hasImage = (url) => {
                        data.images.forEach((e) => {
                            if (e.url === url) {
                                return true;
                            }
                        });
                        return false;
                    }

                    if (topic.replies) {
                        topic.replies.forEach(r => {
                            if (!data.users[r.user]) {
                                data.users[r.user] = {
                                    posts: 0
                                }
                            }
                            data.users[r.user].posts++;

                            const $ = cheerio.load(r.replyBody as string, {
                                xmlMode: true
                            });

                            $('a').each(function () {
                                const url = $(this).attr("href");
                                if (url && url.indexOf && !hasLink(url)) {
                                    if (url.indexOf('davehakkens.nl') === -1 && url.indexOf('onearmy.world') === -1) {
                                        data.links.push({ url: url, topic: topic.url });
                                    }
                                }

                                if (url && url.indexOf && !hasYoutube(url) && url.indexOf('youtube') !== -1) {
                                    data.youtube.push({ url: url, topic: topic.url });
                                }
                            });

                            $('img').each(function () {
                                const url = $(this).attr("src");
                                if (!hasImage(url)) {
                                    data.images.push({ url: url, topic: topic.url });
                                    if (url.indexOf('emoji') == -1) {
                                        hasImages = true;
                                        image_topic_index += image(topic.url, url);
                                    }
                                }
                            });

                        });
                    }

                    const $ = cheerio.load(topic.body as string, {
                        xmlMode: true
                    });

                    $('a').each(function () {
                        const url = $(this).attr("href");
                        if (!hasLink(url)) {
                            if (url.indexOf('davehakkens.nl') === -1 && url.indexOf('onearmy.world') === -1) {
                                data.links.push({ url: url, topic: topic.url });
                            }
                        }

                        if (url && url.indexOf && !hasYoutube(url) && url.indexOf('youtube') !== -1) {
                            data.youtube.push({ url: url, topic: topic.url });
                        }
                    });

                    $('img').each(function () {
                        const url = $(this).attr("src");
                        if (!hasImage(url)) {
                            data.images.push({ url: url, topic: topic.url });
                            if (url.indexOf('emoji') == -1) {
                                hasImages = true;
                                image_topic_index += image(topic.url, url);
                            }
                        }
                    });
                });

                if (hasImages) {
                    image_index += image_topic_index;
                }
            }
        }
    };


    treeWalker(folder, {
        inspectOptions: {
            mode: true,
            symlinks: true
        }
    }, visitor);

    write(dst + '/index_urls.json', data.links);
    delete data['links'];

    write(dst + '/index_youtube.json', data.youtube);
    delete data['youtube'];

    write(dst + '/index_images.json', data.images);
    delete data['images'];

    write(dst + '/index.json', data);

    write(dst + '/images.md', image_index);

    const remove = clean(folder).remove;
    remove.map((f) => rm(f));
    return data;

}

// node ./build/main.js forum --type="download" --output="../forum"
// node ./build/main.js forum --type="index" --input="../forum/topics" --output="../forum"
export const register = (cli: CLI.Argv) => {
    return cli.command('forum', 'forum crawler', options, async (argv: CLI.Arguments) => {
        process.env['APIFY_LOCAL_STORAGE_DIR'] = '.';
        rm(path.resolve('./request_queues'));
        if (argv.help) { return; }
        const dst = path.resolve('' + argv.output || './');
        if (!exists(dst)) {
            dir(dst);

        }

        if (argv.type === "index") {
            const input = path.resolve('' + argv.input);
            const output = path.resolve('' + argv.output);
            index(input, output);
        }

        if (argv.type === "download") {
            const map = [
                {
                    url: 'https://davehakkens.nl/community/forums/forum/general/community/',
                    prefix: 'topics/community'
                },
                {
                    url: 'https://davehakkens.nl/community/forums/forum/precious-plastic/general/',
                    prefix: 'topics/general'
                },
                {
                    url: 'https://davehakkens.nl/community/forums/forum/precious-plastic/development/',
                    prefix: 'topics/development'
                },
                {
                    url: 'https://davehakkens.nl/community/forums/forum/precious-plastic/research/',
                    prefix: 'topics/research'
                },
                {
                    url: 'https://davehakkens.nl/community/forums/forum/general/your-own-project/',
                    prefix: 'topics/creations'
                }
            ]
            const i = 0;
            crawler(map[i].url, argv, map[i].prefix);
        }
    });
};

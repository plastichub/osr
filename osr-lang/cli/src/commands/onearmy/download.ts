import * as CLI from 'yargs';
import * as debug from '../../log';
import * as utils from '../../lib/common/strings';
import { Puppeteer } from '../../renderer/puppeteer';

import { getScope, Scope } from '../../renderer/puppeteer/Scope';

import { Options, info, inspect, default_path_crawler, default_path_howto } from '../../';
import { Request } from 'puppeteer';

const YAML = require('json-to-pretty-yaml');
// import { download } from '../../lib/net/download';

import * as path from 'path';
import { sync as read } from '@plastichub/fs/read';
import { sync as exists } from '@plastichub/fs/exists';
import { sync as dir } from '@plastichub/fs/dir';
import { sync as write } from '@plastichub/fs/write';
import { sync as remove } from '@plastichub/fs/remove';
import { sync as rm } from '@plastichub/fs/remove';
import { Converter } from 'showdown';

var escapeHtml = require('escape-html');

import { launch, Page, Response } from 'puppeteer'
import * as download from 'download';
import { read_fragments, resolveConfig, substitute } from '../../lib';

const fg = require('fast-glob');
const pretty = require('pretty');
let root: string;

const defaultOptions = (yargs: CLI.Argv) => {
    return yargs.option('output', {
        default: '../../onearmy/howtos',
        describe: 'The output directory'
    }).option('onearmy', {
        default: '../../onearmy',
        describe: 'location of onearmy folder'
    }).option('src', {
        default: '../../onearmy/data/2020/08/raw.json',
        describe: 'The source file'
    }).option('debug', {
        default: 'false',
        describe: 'Enable internal debug message'
    }).option('type', {
        default: 'user'
    })
};

let options = (yargs: CLI.Argv) => defaultOptions(yargs);

async function onResponse(response: Response, scope: Scope) {

    const url = response.url();
    if (url.includes('.js')) {
        console.log('url ', url);
    };
    if (url.includes('firestore')) {
        const data = await response.text();
        if (data.includes('documentChange')) {
            info('got user detail : ' + url);
            await scope.page.evaluate(() => {
            });
        }
    }
}

async function onRequest(request: Request, scope: Scope) {

    if (request.url().endsWith('.js')) {
        console.log('req ' + request.url());
    }

    if (request.url().endsWith('2.34918441.chunk.js')) {
        let patched = read(path.resolve(`${root}/static/js/2.4511a5ff.chunk.js`), 'string') as string;
        console.log('patch ' + `${root}/static/js/sub.js` + ": " + patched.length);
        request.respond({
            status: 200,
            contentType: 'application/javascript; charset=utf-8',
            body: patched as string
        });
        // request.continue();
        return;
    } else {
        request.continue();
    }

    if (request.url().endsWith('main.4430561e.chunk.js')) {

        let patched = read(path.resolve(`${root}/static/js/main.8469d707.chunk.js`), 'string') as string;
        console.log('patch ' + `${root}/static/js/main.js` + ": " + patched.length);

        request.respond({
            status: 200,
            contentType: 'application/javascript; charset=utf-8',
            body: patched as string
        });
        // request.continue();
    } else {
        request.continue();
    }
}

let browser;

let scope: Scope;

async function downloadStep(dst, s) {
    await Promise.all(s.images.map((i) => {
        const image = path.resolve(`${dst}/${i.name}`);
        if (!exists(image)) {
            try {
                console.log(`download step ` + i.downloadUrl);
                return download(i.downloadUrl, dst,{                    
                });
            } catch (e) {
                debug.error('error download step image', e);
            }
        }
    }));
}

async function downloadFiles(dst, h, fetchFiles) {
    await Promise.all(h.files.map((i) => {
        if (!fetchFiles) {
            return Promise.resolve();
        }
        const image = path.resolve(`${dst}/${i.name}`);
        if (!exists(image) && fetchFiles) {
            try {
                return download(i.downloadUrl, dst);
            } catch (e) {
                debug.error('error download step file', e);
            }
        }
    }));
}

function removeEmojis(string) {
    return string.replace(/([#0-9]\u20E3)|[\xA9\xAE\u203C\u2047-\u2049\u2122\u2139\u3030\u303D\u3297\u3299][\uFE00-\uFEFF]?|[\u2190-\u21FF][\uFE00-\uFEFF]?|[\u2300-\u23FF][\uFE00-\uFEFF]?|[\u2460-\u24FF][\uFE00-\uFEFF]?|[\u25A0-\u25FF][\uFE00-\uFEFF]?|[\u2600-\u27BF][\uFE00-\uFEFF]?|[\u2900-\u297F][\uFE00-\uFEFF]?|[\u2B00-\u2BF0][\uFE00-\uFEFF]?|(?:\uD83C[\uDC00-\uDFFF]|\uD83D[\uDC00-\uDEFF])[\uFE00-\uFEFF]?/g, '');
}

function createTextLinks_(text) {
    return (text || "").replace(
        /([^\S]|^)(((https?\:\/\/)|(www\.))(\S+))/gi,
        function (match, space, url) {
            var hyperlink = url;
            if (!hyperlink.match('^https?:\/\/')) {
                hyperlink = 'http://' + hyperlink;
            }
            return space + '<a href="' + hyperlink + '">' + url + '</a>';
        }
    );
};

async function downloadHowto(dst, howto, data, fragments, templates, fetchFiles = false) {

    const out = path.resolve(`${dst}/${howto.slug}`);
    if (exists(out)) {
        // return;
    }
    !exists(out) && dir(out);

    const cover = path.resolve(`${dst}/${howto.slug}/${howto.cover_image.name}`);
    if (!exists(cover)) {
        try {
            fetchFiles && await download(howto.cover_image.downloadUrl, out);
        } catch (e) {
            debug.error('error download cover image', e);
        }
    }

    fetchFiles && await Promise.all(howto.steps.map((s) => downloadStep(out, s)));

    fetchFiles && await downloadFiles(out, howto, fetchFiles);

    howto.steps.forEach((s, i) => {
        const step = path.resolve(`${out}/step_${i}.md`);
        const stepText = `### ${s.title} \n\n ${s.text}`;
        write(step, stepText);
    })

    const tags = data.v3_tags;
    const howtoTags = [];
    for (const ht in howto.tags) {
        const gt = tags.find((t) => t._id === ht);
        if (gt) {
            howtoTags.push(gt.label);
        }
    }

    howto.slug = howto.slug.trim();
    howto.tags = howtoTags;
    howto.user = data.v3_mappins.find((u) => u._id == howto._createdBy);

    const howtoHeader = path.resolve(`${out}/howto_in.md`);
    const header = `### ${howto.title} \n\n\n${howto.description}`;
    write(howtoHeader, header);

    const howto_config = path.resolve(`${out}/config.yaml`);
    const config = YAML.stringify(howto);
    write(howto_config, config);

    const howto_config_json = path.resolve(`${out}/config.json`);
    const config_json = JSON.stringify(howto, null, 2);
    write(howto_config_json, config_json);

    let s: string = '';

    const index_md = path.resolve(`${dst}/../_howtos/${howto.slug.trim()}.md`);
    let step_template = "" + templates.step;

    const step_image = (i) => {
        const image = `/howtos/${howto.slug}/${i.name}`;
        return `
            <div class="col-sm">
                <a href="${image}">
                    <img class="step-image" src="${image}"/>
                </a>
            </div>
            `
    }

    const step_images = (s) => {
        const images = s.images.map(step_image).join('<br/>\n');
        return `<div class="row">
                ${images}
            </div>
        `
    }

    const step = (s, i) => {
        const t = substitute(step_template, {
            title: s.title,
            text: createTextLinks_(escapeHtml(s.text.trim()).replace(/(?:\r\n|\r|\n)/g, '<br/>\n\n')),
            step_number: i + 1,
            images: step_images(s)
        });
        return t;
    }

    const steps = howto.steps.map((s, i) => step(s, i)).join('\n<br/>\n\n\n');


    const files = howto.files.map((f) => {
        return `<li><a href="${f.downloadUrl}">${f.name}</a></li>`
    })

    

    howto.description = removeEmojis(howto.description);
    howto.description = createTextLinks_(howto.description);
    
    let index = substitute(templates.howto, {
        ...fragments,
        image: `/howtos/${howto.slug}/${howto.cover_image.name}`,
        title: howto.title.trim(),
        description: escapeHtml(howto.description.trim()).replace(/(?:\r\n|\r|\n)/g, '<br/>') || "",
        config: YAML.stringify({
            tags: howto.tags
        }),
        enabled: howto.moderation == "accepted" ? true : false,
        steps: pretty(steps, { ocd: true }),
        keywords: howtoTags.join(','),
        user: howto._createdBy,
        files: `<h3>Files</h3>
                <ul>${files}</ul>`

    });

    debug.log("write howto " + index_md);

    write(index_md, pretty(index, { ocd: true }));

    if (howto.moderation !== "accepted") {
        rm(index_md);
    }

}

//node ./build/main.js onearmy-download --type=howto --src="../../../PlasticHubOld/pp-next/data/2021/01/raw.json" --output=../../../PlasticHubOld/pp-next/howtos --download=false --onearmy="../../../PlasticHubOld/pp-next"

// node ./build/main.js onearmy-download --type=user --output=../../onearmy/user --download=false --onearmy="../../onearmy"

export const register = (cli: CLI.Argv) => {
    return cli.command('onearmy-download', '', options, async (argv: CLI.Arguments) => {
        if (argv.help) { return; }

        const src = path.resolve('' + argv.src || './');
        root = path.resolve(`${argv.onearmy}`);
        const isDebug = argv.debug === 'true';
        const data = read(src, 'json') as any;
        const download = argv.download === 'true';
        const type = argv.type;

        if (type === 'howto') {
            // global config
            const cPath = argv.onearmy ? path.resolve(`${argv.onearmy}/templates/site/config.json`) : path.resolve('./config.json');
            isDebug && debug.info(`read config at ${cPath}`);
            const config = read(cPath, 'json') as any;

            // jekyll howtos pages root
            const machines_directory = path.resolve(`${argv.onearmy}/howtos/`);

            const templates_path = path.resolve(`${argv.onearmy}/templates/jekyll`);
            if (!exists(templates_path)) {
                debug.error(`\t Cant find templates at ${templates_path}, path doesn't exists`);
                return;
            }

            let fragments: any = { ...config };

            read_fragments(templates_path, fragments, "product_rel_path_name", "machine");

            let template = read(path.resolve(`${templates_path}/howto.md`), 'string');
            let step = read(path.resolve(`${templates_path}/step.md`), 'string');

            resolveConfig(fragments);

            const dst = path.resolve('' + argv.output || './');
            let howtos = data.v3_howtos as any[];
            if (!exists(dst)) {
                dir(dst);
            }

            await Promise.all(howtos.map((h) => downloadHowto(dst, h, data, fragments, {
                howto: template,
                step: '' + step
            }, download)));

            const censored = {
                users: data.v3_mappins.filter((u) => u.moderation == 'rejected').length,
                howtos: howtos.filter((u) => u.moderation !== 'accepted').length
            }

            debug.inspect('censored', censored);

            write(path.resolve(dst + '/censored.json'), JSON.stringify(censored, null, 2));
            write(path.resolve(dst + '/censored_users.json'), JSON.stringify(data.v3_mappins.filter((u) => u.moderation == 'rejected'), null, 2));

        }

        if (type === 'user') {
            let users = data.v3_mappins as any[];
            let user = users[1];
            const url = user.detail.profileUrl;
            const options = {
                headless: false,
                url: url
            };

            scope = getScope(options);
            //scope.onResponse = onResponse;
            //scope.onRequest = onRequest;
            await scope.init();
            await scope.page.goto(options.url, {
                timeout: 10000,
                waitUntil: 'networkidle0'
            }).then((v) => {
                console.log('ready');
            }).catch((e) => {
                scope.page.content().then((c) => {
                    console.log(c);
                    write(path.resolve(`${root}/data/user/${user['_id']}.html`), c);
                })

            });
        }

        if (argv.debug) {
        }
    });
};

import * as CLI from 'yargs';
import * as debug from '../../log';
import * as utils from '../../lib/common/strings';
import { Puppeteer } from '../../renderer/puppeteer';

import { getScope, Scope } from '../../renderer/puppeteer/Scope';

import { Options, info, inspect, default_path_crawler, default_path_howto } from '../..';
import { Request } from 'puppeteer';

const YAML = require('json-to-pretty-yaml');
// import { download } from '../../lib/net/download';

import * as path from 'path';
import { sync as read } from '@plastichub/fs/read';
import { sync as exists } from '@plastichub/fs/exists';
import { sync as dir } from '@plastichub/fs/dir';
import { sync as write } from '@plastichub/fs/write';
import { sync as remove } from '@plastichub/fs/remove';
import { sync as rm } from '@plastichub/fs/remove';
import { Converter } from 'showdown';
var TurndownService = require('turndown');
import { Discourser } from './index';

var escapeHtml = require('escape-html');

import { launch, Page, Response } from 'puppeteer'
import * as download from 'download';
import { read_fragments, resolveConfig, substitute } from '../../lib';

const fg = require('fast-glob');
const pretty = require('pretty');
let root: string;


const defaultOptions = (yargs: CLI.Argv) => {
    return yargs.option('output', {
        default: '../../onearmy/howtos',
        describe: 'The output directory'
    }).option('onearmy', {
        default: '../../onearmy',
        describe: 'location of onearmy folder'
    }).option('src', {
        default: '../../onearmy/data/2020/08/raw.json',
        describe: 'The source file'
    }).option('debug', {
        default: 'false',
        describe: 'Enable internal debug message'
    }).option('type', {
        default: 'user'
    })
};

let options = (yargs: CLI.Argv) => defaultOptions(yargs);

async function onResponse(response: Response, scope: Scope) {

    const url = response.url();
    if (url.includes('.js')) {
        console.log('url ', url);
    };
    if (url.includes('firestore')) {
        const data = await response.text();
        if (data.includes('documentChange')) {
            info('got user detail : ' + url);
            await scope.page.evaluate(() => {
            });
        }
    }
}

async function onRequest(request: Request, scope: Scope) {

    if (request.url().endsWith('.js')) {
        console.log('req ' + request.url());
    }

    if (request.url().endsWith('2.34918441.chunk.js')) {
        let patched = read(path.resolve(`${root}/static/js/2.4511a5ff.chunk.js`), 'string') as string;
        console.log('patch ' + `${root}/static/js/sub.js` + ": " + patched.length);
        request.respond({
            status: 200,
            contentType: 'application/javascript; charset=utf-8',
            body: patched as string
        });
        // request.continue();
        return;
    } else {
        request.continue();
    }

    if (request.url().endsWith('main.4430561e.chunk.js')) {

        let patched = read(path.resolve(`${root}/static/js/main.8469d707.chunk.js`), 'string') as string;
        console.log('patch ' + `${root}/static/js/main.js` + ": " + patched.length);

        request.respond({
            status: 200,
            contentType: 'application/javascript; charset=utf-8',
            body: patched as string
        });
        // request.continue();
    } else {
        request.continue();
    }
}

let browser;

let scope: Scope;

async function downloadStep(dst, s) {
    await Promise.all(s.images.map((i) => {
        const image = path.resolve(`${dst}/${i.name}`);
        if (!exists(image)) {
            try {
                console.log(`download step ` + i.downloadUrl);
                return download(i.downloadUrl, dst);
            } catch (e) {
                debug.error('error download step image', e);
            }
        }
    }));
}

async function downloadFiles(dst, h, fetchFiles) {
    await Promise.all(h.files.map((i) => {
        if (!fetchFiles) {
            return Promise.resolve();
        }
        // downloadStep(out, s)));
        const image = path.resolve(`${dst}/${i.name}`);
        if (!exists(image) && fetchFiles) {
            try {
                return download(i.downloadUrl, dst);
            } catch (e) {
                debug.error('error download step file', e);
            }
        }
    }));
}
let did = false;

async function downloadHowto(dst, howto, data, fragments, templates, fetchFiles = false) {

    const out = path.resolve(`${dst}/${howto.slug}`);
    if (exists(out)) {
        // return;
    }
    !exists(out) && dir(out);

    const cover = path.resolve(`${dst}/${howto.slug}/${howto.cover_image.name}`);
    if (!exists(cover)) {
        try {
            fetchFiles && await download(howto.cover_image.downloadUrl, out);
        } catch (e) {
            debug.error('error download cover image', e);
        }
    }

    fetchFiles && await Promise.all(howto.steps.map((s) => downloadStep(out, s)));

    fetchFiles && await downloadFiles(out, howto, fetchFiles);

    // console.log(`download ${howto.slug} `);

    howto.steps.forEach((s, i) => {
        const step = path.resolve(`${out}/step_${i}.md`);
        const stepText = `### ${s.title} \n\n ${s.text}`;
        write(step, stepText);
    })

    const tagData = {
        "_created": "2019-11-27T16:51:14.169Z",
        "_deleted": false,
        "_id": "7rdha1rbfxRSjHbfsv4a",
        "_modified": "2019-11-27T16:51:14.169Z",
        "categories": [
            "how-to"
        ],
        "label": "starterkit"
    };

    const tags = data.v3_tags;
    const howtoTags = [];
    for (const ht in howto.tags) {
        const gt = tags.find((t) => t._id === ht);
        if (gt) {
            howtoTags.push(gt.label);
        }
    }

    howto.slug = howto.slug.trim();
    howto.tags = howtoTags;
    howto.user = data.v3_mappins.find((u) => u._id == howto._createdBy);

    const howtoHeader = path.resolve(`${out}/howto_in.md`);
    const header = `### ${howto.title} \n\n\n${howto.description}`;
    write(howtoHeader, header);

    const howto_config = path.resolve(`${out}/config.yaml`);
    const config = YAML.stringify(howto);
    write(howto_config, config);

    const howto_config_json = path.resolve(`${out}/config.json`);
    const config_json = JSON.stringify(howto, null, 2);
    write(howto_config_json, config_json);

    let s: string = '';

    const index_md = path.resolve(`${dst}/../_howtos/${howto.slug.trim()}.md`);
    // const index_md_old = path.resolve(`${dst}/${howto.slug}/index.md`);
    // remove(index_md_old);

    let step_template = "" + templates.step;

    const step_image = (i) => {
        const image = `/howtos/${howto.slug}/${i.name}`;
        return `
            <div class="col-sm">
                <a href="${image}">
                    <img class="step-image" src="${image}"/>
                </a>
            </div>
            `
    }

    const step_images = (s) => {
        const images = s.images.map(step_image).join('<br/>\n');
        return `<div class="row">
                ${images}
            </div>
        `
    }

    const step = (s, i) => {
        const t = substitute(step_template, {
            title: s.title,
            text: escapeHtml(s.text.trim()).replace(/(?:\r\n|\r|\n)/g, '<br/>\n\n'),
            step_number: i + 1,
            images: step_images(s)
        });
        return t;
    }

    const steps = howto.steps.map((s, i) => step(s, i)).join('\n<br/>\n\n\n');


    const files = howto.files.map((f) => {
        return `<li><a href="${f.downloadUrl}">${f.name}</a></li>`
    })

    let index = substitute(templates.howto, {
        ...fragments,
        image: `/howtos/${howto.slug}/${howto.cover_image.name}`,
        title: howto.title.trim(),
        description: escapeHtml(howto.description.trim()).replace(/(?:\r\n|\r|\n)/g, '<br/>') || "",
        config: YAML.stringify({
            tags: howto.tags
        }),
        steps: pretty(steps, { ocd: true }),
        keywords: howtoTags.join(','),
        user: howto._createdBy,
        files: `<h3>Files</h3>
                <ul>${files}</ul>`

    });
    write(index_md, pretty(index, { ocd: true }));

}

export const convert = (input: string) => {
    var turndownService = new TurndownService()
    return turndownService.turndown(input);
}

//node ./build/main.js onearmy-download --type=howto --output=../../onearmy/howtos --download=false
// node ./build/main.js discourse --type=user --output=../../onearmy/user --download=false --onearmy="../../onearmy"
export const register = (cli: CLI.Argv) => {
    return cli.command('discourse', '', options, async (argv: CLI.Arguments) => {
        if (argv.help) { return; }

        let d: Discourser = new Discourser({
            host: 'https://forum.plastic-hub.com',
            key: 'cb71e96b2eb5e4a1924105dc91a9f0380d933b14dd0f53cfbc7ce22e5c669cc9',
            username: 'admin',
            rateLimitConcurrency: 1

        });
        //let cats = await d.getCategories();
        //debug.inspect('cats', cats);
        // let topics = d.getTopics();
        // debug.inspect('posts', topics);
        let bodyLong = "\n<p>Hey everybody,<br>\nI’ve summarised a bit of the various posts on the motors that can be found around in this forum, and wrote this <strong><a href=\"https://docs.google.com/document/d/1w5OhOzIXYDGl8slvoAxqdnRk757hVYFX0evk88c2Shw/edit?usp=sharing\" target=\"_blank\" rel=\"nofollow\">Google Doc</a></strong> that you can all see, comment and edit.</p>\n<p>check it out and help complete it if you can / want.<br>\nThe idea is to start preparing a simple, clean doc about the motors to expand the awesome specsheets that <a href=\"https://davehakkens.nl/community/members/davehakkens/\" rel=\"nofollow\">@davehakkens</a> produced. </p>\n<p>let’s keep the messy discussion here in the forum or as comments in the doc and there put only the final results.</p>\n<p>It would be great to put some <strong>examples, with the pictures of the label of the working motors that people are using</strong>.<br>\n<a href=\"https://davehakkens.nl/community/members/davehakkens/\" rel=\"nofollow\">@davehakkens</a> could you post a pic of yours or is it all painted in grey? <img draggable=\"false\" class=\"emoji\" alt=\"😉\" src=\"https://s.w.org/images/core/emoji/11/svg/1f609.svg\"></p>\n<p>If anyone here has made the machines and they are working, <strong>please post images of the nameplate of your motor and what the pros and cons you see in them</strong></p>\n<p>this doc builds on many other posts and contributions that you can find in these forums:<br>\n– <a href=\"http://onearmy.world/community/forums/topic/motors/\" target=\"_blank\" rel=\"nofollow\">Motors</a><br>\n– <a href=\"http://onearmy.world/community/forums/topic/shredder-motor/#post-69044\" target=\"_blank\" rel=\"nofollow\">Shredder Motor</a><br>\n– <a href=\"http://onearmy.world/community/forums/topic/realistic-coats-shredder/\" target=\"_blank\" rel=\"nofollow\">Realistic Cost Shredder</a><br>\n– <a href=\"http://onearmy.world/community/forums/search/motor/\" target=\"_blank\" rel=\"nofollow\">All posts that name “motor”</a></p>\n<p>Let me know what you think<br>\nthanks to all</p>\n<p>ps I’m in Malaga, Spain too if any other spaniard wants to hook up. We also found some providers we can talk about and if we order all together the pieces maybe we get a discount <img draggable=\"false\" class=\"emoji\" alt=\"😉\" src=\"https://s.w.org/images/core/emoji/11/svg/1f609.svg\"></p>\n";
        let b = await d.createPost("This makes now some more sense 2", convert(bodyLong), 6);
        return;

        const src = path.resolve('' + argv.src || './');
        root = path.resolve(`${argv.onearmy}`);
        const isDebug = argv.debug === 'true';
        const data = read(src, 'json') as any;
        const download = argv.download === 'true';
        const type = argv.type;
        if (type === 'howto') {

            // global config
            const cPath = argv.onearmy ? path.resolve(`${argv.onearmy}/templates/site/config.json`) : path.resolve('./config.json');
            isDebug && debug.info(`read config at ${cPath}`);
            const config = read(cPath, 'json') as any;

            // jekyll howtos pages root
            const machines_directory = path.resolve(`${argv.onearmy}/howtos/`);

            const templates_path = path.resolve(`${argv.onearmy}/templates/jekyll`);
            if (!exists(templates_path)) {
                debug.error(`\t Cant find templates at ${templates_path}, path doesn't exists`);
                return;
            }

            let fragments: any = { ...config };

            read_fragments(templates_path, fragments, "product_rel_path_name", "machine");

            let template = read(path.resolve(`${templates_path}/howto.md`), 'string');
            let step = read(path.resolve(`${templates_path}/step.md`), 'string');

            resolveConfig(fragments);

            const dst = path.resolve('' + argv.output || './');
            let howtos = data.v3_howtos as any[];
            if (!exists(dst)) {
                dir(dst);
            }
            await Promise.all(howtos.map((h) => downloadHowto(dst, h, data, fragments, {
                howto: template,
                step: '' + step
            }, download)));

            const censored = {
                users: data.v3_mappins.filter((u) => u.moderation == 'rejected').length,
                howtos: howtos.filter((u) => u.moderation !== 'accepted').length
            }

            debug.inspect('censored', censored);

            write(path.resolve(dst + '/censored.json'), JSON.stringify(censored, null, 2));
            write(path.resolve(dst + '/censored_users.json'), JSON.stringify(data.v3_mappins.filter((u) => u.moderation == 'rejected'), null, 2));

        }

        if (type === 'user') {
            let users = data.v3_mappins as any[];
            let user = users[1];
            const url = user.detail.profileUrl;
            const options = {
                headless: false,
                url: url
            };

            scope = getScope(options);
            //scope.onResponse = onResponse;
            //scope.onRequest = onRequest;
            await scope.init();
            await scope.page.goto(options.url, {
                timeout: 10000,
                waitUntil: 'networkidle0'
            }).then((v) => {
                console.log('ready');
            }).catch((e) => {
                scope.page.content().then((c) => {
                    console.log(c);
                    write(path.resolve(`${root}/data/user/${user['_id']}.html`), c);
                })

            });



        }

        if (argv.debug) {
        }
    });
};

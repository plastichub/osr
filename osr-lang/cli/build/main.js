#!/usr/bin/env node
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const _cli_1 = require("./_cli");
_cli_1.defaults();
const cli = require("yargs");
const markdown_1 = require("./commands/markdown");
markdown_1.register(cli);
const pdf2jpg_1 = require("./commands/pdf2jpg");
pdf2jpg_1.register(cli);
const svg2jpg_1 = require("./commands/svg2jpg");
svg2jpg_1.register(cli);
const watch_1 = require("./commands/watch");
watch_1.register(cli);
const academy_1 = require("./commands/academy");
academy_1.register(cli);
const forumDownloader_1 = require("./commands/forumDownloader");
forumDownloader_1.register(cli);
const tests_1 = require("./commands/tests");
tests_1.register(cli);
const bom_1 = require("./commands/v4/bom");
bom_1.register(cli);
const product_1 = require("./commands/bazar/product");
product_1.register(cli);
const vendor_1 = require("./commands/bazar/vendor");
vendor_1.register(cli);
const library_1 = require("./commands/library");
library_1.register(cli);
const sanitize_filename_1 = require("./commands/common/sanitize-filename");
sanitize_filename_1.register(cli);
const jekyll_machine_1 = require("./commands/github/jekyll_machine");
jekyll_machine_1.register(cli);
const jekyll_product_1 = require("./commands/github/jekyll_product");
jekyll_product_1.register(cli);
const jekyll_product_datasheet_1 = require("./commands/github/jekyll_product_datasheet");
jekyll_product_datasheet_1.register(cli);
const project_index_1 = require("./commands/github/project_index");
project_index_1.register(cli);
const md_thumbs_1 = require("./commands/github/md_thumbs");
md_thumbs_1.register(cli);
const page_1 = require("./commands/github/page");
page_1.register(cli);
const howto_1 = require("./commands/howto");
howto_1.register(cli);
const download_1 = require("./commands/onearmy/download");
download_1.register(cli);
const discourse_1 = require("./commands/forum/discourse");
discourse_1.register(cli);
const argv = cli.argv;
if (argv.h || argv.help) {
    cli.showHelp();
    process.exit();
}
else if (argv.v || argv.version) {
    // tslint:disable-next-line:no-var-requires
    const pkginfo = require('../package.json');
    process.exit();
}
//# sourceMappingURL=main.js.map
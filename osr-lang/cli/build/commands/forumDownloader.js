"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const crawler_1 = require("../lib/net/crawler");
const path = require("path");
const exists_1 = require("@plastichub/fs/exists");
const dir_1 = require("@plastichub/fs/dir");
const read_1 = require("@plastichub/fs/read");
const write_1 = require("@plastichub/fs/write");
const remove_1 = require("@plastichub/fs/remove");
const tree_walker_1 = require("@plastichub/fs/utils/tree_walker");
const fg = require('fast-glob');
const cheerio = require("cheerio");
const defaultOptions = (yargs) => {
    return yargs.option('output', {
        default: './',
        describe: 'The output directory'
    }).option('debug', {
        default: 'false',
        describe: 'Enable internal debug message'
    });
};
let options = (yargs) => defaultOptions(yargs);
// removes orhpaned or spammy topics
function clean(folder) {
    const data = {
        remove: []
    };
    const visitor = (path, inspectData) => {
        if (inspectData.type === 'dir') {
            const files = fg.sync('*.json', { dot: true, cwd: path, absolute: true });
            if (files.length == 1) {
                const topic = read_1.sync(files[0], 'json');
                if (topic.saved == 0 &&
                    topic.nbReplies == 0 &&
                    topic.replies.length == 0 &&
                    topic.pics.length == 0 &&
                    !topic.likes) {
                    console.log("delete " + path);
                    data.remove.push(path);
                }
            }
        }
    };
    tree_walker_1.sync(folder, {
        inspectOptions: {
            mode: true,
            symlinks: true
        }
    }, visitor);
    return data;
}
function index(folder, dst) {
    const data = {
        topics: {},
        users: {},
        mostLiked: [],
        links: [],
        youtube: [],
        images: []
    };
    let image_index = '';
    const image = (url, src) => `<a href="${url}"><img src="${src}" style="max-width:300px;float:left;margin:8px"/><a><br/>\n\n`;
    const visitor = (path, inspectData) => {
        if (inspectData.type === 'dir') {
            const files = fg.sync('*.json', { dot: true, cwd: path, absolute: true });
            if (files.length) {
                let image_topic_index = null;
                let hasImages = false;
                files.forEach(f => {
                    const topic = read_1.sync(f, 'json');
                    if (!image_topic_index) {
                        image_topic_index = `<div style="clear:both">\n\n## [${topic.title}](${topic.url})\n\n</div>\n`;
                    }
                    let name = inspectData.name;
                    if (topic.url.split('page').length == 2) {
                        name = name + "-" + parseInt(topic.url.split('page')[1].replace('/', ''));
                    }
                    data.topics[name] = topic;
                    if (!data.users[topic.authorName]) {
                        data.users[topic.authorName] = {
                            posts: 0
                        };
                    }
                    data.users[topic.authorName].posts++;
                    const hasLink = (url) => {
                        data.links.forEach((e) => {
                            if (e.url === url) {
                                return true;
                            }
                        });
                        return false;
                    };
                    const hasYoutube = (url) => {
                        data.youtube.forEach((e) => {
                            if (e.url === url) {
                                return true;
                            }
                        });
                        return false;
                    };
                    const hasImage = (url) => {
                        data.images.forEach((e) => {
                            if (e.url === url) {
                                return true;
                            }
                        });
                        return false;
                    };
                    if (topic.replies) {
                        topic.replies.forEach(r => {
                            if (!data.users[r.user]) {
                                data.users[r.user] = {
                                    posts: 0
                                };
                            }
                            data.users[r.user].posts++;
                            const $ = cheerio.load(r.replyBody, {
                                xmlMode: true
                            });
                            $('a').each(function () {
                                const url = $(this).attr("href");
                                if (url && url.indexOf && !hasLink(url)) {
                                    if (url.indexOf('davehakkens.nl') === -1 && url.indexOf('onearmy.world') === -1) {
                                        data.links.push({ url: url, topic: topic.url });
                                    }
                                }
                                if (url && url.indexOf && !hasYoutube(url) && url.indexOf('youtube') !== -1) {
                                    data.youtube.push({ url: url, topic: topic.url });
                                }
                            });
                            $('img').each(function () {
                                const url = $(this).attr("src");
                                if (!hasImage(url)) {
                                    data.images.push({ url: url, topic: topic.url });
                                    if (url.indexOf('emoji') == -1) {
                                        hasImages = true;
                                        image_topic_index += image(topic.url, url);
                                    }
                                }
                            });
                        });
                    }
                    const $ = cheerio.load(topic.body, {
                        xmlMode: true
                    });
                    $('a').each(function () {
                        const url = $(this).attr("href");
                        if (!hasLink(url)) {
                            if (url.indexOf('davehakkens.nl') === -1 && url.indexOf('onearmy.world') === -1) {
                                data.links.push({ url: url, topic: topic.url });
                            }
                        }
                        if (url && url.indexOf && !hasYoutube(url) && url.indexOf('youtube') !== -1) {
                            data.youtube.push({ url: url, topic: topic.url });
                        }
                    });
                    $('img').each(function () {
                        const url = $(this).attr("src");
                        if (!hasImage(url)) {
                            data.images.push({ url: url, topic: topic.url });
                            if (url.indexOf('emoji') == -1) {
                                hasImages = true;
                                image_topic_index += image(topic.url, url);
                            }
                        }
                    });
                });
                if (hasImages) {
                    image_index += image_topic_index;
                }
            }
        }
    };
    tree_walker_1.sync(folder, {
        inspectOptions: {
            mode: true,
            symlinks: true
        }
    }, visitor);
    write_1.sync(dst + '/index_urls.json', data.links);
    delete data['links'];
    write_1.sync(dst + '/index_youtube.json', data.youtube);
    delete data['youtube'];
    write_1.sync(dst + '/index_images.json', data.images);
    delete data['images'];
    write_1.sync(dst + '/index.json', data);
    write_1.sync(dst + '/images.md', image_index);
    const remove = clean(folder).remove;
    remove.map((f) => remove_1.sync(f));
    return data;
}
// node ./build/main.js forum --type="download" --output="../forum"
// node ./build/main.js forum --type="index" --input="../forum/topics" --output="../forum"
exports.register = (cli) => {
    return cli.command('forum', 'forum crawler', options, (argv) => __awaiter(void 0, void 0, void 0, function* () {
        process.env['APIFY_LOCAL_STORAGE_DIR'] = '.';
        remove_1.sync(path.resolve('./request_queues'));
        if (argv.help) {
            return;
        }
        const dst = path.resolve('' + argv.output || './');
        if (!exists_1.sync(dst)) {
            dir_1.sync(dst);
        }
        if (argv.type === "index") {
            const input = path.resolve('' + argv.input);
            const output = path.resolve('' + argv.output);
            index(input, output);
        }
        if (argv.type === "download") {
            const map = [
                {
                    url: 'https://davehakkens.nl/community/forums/forum/general/community/',
                    prefix: 'topics/community'
                },
                {
                    url: 'https://davehakkens.nl/community/forums/forum/precious-plastic/general/',
                    prefix: 'topics/general'
                },
                {
                    url: 'https://davehakkens.nl/community/forums/forum/precious-plastic/development/',
                    prefix: 'topics/development'
                },
                {
                    url: 'https://davehakkens.nl/community/forums/forum/precious-plastic/research/',
                    prefix: 'topics/research'
                },
                {
                    url: 'https://davehakkens.nl/community/forums/forum/general/your-own-project/',
                    prefix: 'topics/creations'
                }
            ];
            const i = 0;
            crawler_1.crawler(map[i].url, argv, map[i].prefix);
        }
    }));
};
//# sourceMappingURL=forumDownloader.js.map
"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const isomorphic_unfetch_1 = require("isomorphic-unfetch");
function getYoutubeVideo(videoID) {
    return __awaiter(this, void 0, void 0, function* () {
        const eurl = `https://youtube.googleapis.com/v/${videoID}`;
        const response = yield isomorphic_unfetch_1.default(`https://www.youtube.com/get_video_info?video_id=${videoID}&el=embedded&eurl=${eurl}&sts=18333`);
        const text = yield response.text();
        const params = new URLSearchParams(text);
        //const data = JSON.parse(Object.fromEntries(params).player_response)
        // return data as YoutubeVideoData
        return {};
    });
}
exports.getYoutubeVideo = getYoutubeVideo;
var ProjectionType;
(function (ProjectionType) {
    ProjectionType["Rectangular"] = "RECTANGULAR";
})(ProjectionType = exports.ProjectionType || (exports.ProjectionType = {}));
//# sourceMappingURL=youtube.js.map
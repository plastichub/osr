"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const fetch = require('isomorphic-unfetch');
const native_promise_pool_1 = require("native-promise-pool");
const path_1 = require("path");
const errlop_1 = require("errlop");
const util_js_1 = require("./util.js");
const axios_1 = require("axios");
/**
 * Discourser is an API Client for the [Discourse API](https://docs.discourse.org)
 * It special features are:
 * - TypeScript Types
 * - Respecting Rate Limits
 * - Optional Heavy Caching
 * - Post Modifiers (can be used for global find and replace across all posts on the forum)
 */
class Discourser {
    /**
     * Construct our Discourser instance
     * See {@link DiscourserConfiguration} for available configuraiton.
     */
    constructor(config) {
        this.host = config.host;
        this.key = config.key;
        this.username = config.username;
        this.cache = config.cache;
        this.useCache = config.useCache;
        this.dry = config.dry || false;
        this.pool = new native_promise_pool_1.default(config.rateLimitConcurrency || 60);
    }
    /** Get the URL of a topic */
    getTopicURL(topic) {
        if (typeof topic === 'number') {
            return `${this.host}/t/${topic}`;
        }
        return `${this.host}/t/${topic.slug}/${topic.id}`;
    }
    /** Fetch a discourse API URL, with rate limit concurrency and optional caching */
    fetch({ url, useCache, request }) {
        var _a;
        return __awaiter(this, void 0, void 0, function* () {
            // check if cache is enabled
            const cache = this.cache &&
                (((_a = request) === null || _a === void 0 ? void 0 : _a.method) || 'get') === 'get' &&
                path_1.join(this.cache, util_js_1.escape(url));
            // check if we should and can read from cache
            if (cache &&
                this.useCache !== false &&
                useCache !== false &&
                (yield util_js_1.exists(cache))) {
                const result = yield util_js_1.readJSON(cache);
                return result;
            }
            // fetch
            const result = yield this.pool.open(() => this._fetch({ url, request }));
            // write to cache if cache is enabled
            if (cache) {
                util_js_1.writeJSON(cache, result);
            }
            // return the result
            return result;
        });
    }
    /** Fetch a discourse API URL, with rate limit retries */
    _post(url, data) {
        var _a;
        return __awaiter(this, void 0, void 0, function* () {
            const opts = {
                headers: {
                    'Api-Key': this.key,
                    'Api-Username': this.username,
                },
            };
            /*
            const retry = (seconds: number) => {
                log('waiting', seconds, 'seconds for', url)
                return new Promise<T>((resolve, reject) => {
                    setTimeout(
                        () =>
                            this._fetch<T>({ url, request })
                                .then(resolve)
                                .catch(reject),
                        (seconds || 60) * 1000
                    )
                })
            }
            */
            let d = data;
            try {
                const res = yield axios_1.default.post(url, d, {
                    headers: opts.headers
                });
                console.log("response : ", res.data);
                // fetch text then parse as json, so that when errors occur we can output what it was
                // rather than being stuck with errors like these:
                // FetchError: invalid json response body at https://discuss.bevry.me/posts/507.json reason: Unexpected token < in JSON at position 0
                const text = yield res.data;
                let data;
                try {
                }
                catch (err) {
                    console.log('Error', err);
                    return;
                    // check if it was cloudflare reporting that the server has been hit too hard
                    if (text.includes('Please try again in a few minutes')) {
                        util_js_1.log('server has stalled, trying again in a minute');
                        // return await retry(60)
                    }
                    // otherwise log the error page and die
                    // log({ text, url , opts })
                    return Promise.reject(new Error(`fetch of [${url}] received invalid response:\n${text}`));
                }
                return text;
                // check if there are errors
                if (typeof data.errors !== 'undefined') {
                    // check if the error is a rate limit
                    const wait = (_a = data.extras) === null || _a === void 0 ? void 0 : _a.wait_seconds;
                    if (wait != null) {
                        // if it was, try later
                        // return await retry(wait + 1)
                    }
                    // otherwise fail
                    // log({ data, url, opts })
                    return Promise.reject(new Error(`fetch of [${url}] received failed response:\n${util_js_1.inspect(data)}`));
                }
                return data;
            }
            catch (err) {
                console.error('Error', err.response.data);
                // log({ err, url, opts });
                return;
                return Promise.reject(new errlop_1.default(`fetch of [${url}] failed with error`, err));
            }
        });
    }
    /** Fetch a discourse API URL, with rate limit retries */
    _fetch({ url, request }) {
        var _a, _b;
        return __awaiter(this, void 0, void 0, function* () {
            const opts = Object.assign(Object.assign({}, request), { headers: Object.assign(Object.assign({}, (_a = request) === null || _a === void 0 ? void 0 : _a.headers), { Accept: 'application/json', 'Content-Type': 'application/json', 'Api-Key': this.key, 'Api-Username': this.username }) });
            const retry = (seconds) => {
                util_js_1.log('waiting', seconds, 'seconds for', url);
                return new Promise((resolve, reject) => {
                    setTimeout(() => this._fetch({ url, request })
                        .then(resolve)
                        .catch(reject), (seconds || 60) * 1000);
                });
            };
            try {
                const res = yield fetch(url, opts);
                // fetch text then parse as json, so that when errors occur we can output what it was
                // rather than being stuck with errors like these:
                // FetchError: invalid json response body at https://discuss.bevry.me/posts/507.json reason: Unexpected token < in JSON at position 0
                const text = yield res.text();
                let data;
                try {
                    data = JSON.parse(text);
                }
                catch (err) {
                    // check if it was cloudflare reporting that the server has been hit too hard
                    if (text.includes('Please try again in a few minutes')) {
                        util_js_1.log('server has stalled, trying again in a minute');
                        return yield retry(60);
                    }
                    // otherwise log the error page and die
                    // log({ text, url , opts })
                    return Promise.reject(new Error(`fetch of [${url}] received invalid response:\n${text}`));
                }
                // check if there are errors
                if (typeof data.errors !== 'undefined') {
                    // check if the error is a rate limit
                    const wait = (_b = data.extras) === null || _b === void 0 ? void 0 : _b.wait_seconds;
                    if (wait != null) {
                        // if it was, try later
                        return yield retry(wait + 1);
                    }
                    // otherwise fail
                    // log({ data, url, opts })
                    return Promise.reject(new Error(`fetch of [${url}] received failed response:\n${util_js_1.inspect(data)}`));
                }
                return data;
            }
            catch (err) {
                // log({ err, url, opts })
                return Promise.reject(new errlop_1.default(`fetch of [${url}] failed with error`, err));
            }
        });
    }
    // =================================
    // CATEGORIES
    /**
     * API Helper for {@link .getCategories}
     */
    getCategoriesResponse(opts = {}) {
        return __awaiter(this, void 0, void 0, function* () {
            const url = `${this.host}/categories.json`;
            return yield this.fetch(Object.assign({ url }, opts));
        });
    }
    /**
     * Fetch the whole information, for all categories of the forum
     */
    getCategories(opts = {}) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.getCategoriesResponse(opts);
            const categories = response.category_list.categories;
            util_js_1.log('forum', 'contains', categories.length, 'categories');
            return categories;
        });
    }
    /**
     * API Helper for {@link .getTopicItemsOfCategory}
     * Discourse does not provide an API for fetching category information for a specific category.
     * Instead, all that it provides is a way of getting the topics for a specific category.
     */
    getCategoryResponse(categoryID, opts = {}) {
        return __awaiter(this, void 0, void 0, function* () {
            const url = `${this.host}/c/${categoryID}.json` +
                (opts.page !== 0 ? `?page=${opts.page}` : '');
            return yield this.fetch(Object.assign({ url }, opts));
        });
    }
    // =================================
    // TOPICS
    /**
     * Fetch the partial information, for all topics of a specific category
     */
    getTopicItemsOfCategory(categoryID, opts = {}) {
        return __awaiter(this, void 0, void 0, function* () {
            // prepare and fetch
            let page = opts.page || 0;
            const response = yield this.getCategoryResponse(categoryID, Object.assign(Object.assign({}, opts), { page }));
            const topics = response.topic_list.topics;
            // fetch the next page
            if (topics.length === response.topic_list.per_page) {
                page += 1;
                util_js_1.log('category', categoryID, 'has more topics, fetching page', page);
                const more = yield this.getTopicItemsOfCategory(categoryID, Object.assign(Object.assign({}, opts), { page }));
                topics.push(...more);
            }
            // if we are the first page, then output count as we now have all of them
            if (page === 0) {
                const ids = topics.map((i) => i.id);
                util_js_1.log('category', categoryID, 'has', ids.length, 'topics', ids);
            }
            // return
            return topics;
        });
    }
    /**
     * Fetch the partial information, for all topics of specific categoires
     */
    getTopicItemsOfCategories(categoryIDs, opts = {}) {
        return __awaiter(this, void 0, void 0, function* () {
            // fetch topic items for specific categories
            const topicsOfCategories = yield Promise.all(categoryIDs.map((id) => this.getTopicItemsOfCategory(id, opts)));
            // @ts-ignore
            return topicsOfCategories.flat();
        });
    }
    /**
     * Fetch the partial information, for all topics of the forum
     */
    getTopicItems(opts = {}) {
        return __awaiter(this, void 0, void 0, function* () {
            const categories = yield this.getCategories();
            const categoryIDs = categories.map((i) => i.id);
            return this.getTopicItemsOfCategories(categoryIDs, opts);
        });
    }
    /**
     * Fetch the whole information, for a specific topic of the forum
     */
    getTopic(id, opts = {}) {
        const url = `${this.host}/t/${id}.json`;
        return this.fetch(Object.assign({ url }, opts));
    }
    /**
     * Fetch the whole information, for all topics, or specific topics, of the forum
     */
    getTopics(topicIDs, opts = {}) {
        return __awaiter(this, void 0, void 0, function* () {
            // if no topics, use all topics
            if (!topicIDs) {
                const topics = yield this.getTopicItems(opts);
                topicIDs = topics.map((i) => i.id);
            }
            // fetch whole topics
            return Promise.all(topicIDs.map((id) => this.getTopic(id, opts)));
        });
    }
    // ---------------------------------
    // TOPICS: UPDATING
    /**
     * Update a topic with a new timestamp
     */
    updateTopicTimestamp(topicID, timestamp) {
        return __awaiter(this, void 0, void 0, function* () {
            let time;
            if (typeof timestamp === 'number') {
                time = timestamp;
            }
            else if (typeof timestamp === 'number') {
                time = Number(timestamp);
            }
            else if (timestamp instanceof Date) {
                // ms to seconds
                time = timestamp.getTime() / 1000;
            }
            else {
                return Promise.reject(new Error('invalid timestamp format'));
            }
            // prepare the request
            const request = {
                timestamp: time,
            };
            // send the update
            util_js_1.log('updating', topicID, 'topic timestamp with', request);
            const url = `${this.host}/t/${topicID}/change-timestamp`;
            const response = yield this.fetch({
                url,
                request: {
                    method: 'put',
                    body: JSON.stringify(request),
                },
            });
            // check it
            if (response.success !== 'OK') {
                return Promise.reject(new Error(`timestamp update of topic ${topicID} failed:\n${util_js_1.inspect({
                    url,
                    request,
                    response,
                })}`));
            }
            // return the response
            util_js_1.log('updated', topicID);
            return response;
        });
    }
    // =================================
    // POSTS
    /**
     * API Helper for {@link .getPostItemsOfTopic}
     */
    getPostItemsOfTopicResponse(topicID, opts = {}) {
        return __awaiter(this, void 0, void 0, function* () {
            const url = `${this.host}/t/${topicID}/posts.json`;
            const response = yield this.fetch(Object.assign({ url }, opts));
            return response;
        });
    }
    /**
     * Fetch the partial information, for all posts of a specific topic
     */
    getPostItemsOfTopic(topicID, opts = {}) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.getPostItemsOfTopicResponse(topicID, opts);
            const posts = response.post_stream.posts;
            const ids = posts.map((i) => i.id);
            util_js_1.log('topic', topicID, 'contains', ids.length, 'posts', ids);
            return posts;
        });
    }
    /**
     * Fetch the partial information, for all posts of specific topics
     */
    getPostItemsOfTopics(topicIDs, opts = {}) {
        return __awaiter(this, void 0, void 0, function* () {
            // fetch post items for specific topics
            const postItemsOfTopics = yield Promise.all(topicIDs.map((id) => this.getPostItemsOfTopic(id, opts)));
            // @ts-ignore
            return postItemsOfTopics.flat();
        });
    }
    /**
     * Fetch the partial information, for all posts of a specific category
     */
    getPostItemsOfCategory(categoryID, opts = {}) {
        return __awaiter(this, void 0, void 0, function* () {
            // fetch topics for the category
            const topics = yield this.getTopicItemsOfCategory(categoryID, opts);
            const topicIDs = topics.map((i) => i.id);
            // fetch
            const posts = yield this.getPostItemsOfTopics(topicIDs);
            const ids = posts.map((i) => i.id);
            util_js_1.log('category', categoryID, 'has', ids.length, 'posts', ids);
            return posts;
        });
    }
    /**
     * Fetch the partial information, for all posts of specific categories
     */
    getPostItemsOfCategories(categoryIDs, opts = {}) {
        return __awaiter(this, void 0, void 0, function* () {
            // fetch post items for specific categories
            const postItemsOfCategories = yield Promise.all(categoryIDs.map((id) => this.getPostItemsOfCategory(id, opts)));
            // @ts-ignore
            return postItemsOfCategories.flat();
        });
    }
    /**
     * Fetch the partial information, for all posts of the forum
     */
    getPostItems(opts = {}) {
        return __awaiter(this, void 0, void 0, function* () {
            const categories = yield this.getCategories();
            const categoryIDs = categories.map((i) => i.id);
            return this.getPostItemsOfCategories(categoryIDs, opts);
        });
    }
    /**
     * Fetch the whole information, for a specific post of the forum
     */
    getPost(id, opts = {}) {
        const url = `${this.host}/posts/${id}.json`;
        return this.fetch(Object.assign({ url }, opts));
    }
    /**
     * Fetch the whole information, for all posts, or specific posts, of the forum
     */
    getPosts(postIDs, opts = {}) {
        return __awaiter(this, void 0, void 0, function* () {
            // if no posts, use all
            if (!postIDs) {
                const posts = yield this.getPostItems(opts);
                postIDs = posts.map((i) => i.id);
            }
            // fetch whole posts
            return yield Promise.all(postIDs.map((id) => this.getPost(id, opts)));
        });
    }
    createPost(title, raw, category) {
        return __awaiter(this, void 0, void 0, function* () {
            // fetch whole posts
            const url = `${this.host}/posts`;
            return yield this._post(url, { raw, title, category });
        });
    }
    // =================================
    // POSTS: UPDATING
    /**
     * Update a post with the content
     * @param postID the identifier of the post to update
     * @param content the new raw content for the post
     * @param reason the reason, if provided, for modifying the post
     * @param old if the old raw content is provided, then the update verified that you are working with the latest post content before applying the update
     */
    updatePost(postID, content, reason = 'api update', old) {
        return __awaiter(this, void 0, void 0, function* () {
            // prepare the request
            const data = {
                post: {
                    raw: content,
                    edit_reason: reason,
                },
            };
            if (old) {
                data.post.raw_old = old;
            }
            // send the update
            util_js_1.log('updating', postID, 'with', data);
            const url = `${this.host}/posts/${postID}.json`;
            const response = yield this.fetch({
                url,
                request: {
                    method: 'put',
                    body: JSON.stringify(data),
                },
            });
            // return the response
            util_js_1.log('updated', postID);
            return response.post;
        });
    }
    // async rebakePost(postID: number): Promise<Post> {
    // 	const url = `${this.host}/posts/${postID}/rebake`
    // 	log('rebaking', postID)
    // 	const response = await this.fetch<PostResponse>(url, {
    // 		method: 'put',
    // 	})
    // 	log('rebaked', postID)
    // 	return response.post
    // }
    /**
     * Modify a post using a modifier
     */
    modifyPost(post, modifier) {
        return __awaiter(this, void 0, void 0, function* () {
            // check if we received a post item, insted of a post response
            if (post.raw == null) {
                util_js_1.log('post had no raw, fetching it', post.id);
                post = yield this.getPost(post.id);
            }
            // check
            if (!post.raw) {
                util_js_1.log('post had empty raw, skipping', post.id);
                return Promise.resolve(null);
            }
            // replace
            const { result, reason } = modifier(post);
            if (result === post.raw) {
                util_js_1.log('replace had no effect on raw post', post.id);
                // if (post.cooked) {
                // 	const { result, reason } = modifier(post.cooked)
                // 	if (result !== post.cooked) {
                // 		log(
                // 			'replace did have an effect on cooked post',
                // 			postID,
                // 			'so rebaking it'
                // 		)
                // 		return await this.rebakePost(postID)
                // 	}
                // }
                return Promise.resolve(null);
            }
            // dry
            if (this.dry) {
                util_js_1.log('skipping update on dry mode');
                return Promise.resolve(Object.assign(Object.assign({}, post), { result,
                    reason }));
            }
            // update
            try {
                return yield this.updatePost(post.id, result, reason, post.raw);
            }
            catch (err) {
                if (err.message.includes('That post was edited by another user and your changes can no longer be saved.')) {
                    util_js_1.log('trying post', post.id, 'again with invalidated cache');
                    return this.modifyPost(yield this.getPost(post.id, { useCache: false }), modifier);
                }
                return Promise.reject(new errlop_1.default(`modifying post ${post.id} failed`, err));
            }
        });
    }
    /**
     * Modify a post (via its post identifier) using a modifier
     */
    modifyPostID(post, modifier) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.modifyPost(yield this.getPost(post), modifier);
        });
    }
    /**
     * Modify a post (via fetching the whole post from the partial post identifier) using a modifier
     */
    modifyPostItem(post, modifier) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.modifyPost(yield this.getPost(post.id), modifier);
        });
    }
    /**
     * Run the post modifier on all specified posts
     */
    modifyPosts(posts, modifier) {
        return __awaiter(this, void 0, void 0, function* () {
            const updates = yield Promise.all(posts.map((post) => this.modifyPost(post, modifier)));
            const updated = updates.filter((i) => i);
            return updated;
        });
    }
    // =================================
    // THREADS
    /**
     * Fetch the partial information, for all posts of a specific topic
     * Alias of {@link .getPostItemsOfTopic}.
     */
    getThread(topicID, opts = {}) {
        return __awaiter(this, void 0, void 0, function* () {
            const topic = yield this.getTopic(topicID, opts);
            const [post, ...replies] = yield this.getPostItemsOfTopic(topicID, opts);
            return {
                topic,
                post,
                replies,
            };
        });
    }
    /**
     * Fetch the partial information, for all posts of specific topics, grouped by topic
     */
    getThreads(topicIDs, opts = {}) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield Promise.all(topicIDs.map((id) => this.getThread(id, opts)));
        });
    }
    /**
     * Fetch the partial information, for all posts of specific categories, grouped by topic
     */
    getThreadsOfCategory(categoryID, opts = {}) {
        return __awaiter(this, void 0, void 0, function* () {
            // fetch topics for the category
            const topics = yield this.getTopicItemsOfCategory(categoryID, opts);
            const topicIDs = topics.map((i) => i.id);
            // return threads
            return yield this.getThreads(topicIDs);
        });
    }
    /**
     * Fetch the partial information, for all posts of specific categories, grouped by category, then topic
     */
    getThreadsOfCategories(categoryIDs, opts = {}) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield Promise.all(categoryIDs.map((id) => this.getThreadsOfCategory(id, opts)));
        });
    }
}
exports.Discourser = Discourser;
//# sourceMappingURL=index.js.map
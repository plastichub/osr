"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const fs_1 = require("fs");
const util_1 = require("util");
const cwd = process.cwd();
function inspect(arg) {
    return util_1.inspect(arg, {
        depth: 5,
        colors: true,
    });
}
exports.inspect = inspect;
function log(...args) {
    console.log(...args.map((arg) => inspect(arg)));
}
exports.log = log;
function mkdirp(path) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            yield fs_1.default.promises.mkdir(path);
        }
        catch (err) {
            // don't care if it already exists
        }
    });
}
exports.mkdirp = mkdirp;
function readJSON(path) {
    return __awaiter(this, void 0, void 0, function* () {
        const text = yield fs_1.default.promises.readFile(path, 'utf8');
        return JSON.parse(text);
    });
}
exports.readJSON = readJSON;
function writeJSON(path, data) {
    return fs_1.default.promises.writeFile(path, JSON.stringify(data));
}
exports.writeJSON = writeJSON;
function exists(path) {
    return new Promise(function (resolve) {
        fs_1.default.exists(path, resolve);
    });
}
exports.exists = exists;
function escape(path) {
    return path.replace(/[^\w]/g, '-').replace(/-+/, '-');
}
exports.escape = escape;
//# sourceMappingURL=util.js.map
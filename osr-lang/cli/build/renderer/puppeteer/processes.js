"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const constants_1 = require("./constants");
function loginFrontEnd(page, options) {
    return __awaiter(this, void 0, void 0, function* () {
        // await page.goto(loginUrl(options.dynatraceUrl), defaultPageOptions());
        yield page.type('#user', 'admin');
        yield page.type('#password', 'admin');
        yield page.click('#login-form > .maindiv > .logindiv > .submitdiv > .button');
        yield page.waitForSelector('.tenant-selector');
        yield page.click('.textboxdiv > .tenant-selector:nth-child(2)');
    });
}
exports.loginFrontEnd = loginFrontEnd;
function loginDemoDev(page, options) {
    return __awaiter(this, void 0, void 0, function* () {
        yield page.goto(constants_1.loginUrlDemoDev(), constants_1.defaultPageOptions());
        yield page.type('#IDToken1', 'guenter.baumgart@ruxit.com');
        yield page.click('.maindiv > .logindiv > form > div > #formsubmit');
        return new Promise((resolve) => {
            setTimeout(() => {
                resolve(1);
                console.log('time out');
            }, 20 * 1000);
        });
        console.log('logged in 1/2!');
        /*
          const puppeteer = require('puppeteer');
        
        (async () => {
          const browser = await puppeteer.launch()
          const page = await browser.newPage()
          
          await page.waitForSelector('.logindiv > form > .textboxdiv > .emailLoginIcon > #IDToken1')
          await page.click('.logindiv > form > .textboxdiv > .emailLoginIcon > #IDToken1')
          
          await page.waitForSelector('.maindiv > .logindiv > form > div > #formsubmit')
          await page.click('.maindiv > .logindiv > form > div > #formsubmit')
          
          const navigationPromise = page.waitForNavigation()
          await navigationPromise
          
          await page.waitForSelector('.logindiv > form > .margin-bottom\3A > .passwordIcon > #IDToken2')
          await page.click('.logindiv > form > .margin-bottom\3A > .passwordIcon > #IDToken2')
          
          await page.waitForSelector('.logindiv > form > fieldset > div > #IDToken3')
          await page.click('.logindiv > form > fieldset > div > #IDToken3')
          
          await page.waitForSelector('form > fieldset > #button-base > div > #loginButton_0')
          await page.click('form > fieldset > #button-base > div > #loginButton_0')
          
          await navigationPromise
          
          await navigationPromise
          
          await navigationPromise
          
          await browser.close()
        })()
        */
        //await page.type('#password', 'admin');
        //await page.click('#login-form > .maindiv > .logindiv > .submitdiv > .button');
        //await page.waitForSelector('.tenant-selector');
        //await page.click('.textboxdiv > .tenant-selector:nth-child(2)');
    });
}
exports.loginDemoDev = loginDemoDev;
function navigateToUserSessions(page, options) {
    return __awaiter(this, void 0, void 0, function* () {
        // await page.goto(userSessionsTab(options.dynatraceUrl), defaultPageOptions());
    });
}
exports.navigateToUserSessions = navigateToUserSessions;
function navigateToUserLocalhost(page, options) {
    return __awaiter(this, void 0, void 0, function* () {
        // await page.goto('http://localhost/', defaultPageOptions());
    });
}
exports.navigateToUserLocalhost = navigateToUserLocalhost;
//# sourceMappingURL=processes.js.map
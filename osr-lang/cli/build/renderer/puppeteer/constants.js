"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.defaultTenant = 1;
exports.userSessionsTab = (base) => `${base}/e/${exports.defaultTenant}/#usersearchoverview;gtf=l_2_HOURS`;
exports.loginUrl = (base) => `${base}/login`;
exports.loginUrlDemoDev = () => `https://proxy-dev.dynatracelabs.com/sso/ProxyLocator.jsp`;
exports.defaultPageOptions = () => {
    return {
        timeout: 5000,
        waitUntil: 'networkidle2'
    };
};
exports.replay_api_overview = 'uemshareddetails/rumoverviewdata/usersearchoverview';
const ts = () => {
    const d = new Date();
    return d.getHours() + '_' + d.getMinutes() + '_' + d.getSeconds();
};
exports.sessionName = (url) => `Pupeteer :${ts()}`;
exports.maxSessionWaitingTime = 1000 * 60 * 3;
exports.responseRetryTime = 1000 * 8;
exports.defaultMutationRoot = '#mutationsRoot';
exports.defaultMutationTag = 'div';
exports.defaultHeavyMutations = 2000;
exports.defaultMediumMutations = 500;
//# sourceMappingURL=constants.js.map
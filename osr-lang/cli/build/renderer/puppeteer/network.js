"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const constants_1 = require("./constants");
const debug = require("../../log");
const url_1 = require("url");
const debugRequests = true;
const debugResponses = false;
exports.default_postdata = (request) => request.postData && request.postData() || {};
exports.HasUserSessions = (request) => (exports.default_postdata(request).users);
exports.MyUserSessions = (url, request) => exports.SessionWithName(request, constants_1.sessionName(url));
exports.SessionWithName = (request, name) => {
    const data = exports.default_postdata(request).users || [];
    return data.find((user) => user.id === name);
};
const default_prepare = (requests) => {
    return requests;
};
const default_filter_json = (r) => ((r.headers()['content-type'] || '').startsWith('application/json;')) === true;
const responses = function (requests) {
    return __awaiter(this, void 0, void 0, function* () { return Promise.all(requests.map(r => r.response().json())); });
};
exports.findRequest = (url, requests, match) => {
    url = decodeURIComponent(url);
    if (!match) {
        return requests.filter((request) => request.url().indexOf(url) !== -1);
    }
    else {
        const results = requests.filter((request) => request.url().indexOf(url) !== -1);
        return results.filter((r) => match(r));
    }
};
function waitForResponse(url, scope, match, timeout = 5000) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            try {
                let requests = default_prepare(scope.requests).filter(default_filter_json).filter(r => r.response() != null);
                requests = requests.filter((request) => request.url().indexOf(url) !== -1);
                responses(requests).then((responses) => {
                    const ret = responses.filter(match);
                    if (ret.length) {
                        resolve(ret);
                    }
                    else {
                        reject('cant find anything yet');
                    }
                });
            }
            catch (error) {
                debug.error('waitForResponse Error ', error);
            }
        }, timeout);
    });
}
exports.waitForResponse = waitForResponse;
function waitForResponseNTimes(url, scope, match, timeout = 5000) {
    return new Promise((resolve, reject) => {
        const maxTime = constants_1.maxSessionWaitingTime;
        const retryTime = 8000;
        let reachedTimeOut = false;
        let interval = null;
        interval = setInterval(() => {
            if (reachedTimeOut) {
                clearInterval(interval);
                debug.error('reached max');
                reject('reached maximum timeout');
                return;
            }
            onReload(scope).then(() => {
                scope.page.reload().then(() => {
                    debug.info('retry ');
                    waitForResponse(url, scope, match, retryTime).then((session) => {
                        debug.inspect('got my session', session);
                        clearInterval(interval);
                        resolve(session);
                    }).catch((e) => {
                        debug.error('found nothing');
                    });
                }).catch((e) => {
                    console.error('error loading page : ', e);
                });
            });
        }, retryTime);
        setTimeout(() => {
            reachedTimeOut = true;
            clearInterval(interval);
            reject('max timeout reached');
        }, maxTime);
    });
}
exports.waitForResponseNTimes = waitForResponseNTimes;
;
function capture_request(where, request) {
    return __awaiter(this, void 0, void 0, function* () {
        debugRequests && debug.inspect('Request', { url: request.url(), data: request.postData() });
        where.push({ url: request.url(), data: yield request.postData(), request: request });
        debugRequests && debug.inspect('requests', where.map(r => r.url));
    });
}
exports.capture_request = capture_request;
function capture_response(where, response) {
    return __awaiter(this, void 0, void 0, function* () {
        debugResponses && debug.inspect('Response', { url: response.url(), data: yield response.json() });
        where.push(response);
    });
}
exports.capture_response = capture_response;
function capture_requests(scope, page) {
    return __awaiter(this, void 0, void 0, function* () {
        yield page.setRequestInterception(true);
        scope.requests = [];
        page.on('request', (interceptedRequest) => {
            if (scope.onRequest) {
                scope.onRequest(interceptedRequest, scope);
                return;
            }
            try {
                const url = decodeURIComponent(interceptedRequest.url());
                const parsed = url_1.parse(url, true);
                if (url.includes('.css') || url.includes('.svg')) {
                    interceptedRequest.abort();
                    return;
                }
                const query = parsed.query;
                const isJson = (interceptedRequest.headers()['content-type'] || '').startsWith('application/json') === true;
                if (isJson) {
                    // capture_request(scope.requests, interceptedRequest);
                    //debugRequests && debug.inspect('q ' + query['contentType'] + ' ' + url);
                }
                interceptedRequest.continue();
            }
            catch (e) {
                debug.error('error parsing request ', e);
            }
        });
    });
}
exports.capture_requests = capture_requests;
function capture_responses(scope, page) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            // await page.setRequestInterception(true);
        }
        catch (e) {
            debug.error('error intercepting responses', e);
        }
        scope.responses = [];
        page.on('response', response => {
            try {
                const isJson = (response.headers()['content-type'] || '').startsWith('application/json;') === true;
                const url = response.url();
                if (response.status() === 200) {
                    if (isJson) {
                        capture_response(scope.responses, response);
                    }
                    if (scope.onResponse) {
                        scope.onResponse(response, scope);
                    }
                }
                else {
                    debugResponses && debug.error(`Error loading ${url} : ${response.status()}`);
                }
            }
            catch (e) {
                debugResponses && debug.error('Error parsing response');
            }
        });
    });
}
exports.capture_responses = capture_responses;
function onReload(scope) {
    return __awaiter(this, void 0, void 0, function* () {
        scope.requests = [];
        try {
            yield scope.page.setRequestInterception(false);
        }
        catch (e) {
        }
        yield scope.page.setRequestInterception(true);
    });
}
exports.onReload = onReload;
//# sourceMappingURL=network.js.map
import * as debug from '../debug';
import * as CLI from 'yargs';
import { defaultOptions, sanitize } from '../argv';
import { IDefaultCLIArgs } from '../types';
import { sync as exists } from '@plastichub/fs/exists';
import { sync as remove } from '@plastichub/fs/remove';
import * as fs from 'fs';
import * as path from 'path';

const options = (yargs: CLI.Argv) => defaultOptions(yargs);

import * as archiver from 'archiver';

const default_ignored_folders = [
    '.git',
    'node_modules',
    '_MACOSX',
    'production',
    'bazar',
    'templates',
    '.*'
]

const default_ignored_files = [
    '!./**/*.zip',
    '!./**/*.tar'
]

const defaultProduct_public_folders = [
    'cad',
    'media/gallery',
    'resources',
    'drawings',
    'vendor',
    'resources',
    'laser',
    'howto',
    'renderings'
]
const defaultProduct_public_files = [
    '*.json',
    '*.md',
    '*.yaml',
    '*.csv',
    '*.xls'
]

const profiles = {
    'product': {
        matching: [
            ...defaultProduct_public_files,
            ...defaultProduct_public_folders.map((e) => `${e}/**`)
        ]
    }
}

// node ./node_modules/osr-sync/dist/main.js zip --clear=true --source='./products/lydia-v4' --target='../out.zip' --debug=true --profile=product
// node ./node_modules/osr-sync/dist/main.js zip --clear=true --source='./products/lydia-v4' --target='../out.zip' --debug=true --profile='./osr-sync-zip.json'
// node ./node_modules/osr-sync/dist/main.js zip --clear=true --source='./products/lydia-v4' --source='./products/components/hal' --cwd="./" --target='../out.zip'  --debug=true --profile='./.osr-sync-zip.json'
export const register = (cli: CLI.Argv) => {
    return cli.command('zip', "Default Zip", options, (argv: CLI.Arguments) => {

        if (argv.help) { return; }

        const args = sanitize(argv) as IDefaultCLIArgs;
        const verbose = args.debug;

        if (exists(args.target) && args.clear) {
            verbose && debug.info(`Deleting ${args.target}`);
            remove(args.target);
        }

        let options: any = {};
        if (typeof args.profile === 'string') {
            const profile = args.profile as string ? (profiles as any)[args.profile as string] : {};
            options = {
                debug: true,
                ...profile
            }
        } else if (Object.keys(args.profile)) {
            options = args.profile;
        }

        debug.inspect('options', args.profile);

        const file = fs.createWriteStream(args.target);
        const archive = archiver('zip', {
            zlib: {
                level: 9
            }
        });
        archive.on('warning', (err) => {
            if (err.code === 'ENOENT') {
                debug.warn('archive warning', err);
            } else {
                throw err;
            }
        });

        archive.on('error', (err) => {
            debug.error('archive error', err);
            return;
        });

        archive.pipe(file);

        if (options.matching) {
            if (argv.cwd) {
                args.source.forEach((s) => {
                    verbose && debug.info(`Archiving ${s}`);
                    options.matching.forEach((e) => {
                        archive.glob(e, { cwd: s, ignore: [] }, { prefix: path.relative(args.cwd, s) });
                    });
                })
            } else {
                args.source.forEach((s) => {
                    verbose && debug.info(`Archiving ${s}`);
                    options.matching.forEach((e) => {
                        archive.glob(e, { cwd: s, ignore: [] });
                    });
                })
            }
        }
        const s = debug.spinner("Write archive ..");
        s.start();
        archive.finalize().then(() => {
            s.stop();
            verbose && debug.info(`Did create archive at ${args.target}`);
        })
    });
};

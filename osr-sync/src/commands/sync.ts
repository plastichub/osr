import * as debug from '../debug';
import * as CLI from 'yargs';
import { defaultOptions, sanitize } from '../argv';
import { IDefaultCLIArgs } from '../types';
import { sync as exists } from '@plastichub/fs/exists';
import { sync as remove } from '@plastichub/fs/remove';
import { async as copy } from '@plastichub/fs/copy';
import { ICopyOptions, ItemProgressCallback } from '@plastichub/fs/interfaces';
import * as path from 'path';

const options = (yargs: CLI.Argv) => defaultOptions(yargs);

const default_ignored_folders = [
    '.git',
    'node_modules',
    '_MACOSX',
    'production',
    'templates',
    '.*'
]

const default_ignored_files = [
    '!./**/*.zip',
    '!./**/*.tar'
]

const defaultProduct_public_folders = [
    'cad',
    'media/gallery',
    'resources',
    'drawings',
    'vendor',
    'resources',
    'laser',
    'bazar',
    'howto',
    'renderings'
]
const defaultProduct_public_files = [
    '*.json',
    '*.md',
    '*.yaml',
    '*.csv',
    '*.xls'
]

const profiles = {
    'product': {
        matching: [
            ...defaultProduct_public_files,
            ...defaultProduct_public_folders.map((e) => `${e}/**`),
            ...default_ignored_folders.map((e) => `!./${e}/**`),
            ...default_ignored_files
        ]
    },
    'promo': {
        matching: [
            ...defaultProduct_public_files.filter((e) => e != 'cad'),
            ...defaultProduct_public_folders.map((e) => `${e}/**`),
            ...default_ignored_folders.map((e) => `!./${e}/**`),
            ...default_ignored_files
        ]
    }
}

// node ./node_modules/osr-sync/dist/main.js sync --clear=true --source='./products/lydia-v4' --target='../plastic-hub-public/products/lydia-v4_test' --debug=true --profile=product
// node ./node_modules/osr-sync/dist/main.js sync --clear=true --source='./products/lydia-v4' --target='../plastic-hub-public/products/lydia-v4_test' --debug=true --profile='./osr-sync.json'
export const register = (cli: CLI.Argv) => {
    return cli.command('sync', "Default Sync", options, (argv: CLI.Arguments) => {

        if (argv.help) { return; }

        const args = sanitize(argv) as IDefaultCLIArgs;
        const verbose = args.debug;

        if (!exists(args.source[0])) {
            verbose && debug.error(`Source directory doesnt exists ${args.source}`);
            return;
        }

        if (exists(args.target) && args.clear) {
            verbose && debug.info(`Emptying target directory ${args.target}`);
            remove(args.target);
        }

        let count = 0;
        const progress: ItemProgressCallback = (path: string, current: number, total: number) => {
            // verbose && console.log(`Copying ${path} - ${current} of ${total}`);
            count++;
            return true;
        };

        let copyOptions: ICopyOptions = {};

        if (typeof args.profile === 'string') {
            const profile = args.profile as string ? (profiles as any)[args.profile as string] : {};
            copyOptions = {
                debug: false,
                ...profile
            }
        } else if (Object.keys(args.profile)) {
            copyOptions = args.profile;
        }

        copyOptions = { ...copyOptions, ...{ progress: progress } };
        copyOptions.overwrite = true;

        if (args.renamer) {
            const script = path.resolve(args.renamer);
            if (!exists(script)) {
                debug.warn(`Renamer script '${args.renamer}' doesn't exists at ${script}`);
            } else {
                try {
                    const rename = require(script);
                    copyOptions.renameCallback = rename.default;
                    if (copyOptions.renameCallback) {
                    } else {
                        console.log('invalid  rename script ' + args.renamer);
                    }
                } catch (e) {
                    debug.error(`Error loading rename script ${script}`, e);
                }
            }
        }

        copy(args.source[0], args.target, copyOptions).then(() => {
            //verbose && debug.inspect('args', { args, copyOptions });
            debug.info(`Copied ${count} files from ${argv.source.join('\n')} to ${argv.target}`);
        }).catch((e) => {
            debug.error(e.message);
        });
    });
};

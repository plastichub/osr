#!/usr/bin/env node
import * as bluebird from 'bluebird';
import * as mkdirp from 'mkdirp';
import * as path from 'path';
import * as cli from 'yargs';
import * as commands from './commands';
import * as debug from './debug';
import { get, Helper } from './lib';

// tslint:disable-next-line:no-var-requires
const yargonaut = require('yargonaut')
    .style('blue')
    .helpStyle('green');

cli.options('v', {
    alias: 'version',
    description: 'Display version number'
});
const defaultArgs = (yargs: any) => {
    return yargs;
};

// tweaks and handlers
const defaults = () => {
    // default command
    const DefaultCommand = 'summary';
    if (process.argv.length === 2) {
        process.argv.push(DefaultCommand);
    }

    // currently no default handler, display only :
    process.on('unhandledRejection', (reason: string) => {
        console.error('Unhandled rejection, reason: ', reason);
    });
};

defaults();

import { register as registerSync } from './commands/sync'; registerSync(cli);
import { register as registerSheets } from './commands/sheet'; registerSheets(cli);
import { register as registerZip } from './commands/zip'; registerZip(cli);

const argv = cli.argv;
if (argv.h || argv.help) {
    cli.showHelp();
    process.exit();
} else if (argv.v || argv.version) {
    // tslint:disable-next-line:no-var-requires
    const pkginfo = require('../package.json');
    debug.info(pkginfo.version);
    process.exit();
}

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.register = exports.convert = void 0;
const debug = require("../debug");
const utils = require("../lib/common/strings");
const sheets_1 = require("../lib/net/sheets");
const mime_1 = require("../lib/net/mime");
const path = require("path");
const write_1 = require("@plastichub/fs/write");
const showdown_1 = require("showdown");
const md_tables = require('markdown-table');
const { Parser } = require('json2csv');
const defaultOptions = (yargs) => {
    return yargs.option('sheet', {
        default: '1oVEiGH4o3SV-mAA3Mb-WNVJMyYl4VMxLjWjrSw_ipJY',
        describe: 'Sheet id, eg : 1oVEiGH4o3SV-mAA3Mb-WNVJMyYl4VMxLjWjrSw_ipJY'
    }).option('out', {
        describe: 'The output file, json is default, \notherwise markdown is supported by using the .md file extension, same for *.csv &.html !'
    }).option('range', {
        describe: 'The range, pick it from Google Sheets ("Get Link to this Range").\n To specify a certain sheet, please use "Sheet1!A:G"'
    }).option('credentials', {
        default: './credentials.json',
        describe: 'The GData API credential file, defaults cwd/credentials.json'
    }).option('debug', {
        default: 'false',
        describe: 'Enable internal debug message'
    });
};
let options = (yargs) => defaultOptions(yargs);
const convert = (input) => {
    let converter = new showdown_1.Converter({ tables: true });
    converter.setOption('literalMidWordUnderscores', 'true');
    return converter.makeHtml(input);
};
exports.convert = convert;
// node ./node_modules/osr-sync/dist/main.js sheet --sheet='1oVEiGH4o3SV-mAA3Mb-WNVJMyYl4VMxLjWjrSw_ipJY' --range='A2:A19'
// node ./node_modules/osr-sync/dist/main.js sheet --sheet='1oVEiGH4o3SV-mAA3Mb-WNVJMyYl4VMxLjWjrSw_ipJY' --range="Sheet11!A:B" --out="out.md"
// node ./node_modules/osr-sync/dist/main.js sheet --sheet='1oVEiGH4o3SV-mAA3Mb-WNVJMyYl4VMxLjWjrSw_ipJY' --range="Sheet11!A:B" --out="out.xlsx"
const register = (cli) => {
    return cli.command('sheet', 'Downloads a Google sheet', options, async (argv) => {
        if (argv.help) {
            return;
        }
        if (!argv.sheet) {
            debug.error('No sheet specified');
            return;
        }
        if (!argv.range) {
            debug.error('No sheet range specified, must be something as A2:A19 or constant');
            return;
        }
        let out = path.resolve(argv.out);
        const ext = path.parse(out).ext;
        if (mime_1.mimes[ext]) {
            await sheets_1.download(argv.sheet, out);
            return;
        }
        let data = await sheets_1.read(argv.sheet, argv.range);
        data = JSON.parse(utils.replaceAll('|', "::", (JSON.stringify(data, null, 2))));
        if (argv.debug) {
            debug.inspect('Data', data);
        }
        if (!argv.out) {
            console.log(JSON.stringify(data, null, 2));
            return;
        }
        else {
            if (out.endsWith('.md')) {
                data = md_tables(data);
                argv.debug && debug.inspect('Markdown : ', data);
                write_1.sync(out, data);
                return;
            }
            if (out.endsWith('.html')) {
                data = exports.convert(md_tables(data));
                argv.debug && debug.inspect('HTML : ', data);
                write_1.sync(out, data);
                return;
            }
            if (out.endsWith('.csv')) {
                try {
                    const opts = {};
                    const parser = new Parser(opts);
                    const csv = parser.parse(data);
                    write_1.sync(out, csv);
                }
                catch (e) {
                    debug.error(e);
                }
                return;
            }
            write_1.sync(out, JSON.stringify(data, null, 2));
            debug.info(`Did write sheet to ${out}`);
            return;
        }
    });
};
exports.register = register;
//# sourceMappingURL=sheet.js.map
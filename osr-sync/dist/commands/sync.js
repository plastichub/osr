"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.register = void 0;
const debug = require("../debug");
const argv_1 = require("../argv");
const exists_1 = require("@plastichub/fs/exists");
const remove_1 = require("@plastichub/fs/remove");
const copy_1 = require("@plastichub/fs/copy");
const path = require("path");
const options = (yargs) => argv_1.defaultOptions(yargs);
const default_ignored_folders = [
    '.git',
    'node_modules',
    '_MACOSX',
    'production',
    'templates',
    '.*'
];
const default_ignored_files = [
    '!./**/*.zip',
    '!./**/*.tar'
];
const defaultProduct_public_folders = [
    'cad',
    'media/gallery',
    'resources',
    'drawings',
    'vendor',
    'resources',
    'laser',
    'bazar',
    'howto',
    'renderings'
];
const defaultProduct_public_files = [
    '*.json',
    '*.md',
    '*.yaml',
    '*.csv',
    '*.xls'
];
const profiles = {
    'product': {
        matching: [
            ...defaultProduct_public_files,
            ...defaultProduct_public_folders.map((e) => `${e}/**`),
            ...default_ignored_folders.map((e) => `!./${e}/**`),
            ...default_ignored_files
        ]
    },
    'promo': {
        matching: [
            ...defaultProduct_public_files.filter((e) => e != 'cad'),
            ...defaultProduct_public_folders.map((e) => `${e}/**`),
            ...default_ignored_folders.map((e) => `!./${e}/**`),
            ...default_ignored_files
        ]
    }
};
// node ./node_modules/osr-sync/dist/main.js sync --clear=true --source='./products/lydia-v4' --target='../plastic-hub-public/products/lydia-v4_test' --debug=true --profile=product
// node ./node_modules/osr-sync/dist/main.js sync --clear=true --source='./products/lydia-v4' --target='../plastic-hub-public/products/lydia-v4_test' --debug=true --profile='./osr-sync.json'
const register = (cli) => {
    return cli.command('sync', "Default Sync", options, (argv) => {
        if (argv.help) {
            return;
        }
        const args = argv_1.sanitize(argv);
        const verbose = args.debug;
        if (!exists_1.sync(args.source[0])) {
            verbose && debug.error(`Source directory doesnt exists ${args.source}`);
            return;
        }
        if (exists_1.sync(args.target) && args.clear) {
            verbose && debug.info(`Emptying target directory ${args.target}`);
            remove_1.sync(args.target);
        }
        let count = 0;
        const progress = (path, current, total) => {
            // verbose && console.log(`Copying ${path} - ${current} of ${total}`);
            count++;
            return true;
        };
        let copyOptions = {};
        if (typeof args.profile === 'string') {
            const profile = args.profile ? profiles[args.profile] : {};
            copyOptions = Object.assign({ debug: false }, profile);
        }
        else if (Object.keys(args.profile)) {
            copyOptions = args.profile;
        }
        copyOptions = Object.assign(Object.assign({}, copyOptions), { progress: progress });
        copyOptions.overwrite = true;
        if (args.renamer) {
            const script = path.resolve(args.renamer);
            if (!exists_1.sync(script)) {
                debug.warn(`Renamer script '${args.renamer}' doesn't exists at ${script}`);
            }
            else {
                try {
                    const rename = require(script);
                    copyOptions.renameCallback = rename.default;
                    if (copyOptions.renameCallback) {
                    }
                    else {
                        console.log('invalid  rename script ' + args.renamer);
                    }
                }
                catch (e) {
                    debug.error(`Error loading rename script ${script}`, e);
                }
            }
        }
        copy_1.async(args.source[0], args.target, copyOptions).then(() => {
            //verbose && debug.inspect('args', { args, copyOptions });
            debug.info(`Copied ${count} files from ${argv.source.join('\n')} to ${argv.target}`);
        }).catch((e) => {
            debug.error(e.message);
        });
    });
};
exports.register = register;
//# sourceMappingURL=sync.js.map
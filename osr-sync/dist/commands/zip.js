"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.register = void 0;
const debug = require("../debug");
const argv_1 = require("../argv");
const exists_1 = require("@plastichub/fs/exists");
const remove_1 = require("@plastichub/fs/remove");
const fs = require("fs");
const path = require("path");
const options = (yargs) => argv_1.defaultOptions(yargs);
const archiver = require("archiver");
const default_ignored_folders = [
    '.git',
    'node_modules',
    '_MACOSX',
    'production',
    'bazar',
    'templates',
    '.*'
];
const default_ignored_files = [
    '!./**/*.zip',
    '!./**/*.tar'
];
const defaultProduct_public_folders = [
    'cad',
    'media/gallery',
    'resources',
    'drawings',
    'vendor',
    'resources',
    'laser',
    'howto',
    'renderings'
];
const defaultProduct_public_files = [
    '*.json',
    '*.md',
    '*.yaml',
    '*.csv',
    '*.xls'
];
const profiles = {
    'product': {
        matching: [
            ...defaultProduct_public_files,
            ...defaultProduct_public_folders.map((e) => `${e}/**`)
        ]
    }
};
// node ./node_modules/osr-sync/dist/main.js zip --clear=true --source='./products/lydia-v4' --target='../out.zip' --debug=true --profile=product
// node ./node_modules/osr-sync/dist/main.js zip --clear=true --source='./products/lydia-v4' --target='../out.zip' --debug=true --profile='./osr-sync-zip.json'
// node ./node_modules/osr-sync/dist/main.js zip --clear=true --source='./products/lydia-v4' --source='./products/components/hal' --cwd="./" --target='../out.zip'  --debug=true --profile='./.osr-sync-zip.json'
const register = (cli) => {
    return cli.command('zip', "Default Zip", options, (argv) => {
        if (argv.help) {
            return;
        }
        const args = argv_1.sanitize(argv);
        const verbose = args.debug;
        if (exists_1.sync(args.target) && args.clear) {
            verbose && debug.info(`Deleting ${args.target}`);
            remove_1.sync(args.target);
        }
        let options = {};
        if (typeof args.profile === 'string') {
            const profile = args.profile ? profiles[args.profile] : {};
            options = Object.assign({ debug: true }, profile);
        }
        else if (Object.keys(args.profile)) {
            options = args.profile;
        }
        debug.inspect('options', args.profile);
        const file = fs.createWriteStream(args.target);
        const archive = archiver('zip', {
            zlib: {
                level: 9
            }
        });
        archive.on('warning', (err) => {
            if (err.code === 'ENOENT') {
                debug.warn('archive warning', err);
            }
            else {
                throw err;
            }
        });
        archive.on('error', (err) => {
            debug.error('archive error', err);
            return;
        });
        archive.pipe(file);
        if (options.matching) {
            if (argv.cwd) {
                args.source.forEach((s) => {
                    verbose && debug.info(`Archiving ${s}`);
                    options.matching.forEach((e) => {
                        archive.glob(e, { cwd: s, ignore: [] }, { prefix: path.relative(args.cwd, s) });
                    });
                });
            }
            else {
                args.source.forEach((s) => {
                    verbose && debug.info(`Archiving ${s}`);
                    options.matching.forEach((e) => {
                        archive.glob(e, { cwd: s, ignore: [] });
                    });
                });
            }
        }
        const s = debug.spinner("Write archive ..");
        s.start();
        archive.finalize().then(() => {
            s.stop();
            verbose && debug.info(`Did create archive at ${args.target}`);
        });
    });
};
exports.register = register;
//# sourceMappingURL=zip.js.map
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.register = void 0;
const debug = require("../debug");
const chalk_1 = require("chalk");
const argv_1 = require("../argv");
const options = (yargs) => argv_1.defaultOptions(yargs);
const description = () => {
    return 'Run a command for all modules' +
        '\n\t Parameters : ' +
        chalk_1.default.green('\n\t\t\t --command=[command to run]');
};
exports.register = (cli) => {
    return cli.command('each', description(), options, (argv) => {
        if (argv.help) {
            return;
        }
        const args = argv_1.sanitize(argv);
        debug.log(argv.clear);
    });
};
//# sourceMappingURL=each.js.map
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.download = exports.read = void 0;
const readline = require("readline");
const googleapis_1 = require("googleapis");
const path = require("path");
const read_1 = require("@plastichub/fs/read");
const write_1 = require("@plastichub/fs/write");
const exists_1 = require("@plastichub/fs/exists");
const fs = require("fs");
const mime_1 = require("./mime");
const debug = require("../../debug");
// https://developers.google.com/sheets/api/quickstart/nodejs
// If modifying these scopes, delete token.json.
const SCOPES = ['https://www.googleapis.com/auth/spreadsheets.readonly',
    'https://www.googleapis.com/auth/drive.readonly'];
// The file token.json stores the user's access and refresh tokens, and is
// created automatically when the authorization flow completes for the first
// time.
const TOKEN_PATH = 'token.json';
/**
 * Get and store new token after prompting for user authorization, and then
 * execute the given callback with the authorized OAuth2 client.
 * @param {google.auth.OAuth2} oAuth2Client The OAuth2 client to get token for.
 * @param {getEventsCallback} callback The callback for the authorized client.
 */
const getNewToken = async (oAuth2Client) => {
    const authUrl = oAuth2Client.generateAuthUrl({
        access_type: 'offline',
        scope: SCOPES,
    });
    debug.warn('Authorize this app by visiting this url:', authUrl);
    const rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout,
    });
    rl.question('Enter the code from that page here: ', (code) => {
        rl.close();
        oAuth2Client.getToken(code, (err, token) => {
            if (err)
                return console.error('Error while trying to retrieve access token', err);
            oAuth2Client.setCredentials(token);
            // Store the token to disk for later program executions
            write_1.sync(TOKEN_PATH, JSON.stringify(token, null, 2));
            return oAuth2Client;
        });
    });
};
const readSheet = async (auth, sheet, range) => {
    const sheets = googleapis_1.google.sheets({ version: 'v4', auth });
    const res = await sheets.spreadsheets.values.get({
        spreadsheetId: sheet,
        range: range,
    });
    return res.data.values;
};
/**
 * Create an OAuth2 client with the given credentials, and then execute the
 * given callback function.
 * @param {Object} credentials The authorization client credentials.
 * @param {function} callback The callback to call with the authorized client.
 */
const authorize = async (credentials) => {
    const { client_secret, client_id, redirect_uris } = credentials.installed;
    const oAuth2Client = new googleapis_1.google.auth.OAuth2(client_id, client_secret, redirect_uris[0]);
    const token = read_1.sync(TOKEN_PATH, 'string');
    if (!token) {
        return getNewToken(oAuth2Client);
    }
    oAuth2Client.setCredentials(JSON.parse(token));
    return oAuth2Client;
};
const read = async (sheet, range, credentialsFile = './credentials.json') => {
    if (!exists_1.sync(path.resolve(credentialsFile))) {
        debug.error('Cant find credentials.json');
        return Promise.reject();
    }
    const creds = read_1.sync(path.resolve(credentialsFile), 'json');
    const client = await authorize(creds);
    return await readSheet(client, sheet, range);
};
exports.read = read;
const download = async (sheet, dst, credentialsFile = './credentials.json') => {
    if (!exists_1.sync(path.resolve(credentialsFile))) {
        debug.error('Cant find credentials.json');
        return;
    }
    const creds = read_1.sync(path.resolve(credentialsFile), 'json');
    const auth = await authorize(creds);
    googleapis_1.google.options({ auth: auth });
    const dest = fs.createWriteStream(dst);
    const drive = googleapis_1.google.drive('v3');
    const res = await drive.files.export({
        fileId: sheet,
        mimeType: mime_1.mimes[path.parse(dst).ext]
    }, {
        responseType: 'stream'
    });
    await new Promise((resolve, reject) => {
        res.data
            .on('error', reject)
            .pipe(dest)
            .on('error', reject)
            .on('finish', resolve);
    });
};
exports.download = download;
//# sourceMappingURL=sheets.js.map
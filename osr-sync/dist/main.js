#!/usr/bin/env node
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const cli = require("yargs");
const debug = require("./debug");
// tslint:disable-next-line:no-var-requires
const yargonaut = require('yargonaut')
    .style('blue')
    .helpStyle('green');
cli.options('v', {
    alias: 'version',
    description: 'Display version number'
});
const defaultArgs = (yargs) => {
    return yargs;
};
// tweaks and handlers
const defaults = () => {
    // default command
    const DefaultCommand = 'summary';
    if (process.argv.length === 2) {
        process.argv.push(DefaultCommand);
    }
    // currently no default handler, display only :
    process.on('unhandledRejection', (reason) => {
        console.error('Unhandled rejection, reason: ', reason);
    });
};
defaults();
const sync_1 = require("./commands/sync");
sync_1.register(cli);
const sheet_1 = require("./commands/sheet");
sheet_1.register(cli);
const zip_1 = require("./commands/zip");
zip_1.register(cli);
const argv = cli.argv;
if (argv.h || argv.help) {
    cli.showHelp();
    process.exit();
}
else if (argv.v || argv.version) {
    // tslint:disable-next-line:no-var-requires
    const pkginfo = require('../package.json');
    debug.info(pkginfo.version);
    process.exit();
}
//# sourceMappingURL=main.js.map
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.spinner = exports.inspect = exports.debug = exports.warn = exports.error = exports.info = exports.log = void 0;
const chalk_1 = require("chalk");
const ora = require("ora");
// tslint:disable-next-line:no-var-requires
const jsome = require('jsome');
jsome.level.show = true;
const glog = console.log;
const log = (msg, d) => glog(chalk_1.default.magenta(msg), d || '');
exports.log = log;
const info = (msg, d) => glog(chalk_1.default.green(msg), d || '');
exports.info = info;
const error = (msg, d) => glog(chalk_1.default.red(msg), d || '');
exports.error = error;
const warn = (msg, d) => glog(chalk_1.default.yellow(msg), d || '');
exports.warn = warn;
const debug = (msg, d) => glog(chalk_1.default.blue(msg), d || '');
exports.debug = debug;
const inspect = (msg, d = null, pretty = true) => {
    glog(chalk_1.default.blue(msg));
    d && jsome(d);
};
exports.inspect = inspect;
const spinner = (msg) => ora(msg);
exports.spinner = spinner;
//# sourceMappingURL=debug.js.map
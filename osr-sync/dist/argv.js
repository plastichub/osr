"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.defaultOptions = exports.sanitize = void 0;
const path = require("path");
const exists_1 = require("@plastichub/fs/exists");
const read_1 = require("@plastichub/fs/read");
const debug = require("./debug");
const primitives_1 = require("./lib/common/primitives");
const sanitize = (argv) => {
    argv = argv;
    try {
        if (!argv.source) {
            debug.error("You must specify a source location!");
            return argv;
        }
        if (typeof argv.source === 'string') {
            argv.source = [path.resolve(argv.source)];
        }
        else if (argv.source && primitives_1.isArray(argv.source)) {
            argv.source = argv.source.map((s) => path.resolve(s));
        }
    }
    catch (e) {
        debug.error("e", e);
    }
    argv.target = path.resolve(argv.target);
    if (argv.cwd) {
        argv.cwd = path.resolve(argv.cwd);
    }
    // try profile file
    const profilePath = path.resolve(argv.profile);
    if (exists_1.sync(profilePath)) {
        try {
            argv.profile = read_1.sync(profilePath, 'json');
        }
        catch (e) {
            debug.error(`Error reading profile ${profilePath}`);
        }
    }
    else {
        argv.profile = argv.profile || '';
    }
    argv.filter = argv.filter || '';
    argv.debug = argv.debug === 'true' ? true : false;
    argv.clear = argv.clear === 'true' ? true : false;
    return argv;
};
exports.sanitize = sanitize;
const defaultOptions = (yargs) => {
    return yargs.option('target', {
        default: process.cwd(),
        describe: 'target'
    }).option('source', {
        default: process.cwd(),
        describe: 'the source'
    }).option('profile', {
        describe: 'only use modules which specified that profile'
    }).option('clear', {
        describe: 'erase the target directory before',
        default: 'false'
    }).option('profile', {
        describe: 'select sync profile: product or from file',
        default: "./.osr-sync.json"
    }).option('renamer', {
        describe: 'path to a relative script file which returns a new file or directory name for a given input path (one argument). For instance : \n' +
            "\t exports.default = function (from, to) {\
            \t }",
        default: "./.osr-renamer.js"
    });
};
exports.defaultOptions = defaultOptions;
//# sourceMappingURL=argv.js.map